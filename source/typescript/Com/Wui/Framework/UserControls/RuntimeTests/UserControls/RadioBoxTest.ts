/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import RadioBox = Com.Wui.Framework.UserControls.BaseInterface.UserControls.RadioBox;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;

    export class RadioBoxTest extends ViewerTestRunner<RadioBox> {
        private object : RadioBox;

        public testTextAPI() : void {
            this.object.Text("test radiobox 6");
            this.assertEquals(this.object.Text(), "test radiobox 6");
        }

        public testTitle() : void {
            this.object.Title().Text("title test");
            this.assertEquals(this.object.Title().Text(), "title test");
        }

        public testCheckedAPI() : void {
            this.object.Checked(true);
            this.assertEquals(this.object.Checked(), true);
            this.object.Checked(false);
            this.assertEquals(this.object.Checked(), false);
        }

        public testgetGuiOptions() : void {
            this.object.getGuiOptions().Add(GuiOptionType.DISABLE);
            this.assertEquals(this.object.getGuiOptions().Contains(GuiOptionType.DISABLE), true);
        }

        public testErrorAPI() : void {
            this.assertEquals(this.object.Error(), false);
            this.object.Error(true);
            this.assertEquals(this.object.Error(), true);
        }

        public testEnabeldAPI() : void {
            this.assertEquals(this.object.Enabled(), true);
            this.object.Enabled(false);
            this.assertEquals(this.object.Enabled(), false);
        }

        public __IgnoretestEnabledGroup() : void {
            RadioBox.EnabledGroup("group", true);
            this.assertEquals(this.object.GroupName(), "groupName");
            RadioBox.EnabledGroup(null, false);
            this.assertEquals(this.object.GroupName(), "groupName");
        }

        public __IgnoretestErrorGroup() : void {
            RadioBox.ErrorGroup("group", true);
            this.assertEquals(this.object.GroupName(), "groupName");
        }

        public __IgnoretestFocusStateAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const focusHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals($eventArgs.Owner(), this.object);
                        this.testTextAPI();
                        this.assertEquals(ElementManager.getClassName(this.object.Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                        this.assertEquals(ElementManager.getClassName(this.object.Id() + "_Check"), "Unchecked");
                        this.object.getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                        $done();
                    }, 500);
                };
                this.object.getEvents().setOnFocus(focusHandler);
                this.emulateEvent(this.object, EventType.ON_FOCUS);
            };
        }

        public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.object.Enabled(false);
                const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals(ElementManager.getClassName(this.object.Id() + "_Check"), "Unchecked");
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                        this.object.getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                        $done();
                    }, 500);
                };
                this.object.getEvents().setOnFocus(focusDisableApiHandler);
                this.emulateEvent(this.object, EventType.ON_FOCUS);
            };
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), this.object);
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Check"), "Unchecked");
                    this.object.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                this.object.getEvents().setOnClick(clickHandler);
                this.emulateEvent(this.object, EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.IsVisible(this.object.Title()), true);
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Check"), "Unchecked");
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Check"), "Unchecked");
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Check"), "Unchecked");
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Check"), "Unchecked");
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_OUT);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseOver(mouseoverHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_OVER, event);
            };
        }

        protected setUp() : void {
            this.object.Visible(true);
            this.object.Enabled(true);
            this.object.Error(false);
            this.object.Text("test radiobox 1");
            this.object.Title().Text("title test");
            this.object.getGuiOptions().Add(GuiOptionType.DISABLE);
            // this.object.Text("test radiobox 6");
        }

        protected before() : string {
            // this.removeInstance();

            this.object = new RadioBox("testGroup", "item1");
            this.setUp();
            const object2 : RadioBox = new RadioBox("testGroup", "item2");
            const object3 : RadioBox = new RadioBox("testGroup", "item3");
            // this.object.Checked(true);

            object2.Checked(true);
            object2.Text("test radiobox 2");
            object2.getGuiOptions().Add(GuiOptionType.DISABLE);

            // object3.Checked(true);
            object3.Text("test radiobox 3");
            object3.getGuiOptions().Add(GuiOptionType.DISABLE);

            this.addButton("Disable", () : void => {
                this.object.Enabled(false);
            });
            this.addButton("Enable", () : void => {
                this.object.Enabled(true);
            });
            this.addButton("Error", () : void => {
                this.object.Error(true);
            });
            this.addButton("Normal", () : void => {
                this.object.Error(false);
            });
            this.addButton("Checked", () : void => {
                this.object.Checked(true);
            });
            this.addButton("Unchecked", () : void => {
                this.object.Checked(false);
            });
            this.addButton("Checked/Unchecked", () : void => {
                RadioBox.ToggleChecked(this.object);
            });
            this.addButton("Set text", () : void => {
                this.object.Text("new checkbox text");
            });
            this.addButton("Disable group", () : void => {
                RadioBox.EnabledGroup("testGroup", false);
            });
            this.addButton("Enable group", () : void => {
                RadioBox.EnabledGroup("testGroup", true);
            });
            this.addButton("Error group", () : void => {
                RadioBox.ErrorGroup("testGroup", true);
            });
            this.addButton("Normal group", () : void => {
                RadioBox.ErrorGroup("testGroup", false);
            });

            return "<div style=\"position: relative; top: 50px; left: 200px; width: 500px;\">" +
                this.object.Draw() + object2.Draw() + object3.Draw() +
                "</div>";
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
