/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import Icon = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Icon;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import IconType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.IconType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    export class IconTest extends ViewerTestRunner<Icon> {

        public testStyleClassNameAPI() : void {
            this.assertEquals(this.getInstance().StyleClassName(), "testCssClass");
            this.getInstance().StyleClassName("NewStyle");
            this.assertEquals(this.getInstance().StyleClassName(), "NewStyle");
        }

        public testIconTypeAPI() : void {
            this.assertEquals(this.getInstance().IconType(), IconType.BLACK_SQUARE);
            this.getInstance().IconType(IconType.RED_SQUARE);
            this.assertEquals(this.getInstance().IconType(), IconType.RED_SQUARE);
        }

        public testEnabledAPI() : void {
            this.assertEquals(this.getInstance().Enabled(), true);
            this.getInstance().Enabled(false);
            this.assertEquals(this.getInstance().Enabled(), false);
            this.assertEquals(this.getInstance().getGuiOptions().Contains(GuiOptionType.DISABLE), true);
        }

        public testHideAPI() : void {
            this.assertEquals(this.getInstance().Visible(), true);
            this.getInstance().Visible(false);
            this.assertEquals(this.getInstance().Visible(), false);
        }

        public __IgnoretestFocusStateAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const focusHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals($eventArgs.Owner(), this.getInstance());
                        this.testStyleClassNameAPI();
                        this.testIconTypeAPI();
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                        $done();
                    }, 500);
                };
                this.getInstance().getEvents().setOnFocus(focusHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.getInstance().Enabled(false);
                const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.testStyleClassNameAPI();
                        this.testIconTypeAPI();
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "Disable");
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                        $done();
                    }, 500);
                };
                this.getInstance().getEvents().setOnFocus(focusDisableApiHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "");
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnClick(clickHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "");
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const icon : Icon = this.getInstance();
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    icon.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(icon, EventType.ON_MOUSE_OUT);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "");
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
            };
        }

        protected setUp() : void {
            const object : Icon = this.getInstance();
            object.Visible(true);
            object.Enabled(true);
            object.IconType(IconType.BLACK_SQUARE);
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
        }

        protected before() : string {
            let clickCounter : number = 0;
            const object : Icon = this.getInstance();
            this.setUp();
            object.getEvents().setOnClick(() : void => {
                clickCounter++;
                Echo.Printf("icon has been clicked " + clickCounter + " times");
            });

            this.addButton("Black", () : void => {
                object.IconType(IconType.BLACK_SQUARE);
            });
            this.addButton("Red", () : void => {
                object.IconType(IconType.RED_SQUARE);
            });
            this.addButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addButton("Normal", () : void => {
                object.Enabled(true);
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
