/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import CheckBox = Com.Wui.Framework.UserControls.BaseInterface.UserControls.CheckBox;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;

    export class CheckBoxTest extends ViewerTestRunner<CheckBox> {

        public testTextAPI() : void {
            this.assertEquals(this.getInstance().Text(), "test checkbox");
            this.getInstance().Text("");
            this.assertEquals(this.getInstance().Text(), "");
        }

        public testTitleAPI() : void {
            this.assertEquals(this.getInstance().Title().Text(), "title test");
        }

        public testStyleClassNameAPI() : void {
            this.assertEquals(this.getInstance().StyleClassName(), "testCssClass");
            this.getInstance().StyleClassName("Style");
            this.assertEquals(this.getInstance().StyleClassName(), "Style");
        }

        public testErrorAPI() : void {
            this.assertEquals(this.getInstance().Error(), false);
            this.getInstance().Error(true);
            this.assertEquals(this.getInstance().Error(), true);
        }

        public testEnabledAPI() : void {
            this.assertEquals(this.getInstance().Enabled(), true);
            this.getInstance().Enabled(false);
            this.assertEquals(this.getInstance().Enabled(), false);
        }

        public testGuiOptions() : void {
            this.assertEquals(this.getInstance().getGuiOptions().Contains(GuiOptionType.DISABLE), true);
            this.assertEquals(this.getInstance().getGuiOptions().Contains(GuiOptionType.SELECTED), false);
        }

        public testCheckedAPI() : void {
            this.getInstance().Checked(true);
            this.assertEquals(this.getInstance().Checked(), true);
            this.getInstance().Checked(false);
            this.assertEquals(this.getInstance().Checked(), false);
        }

        public testHideAPI() : void {
            this.assertEquals(this.getInstance().Visible(), true);
            this.getInstance().Visible(false);
            this.assertEquals(this.getInstance().Visible(), false);
        }

        public __IgnoretestFocusStateAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const focusHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals($eventArgs.Owner(), this.getInstance());
                        this.testCheckedAPI();
                        this.testTextAPI();
                        this.testErrorAPI();
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                        $done();
                    }, 500);
                };
                this.getInstance().getEvents().setOnFocus(focusHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.getInstance().Enabled(false);
                const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                        $done();
                    }, 500);
                };
                this.getInstance().getEvents().setOnFocus(focusDisableApiHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const checkbox : CheckBox = this.getInstance();
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), checkbox);
                    checkbox.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                checkbox.getEvents().setOnClick(clickHandler);
                this.emulateEvent(checkbox, EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const checkbox : CheckBox = this.getInstance();
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.IsVisible(checkbox.Title()), true);
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    checkbox.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                checkbox.getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(checkbox, EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const checkbox : CheckBox = this.getInstance();
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                    checkbox.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                checkbox.getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(checkbox, EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const checkbox : CheckBox = this.getInstance();
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ON);
                    checkbox.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                checkbox.getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(checkbox, EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const checkbox : CheckBox = this.getInstance();
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                    checkbox.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                checkbox.getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(checkbox, EventType.ON_MOUSE_OUT);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const checkbox : CheckBox = this.getInstance();
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ON);
                    checkbox.getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                    $done();
                };
                checkbox.getEvents().setOnMouseOver(mouseoverHandler);
                this.emulateEvent(checkbox, EventType.ON_MOUSE_OVER, event);
            };
        }

        protected setUp() : void {
            const object : CheckBox = this.getInstance();
            object.Visible(true);
            object.Enabled(true);
            object.Error(false);
            object.Checked(true);
            object.Text("test checkbox");
            object.Title().Text("title test");
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
        }

        protected before() : string {
            const object : CheckBox = this.getInstance();
            object.IsPersistent(false);
            // object.Text("");
            this.setUp();

            this.addButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addButton("Error", () : void => {
                object.Error(true);
            });
            this.addButton("Normal", () : void => {
                object.Error(false);
            });
            this.addButton("Checked", () : void => {
                object.Checked(true);
            });
            this.addButton("Unchecked", () : void => {
                object.Checked(false);
            });
            this.addButton("Checked/Unchecked", () : void => {
                CheckBox.ToggleChecked(object);
            });
            this.addButton("Set text", () : void => {
                object.Text("new checkbox text");
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
