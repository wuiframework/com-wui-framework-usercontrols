/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import Dialog = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Dialog;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import DialogType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.DialogType;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import DialogEventType = Com.Wui.Framework.Gui.Enums.Events.DialogEventType;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;

    export class DialogTest extends ViewerTestRunner<Dialog> {

        constructor() {
            super();
            this.setMethodFilter("ignoreAll");
        }

        public testTitleAPI() : void {
            this.assertEquals(this.getInstance().Title().Text(), "Text filed tooltip text");
        }

        public testStyleClassNameAPI() : void {
            this.assertEquals(this.getInstance().StyleClassName(), "testCssClass");
            this.getInstance().StyleClassName("NewStyle");
            this.assertEquals(this.getInstance().StyleClassName(), "NewStyle");
        }

        public testGuiType() : void {
            this.assertEquals(this.getInstance().GuiType(), DialogType.GENERAL);
            this.getInstance().GuiType(DialogType.GREEN);
            this.assertEquals(this.getInstance().GuiType(), DialogType.GREEN);
        }

        public testChecked() : void {
            this.assertEquals(this.getInstance().Draggable(), false);
        }

        public testModal() : void {
            const dialog : Dialog = this.getInstance();
            this.assertEquals(dialog.Modal(), true);
            this.assertEquals(dialog.headerText.Visible(), false);
        }

        public testWidthAPI() : void {
            this.assertEquals(this.getInstance().Width(), 500);
        }

        public testHeightAPI() : void {
            this.assertEquals(this.getInstance().Height(), 300);
        }

        public testEnabledAPI() : void {
            this.assertEquals(this.getInstance().Enabled(), true);
            this.getInstance().Enabled(false);
            this.assertEquals(this.getInstance().Enabled(), false);
            this.getInstance().Enabled(true);
        }

        public testVisibleAPI() : void {
            this.assertEquals(this.getInstance().Visible(), true);
            this.getInstance().Visible(false);
            this.assertEquals(this.getInstance().Visible(), false);
        }

        public __IgnoretestOpen() : void {
            const eventArgs : EventArgs = new EventArgs();
            Dialog.Open(this.getInstance());
            eventArgs.Owner(this.getInstance());
            this.getEventsManager().FireEvent(this.getInstance(), DialogEventType.ON_OPEN, eventArgs);
            this.assertEquals(this.getInstance().IsPrepared(), true);
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnClick(clickHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
            };
        }

        public __IgnoretestFocusStateAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const focusHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals($eventArgs.Owner(), this.getInstance());
                        // this.testWidthAPI();
                        // this.testHeightAPI();
                        // this.testStyleClassNameAPI();
                        // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ON);
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                        $done();
                    }, 500);
                };
                this.getInstance().getEvents().setOnFocus(focusHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        // this.assertEquals(this.getInstance().StyleClassName(), "testCssClass");
                        // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ON);
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                        $done();
                    }, 500);
                };
                this.getInstance().getEvents().setOnFocus(focusDisableApiHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
            };
        }

        protected setUp() : void {
            const object : Dialog = this.getInstance();
            // object.Visible(true);
            object.Enabled(true);
            object.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
            object.StyleClassName("testCssClass");
            object.Draggable(true);
            object.Width(500);
            object.Height(300);
            object.GuiType(DialogType.GENERAL);
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
            object.Title().Text("Text filed tooltip text");
        }

        protected before() : string {
            const object : Dialog = this.getInstance();

            this.setUp();
            // object.Modal(false);
            // object.Visible(true);
            // object.AutoResize(true);
            // object.AutoCenter(true);
            // object.TopOffset(100);
            // object.MaxWidth(500);
            // object.MaxHeight(500);

            const object2 : Dialog = new Dialog(DialogType.RED);
            object2.Visible(true);
            object2.Modal(false);
            object2.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
            object2.Draggable(true);
            object2.headerIcon.Visible(true);
            object2.headerText.Visible(true);
            object2.closeButton.Visible(false);
            object2.headerText.Text("dialog 2 header text");

            this.addButton("Open dialog 1", () : void => {
                Dialog.Open(object);
            });
            this.addButton("Open dialog 2", () : void => {
                Dialog.Open(object2);
            });
            this.addButton("set header", () : void => {
                object2.headerText.Text("test test test test test test test");
            });

            return "<style>.testCssClass {position: absolute; top: 50px; left: 200px; float: left;}</style>" +
                StringUtils.NewLine() +
                StringUtils.NewLine() +
                object2.Draw();
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
