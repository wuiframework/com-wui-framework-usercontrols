/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import LabelList = Com.Wui.Framework.UserControls.BaseInterface.UserControls.LabelList;
    import Label = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Label;
    import IconType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.IconType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;

    export class LabelListTest extends ViewerTestRunner<LabelList> {

        public testIconNameAPI() : void {
            this.assertEquals(this.getInstance().IconName(), IconType.RED_SQUARE);
            this.getInstance().IconName(IconType.BLUE_SQUARE);
            this.assertEquals(this.getInstance().IconName(), IconType.BLUE_SQUARE);
            this.getInstance().IconName(IconType.RED_SQUARE);
        }

        public testgetItemAPI() : void {
            this.assertEquals(this.getInstance().getItem(1).Text(), "test label 2");
        }

        public testStyleClassnameAPI() : void {
            this.assertEquals(this.getInstance().StyleClassName(), "testCssClass");
            this.getInstance().StyleClassName("Style");
            this.assertEquals(this.getInstance().StyleClassName(), "Style");
        }

        public testEnabledAPI() : void {
            this.assertEquals(this.getInstance().Enabled(), true);
            this.getInstance().Enabled(false);
            this.assertEquals(this.getInstance().Enabled(), false);
        }

        public testVisibleAPI() : void {
            this.assertEquals(this.getInstance().Visible(), true);
            this.getInstance().Visible(false);
            this.assertEquals(this.getInstance().Visible(), false);
        }

        public testAddAPI() : void {
            this.assertEquals(this.getInstance().getChildElements().Length(), 4);
            this.getInstance().Add("test label 5");
            this.assertEquals(this.getInstance().getChildElements().Length(), 5);
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const list : LabelList = this.getInstance();
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), list);
                    $done();
                };
                list.getEvents().setOnClick(clickHandler);
                this.emulateEvent(list, EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const list : LabelList = this.getInstance();
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    list.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                list.getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(list, EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const list : LabelList = this.getInstance();
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    list.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                list.getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(list, EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const list : LabelList = this.getInstance();
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    list.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                list.getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(list, EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const list : LabelList = this.getInstance();
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    list.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                list.getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(list, EventType.ON_MOUSE_OUT);
            };
        }

        protected setUp() : void {
            const object : LabelList = this.getInstance();
            object.Visible(true);
            object.Enabled(true);
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
        }

        protected before() : string {
            const object : LabelList = this.getInstance();
            this.setUp();
            let item : Label;
            let item2 : Label;
            if (object.InstanceOwner().IsCached()) {
                item2 = object.getItem(3);
            } else {
                object.Clear();
                object.IconName(IconType.RED_SQUARE);
                // object.Title().Text("label list title");
                object.Add("test label 1");
                object.Add("test label 2");

                item = new Label("test label 3");
                item.Enabled(false);
                object.Add(item);

                item2 = new Label("test label 4");
                item2.Title().Text("label 4 title");
                object.Add(item2);
            }

            // object.Enabled(false);

            this.addButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addButton("Show", () : void => {
                object.Visible(true);
            });
            this.addButton("Hide", () : void => {
                object.Visible(false);
            });
            this.addButton("Set text", () : void => {
                item2.Text("new label text");
            });
            this.addButton("Set icon", () : void => {
                object.IconName(IconType.BLACK_SQUARE);
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
