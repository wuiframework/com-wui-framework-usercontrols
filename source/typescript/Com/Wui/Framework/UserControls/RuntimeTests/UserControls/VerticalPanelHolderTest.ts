/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import VerticalPanelHolder = Com.Wui.Framework.UserControls.BaseInterface.UserControls.VerticalPanelHolder;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;

    export class VerticalPanelHolderTest extends ViewerTestRunner<VerticalPanelHolder> {

        public testStyleClassnameAPI() : void {
            this.assertEquals(this.getInstance().StyleClassName(), "TestCss");
            this.getInstance().StyleClassName("NewStyle");
            this.assertEquals(this.getInstance().StyleClassName(), "NewStyle");
        }

        public testTitleAPI() : void {
            this.assertEquals(this.getInstance().Title().Text(), "New Title");
        }

        public testWidthAPI() : void {
            this.assertEquals(this.getInstance().Width(), 600);
            this.getInstance().Width(800);
            this.assertEquals(this.getInstance().Width(), 800);
        }

        public testHeightAPI() : void {
            this.assertEquals(this.getInstance().Height(), 38);
        }

        public testEnabledAPI() : void {
            this.assertEquals(this.getInstance().Enabled(), true);
            this.getInstance().Enabled(false);
            this.assertEquals(this.getInstance().Enabled(), false);
        }

        public testVisibleAPI() : void {
            this.assertEquals(this.getInstance().Visible(), true);
            this.getInstance().Visible(false);
            this.assertEquals(this.getInstance().Visible(), false);
        }

        public __IgnoretestFocusStateAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const focusHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals($eventArgs.Owner(), this.getInstance());
                        this.testWidthAPI();
                        this.testStyleClassnameAPI();
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), null);
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                        $done();
                    }, 500);
                };
                this.getInstance().getEvents().setOnFocus(focusHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.getInstance().Enabled(false);
                const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals(this.getInstance().StyleClassName(), "TestCss");
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), null);
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                        $done();
                    }, 500);
                };
                this.getInstance().getEvents().setOnFocus(focusDisableApiHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), null);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnClick(clickHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), null);
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), null);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), null);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
            };
        }

        protected setUp() : void {
            const instance : VerticalPanelHolder = this.getInstance();
            instance.Title().Text("New Title");
            instance.Enabled(true);
            instance.Visible(true);
            instance.Width(600);
            instance.StyleClassName("TestCss");
            instance.getGuiOptions().Add(GuiOptionType.DISABLE);
        }

        protected before() : string {
            const instance : VerticalPanelHolder = this.getInstance();
            this.setUp();
            (<any>this).owner.normalImplementation();

            this.addButton("width : 400", () : void => {
                instance.Width(400);
            });

            this.addButton("width : 600", () : void => {
                instance.Width(600);
            });

            this.addButton("width : 800", () : void => {
                instance.Width(800);
            });

            return "<style>.TestCss {position: relative; top: 20px;}</style>" +
                "<div style=\"clear: both; height: 100px;\"></div>";
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
