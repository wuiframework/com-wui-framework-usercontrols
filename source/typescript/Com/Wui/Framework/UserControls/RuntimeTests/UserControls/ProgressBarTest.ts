/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import ProgressBar = Com.Wui.Framework.UserControls.BaseInterface.UserControls.ProgressBar;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ProgressBarType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.ProgressBarType;
    import Label = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Label;
    import ProgressBarEventArgs = Com.Wui.Framework.Gui.Events.Args.ProgressBarEventArgs;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;

    export class ProgressBarTest extends ViewerTestRunner<ProgressBar> {

        public testRangeStartAPI() : void {
            this.assertEquals(this.getInstance().RangeStart(), 0);
        }

        public testRangeEndAPI() : void {
            this.assertEquals(this.getInstance().RangeEnd(), 100);
        }

        public testWidthAPI() : void {
            this.assertEquals(this.getInstance().Width(), 200);
        }

        public testGuiTypeAPI() : void {
            this.getInstance().GuiType(ProgressBarType.RED);
            this.assertEquals(this.getInstance().GuiType(), ProgressBarType.RED);
            this.getInstance().GuiType(ProgressBarType.GENERAL);
            this.assertEquals(this.getInstance().GuiType(), ProgressBarType.GENERAL);
        }

        public testValueAPI() : void {
            this.getInstance().RangeEnd(100);
            this.getInstance().RangeStart(0);
            this.getInstance().Value(50);
            this.assertEquals(this.getInstance().Value(), 50);
            this.getInstance().Value(30);
            this.assertEquals(this.getInstance().Value(), 30);
        }

        public testEnabledAPI() : void {
            this.assertEquals(this.getInstance().Enabled(), true);
            this.getInstance().Enabled(false);
            this.assertEquals(this.getInstance().Enabled(), false);
            this.testRangeEndAPI();
            this.testRangeStartAPI();
            this.getInstance().Value(100);
            this.assertEquals(this.getInstance().Value(), 30);
            this.testGuiTypeAPI();
            this.testWidthAPI();
        }

        public testVisibleAPI() : void {
            this.assertEquals(this.getInstance().Visible(), true);
            this.getInstance().Visible(false);
            this.assertEquals(this.getInstance().Visible(), false);
            this.assertEquals(this.getInstance().Value(), 30);
            this.testGuiTypeAPI();
            this.testWidthAPI();
        }

        public testStyleClassName() : void {
            const progress : ProgressBar = this.getInstance();
            this.assertEquals(progress.StyleClassName(), "testCssClass");
        }

        public testgetGuiOptions() : void {
            const picker : ProgressBar = this.getInstance();
            this.assertEquals(picker.getGuiOptions().Contains(GuiOptionType.DISABLE), true);
        }

        public testFocusStateAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const focusHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals($eventArgs.Owner(), this.getInstance());
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                        $done();
                    }, 500);
                };
                EventsManager.getInstanceSingleton().setEvent(this.getInstance(),  EventType.ON_FOCUS, focusHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public testFocusStateDisabledAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.getInstance().Enabled(false);
                const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals(this.getInstance().GuiType(), ProgressBarType.GENERAL);
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "Disable");
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                        $done();
                    }, 500);
                };
                EventsManager.getInstanceSingleton().setEvent(this.getInstance(), EventType.ON_FOCUS, focusDisableApiHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "");
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnClick(clickHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Pointer"), null);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "");
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
            };
        }

        protected setUp() : void {
            const object : ProgressBar = this.getInstance();
            object.Visible(true);
            object.Enabled(true);
            object.GuiType(ProgressBarType.GENERAL);
            object.RangeStart(0);
            object.RangeEnd(100);
            object.Width(200);
            object.Title().Text("title test");
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
        }

        protected before() : string {
            const object : ProgressBar = this.getInstance();
            this.setUp();
            // object.Value(50);
            this.addButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addButton("set 0", () : void => {
                object.Value(0);
            });
            this.addButton("set 50", () : void => {
                object.Value(50);
            });
            this.addButton("set 100", () : void => {
                object.Value(100);
            });
            this.addButton("General", () : void => {
                object.GuiType(ProgressBarType.GENERAL);
            });
            this.addButton("Blue", () : void => {
                object.GuiType(ProgressBarType.BLUE);
            });
            this.addButton("Green", () : void => {
                object.GuiType(ProgressBarType.GREEN);
            });
            this.addButton("Red", () : void => {
                object.GuiType(ProgressBarType.RED);
            });
            this.addButton("Set size", () : void => {
                object.Width(400);
            });

            const label : Label = new Label("Progress 0 %", "progressShow");
            object.getEvents().setOnChange(($eventArgs : ProgressBarEventArgs) : void => {
                label.Text("Progress " + $eventArgs.Percentage() + " %");
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>" +
                StringUtils.NewLine() +
                StringUtils.NewLine() +
                label.Draw() +
                StringUtils.NewLine();
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
