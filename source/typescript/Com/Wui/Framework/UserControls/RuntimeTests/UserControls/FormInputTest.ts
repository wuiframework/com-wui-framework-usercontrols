/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import FormInput = Com.Wui.Framework.UserControls.BaseInterface.UserControls.FormInput;
    import DropDownListFormArgs = Com.Wui.Framework.UserControls.Structures.DropDownListFormArgs;
    import CheckBoxFormArgs = Com.Wui.Framework.UserControls.Structures.CheckBoxFormArgs;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import TextFieldFormArgs = Com.Wui.Framework.UserControls.Structures.TextFieldFormArgs;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    export class FormInputTest extends ViewerTestRunner<FormInput> {
        private object : FormInput;

        public testErrorAPI() : void {
            this.assertEquals(this.object.Error(), false);
            this.object.Error(true);
            this.assertEquals(this.object.Error(), true);
        }

        public testDisabledAPI() : void {
            this.assertEquals(this.object.Enabled(), true);
            this.object.Enabled(false);
            this.assertEquals(this.object.Enabled(), false);
            this.assertEquals(this.object.getGuiOptions().Contains(GuiOptionType.DISABLE), true);
        }

        public testValueAPI() : void {
            this.object.Value(10);
            this.assertEquals(this.object.Value(), 10);
        }

        public testWidthAPI() : void {
            this.object.Value(
                "Currently, I set value in FormInput props but I am still able to delete it and the value gets reset to original value.");
            this.object.Width(300);
            this.assertEquals(this.object.getWidth(), 100);
        }

        public testConfigurationAPI() : void {
            this.object.Configuration().Hint("Choose item");
            this.assertDeepEqual(this.object.Configuration().Hint(), "Choose item");
            this.object.Configuration().Name("Form label: ");
            this.assertEquals(this.object.Configuration().Name(), "Form label: ");
        }

        public testHideAPI() : void {
            this.object.Visible(false);
            this.assertEquals(this.object.Visible(), false);
            this.assertEquals(this.object.getWidth(), 100);
            this.assertEquals(this.object.Value(),
                "Currently, I set value in FormInput props but I am still able to delete it and the value gets reset to original value.");
        }

        public testFocusStateAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const focusHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals($eventArgs.Owner(), this.object);
                        this.testValueAPI();
                        this.testErrorAPI();
                        this.object.getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                        $done();
                    }, 500);
                };
                this.object.getEvents().setOnFocus(focusHandler);
                this.emulateEvent(this.object, EventType.ON_FOCUS);
            };
        }

        public testFocusStateDisabledAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.object.Enabled(false);
                const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals(this.object.StyleClassName(), "testCssClass");
                        this.object.getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                        $done();
                    }, 500);
                };
                this.object.getEvents().setOnFocus(focusDisableApiHandler);
                this.emulateEvent(this.object, EventType.ON_FOCUS);
            };
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), this.object);
                    this.object.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                this.object.getEvents().setOnClick(clickHandler);
                this.emulateEvent(this.object, EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), null);
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), null);
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), null);
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_OUT);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseOver(mouseoverHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_OVER, event);
            };
        }

        protected setUp() : void {
            this.object.Visible(true);
            this.object.Error(false);
            this.object.Enabled(true);
            this.object.StyleClassName("testCssClass");
            this.object.Width(-1);
        }

        protected before() : string {
            // this.removeInstance();

            this.object = new FormInput(new TextFieldFormArgs());
            this.setUp();
            const args : TextFieldFormArgs = <TextFieldFormArgs>this.object.Configuration();
            args.AddGuiOption(GuiOptionType.DISABLE);
            args.Name("Form label: ");
            args.Value("test value");
            args.IsRequired(true);
            args.Width(240);
            args.TitleText("Title text");
            // args.Error(true);
            // args.Visible(false);
            // args.Enabled(false);

            this.addButton("Disable", () : void => {
                this.object.Enabled(false);
            });
            this.addButton("Enable", () : void => {
                this.object.Enabled(true);
            });
            this.addButton("Error", () : void => {
                this.object.Error(true);
            });
            this.addButton("Normal", () : void => {
                this.object.Error(false);
            });

            const object2 : FormInput = new FormInput(new CheckBoxFormArgs());
            object2.StyleClassName("testCssClass");
            object2.InstancePath(this.getClassName() + "_object2");
            const args2 : CheckBoxFormArgs = <CheckBoxFormArgs>object2.Configuration();
            args2.Name("checkbox form input");
            args2.IsRequired(true);
            args2.Width(240);

            const object3 : FormInput = new FormInput(new DropDownListFormArgs());
            object3.StyleClassName("testCssClass");
            object3.InstancePath(this.getClassName() + "_object3");
            const args3 : DropDownListFormArgs = <DropDownListFormArgs>object3.Configuration();
            args3.Name("Form label 3: ");
            args3.IsRequired(true);
            args3.Width(240);
            args3.Hint("Choose item");
            args3.AddItem("item 1", 0);
            args3.AddItem("item 2", 1);
            // args3.Value(0);

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left; clear: both; " +
                "margin-bottom: 10px;}</style>" +
                this.object.Draw() +
                object2.Draw() +
                object3.Draw();
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
