/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import TextField = Com.Wui.Framework.UserControls.BaseInterface.UserControls.TextField;
    import IconType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.IconType;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import AutocompleteOption = Com.Wui.Framework.UserControls.Structures.AutocompleteOption;
    import TextFieldType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.TextFieldType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;

    export class TextFieldTest extends ViewerTestRunner<TextField> {

        public testTitle() : void {
            const field : TextField = this.getInstance();
            this.assertEquals(field.Title().Text(), "Text filed tooltip text");
        }

        public testIconNameAPI() : void {
            this.assertEquals(this.getInstance().IconName(), IconType.BLACK_SQUARE);
            this.getInstance().IconName(IconType.RED_SQUARE);
            this.assertEquals(this.getInstance().IconName(), IconType.RED_SQUARE);

        }

        public testgetGuiOptions() : void {
            this.assertEquals(this.getInstance().getGuiOptions().Contains(GuiOptionType.DISABLE), true);
        }

        public testWidthAPI() : void {
            this.assertEquals(this.getInstance().Width(), 300);
            this.getInstance().Width(500);
            this.assertEquals(this.getInstance().Width(), 500);
        }

        public testErrorAPI() : void {
            this.assertEquals(this.getInstance().Error(), false);
            this.getInstance().Error(true);
            this.assertEquals(this.getInstance().Error(), true);
        }

        public testStyleClassNameAPI() : void {
            this.assertEquals(this.getInstance().StyleClassName(), "testCssClass");
            this.getInstance().StyleClassName("NewStyle");
            this.assertEquals(this.getInstance().StyleClassName(), "NewStyle");
        }

        public testValueAPI() : void {
            this.assertEquals(this.getInstance().Value(), "Article full of text");
            this.getInstance().Value("New Value of text");
            this.assertEquals(this.getInstance().Value(), "New Value of text");
        }

        public testEnabledAPI() : void {
            this.assertEquals(this.getInstance().Enabled(), true);
            this.getInstance().Enabled(false);
            this.assertEquals(this.getInstance().Enabled(), false);
        }

        public testGuiTypeAPI() : void {
            this.getInstance().GuiType(TextFieldType.RED);
            this.assertEquals(this.getInstance().GuiType(), TextFieldType.RED);
            this.getInstance().GuiType(TextFieldType.GREEN);
            this.assertEquals(this.getInstance().GuiType(), TextFieldType.GREEN);
        }

        public testHintAPI() : void {
            this.assertEquals(this.getInstance().Hint(), "Write some text here ...");
        }

        public testLengthLimitAPI() : void {
            this.assertEquals(this.getInstance().LengthLimit(), -1);
            this.getInstance().LengthLimit(50);
            this.assertEquals(this.getInstance().LengthLimit(), 50);
        }

        public testReadOnlyAPI() : void {
            this.assertEquals(this.getInstance().ReadOnly(), false);
            this.getInstance().ReadOnly(true);
            this.assertEquals(this.getInstance().ReadOnly(), true);
            this.getInstance().ReadOnly(false);
        }

        public __IgnoretestFocusStateAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const focusHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals($eventArgs.Owner(), this.getInstance());
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                        $done();
                    }, 2000);
                };
                this.getInstance().getEvents().setOnFocus(focusHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnClick(clickHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
            };
        }

        public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.getInstance().Enabled(false);
                const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.testGuiTypeAPI();
                        this.testStyleClassNameAPI();
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                        $done();
                    }, 500);
                };
                this.getInstance().getEvents().setOnFocus(focusDisableApiHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        protected setUp() : void {
            const object : TextField = this.getInstance();
            object.Visible(true);
            object.Enabled(true);
            object.GuiType(TextFieldType.GENERAL);
            object.Title().Text("Text filed tooltip text");
            this.getInstance().IconName(IconType.BLACK_SQUARE);
            this.getInstance().Error(false);
            this.getInstance().Value("Article full of text");
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
            object.Hint("Write some text here ...");
        }

        protected before() : string {
            const object : TextField = this.getInstance();
            this.setUp();
            if (!object.InstanceOwner().IsCached()) {
                object.Width(300);
            }
            // object.Enabled(false);
            // object.setPasswordEnabled();
            // object.setOnlyNumbersAllowed();
            // object.setAutocompleteDisable();
            // object.LengthLimit(5);

            const autocompleteData : ArrayList<AutocompleteOption> = new ArrayList<AutocompleteOption>();
            for (let index : number = 0; index < 100; index++) {
                autocompleteData.Add(new AutocompleteOption("value " + index));
            }
            autocompleteData.Add(new AutocompleteOption("1234"));
            autocompleteData.Add(new AutocompleteOption("12345678"));
            autocompleteData.Add(new AutocompleteOption("5678"));
            object.setAutocompleteData(autocompleteData);

            this.addButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addButton("Error", () : void => {
                object.Error(true);
            });
            this.addButton("Normal", () : void => {
                object.Error(false);
            });
            this.addButton("General", () : void => {
                object.GuiType(TextFieldType.GENERAL);
            });
            this.addButton("Red", () : void => {
                object.GuiType(TextFieldType.RED);
            });
            this.addButton("Green", () : void => {
                object.GuiType(TextFieldType.GREEN);
            });
            this.addButton("Blue", () : void => {
                object.GuiType(TextFieldType.BLUE);
            });
            this.addButton("Set text", () : void => {
                object.Value("new text filed value");
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
