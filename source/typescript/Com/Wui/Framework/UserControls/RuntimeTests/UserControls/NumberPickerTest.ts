/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import NumberPicker = Com.Wui.Framework.UserControls.BaseInterface.UserControls.NumberPicker;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import NumberPickerType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.NumberPickerType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class NumberPickerTest extends ViewerTestRunner<NumberPicker> {

        public testRangeStartAPI() : void {
            this.assertEquals(this.getInstance().RangeStart(), -100);
        }

        public testRangeEndAPI() : void {
            this.assertEquals(this.getInstance().RangeEnd(), 100);
        }

        public testWidthAPI() : void {
            this.assertEquals(this.getInstance().Width(), 200);
        }

        public testTextAPI() : void {
            this.assertEquals(this.getInstance().Text(), "<span id=\"" + this.getInstance().Id() + "_TextValue\"></span> units");
            this.getInstance().Text("{0} mm");
            this.assertEquals(this.getInstance().Text(), "<span id=\"" + this.getInstance().Id() + "_TextValue\"></span> mm");
        }

        public testDecimalPlacesAPI() : void {
            this.assertEquals(this.getInstance().DecimalPlaces(), 1);
        }

        public testStyleClassNameAPI() : void {
            this.assertEquals(this.getInstance().StyleClassName(), "testCssClass");
        }

        public testgetGuiOptions() : void {
            this.assertEquals(this.getInstance().getGuiOptions().Contains(GuiOptionType.DISABLE), true);
        }

        public testValueAPI() : void {
            this.assertEquals(this.getInstance().Value(), 50);
            this.getInstance().Value(100);
            this.assertEquals(this.getInstance().Value(), 100);
            this.getInstance().Value(30);
            this.assertEquals(this.getInstance().Value(), 30);
            this.getInstance().Value(-20);
            this.assertEquals(this.getInstance().Value(), -20);
        }

        public testGuiTypeAPI() : void {
            const picker : NumberPicker = this.getInstance();
            picker.GuiType(NumberPickerType.GREEN);
            this.assertEquals(picker.GuiType(), NumberPickerType.GREEN);
            picker.GuiType(NumberPickerType.BLUE);
            this.assertEquals(picker.GuiType(), NumberPickerType.BLUE);
            picker.GuiType(NumberPickerType.RED);
            this.assertEquals(picker.GuiType(), NumberPickerType.RED);
        }

        public testEnabledAPI() : void {
            this.getInstance().Enabled(true);
            this.assertEquals(this.getInstance().Enabled(), true);
            this.getInstance().Enabled(false);
            this.assertEquals(this.getInstance().Enabled(), false);
            this.assertEquals(this.getInstance().DecimalPlaces(), 1);
            // this.testValueAPI();
            this.testGuiTypeAPI();
            this.testWidthAPI();
        }

        public testVisibleAPI() : void {
            this.assertEquals(this.getInstance().Visible(), true);
            this.getInstance().Visible(false);
            this.assertEquals(this.getInstance().Visible(), false);
            this.testGuiTypeAPI();
            this.testValueAPI();
            this.testWidthAPI();
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Pointer"), GeneralCssNames.OFF);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnClick(clickHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
            };
        }

        public __IgnoretestFocusStateAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const focusHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals($eventArgs.Owner(), this.getInstance());
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Pointer"), GeneralCssNames.ACTIVE);
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                        $done();
                    }, 500);
                };
                this.getInstance().getEvents().setOnFocus(focusHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.getInstance().Enabled(false);
                const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Pointer"), GeneralCssNames.ACTIVE);
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                        $done();
                    }, 500);
                };
                this.getInstance().getEvents().setOnFocus(focusDisableApiHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    // this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Pointer"), GeneralCssNames.OFF);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Pointer"), GeneralCssNames.OFF);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Pointer"), GeneralCssNames.OFF);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Pointer"), GeneralCssNames.OFF);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
            };
        }

        protected setUp() : void {
            const object : NumberPicker = this.getInstance();
            object.Enabled(true);
            object.Visible(true);
            object.GuiType(NumberPickerType.GENERAL);
            object.RangeStart(-100);
            object.RangeEnd(100);
            object.Value(50);
            object.Text("{0} units");
            object.Width(200);
            object.DecimalPlaces(1);
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
        }

        protected before() : string {
            const object : NumberPicker = this.getInstance();
            this.setUp();
            // object.Title().Text("title test");
            // object.Enabled(false);

            this.addButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addButton("set 0", () : void => {
                object.Value(0);
            });
            this.addButton("set 50", () : void => {
                object.Value(50);
            });
            this.addButton("set 100", () : void => {
                object.Value(100);
            });
            this.addButton("General", () : void => {
                object.GuiType(NumberPickerType.GENERAL);
            });
            this.addButton("Blue", () : void => {
                object.GuiType(NumberPickerType.BLUE);
            });
            this.addButton("Green", () : void => {
                object.GuiType(NumberPickerType.GREEN);
            });
            this.addButton("Red", () : void => {
                object.GuiType(NumberPickerType.RED);
            });
            this.addButton("Set size", () : void => {
                object.Width(800);
            });
            this.addButton("Set text", () : void => {
                object.Text("{0} px");
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
