/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import InputLabel = Com.Wui.Framework.UserControls.BaseInterface.UserControls.InputLabel;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import InputLabelType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.InputLabelType;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;

    export class InputLabelTest extends ViewerTestRunner<InputLabel> {

        public testTextAPI() : void {
            this.assertEquals(this.getInstance().Text(), "test label:");
            this.assertEquals(this.getInstance().Title().Text(), "Label tooltip text");
        }

        public testGuiOption() : void {
            const input : InputLabel = this.getInstance();
            this.assertEquals(input.getGuiOptions().Contains(GuiOptionType.DISABLE), true);
            this.assertEquals(input.getGuiOptions().Contains(GuiOptionType.ACTIVED), false);
        }

        public testEnabledAPI() : void {
            this.assertEquals(this.getInstance().Enabled(), true);
            this.getInstance().Enabled(false);
            this.assertEquals(this.getInstance().Enabled(), false);
        }

        public testErrorAPI() : void {
            this.assertEquals(this.getInstance().Error(), false);
            this.getInstance().Error(true);
            this.assertEquals(this.getInstance().Error(), true);
        }

        public testGuiTypeAPI() : void {
            this.getInstance().GuiType(InputLabelType.GREEN);
            this.assertEquals(this.getInstance().GuiType(), InputLabelType.GREEN);
            this.getInstance().GuiType(InputLabelType.RED);
            this.assertEquals(this.getInstance().GuiType(), InputLabelType.RED);
        }

        public testWidthAPI() : void {
            this.assertEquals(this.getInstance().Width(), 200);
            this.getInstance().Width(400);
            this.assertEquals(this.getInstance().Width(), 400);
            this.getInstance().Width(200);
        }

        public testHideAPI() : void {
            this.assertEquals(this.getInstance().Visible(), true);
            this.getInstance().Visible(false);
            this.assertEquals(this.getInstance().Visible(), false);
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const inputLabel : InputLabel = this.getInstance();
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), inputLabel);
                    inputLabel.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                inputLabel.getEvents().setOnClick(clickHandler);
                this.emulateEvent(inputLabel, EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const inputLabel : InputLabel = this.getInstance();
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    inputLabel.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                inputLabel.getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(inputLabel, EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const inputLabel : InputLabel = this.getInstance();
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    inputLabel.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                inputLabel.getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(inputLabel, EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const inputLabel : InputLabel = this.getInstance();
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    inputLabel.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                inputLabel.getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(inputLabel, EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const inputLabel : InputLabel = this.getInstance();
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    inputLabel.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                inputLabel.getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(inputLabel, EventType.ON_MOUSE_OUT);
            };
        }

        protected setUp() : void {
            const object : InputLabel = this.getInstance();
            object.Visible(true);
            object.Enabled(true);
            object.GuiType(InputLabelType.GENERAL);
            object.Error(false);
            object.Text("test label:");
            object.Width(200);
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
        }

        protected before() : string {
            const object : InputLabel = this.getInstance();
            object.Title().Text("Label tooltip text");
            this.setUp();
            // object.Enabled(false);
            // object.Error(true);

            this.addButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addButton("Show", () : void => {
                object.Visible(true);
            });
            this.addButton("Hide", () : void => {
                object.Visible(false);
            });
            this.addButton("Error", () : void => {
                object.Error(true);
            });
            this.addButton("Normal", () : void => {
                object.Error(false);
            });
            this.addButton("Set text", () : void => {
                object.Text("new label text");
            });
            this.addButton("Set width", () : void => {
                object.Width(30);
            });
            this.addButton("General", () : void => {
                object.GuiType(InputLabelType.GENERAL);
            });
            this.addButton("Red", () : void => {
                object.GuiType(InputLabelType.RED);
            });
            this.addButton("Green", () : void => {
                object.GuiType(InputLabelType.GREEN);
            });
            this.addButton("Blue", () : void => {
                object.GuiType(InputLabelType.BLUE);
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
