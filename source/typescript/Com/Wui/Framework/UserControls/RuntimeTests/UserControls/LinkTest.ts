/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import Link = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Link;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;

    export class LinkTest extends ViewerTestRunner<Link> {

        public testLinkEnabledAPI() : void {
            this.assertEquals(this.getInstance().Enabled(), true);
            this.getInstance().Enabled(false);
            this.assertEquals(this.getInstance().Enabled(), false);
        }

        public testVisibleAPI() : void {
            this.assertEquals(this.getInstance().Visible(), true);
            this.getInstance().Visible(false);
            this.assertEquals(this.getInstance().Visible(), false);
        }

        public testTextAPI() : void {
            this.getInstance().Text("new link text");
            this.assertEquals(this.getInstance().Text(), "new link text");
        }

        public testSetLinkAPI() : void {
            this.getInstance().OpenInNewWindow(true);
            this.getInstance().ReloadTo("www.wuiframework.com");
            this.assertEquals(this.getInstance().IsLoaded(), true);
            this.assertEquals(this.getInstance().OpenInNewWindow(), true);
            this.assertEquals(this.getInstance().IsCompleted(), true);
        }

        public testLinkScrollAPI() : void {
            this.getInstance().IsPreventingScroll();
            this.assertEquals(this.getInstance().IsPreventingScroll(), true);
        }

        public testOpenInNewWindowAPI() : void {
            this.getInstance().OpenInNewWindow(true);
            this.assertEquals(this.getInstance().OpenInNewWindow(), true);
            this.getInstance().OpenInNewWindow(false);
            this.assertEquals(this.getInstance().OpenInNewWindow(), false);
        }

        public testFocusStateAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const focusHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals($eventArgs.Owner(), this.getInstance());
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                        $done();
                    }, 500);
                };
                EventsManager.getInstanceSingleton().setEvent(this.getInstance(), EventType.ON_FOCUS, focusHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public testFocusStateDisabledAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.getInstance().Enabled(false);
                const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                        this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                        $done();
                    }, 500);
                };
                EventsManager.getInstanceSingleton().setEvent(this.getInstance(), EventType.ON_FOCUS, focusDisableApiHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
            };
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const link : Link = this.getInstance();
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), link);
                    link.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                link.getEvents().setOnClick(clickHandler);
                this.emulateEvent(link, EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const link : Link = this.getInstance();
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.IsVisible(link.Title()), true);
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    link.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                link.getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(link, EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const link : Link = this.getInstance();
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                    link.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                link.getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(link, EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const link : Link = this.getInstance();
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                    link.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                link.getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(link, EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const link : Link = this.getInstance();
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                    link.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                link.getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(link, EventType.ON_MOUSE_OUT);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const link : Link = this.getInstance();
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                    link.getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                    $done();
                };
                link.getEvents().setOnMouseOver(mouseoverHandler);
                this.emulateEvent(link, EventType.ON_MOUSE_OVER, event);
            };
        }

        protected setUp() : void {
            const object : Link = this.getInstance();
            object.Visible(true);
            object.Enabled(true);
            object.OpenInNewWindow(true);
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
            object.Text("test link");
            object.Title().Text("tooltip");
        }

        protected before() : string {
            const object : Link = this.getInstance();
            this.setUp();

            this.addButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addButton("Show", () : void => {
                object.Visible(true);
            });
            this.addButton("Hide", () : void => {
                object.Visible(false);
            });
            this.addButton("Set text", () : void => {
                object.Text("new link text");
            });
            this.addButton("Set link", () : void => {
                object.ReloadTo("/testLink/test");
            });
            this.addButton("This window", () : void => {
                object.OpenInNewWindow(false);
            });
            this.addButton("New window", () : void => {
                object.OpenInNewWindow(true);
            });

            const object2 : Link = new Link("TestLink2");
            object2.Text("link to wuiframework.com");
            object2.ReloadTo("www.wuiframework.com");
            object2.StyleClassName("testCssClass2");
            object2.OpenInNewWindow(true);

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;} " +
                ".testCssClass2 {position: relative; top: 50px; left: 210px; float: left;}</style>";
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
