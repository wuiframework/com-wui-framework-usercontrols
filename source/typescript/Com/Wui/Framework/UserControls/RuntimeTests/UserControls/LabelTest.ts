/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.UserControls {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import Label = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Label;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    export class LabelTest extends ViewerTestRunner<Label> {

        public testTextAPI() : void {
            this.assertEquals(this.getInstance().Text(), "test label:");
            this.getInstance().Text("Change Label");
            this.assertEquals(this.getInstance().Text(), "Change Label");
        }

        public testEnabledAPI() : void {
            this.getInstance().Enabled(true);
            this.assertEquals(this.getInstance().Enabled(), true);
            this.getInstance().Enabled(false);
            this.assertEquals(this.getInstance().Enabled(), false);
        }

        public testVisibleAPI() : void {
            this.assertEquals(this.getInstance().Visible(), true);
            this.getInstance().Visible(false);
            this.assertEquals(this.getInstance().Visible(), false);
        }

        public testValueAPI() : void {
            this.getInstance().Value("New Value");
            /// TODO: Validate Value() returns null?
            this.assertEquals(this.getInstance().Value(), null);
            this.assertEquals(this.getInstance().Text(), "New Value");
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const label : Label = this.getInstance();
                const onclickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), label);
                    label.getEvents().RemoveHandler(EventType.ON_CLICK, onclickHandler);
                    $done();
                };
                label.getEvents().setOnClick(onclickHandler);
                this.emulateEvent(label, EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const label : Label = this.getInstance();
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const onmousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.IsVisible(label.Title()), true);
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    label.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, onmousemoveHandler);
                    $done();
                };
                label.getEvents().setOnMouseMove(onmousemoveHandler);
                this.emulateEvent(label, EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const label : Label = this.getInstance();
                const onmousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    label.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, onmousedownHandler);
                    $done();
                };
                label.getEvents().setOnMouseDown(onmousedownHandler);
                this.emulateEvent(label, EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const label : Label = this.getInstance();
                const onmouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    label.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, onmouseupHandler);
                    $done();
                };
                label.getEvents().setOnMouseUp(onmouseupHandler);
                this.emulateEvent(label, EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const label : Label = this.getInstance();
                const onmouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    label.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, onmouseoutHandler);
                    $done();
                };
                label.getEvents().setOnMouseOut(onmouseoutHandler);
                this.emulateEvent(label, EventType.ON_MOUSE_OUT);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const label : Label = this.getInstance();
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const onmouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.assertEquals($eventArgs.NativeEventArgs().button, 2);
                    label.getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, onmouseoverHandler);
                    $done();
                };
                label.getEvents().setOnMouseOver(onmouseoverHandler);
                this.emulateEvent(label, EventType.ON_MOUSE_OVER, event);
            };
        }

        protected setUp() : void {
            const object : Label = this.getInstance();
            object.Visible(true);
            object.Enabled(true);
            object.Text("test label:");
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
            object.Title().Text("Label tooltip text");
        }

        protected before() : string {
            const object : Label = this.getInstance();
            this.setUp();
            // object.Enabled(false);

            this.addButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addButton("Show", () : void => {
                object.Visible(true);
            });
            this.addButton("Hide", () : void => {
                object.Visible(false);
            });
            this.addButton("Set text", () : void => {
                object.Text("new label text");
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
