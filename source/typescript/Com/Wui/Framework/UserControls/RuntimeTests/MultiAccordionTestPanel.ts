/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import AccordionViewer = Com.Wui.Framework.UserControls.BaseInterface.Viewers.UserControls.AccordionViewer;
    import Accordion = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Accordion;
    import AccordionViewerArgs = Com.Wui.Framework.UserControls.BaseInterface.ViewersArgs.UserControls.AccordionViewerArgs;
    import AccordionResizeType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionResizeType;
    import AccordionType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionType;
    import BasePanelHolderViewerArgs = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewerArgs;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;

    export class MultiAccordionTestPanel extends Com.Wui.Framework.UserControls.Primitives.BasePanel {
        public panel1 : Accordion;
        public panel2 : Accordion;
        public panel3 : Accordion;
        public panel4 : Accordion;
        public panel5 : Accordion;
        public panel6 : Accordion;
        public panel7 : Accordion;
        public panel8 : Accordion;

        constructor($id? : string) {
            super($id);

            const addArgs : any = ($args : AccordionViewerArgs) => {
                const holderArgs1 : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
                const holderArgs2 : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
                const holderArgs3 : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();

                holderArgs1.HeaderText("Holder 1");
                holderArgs1.DescriptionText("Desc.");
                holderArgs1.PrioritySize(new PropagableNumber({number: 200, unitType: UnitType.PCT}));
                holderArgs1.IsOpened(false);

                holderArgs2.HeaderText("Holder 2");
                holderArgs2.DescriptionText("Description txt");
                holderArgs2.IsOpened(false);
                holderArgs2.BodyViewerClass(UserControlsTestPanelViewer);

                holderArgs3.HeaderText("Holder 3");
                holderArgs3.PrioritySize(new PropagableNumber({number: 100, unitType: UnitType.PX}));
                holderArgs3.IsOpened(false);

                $args.AddPanelHoldersArgs(holderArgs1);
                $args.AddPanelHoldersArgs(holderArgs2);
                $args.AddPanelHoldersArgs(holderArgs3);
            };

            let args : AccordionViewerArgs = new AccordionViewerArgs(AccordionType.HORIZONTAL);
            args.ResizeType(AccordionResizeType.RESPONSIVE);
            addArgs(args);
            this.addChildPanel(AccordionViewer, args,
                ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                    $parent.panel1 = $child;
                });

            args = new AccordionViewerArgs(AccordionType.VERTICAL);
            args.ResizeType(AccordionResizeType.RESPONSIVE);
            addArgs(args);
            this.addChildPanel(AccordionViewer, args,
                ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                    $parent.panel2 = $child;
                });

            args = new AccordionViewerArgs(AccordionType.VERTICAL);
            args.ResizeType(AccordionResizeType.DEFAULT);
            addArgs(args);
            this.addChildPanel(AccordionViewer, args,
                ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                    $parent.panel3 = $child;
                });

            args = new AccordionViewerArgs(AccordionType.HORIZONTAL);
            args.ResizeType(AccordionResizeType.DEFAULT);
            addArgs(args);
            this.addChildPanel(AccordionViewer, args,
                ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                    $parent.panel4 = $child;
                });

            args = new AccordionViewerArgs(AccordionType.HORIZONTAL);
            args.ResizeType(AccordionResizeType.DEFAULT);
            addArgs(args);
            this.addChildPanel(AccordionViewer, args,
                ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                    $parent.panel5 = $child;
                });

            args = new AccordionViewerArgs(AccordionType.HORIZONTAL);
            args.ResizeType(AccordionResizeType.DEFAULT);
            addArgs(args);
            this.addChildPanel(AccordionViewer, args,
                ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                    $parent.panel6 = $child;
                });

            args = new AccordionViewerArgs(AccordionType.HORIZONTAL);
            args.ResizeType(AccordionResizeType.DEFAULT);
            addArgs(args);
            this.addChildPanel(AccordionViewer, args,
                ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                    $parent.panel7 = $child;
                });

            args = new AccordionViewerArgs(AccordionType.HORIZONTAL);
            args.ResizeType(AccordionResizeType.DEFAULT);
            addArgs(args);
            this.addChildPanel(AccordionViewer, args,
                ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                    $parent.panel8 = $child;
                });
        }

        protected innerCode() : IGuiElement {
            this.Width(1200);
            this.Height(800);

            this.panel1.Visible(true);
            this.panel2.Visible(true);
            this.panel3.Visible(true);
            this.panel4.Visible(true);
            this.panel5.Visible(false);
            this.panel6.Visible(false);
            this.panel7.Visible(false);
            this.panel8.Visible(false);

            return super.innerCode().Add(
                "<style>.MultiAccordionTestPanel{position:relative; float: left;} " +
                ".Accordion{position:relative; float: left;} " +
                //  ".Accordion .Panel {display: block !important;height: auto;width: auto;}" +
                "[guiType=\"GuiRow\"] > .Panel {display: table !important;height: 100%;width: 100%;}" +
                ".TestPanelCss .PanelScrollBar .Tracker{background-color: transparent;}</style>");
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn().FitToParent(FitToParent.FULL)
                .Add(this.addRow()
                    .Add(this.addColumn().Add(this.panel1))
                    .Add(this.addColumn().Add(this.panel2)))
                .Add(this.addRow()
                    .Add(this.addColumn().Add(this.panel3))
                    .Add(this.addColumn().Add(this.panel4)))
                /*.Add(this.addRow()
                    .Add(this.addColumn().Add(this.panel5))
                    .Add(this.addColumn().Add(this.panel6)))
                .Add(this.addRow()
                    .Add(this.addColumn().Add(this.panel7))
                    .Add(this.addColumn().Add(this.panel8)))*/;
        }
    }
}
/* dev:end */
