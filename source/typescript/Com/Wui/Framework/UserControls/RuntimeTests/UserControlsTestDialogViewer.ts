/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests {
    "use strict";
    import DialogViewerArgs = Com.Wui.Framework.UserControls.BaseInterface.ViewersArgs.UserControls.DialogViewerArgs;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import Dialog = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Dialog;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import DialogType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.DialogType;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class UserControlsTestDialogViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        protected static getTestViewerArgs() : DialogViewerArgs {
            const args : DialogViewerArgs = new DialogViewerArgs();
            const panelArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
//            panelArgs.AsyncEnabled(true);
            args.PanelArgs(panelArgs);

            args.Visible(true);
            args.HeaderText("Integration test dialog");

            return args;
        }

        constructor($args? : DialogViewerArgs) {
            super($args);

            const instance : Dialog = new Dialog(DialogType.GENERAL);
            instance.PanelViewer(new UserControlsTestPanelViewer());
            instance.Visible(true);
            instance.Modal(true);
            instance.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
            instance.headerText.Visible(true);
            instance.AutoResize(false);

            this.setInstance(instance);
        }

        public getInstance() : Dialog {
            return <Dialog>super.getInstance();
        }

        public ViewerArgs($args? : DialogViewerArgs) : DialogViewerArgs {
            return <DialogViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : Dialog, $args : DialogViewerArgs) : void {
            $instance.headerText.Text($args.HeaderText());
            if (!ObjectValidator.IsEmptyOrNull($args.PanelArgs())) {
                $instance.Value($args.PanelArgs());
            }
        }

        protected testImplementation($instance : Dialog) : void {
            this.addTestButton("Open dialog", () : void => {
                Dialog.Open($instance);
            });
        }
    }
}
/* dev:end */
