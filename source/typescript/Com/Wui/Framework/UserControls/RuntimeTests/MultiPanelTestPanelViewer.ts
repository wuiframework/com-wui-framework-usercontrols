/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class MultiPanelTestPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        protected static getTestViewerArgs() : BasePanelViewerArgs {
            const args : BasePanelViewerArgs = new BasePanelViewerArgs();
            args.AsyncEnabled(true);
            return args;
        }

        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new MultiPanelTestPanel());
        }

        public getInstance() : MultiPanelTestPanel {
            return <MultiPanelTestPanel>super.getInstance();
        }

        protected beforeLoad($instance : MultiPanelTestPanel) : void {
            // instance.Visible(false);
            $instance.Scrollable(true);
            $instance.Width(1200);
            $instance.Height(800);
            $instance.panel1.Scrollable(true);
            $instance.panel2.Scrollable(true);
            $instance.panel3.Scrollable(true);
        }

        protected argsHandler($instance : MultiPanelTestPanel, $args : BasePanelViewerArgs) : void {
            $instance.panel1.Value($args);
            $instance.panel2.Value($args);
            $instance.panel3.Value($args);
        }
    }
}
/* dev:end */
