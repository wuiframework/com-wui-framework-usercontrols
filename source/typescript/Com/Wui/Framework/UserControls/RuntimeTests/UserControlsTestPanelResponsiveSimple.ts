/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests {
    "use strict";
    import UserControls = Com.Wui.Framework.UserControls.BaseInterface.UserControls;
    import Enums = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import Button = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Button;
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;
    import VisibilityStrategy = Com.Wui.Framework.Gui.Enums.VisibilityStrategy;

    export class UserControlsTestPanelResponsiveSimple extends Com.Wui.Framework.UserControls.Primitives.BasePanel {
        public buttonResizeSmall : UserControls.Button;
        public buttonResizeMedium : UserControls.Button;
        public buttonResizeLarge : UserControls.Button;
        public toggleShowField1 : UserControls.Button;
        public toggleShowField2 : UserControls.Button;
        public toggleShowButton1 : UserControls.Button;
        public toggleShowButton2 : UserControls.Button;
        public toggleShowButton3 : UserControls.Button;
        public textField1 : UserControls.TextField;
        public textField2 : UserControls.TextField;

        constructor($id? : string) {
            super($id);

            this.buttonResizeSmall = new UserControls.Button(Enums.ButtonType.RED);
            this.buttonResizeMedium = new UserControls.Button(Enums.ButtonType.GREEN);
            this.buttonResizeLarge = new UserControls.Button(Enums.ButtonType.BLUE);

            this.toggleShowField1 = new UserControls.Button(Enums.ButtonType.GENERAL);
            this.toggleShowField2 = new UserControls.Button(Enums.ButtonType.GENERAL);

            this.toggleShowButton1 = new UserControls.Button(Enums.ButtonType.GENERAL);
            this.toggleShowButton2 = new UserControls.Button(Enums.ButtonType.GENERAL);
            this.toggleShowButton3 = new UserControls.Button(Enums.ButtonType.GENERAL);

            this.textField1 = new UserControls.TextField();
            this.textField2 = new UserControls.TextField(Enums.TextFieldType.BLUE);
        }

        protected innerCode() : IGuiElement {
            this.buttonResizeSmall.IconName(Enums.IconType.BLUE_SQUARE);
            this.buttonResizeSmall.Text("540px");
            this.buttonResizeSmall.getEvents().setOnClick(($eventArgs : EventArgs) => {
                (<BasePanel>(<Button>($eventArgs.Owner())).Parent()).Width(540);
            });

            this.buttonResizeMedium.IconName(Enums.IconType.BLACK_SQUARE);
            this.buttonResizeMedium.Text("720px");
            this.buttonResizeMedium.getEvents().setOnClick(($eventArgs : EventArgs) => {
                (<BasePanel>(<Button>($eventArgs.Owner())).Parent()).Width(720);
            });
            this.buttonResizeMedium.Visible(false);

            this.buttonResizeLarge.IconName(Enums.IconType.RED_SQUARE);
            this.buttonResizeLarge.Text("1100px");
            this.buttonResizeLarge.getEvents().setOnClick(($eventArgs : EventArgs) => {
                (<BasePanel>(<Button>($eventArgs.Owner())).Parent()).Width(1100);
            });

            this.toggleShowField1.IconName(Enums.IconType.GENERAL);
            this.toggleShowField1.Text("toggle field 1");
            this.toggleShowField1.getEvents().setOnClick(() => {
                this.textField1.Visible(!this.textField1.Visible());
            });

            this.toggleShowField2.IconName(Enums.IconType.GENERAL);
            this.toggleShowField2.Text("toggle field 2");
            this.toggleShowField2.getEvents().setOnClick(() => {
                this.textField2.Visible(!this.textField2.Visible());
            });

            this.toggleShowButton1.IconName(Enums.IconType.GENERAL);
            this.toggleShowButton1.Text("toggle button 1");
            this.toggleShowButton1.getEvents().setOnClick(() => {
                this.buttonResizeSmall.Visible(!this.buttonResizeSmall.Visible());
            });

            this.toggleShowButton2.IconName(Enums.IconType.GENERAL);
            this.toggleShowButton2.Text("toggle button 2");
            this.toggleShowButton2.getEvents().setOnClick(() => {
                this.buttonResizeMedium.Visible(!this.buttonResizeMedium.Visible());
            });

            this.toggleShowButton3.IconName(Enums.IconType.GENERAL);
            this.toggleShowButton3.Text("toggle button 3");
            this.toggleShowButton3.getEvents().setOnClick(() => {
                this.buttonResizeLarge.Visible(!this.buttonResizeLarge.Visible());
            });

            this.textField1.Hint("type something here");
            this.textField1.Width(300);
            this.textField1.Visible(false);

            this.textField2.Value("type something here");
            this.textField2.Width(300);

            return super.innerCode().Add("<style>" +
                "p,h4 {margin:8px;}" +
                ".ComWuiFrameworkUserControlsPrimitivesBasePanel {border : 1px dashed #80c4da; margin:20px;}" +
                ".Row1 {background: lightcyan;}" +
                ".Row2 {background: lightpink;}" +
                "</style>");
        }

        protected innerHtml() : IGuiElement {
            return this.addRow()
                .WidthOfColumn("100%")
                .FitToParent(FitToParent.FULL)
                .Alignment(Alignment.CENTER_PROPAGATED)
                .Add(this.addColumn().WidthOfColumn("8px"))
                .Add(this.addColumn()
                    .Add(this.addRow().HeightOfRow("8px"))
                    .Add(this.addRow()
                        .HeightOfRow("32px")
                        .WidthOfColumn("25%", true)
                        .Alignment(Alignment.RIGHT_PROPAGATED)
                        .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY)
                        // .Add(this.addColumn())
                        .Add(this.addColumn().WidthOfColumn("40%")
                            .Add(this.buttonResizeSmall)
                        )
                        .Add(this.buttonResizeMedium)
                        .Add(this.buttonResizeLarge)
                    )
                    .Add(this.addRow().HeightOfRow("8px"))
                    .Add(this.addRow()
                        .HeightOfRow("32px")
                        .WidthOfColumn("25%", true)
                        .Alignment(Alignment.RIGHT_PROPAGATED)
                        .Add(this.toggleShowButton1)
                        .Add(this.toggleShowButton2)
                        .Add(this.toggleShowButton3)
                    )
                    .Add(this.addRow()
                        .HeightOfRow("32px")
                        .WidthOfColumn("25%", true)
                        .Alignment(Alignment.RIGHT_PROPAGATED)
                        .Add(this.toggleShowField1)
                        .Add(this.toggleShowField2)
                    )
                    .Add(this.addRow().HeightOfRow("8px"))
                    .Add(this.addRow()
                        .Add(this.addColumn().VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY)
                            .Add(this.addRow()
                                .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                                .StyleClassName("Row1")
                                .Add(this.textField1)
                            )
                            .Add(this.addRow()
                                .StyleClassName("Row2")
                                .Add(this.textField2)
                            ))
                    )
                    .Add(this.addRow().HeightOfRow("8px"))
                )
                .Add(this.addColumn().WidthOfColumn("8px"));
        }
    }
}
/* dev:end */
