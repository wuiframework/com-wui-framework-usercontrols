/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;

    export class MultiPanelTestPanel extends Com.Wui.Framework.UserControls.Primitives.BasePanel {
        public panel1 : UserControlsTestPanel;
        public panel2 : UserControlsTestPanel;
        public panel3 : UserControlsTestPanel;

        constructor($id? : string) {
            super($id);

//            this.panel1 = <UserControlsTestPanel>this.addChildPanel(UserControlsTestPanelViewer);
//            this.panel2 = <UserControlsTestPanel>this.addChildPanel(UserControlsTestPanelViewer);
//            this.panel2.StyleClassName("TestPanelCss");
//            this.panel3 = <UserControlsTestPanel>this.addChildPanel(UserControlsTestPanelViewer);

            this.addChildPanel(UserControlsTestPanelViewer, null,
                ($parent : MultiPanelTestPanel, $child : UserControlsTestPanel) : void => {
                    $parent.panel1 = $child;
                });

            this.addChildPanel(UserControlsTestPanelViewer, null,
                ($parent : MultiPanelTestPanel, $child : UserControlsTestPanel) : void => {
                    $parent.panel2 = $child;
                });

            this.addChildPanel(UserControlsTestPanelViewer, null,
                ($parent : MultiPanelTestPanel, $child : UserControlsTestPanel) : void => {
                    $parent.panel3 = $child;
                });
        }

        protected innerCode() : IGuiElement {
            this.panel2.StyleClassName("TestPanelCss");
            return super.innerCode().Add(
                "<style>.TestPanelCss{background-color: lightgrey; padding: 20px; float: left;} " +
                ".TestPanelCss .PanelScrollBar .Tracker{background-color: transparent;}</style>");
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.panel1)
                .Add(this.panel2)
                .Add(this.panel3);
        }
    }
}
/* dev:end */
