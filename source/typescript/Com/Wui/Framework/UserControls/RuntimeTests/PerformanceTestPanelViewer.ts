/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class PerformanceTestPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new PerformanceTestPanel());
        }

        public getInstance() : PerformanceTestPanel {
            return <PerformanceTestPanel>super.getInstance();
        }

        protected normalImplementation() : void {
            const instance : PerformanceTestPanel = this.getInstance();
//            instance.Visible(false);
            instance.Scrollable(true);
            instance.Width(500);
            instance.Height(500);
        }
    }
}
/* dev:end */
