/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import UserControls = Com.Wui.Framework.UserControls.BaseInterface.UserControls;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;

    export class PerformanceTestPanel extends Com.Wui.Framework.UserControls.Primitives.BasePanel {

        private items : ArrayList<string>;
        private startTime : number;

        constructor($id? : string) {
            super($id);
            this.startTime = new Date().getTime();

            this.items = new ArrayList<string>();

            let index : number;
            for (index = 0; index < 5000; index++) {
                const newUserControl : UserControls.Label = new UserControls.Label("test label " + index);
                this.items.Add(newUserControl.Id(), index);
                this[newUserControl.Id()] = newUserControl;
            }

            Echo.Printf("init " + Convert.TimeToSeconds(new Date().getTime() - this.startTime).toString() + " s");
        }

        public Draw($EOL? : string) : string {
            Echo.Printf("draw " + Convert.TimeToSeconds(new Date().getTime() - this.startTime).toString() + " s");
            return super.Draw($EOL);
        }

        protected guiContent() : IGuiElement {
            Echo.Printf("guiContent " + Convert.TimeToSeconds(new Date().getTime() - this.startTime).toString() + " s");
            return super.guiContent();
        }

        protected innerCode() : IGuiElement {
            Echo.Printf("inner code " + Convert.TimeToSeconds(new Date().getTime() - this.startTime).toString() + " s");
            return super.innerCode().Add("<style>.UserControlsTestPanel {width: 600px;}</style>");
        }

        protected innerHtml() : IGuiElement {
            Echo.Printf("inner html " + Convert.TimeToSeconds(new Date().getTime() - this.startTime).toString() + " s");
            return this.addElement().Add(this.Id());
        }
    }
}
/* dev:end */
