/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class MultiAccordionTestPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        protected static getTestViewerArgs() : BasePanelViewerArgs {
            const args : BasePanelViewerArgs = new BasePanelViewerArgs();
            args.AsyncEnabled(true);
            return args;
        }

        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new MultiAccordionTestPanel());
        }

        public getInstance() : MultiAccordionTestPanel {
            return <MultiAccordionTestPanel>super.getInstance();
        }

        protected before($instance : MultiAccordionTestPanel) : void {
            // instance.Visible(false);

            $instance.Scrollable(true);
        }

        protected argsHandler($instance : MultiAccordionTestPanel, $args : BasePanelViewerArgs) : void {
            $instance.panel1.Value($args);
            $instance.panel2.Value($args);
            $instance.panel3.Value($args);
            $instance.panel4.Value($args);
        }
    }
}
/* dev:end */
