/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests {
    "use strict";
    import HorizontalPanelHolderViewer =
        Com.Wui.Framework.UserControls.BaseInterface.Viewers.UserControls.HorizontalPanelHolderViewer;
    import BasePanelHolderViewerArgs = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewerArgs;
    import HorizontalPanelHolder = Com.Wui.Framework.UserControls.BaseInterface.UserControls.HorizontalPanelHolder;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;

    export class HorizontalTestPanelHolderViewer extends HorizontalPanelHolderViewer {
        protected static getTestViewerArgs() : BasePanelHolderViewerArgs {
            const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            args.BodyViewerClass(UserControlsTestPanelViewer);
            args.HeaderText("Horizontal panel holder");
            args.DescriptionText("This is description text for horizontal panel holder");
            // args.PrioritySize(new PropagableNumber({number: 468, unitType: UnitType.PX}));
            // args.IsOpened(false);

            return args;
        }

        public getInstance() : HorizontalPanelHolder {
            return <HorizontalPanelHolder>super.getInstance();
        }

        protected testImplementation() : void {
            const instance : HorizontalPanelHolder = this.getInstance();
            instance.StyleClassName("TestCss");

            if (!ObjectValidator.IsEmptyOrNull(instance.getBody())) {
                const body : UserControlsTestPanel = <UserControlsTestPanel>instance.getBody();
                body.StyleClassName("Body");

                // body.image1.Visible(false);
                // body.image2.Visible(false);
                body.tabs1.Visible(false);
                body.tabs2.Visible(false);
                body.inputLabel1.Visible(false);
                body.inputLabel2.Visible(false);
                body.progressBar1.Visible(false);
                body.progressBar2.Visible(false);
                body.numberPicker1.Visible(false);
                body.numberPicker2.Visible(false);
                body.numberPicker1.Visible(false);
                body.link1.Visible(false);
                body.link2.Visible(false);
                body.imageButton1.Visible(false);
                body.imageButton2.Visible(false);
                body.button1.Visible(false);
                body.button2.Visible(false);
            }

            this.normalImplementation();
        }
    }
}
/* dev:end */
