/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class UserControlsTestPanelResponsiveViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new UserControlsTestPanelResponsive());
        }

        public getInstance() : UserControlsTestPanelResponsive {
            return <UserControlsTestPanelResponsive>super.getInstance();
        }

        protected normalImplementation() : void {
            const instance : UserControlsTestPanelResponsive = this.getInstance();
            instance.Scrollable(true);
            instance.Width(720);
            instance.Height(800);
        }

        protected testImplementation() : string {
            const args : BasePanelViewerArgs = new BasePanelViewerArgs();
            args.AsyncEnabled(true);
            this.ViewerArgs(args);
            const instance : UserControlsTestPanelResponsive = this.getInstance();
            instance.Scrollable(false);
            return "";
        }
    }
}
/* dev:end */
