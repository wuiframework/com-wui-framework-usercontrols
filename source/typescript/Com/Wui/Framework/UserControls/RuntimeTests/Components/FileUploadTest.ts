/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.Components {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import FileUpload = Com.Wui.Framework.UserControls.BaseInterface.Components.FileUpload;
    import AbstractGuiObject = Com.Wui.Framework.UserControls.Primitives.AbstractGuiObject;
    import Button = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Button;
    import ProgressBar = Com.Wui.Framework.UserControls.BaseInterface.UserControls.ProgressBar;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ProgressEventArgs = Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import FileUploadEventArgs = Com.Wui.Framework.Gui.Events.Args.FileUploadEventArgs;
    import FileUploadEventType = Com.Wui.Framework.Gui.Enums.Events.FileUploadEventType;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;

    export class FileUploadTest extends ViewerTestRunner<AbstractGuiObject> {
        private object : FileUpload;

        public testMaxChunkSize() : void {
            this.assertEquals(this.object.MaxChunkSize(), 2097152);
        }

        public testsetDropZone() : void {
            this.assertEquals(this.object.MaxFileSize(), 0);
        }

        public testMultipleSelectEnabled() : void {
            this.assertEquals(this.object.MultipleSelectEnabled(), true);
        }

        public testFilter() : void {
            this.assertEquals(this.object.Filter(".jpg", "*.png", "video/*"), ".jpg,.png,video/*");
        }

        public testValue() : void {
            this.assertEquals(this.object.Value(), "");
        }

        public __IgnoretestEvents() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.object.getEvents().setOnUploadStart(($eventArgs : FileUploadEventArgs) : void => {
                    this.assertEquals(this.object.getEvents().Exists(FileUploadEventType.ON_UPLOAD_START), true);
                    $done();
                });
            };
        }

        public __IgnoretestOnAboard() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.object.getEvents().setOnAboard(($eventArgs : FileUploadEventArgs) : void => {
                    this.assertEquals(this.object.getEvents().Exists(FileUploadEventType.ON_ABOARD), true);
                    $done();
                });
            };
        }

        protected before() : string {
            // this.removeInstance();

            const button : Button = new Button();
            button.Text("Open");

            const progressBar : ProgressBar = new ProgressBar();
            progressBar.Width(400);

            this.object = new FileUpload(
                "http://localhost.wuiframework.com/com-wui-framework-rest-services/connector.config.jsonp");
            this.object.setOpenElement(button);
            this.object.setDropZone("DropZone");
            this.object.MaxChunkSize("2,5 MB");
            this.object.getEvents().setOnChange(() : void => {
                ElementManager.setInnerHtml("FilesList", this.object.Value());
            });
            this.object.getEvents().setOnError(($eventArgs : ErrorEventArgs) : void => {
                Echo.Printf($eventArgs.Message());
            });
            this.object.getEvents().setOnAboard(() : void => {
                progressBar.Value(0);
            });

            this.object.getEvents().setOnUploadStart(() : void => {
                progressBar.Value(0);
            });
            this.object.getEvents().setOnUploadChange(($eventArgs : ProgressEventArgs) : void => {
                progressBar.Value(Convert.ToFixed(100 / $eventArgs.RangeEnd() * $eventArgs.CurrentValue(), 0));
            });
            this.object.getEvents().setOnUploadComplete(() : void => {
                progressBar.Value(progressBar.RangeEnd());
            });

            this.addButton("SingleFile", () : void => {
                this.object.MultipleSelectEnabled(false);
            });
            this.addButton("MultiFile", () : void => {
                this.object.MultipleSelectEnabled(true);
            });
            this.addButton("set Filter", () : void => {
                this.object.Filter(".jpg", "*.png", "video/*");
            });
            this.addButton("set Size limit", () : void => {
                this.object.MaxFileSize(10 * 1024 * 1024);
            });
            this.addButton("Load", () : void => {
                this.object.getStream(($data : ArrayList<string>) : void => {
                    $data.foreach(($value : string, $key : string) : void => {
                        Echo.Printf("<hr><b>{0}:</b>{1}", $key, StringUtils.NewLine());
                        if (StringUtils.Length($value) > 1024 * 20) {
                            Echo.PrintCode(StringUtils.Substring($value, 0, 1024 * 20) + " ...");
                        } else {
                            Echo.PrintCode($value);
                        }
                    });
                });
            });
            this.addButton("Upload", () : void => {
                this.object.Upload();
            });
            this.addButton("Aboard", () : void => {
                this.object.Aboard();
            });

            return this.object.Draw() +
                "<div style=\"clear: both;\">" + button.Draw() + "</div>" +
                "<div id=\"FilesList\" style=\"clear: both;\"></div>" +
                "<div id=\"DropZone\" style=\"border: 1px dashed red; width: 400px; height: 300px;\">Drop files here</div>" +
                progressBar.Draw();
        }
    }
}
/* dev:end */
