/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.Components {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import AbstractGuiObject = Com.Wui.Framework.Gui.Primitives.AbstractGuiObject;
    import SelectBox = Com.Wui.Framework.UserControls.BaseInterface.Components.SelectBox;
    import IconType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.IconType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import SelectBoxType = Com.Wui.Framework.UserControls.BaseInterface.Enums.Components.SelectBoxType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeEventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;

    export class SelectBoxTest extends ViewerTestRunner<AbstractGuiObject> {
        private object : SelectBox;

        public testMaxVisibleItemCountAPI() : void {
            this.object.MaxVisibleItemsCount(3);
            this.assertEquals(this.object.MaxVisibleItemsCount(), 3);
            this.assertEquals(this.object.OptionsCount(), 6);
            this.assertDeepEqual(this.object.Value(3), "4");
            this.assertEquals(this.object.GuiType(), SelectBoxType.GENERAL);
            this.object.Width(300);
            this.assertEquals(this.object.Width(), 300);
        }

        public testHideAPI() : void {
            this.assertEquals(this.object.Enabled(), true);
            SelectBox.Hide(this.object);
            this.assertEquals(this.object.Visible(), true);
            this.object.Enabled(false);
            this.assertEquals(this.object.Enabled(), false);
        }

        public AddAPI() : void {
            this.object.Add("test7", "value");
            this.assertDeepEqual(this.object.Value("value"), this.object.Id());
        }

        public testGuiType() : void {
            this.assertEquals(this.object.GuiType(), SelectBoxType.GENERAL);
            this.object.GuiType(SelectBoxType.GREEN);
            this.assertEquals(this.object.GuiType(), SelectBoxType.GREEN);
            this.object.GuiType(SelectBoxType.GENERAL);
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), this.object);
                    this.assertEquals(ElementManager.getClassName(this.object.Id() + "_Status"), null);
                    this.assertEquals(ElementManager.getEnvelopWidth(this.object.Id() + "_Envelop"), 100);
                    this.object.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                this.object.getEvents().setOnClick(clickHandler);
                this.emulateEvent(this.object, EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getEnvelopWidth(this.object.Id() + "_Envelop"), 120);
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventDownME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.getEnvelopWidth(this.object.Id() + "_Envelop"), 120);
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseDown(mousedownHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_DOWN);
            };
        }

        public testEventUpME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseUp(mouseupHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_UP);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_OUT);
            };
        }

        public __IgnoretestOnSelectAPI() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.object.getEvents().setOnSelect(($eventArgs : EventArgs) : void => {
                    this.object.Select(4);
                    this.assertEquals(this.object.getEvents().Exists("onselect"), true);
                    $done();
                });
            };
        }

        public __IgnoretestOnResize() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.object.getEvents().setOnResize(($eventArgs : ResizeEventArgs) : void => {
                    this.assertEquals(this.object.getEvents().Exists(EventType.ON_RESIZE), true);
                    $done();
                });
            };
        }

        public __IgnoretestOnChange() : IViewerTestPromise {
            return ($done : () => void) : void => {
                this.object.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                    this.assertEquals(this.object.getEvents().Exists(EventType.ON_CHANGE), true);
                    $done();
                });
            };
        }

        protected setUp() : void {
            this.object.Visible(true);
            this.object.Enabled(true);
        }

        protected before() : string {
            // this.removeInstance();

            this.object = new SelectBox();
            this.object.StyleClassName("testCssClass");
            // this.object.Value("test value");

            // this.object.Visible(false);
            this.object.setInitSize(100, 20);

            // this.object.setOpenDirection(DirectionType.DOWN, DirectionType.LEFT);
            // this.object.setOpenDirection(DirectionType.UP, DirectionType.RIGHT);
            // this.object.setOpenDirection(DirectionType.UP, DirectionType.LEFT);

            this.object.Width(300);
            // this.object.Height(500);
            this.object.MaxVisibleItemsCount(3);
            // this.object.Clear();
            this.object.Add("18", 1, IconType.BLUE_SQUARE);
            this.object.Add("35", 2, "withSeparator");
            this.object.Add("test4", 3);
            this.object.Add("test5", 4);
            this.object.Add("test7 with long visible text", "string value 2");
            this.object.Add("test6", "string value");

            this.object.Select(3);
            this.object.Select("test6");

            this.object.getEvents().setOnChange(() : void => {
                Echo.Printf("Selected {0}", this.object.Value());
            });

            this.addButton("Show", () : void => {
                this.object.Visible(true);
            });
            this.addButton("Hide", () : void => {
                this.object.Visible(false);
            });
            this.addButton("General", () : void => {
                this.object.GuiType(SelectBoxType.GENERAL);
            });
            this.addButton("Red", () : void => {
                this.object.GuiType(SelectBoxType.RED);
            });
            this.addButton("Green", () : void => {
                this.object.GuiType(SelectBoxType.GREEN);
            });
            this.addButton("Blue", () : void => {
                this.object.GuiType(SelectBoxType.BLUE);
            });
            this.addButton("Add item", () : void => {
                this.object.Add("new text list item");
            });
            this.addButton("Clear", () : void => {
                this.object.Clear();
            });
            this.addButton("Set size", () : void => {
                this.object.Width(500);
            });
            this.addButton("Select item 2", () : void => {
                this.object.Select("35");
            });

            return "<style>" +
                ".testCssClass {position: relative; top: 500px; left: 500px; float: left;} " +
                ".withSeparator .Separator {border-bottom: 1px solid red;} " +
                "</style>" +
                this.object.Draw();
        }
    }
}
/* dev:end */
