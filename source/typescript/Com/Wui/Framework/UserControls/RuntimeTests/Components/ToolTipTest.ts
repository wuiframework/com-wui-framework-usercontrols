/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.Components {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import AbstractGuiObject = Com.Wui.Framework.UserControls.Primitives.AbstractGuiObject;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import ToolTip = Com.Wui.Framework.UserControls.BaseInterface.Components.ToolTip;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    export class ToolTipTest extends ViewerTestRunner<AbstractGuiObject> {

        public testHideAPI() : void {
            const object : AbstractGuiObject = this.getInstance();
            object.Title().Text("My ToolTip");
            ToolTip.Show(object.Title());
            this.assertEquals(object.Title().Visible(), false);
            ToolTip.Hide(object.Title());
            this.assertEquals(object.Title().Visible(), false);
        }

        public testShowAPI() : void {
            const object : AbstractGuiObject = this.getInstance();
            object.Title().Text("My ToolTip");
            object.Visible(true);
            object.Enabled(true);
            object.getGuiOptions().Add(GuiOptionType.TOOL_TIP);
            this.getEventsManager().FireAsynchronousMethod(() => {
                ToolTip.Show(object.Title());
                this.assertEquals(object.Title().Visible(), false);
                this.getEventsManager().FireAsynchronousMethod(() => {
                    this.assertEquals(object.Title().Visible(), false);
                }, 2000);
            }, 5000);
        }

        public testShowNext() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const object : AbstractGuiObject = this.getInstance();
                object.Title().Text("My ToolTip");
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    ToolTip.Show(object.Title());
                    this.getEventsManager().FireAsynchronousMethod(() : void => {

                        this.assertEquals(object.Title().Visible(), false);
                    }, 600);
                }, 2000);
                $done();
            };
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnClick(clickHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
            };
        }

        public testEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventOutME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                    $done();
                };
                this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
                this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
            };
        }

        public __IgnoretestEvents() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const object : AbstractGuiObject = this.getInstance();
                object.getEvents().setOnMouseMove(() : void => {
                    this.assertEquals(object.Title().Visible(), true);
                    $done();
                });
                this.getEventsManager().FireEvent(object, EventType.ON_MOUSE_MOVE);
            };
        }

        protected setUp() : void {
            const object : AbstractGuiObject = this.getInstance();
            object.Title().Text("Gui object");
            object.Visible(true);
            object.Enabled(true);
            ToolTip.Show(object.Title());
        }

        protected before() : string {
            const object : AbstractGuiObject = this.getInstance();
            object.Title().Text("test " + StringUtils.NewLine() + StringUtils.NewLine() + "asd");
            object.StyleClassName("testCssClass");

            this.addButton("force show", () : void => {
                ToolTip.Show(object.Title());
            });
            this.addButton("force hide", () : void => {
                object.Title().Visible(false);
            });

            return "<style>.testCssClass {float: left; position: relative; top: 50px; left: 150px;}</style>";
        }
    }
}
/* dev:end */
