/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests.Components {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import AbstractGuiObject = Com.Wui.Framework.UserControls.Primitives.AbstractGuiObject;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ResizeBarEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeBarEventArgs;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;

    export class ResizeBarTest extends ViewerTestRunner<AbstractGuiObject> {
        private object : ResizeBar;

        public testResizeTypeAPI() : void {
            this.assertEquals(this.object.ResizeableType(), ResizeableType.HORIZONTAL_AND_VERTICAL);
            this.object.ResizeableType(ResizeableType.VERTICAL);
            this.assertEquals(this.object.ResizeableType(), ResizeableType.VERTICAL);
            this.object.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
        }

        public __IgnoretestPositionAPI() : void {
            this.assertEquals(this.object.getScreenPosition().Left(), 417);
            this.assertEquals(this.object.getScreenPosition().Top(), 576);
        }

        public testVisibleAPI() : void {
            this.assertEquals(this.object.Visible(), true);
            this.object.Visible(false);
            this.assertEquals(this.object.Visible(), false);
        }

        public testEventClickME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const clickHandler : any = ($eventArgs : EventArgs) : void => {
                    this.assertEquals($eventArgs.Owner(), this.object);
                    this.object.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                    $done();
                };
                this.object.getEvents().setOnClick(clickHandler);
                this.emulateEvent(this.object, EventType.ON_CLICK);
            };
        }

        public __IgnoretestEventMoveME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mousemoveHandler : any =
                    this.object.getEvents().setOnMouseMove(($eventArgs : MouseEventArgs) : void => {
                        this.object.setArg(<IGuiCommonsArg>{
                            name : "Width",
                            type : "Number",
                            value: 300
                        }, true);
                        ElementManager.setCssProperty(this.object.Id(), "_Width", 300);
                        this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                        this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                        this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                        $done();
                    });
                this.object.getEvents().setOnMouseMove(mousemoveHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_MOVE, event);
            };
        }

        public testEventOverME() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
                const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                    $done();
                };
                this.object.getEvents().setOnMouseOver(mouseoverHandler);
                this.emulateEvent(this.object, EventType.ON_MOUSE_OVER, event);
            };
        }

        protected setUp() : void {
            this.object.Visible(true);
            this.object.Enabled(true);
            this.object.Width(200);
        }

        protected before() : string {
            // this.removeInstance();

            let parentSize : Size;
            this.object = new ResizeBar(ResizeableType.HORIZONTAL_AND_VERTICAL, "Test_ResizeBar");

            this.object.getEvents().setOnResizeStart(() : void => {
                const parentId : string = "Test";
                if (ElementManager.Exists(parentId)) {
                    parentSize = new Size(parentId, true);
                }
            });

            this.object.getEvents().setOnResizeChange(($eventArgs : ResizeBarEventArgs) : void => {
                const parentId : string = "Test";
                if (ElementManager.Exists(parentId)) {
                    let width : number = parentSize.Width() + $eventArgs.getDistanceX();
                    let height : number = parentSize.Height() + $eventArgs.getDistanceY();

                    if (width < 100) {
                        width = 100;
                    }
                    if (width > 500) {
                        width = 500;
                    }

                    if (height < 100) {
                        height = 100;
                    }
                    if (height > 500) {
                        height = 500;
                    }

                    ElementManager.setWidth(parentId, width);
                    ElementManager.setHeight(parentId, height);
                }
            });

            return "<div style=\"float: left; position: relative; top: 100px; left: 100px;\">" + StringUtils.NewLine(false) +
                "   <div id=\"Test\" style=\"border: 1px solid red; width: 300px; height: 300px;\"></div>" + StringUtils.NewLine(false) +
                "   " + this.object.Draw() + StringUtils.NewLine(false) +
                "</div>";
        }
    }
}
/* dev:end */
