/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.UserControls.RuntimeTests {
    "use strict";
    import UserControls = Com.Wui.Framework.UserControls.BaseInterface.UserControls;
    import Enums = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;

    export class UserControlsTestPanel extends Com.Wui.Framework.UserControls.Primitives.BasePanel {
        public label1 : UserControls.Label;
        public label2 : UserControls.Label;
        public labelList1 : UserControls.LabelList;
        public labelList2 : UserControls.LabelList;
        public link1 : UserControls.Link;
        public link2 : UserControls.Link;
        public imageButton1 : UserControls.ImageButton;
        public imageButton2 : UserControls.ImageButton;
        public button1 : UserControls.Button;
        public button2 : UserControls.Button;
        public textField1 : UserControls.TextField;
        public textField2 : UserControls.TextField;
        public textArea1 : UserControls.TextArea;
        public textArea2 : UserControls.TextArea;
        public checkBox1 : UserControls.CheckBox;
        public checkBox2 : UserControls.CheckBox;
        public radioBox1 : UserControls.RadioBox;
        public radioBox2 : UserControls.RadioBox;
        public dropDownList1 : UserControls.DropDownList;
        public dropDownList2 : UserControls.DropDownList;
        public numberPicker1 : UserControls.NumberPicker;
        public numberPicker2 : UserControls.NumberPicker;
        public progressBar1 : UserControls.ProgressBar;
        public progressBar2 : UserControls.ProgressBar;
        // public image1 : UserControls.Image;
        // public image2 : UserControls.Image;
        public tabs1 : UserControls.Tabs;
        public tabs2 : UserControls.Tabs;
        public inputLabel1 : UserControls.InputLabel;
        public inputLabel2 : UserControls.InputLabel;

        constructor($id? : string) {
            super($id);

            this.label1 = new UserControls.Label("test label: ");
            this.label2 = new UserControls.Label("test label2");

            this.labelList1 = new UserControls.LabelList(Enums.IconType.BLUE_SQUARE);
            this.labelList2 = new UserControls.LabelList(Enums.IconType.RED_SQUARE);

            this.link1 = new UserControls.Link();
            this.link2 = new UserControls.Link();

            this.imageButton1 = new UserControls.ImageButton();
            this.imageButton2 = new UserControls.ImageButton(Enums.ImageButtonType.GREEN);

            this.button1 = new UserControls.Button();
            this.button2 = new UserControls.Button(Enums.ButtonType.RED);

            this.textField1 = new UserControls.TextField();
            this.textField2 = new UserControls.TextField(Enums.TextFieldType.BLUE);

            this.textArea1 = new UserControls.TextArea();
            this.textArea2 = new UserControls.TextArea();

            this.checkBox1 = new UserControls.CheckBox();
            this.checkBox2 = new UserControls.CheckBox();

            this.radioBox1 = new UserControls.RadioBox("testGroup");
            this.radioBox2 = new UserControls.RadioBox("testGroup");

            this.dropDownList1 = new UserControls.DropDownList();
            this.dropDownList2 = new UserControls.DropDownList();

            this.numberPicker1 = new UserControls.NumberPicker();
            this.numberPicker2 = new UserControls.NumberPicker();

            this.progressBar1 = new UserControls.ProgressBar();
            this.progressBar2 = new UserControls.ProgressBar();

            // this.image1 = new UserControls.Image("test/resource/graphics/Com/Wui/Framework/UserControls/img1.jpg");
            // this.image2 = new UserControls.Image("test/resource/graphics/Com/Wui/Framework/UserControls/img2.jpg");

            this.tabs1 = new UserControls.Tabs();
            this.tabs2 = new UserControls.Tabs();

            this.inputLabel1 = new UserControls.InputLabel();
            this.inputLabel2 = new UserControls.InputLabel(Enums.InputLabelType.RED, "label text 2");
        }

        protected innerCode() : IGuiElement {
            this.labelList1.Clear();
            this.labelList1.Add("list item 1");
            this.labelList1.Add("list item 2");

            this.labelList2.Clear();
            this.labelList2.Add("list item 1");
            this.labelList2.Add("list item 2");
            this.labelList2.Add("list item 3");
            this.labelList2.Enabled(false);

            this.link1.Text("test link");

            this.link2.Text("test link2");
            this.link2.ReloadTo("www.wuiframework.com");

            this.imageButton1.Title().Text("this is test image button");

            this.button1.IconName(Enums.IconType.RED_SQUARE);
            this.button1.Text("test button");

            this.button2.IconName(Enums.IconType.BLACK_SQUARE);
            this.button2.Text("test button 2");

            this.textField1.Hint("type some text here");
            this.textField1.Width(300);

            this.textField2.Value("this is textfield");
            this.textField2.Width(300);

            this.textArea1.Hint("type some text here");
            this.textArea1.Width(300);
            this.textArea1.Height(150);

            this.textArea2.Width(300);
            this.textArea2.Height(100);
            this.textArea2.LengthLimit(500);
            this.textArea2.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);

            this.checkBox1.Text("check box item");

            this.checkBox2.Text(" ");
            this.checkBox2.Checked(true);
            this.checkBox2.Enabled(false);

            this.radioBox1.Text("radio item 1");
            this.radioBox1.Checked(true);

            this.radioBox2.Text("radio item 2");

            this.dropDownList1.Width(300);
            this.dropDownList1.Clear();
            this.dropDownList1.Add("item 1");
            this.dropDownList1.Add("item 2");
            this.dropDownList1.Add("item 3");
            this.dropDownList1.Add("item 4");
            this.dropDownList1.MaxVisibleItemsCount(2);
            this.dropDownList1.Hint("choose some item");
//            this.dropDownList1.Visible(false);

            this.dropDownList2.Width(300);
            this.dropDownList2.Height(300);
            this.dropDownList2.Clear();
            this.dropDownList2.Add("item 1");
            this.dropDownList2.Add("item 2");
            this.dropDownList2.Select(1);

            this.numberPicker1.Width(300);
            this.numberPicker1.Value(25);

            this.numberPicker2.Width(300);
            this.numberPicker2.Value(50);
            this.numberPicker2.RangeStart(-100);
            this.numberPicker2.RangeEnd(100);
            this.numberPicker2.DecimalPlaces(2);

            this.progressBar1.Width(300);
            this.progressBar1.Value(50);

            this.progressBar2.Width(300);
            this.progressBar2.RangeStart(-100);
            this.progressBar2.RangeEnd(100);
            this.progressBar2.Value(50);

            // this.image1.setSize(200, 200);
            //
            // this.image2.setSize(300, 300);
            // this.image2.Link("www.wuiframework.com");
            // this.image2.Visible(false);

            this.tabs1.Width(300);
            if (!this.IsCached()) {
                this.tabs1.Clear();
                this.tabs1.Add("Tab 1");
                this.tabs1.Add("Tab 2");
                this.tabs1.Add("Tab 3");
                this.tabs1.Add("Tab 4");
                this.tabs1.Add("Tab 5");
            }
            this.tabs1.Select(4);

            this.tabs2.Width(300);
            if (!this.IsCached()) {
                this.tabs2.Clear();
                this.tabs2.Add("Tab 1");
                this.tabs2.Add("Tab 2");
                this.tabs2.Add("Tab 3");
            }
            this.tabs2.Select(1);

            this.inputLabel1.Width(300);
            this.inputLabel2.Width(200);

            return super.innerCode().Add("<style>.UserControlsTestPanel >[guiType=\"Panel\"] >.Envelop >.Content {width: 600px;}</style>");
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()

                .Add(this.Id())

                .Add(this.label1).Add(this.label2)
                .Add(this.labelList1).Add(this.labelList2).Add(StringUtils.NewLine())
                .Add(this.link1).Add(this.link2).Add(StringUtils.NewLine())

                .Add(this.imageButton1).Add(this.imageButton2).Add(StringUtils.NewLine())
                .Add(this.button1).Add(this.button2).Add(StringUtils.NewLine())

                .Add(this.textField1).Add(this.textField2).Add(StringUtils.NewLine())
                .Add(this.textArea1).Add(StringUtils.NewLine()).Add(this.textArea2).Add(StringUtils.NewLine())

                .Add(this.checkBox1).Add(this.checkBox2).Add(StringUtils.NewLine())
                .Add(this.radioBox1).Add(this.radioBox2).Add(StringUtils.NewLine())

                .Add(this.dropDownList1).Add(this.dropDownList2).Add(StringUtils.NewLine())

                .Add(this.numberPicker1).Add(this.numberPicker2).Add(StringUtils.NewLine())
                .Add(this.progressBar1).Add(this.progressBar2).Add(StringUtils.NewLine())

                // .Add(this.image1).Add(this.image2).Add(StringUtils.NewLine())

                .Add(this.tabs1).Add(this.tabs2).Add(StringUtils.NewLine())

                .Add(this.inputLabel1).Add(this.inputLabel2)
                .Add("Sample Text");
        }
    }
}
/* dev:end */
