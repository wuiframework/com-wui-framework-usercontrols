/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Structures {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * AutocompleteOption class provides structure for autocomplete options.
     */
    export class AutocompleteOption extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private readonly text : string;
        private readonly value : string | number;
        private readonly data : string;
        private readonly styleClassName : string;

        /**
         * @param {string} $text Specify value for visible option text.
         * @param {string|number} [$value] Specify value for selected option.
         * @param {string} [$lookupData] Specify value for option lookup.
         * @param {string} [$styleClassName] Specify option's css style class.
         */
        constructor($text : string, $value? : string | number, $lookupData? : string, $styleClassName? : string) {
            super();
            this.text = StringUtils.StripTags($text);
            if (ObjectValidator.IsEmptyOrNull($value)) {
                this.value = this.text;
            } else {
                this.value = $value;
            }
            if (ObjectValidator.IsEmptyOrNull($lookupData)) {
                this.data = this.text;
            } else {
                this.data = $lookupData;
            }
            if (ObjectValidator.IsEmptyOrNull($styleClassName)) {
                this.styleClassName = "";
            } else {
                this.styleClassName = $styleClassName;
            }
        }

        /**
         * @return {string} Returns value, which should be used as visible option text.
         */
        public Text() : string {
            return this.text;
        }

        /**
         * @return {string|number} Returns value, which should be used as selected option value.
         */
        public Value() : string | number {
            return this.value;
        }

        /**
         * @return {string} Returns value, which should be used for option lookup.
         */
        public LookupData() : string {
            return this.data;
        }

        /**
         * @return {string} Returns value, which should be used for option's css style.
         */
        public StyleClassName() : string {
            return this.styleClassName;
        }
    }
}
