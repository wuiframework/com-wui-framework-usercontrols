/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Structures {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * TextFieldFormArgs is structure for handling of FormInput with TextField user control.
     */
    export class TextFieldFormArgs extends BaseFormInputArgs {
        private readOnly : boolean;
        private integerOnly : boolean;
        private passwordEnabled : boolean;
        private size : number;

        constructor() {
            super();

            this.readOnly = false;
            this.integerOnly = false;
            this.passwordEnabled = false;
            this.size = -1;
        }

        /**
         * @param {(string|number)} [$value] Specify text field value.
         * @return {(string|number)} Returns text field value.
         */
        public Value($value? : string | number) : string | number {
            if (ObjectValidator.IsSet($value) && (ObjectValidator.IsString($value) || ObjectValidator.IsDigit($value))) {
                return <string | number>super.Value($value);
            }
            return <string | number>super.Value();
        }

        /**
         * @param {boolean} [$value] Specify, if text field can be edited or not.
         * @return {boolean} Returns true, if text field can be edited, otherwise false.
         */
        public ReadOnly($value? : boolean) : boolean {
            return this.readOnly = Property.Boolean(this.readOnly, $value);
        }

        /**
         * @param {boolean} [$value] Specify, if entered value can be only number character.
         * @return {boolean} Returns true, if entered value can be only number character, otherwise false.
         */
        public IntegerOnly($value? : boolean) : boolean {
            return this.integerOnly = Property.Boolean(this.integerOnly, $value);
        }

        /**
         * @param {boolean} [$value] Specify, if value should be hidden as password.
         * @return {boolean} Returns true, if value should be hidden as password, otherwise false.
         */
        public PasswordEnabled($value? : boolean) : boolean {
            return this.passwordEnabled = Property.Boolean(this.passwordEnabled, $value);
        }

        /**
         * @param {number} [$value] Specify maximal length of entered value.
         * @return {number} Returns maximal length of entered value, if size has been specified, otherwise -1.
         */
        public Size($value? : number) : number {
            return this.size = Property.Integer(this.size, $value, 1);
        }
    }
}
