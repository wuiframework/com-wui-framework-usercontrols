/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Structures {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    /**
     * DropDownListFormArgs is structure for handling of FormInput with DropDownList user control.
     */
    export class DropDownListFormArgs extends BaseFormInputArgs {
        private readonly namesList : ArrayList<string>;
        private readonly valuesList : ArrayList<string | number>;
        private maxVisibleItemsCount : number;

        constructor() {
            super();

            this.namesList = new ArrayList<string>();
            this.valuesList = new ArrayList<string | number>();
            this.maxVisibleItemsCount = -1;
        }

        /**
         * @param {(string|number)} [$value] Specify text field value.
         * @return {(string|number)} Returns text field value.
         */
        public Value($value? : string | number) : string | number {
            if (ObjectValidator.IsSet($value) && (ObjectValidator.IsString($value) || ObjectValidator.IsDigit($value))) {
                return <string | number>super.Value($value);
            }
            return <string | number>super.Value();
        }

        /**
         * @param {string} $text Specify DropDownList item's text value, which should be displayed.
         * @param {string|number} [$value] Specify DropDownList item's value, which should be passed as chosen value.
         * If not specified, it will be equal to the $text value.
         * @return {void}
         */
        public AddItem($text : string, $value? : string | number) : void {
            if (!ObjectValidator.IsEmptyOrNull($text)) {
                this.namesList.Add($text);
                if (!ObjectValidator.IsSet($value)) {
                    $value = $text;
                }
                this.valuesList.Add($value);
            }
        }

        /**
         * @return {ArrayList<string>} Returns DropDownList item names list.
         */
        public getNames() : ArrayList<string> {
            return this.namesList;
        }

        /**
         * @return {ArrayList<string|number>} Returns DropDownList item values list.
         */
        public getValues() : ArrayList<string | number> {
            return this.valuesList;
        }

        /**
         * Clear all items hold by the args instance.
         * @return {void}
         */
        public Clear() : void {
            this.valuesList.Clear();
            this.namesList.Clear();
            this.Value("");
        }

        /**
         * @param {number} [$value] Specify maximal number of items, which can be shown in menu content.
         * @return {number} Returns maximal number of visible items.
         */
        public MaxVisibleItemsCount($value? : number) : number {
            return this.maxVisibleItemsCount = Property.Integer(this.maxVisibleItemsCount, $value);
        }
    }
}
