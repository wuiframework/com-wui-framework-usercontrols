/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Structures {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * CheckBoxFormArgs is structure for handling of FormInput with CheckBox user control.
     */
    export class CheckBoxFormArgs extends BaseFormInputArgs {

        constructor() {
            super();
            this.Value(false);
        }

        /**
         * @param {boolean} [$value] Specify check box value.
         * @return {boolean} Returns check box value.
         */
        public Value($value? : boolean) : boolean {
            if (ObjectValidator.IsSet($value) && ObjectValidator.IsBoolean($value)) {
                super.Value($value);
            }
            return <boolean>super.Value();
        }
    }
}
