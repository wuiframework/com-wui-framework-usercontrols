/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import IBaseGuiObject = Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiObject;

    /**
     * BaseGuiObject should be used as abstract class for extending to the GUI objects
     * and it is providing base GUI object methods.
     */
    export abstract class BaseGuiObject extends Com.Wui.Framework.Gui.Primitives.BaseGuiObject implements IBaseGuiObject {

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return Com.Wui.Framework.UserControls.BaseInterface.Components.ToolTip;
        }
    }
}
