/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import KeyEventHandler = Com.Wui.Framework.Gui.Utils.KeyEventHandler;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import TextSelectionManager = Com.Wui.Framework.Gui.Utils.TextSelectionManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;

    /**
     * BaseField should be used as abstract class for extending to GUI element type of field or select.
     */
    export abstract class BaseField extends FormsObject implements Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject {
        protected static minWidth : number = 20;

        private value : string;
        private width : number;
        private guiType : any;
        private hint : string;

        /**
         * @param {BaseField} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnActive($element : BaseField, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!ObjectValidator.IsSet((<any>$element).ReadOnly) || !(<any>$element).ReadOnly()) {
                ElementManager.TurnActive($element.Id() + "_Status");
            }
        }

        /**
         * @param {BaseField} $element Specify element, which should be handled.
         * @return {void}
         */
        public static Focus($element : BaseField) : void {
            if (!ObjectValidator.IsSet((<any>$element).ReadOnly) || !(<any>$element).ReadOnly()) {
                FormsObject.Focus($element);
            } else {
                TextSelectionManager.SelectAll(<HTMLInputElement>ElementManager.getElement($element.Id() + "_Input"));
            }
        }

        /**
         * @return {void}
         */
        public static Blur() : void {
            const elements : ArrayList<IGuiCommons> = GuiObjectManager.getInstanceSingleton().getActive(
                Reflection.getInstance().getClass(this.ClassName()));
            elements.foreach(($element : BaseField) : void => {
                $element.Error($element.Error());
            });
            FormsObject.Blur();
        }

        protected static onHoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void {
            $eventArgs.StopAllPropagation();
            $manager.setHovered($eventArgs.Owner(), true);
        }

        protected static onKeyEventHandler($eventArgs : KeyEventArgs) : void {
            // do not handle global key press event
        }

        protected static onKeyDownEventHandler($eventArgs : KeyEventArgs, $manager? : GuiObjectManager,
                                               $reflection? : Reflection) : void {
            const nativeEvent : KeyboardEvent = $eventArgs.NativeEventArgs();
            const value : string = (<HTMLInputElement>ElementManager.getElement($eventArgs.Owner().Id() + "_Input")).value;

            if (!KeyEventHandler.IsNavigate(nativeEvent)
                && !((nativeEvent.ctrlKey && nativeEvent.charCode === KeyMap.A) /* Ctrl+A */ ||
                    (nativeEvent.ctrlKey && nativeEvent.charCode === KeyMap.C) /* Ctrl+C */ ||
                    $eventArgs.getKeyCode() === KeyMap.ESC)
                || ($eventArgs.getKeyCode() === KeyMap.BACKSPACE && !ObjectValidator.IsEmptyOrNull(value))) {
                if ($manager.IsActive($eventArgs.Owner())) {
                    const element : any = $eventArgs.Owner();
                    if (!ObjectValidator.IsEmptyOrNull(value)) {
                        ElementManager.Hide(element.Id() + "_HintInput");
                    }
                    if (element.value !== value) {
                        element.value = value;
                        element.setChanged();
                        element.getEventsManager().FireEvent(element, EventType.ON_CHANGE, $eventArgs);
                        element.getEventsManager().FireEvent(element.getClassName(), EventType.ON_CHANGE, $eventArgs);
                    }
                }
            } else {
                BaseField.hintShow($eventArgs.Owner());
            }
        }

        protected static onKeyUpEventHandler($eventArgs : KeyEventArgs, $manager? : GuiObjectManager) : void {
            if (!KeyEventHandler.IsNavigate($eventArgs.NativeEventArgs()) && $eventArgs.getKeyCode() !== KeyMap.ESC
                || $eventArgs.getKeyCode() === KeyMap.BACKSPACE
                || $eventArgs.getKeyCode() === KeyMap.ENTER) {
                if ($manager.IsActive($eventArgs.Owner())) {
                    const element : any = $eventArgs.Owner();
                    let inputElement : HTMLInputElement = <HTMLInputElement>ElementManager.getElement(element.Id() + "_Input");
                    if (element.value !== inputElement.value || $eventArgs.getKeyCode() === KeyMap.ENTER) {
                        if (element.value !== inputElement.value) {
                            element.value = inputElement.value;
                            BaseField.hintShow($eventArgs.Owner());
                            inputElement = <HTMLInputElement>ElementManager.getElement(element.Id() + "_DisabledInput");
                            if (ElementManager.Exists(inputElement)) {
                                inputElement.value = element.value;
                            }
                        }
                        element.getEventsManager().FireEvent(element, EventType.ON_CHANGE, $eventArgs);
                        element.getEventsManager().FireEvent(element.getClassName(), EventType.ON_CHANGE, $eventArgs);
                    }
                }
            }
        }

        protected static onKeyPressEventHandler($eventArgs : KeyEventArgs) : void {
            // place holder for overriding
        }

        protected static resize($element : BaseField) : void {
            // place holder for overriding
        }

        protected static hintShow($element : BaseField) : void {
            const id : string = $element.Id();
            const inputElement : HTMLInputElement = <HTMLInputElement>ElementManager.getElement(id + "_Input");
            if ($element.Enabled() && ElementManager.Exists(inputElement)
                && !ObjectValidator.IsEmptyOrNull($element.Hint()) && ObjectValidator.IsEmptyOrNull(inputElement.value)) {
                if (ElementManager.Exists(id + "_HintInput")) {
                    const hintElement : HTMLInputElement = <HTMLInputElement>ElementManager.getElement(id + "_HintInput");
                    if (ObjectValidator.IsEmptyOrNull(hintElement.value)) {
                        hintElement.value = $element.Hint();
                    }
                    ElementManager.Show(id + "_HintInput");
                }
            } else {
                ElementManager.Hide(id + "_HintInput");
            }
        }

        protected static forceSetValue($element : BaseField, $value : string) : void {
            const setValue : any = ($id : string, $value : string) : void => {
                const inputElement : HTMLInputElement = <HTMLInputElement>ElementManager.getElement($id);
                if (ElementManager.Exists(inputElement)) {
                    inputElement.value = $value;
                }
            };
            setValue($element.Id() + "_Input", $value);
            setValue($element.Id() + "_DisabledInput", $value);
        }

        protected static setErrorStyle($element : BaseField) : void {
            if (!ObjectValidator.IsEmptyOrNull($element)) {
                if (ObjectValidator.IsSet((<any>$element).ReadOnly) && (<any>$element).ReadOnly()) {
                    ElementManager.setClassName($element.Id() + "_Status", GeneralCssNames.READONLY);
                } else {
                    if ($element.Error()) {
                        ElementManager.setClassName($element.Id() + "_Status", GeneralCssNames.ERROR);
                    } else {
                        ElementManager.setClassName($element.Id() + "_Status", "");
                    }
                }
            }
        }

        /**
         * @param {any} [$fieldType] Specify type of element look and feel.
         * @param {string} [$id] Force to set element's id, instead of generated one.
         */
        constructor($fieldType? : any, $id? : string) {
            super($id);
            this.guiType = this.guiTypeValueSetter($fieldType);
        }

        /**
         * @param {any} [$type] Specify type of element look and feel.
         * @return {any} Returns type of element's look and feel.
         */
        public GuiType($type? : any) : any {
            if (ObjectValidator.IsSet($type)) {
                this.guiType = this.guiTypeValueSetter($type);
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                }
            }

            return this.guiType;
        }

        /**
         * @param {string|number} [$value]  Value or arguments object, which should be set to the BaseField.
         * @return {string|number} Returns value.
         */
        public Value($value? : string | number) : string | number {
            let thisClass : any;
            if (!ObjectValidator.IsSet(this.value)) {
                this.value = "";
            }
            if (ObjectValidator.IsSet($value) && this.IsLoaded()) {
                if (this.value !== $value) {
                    this.setChanged();
                    const eventArgs : EventArgs = new EventArgs();
                    eventArgs.Owner(this);
                    this.getEventsManager().FireEvent(this, EventType.ON_CHANGE, eventArgs);
                    this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_CHANGE, eventArgs);
                }

                thisClass = Reflection.getInstance().getClass(this.getClassName());
                thisClass.forceSetValue(this, $value);
            }

            if (!this.IsCompleted() && this.IsPersistent()) {
                this.value = this.valuesPersistence.Variable(this.InstancePath());
            } else if (ObjectValidator.IsSet($value)) {
                this.value = Property.NullString(this.value, <string>$value);
                thisClass = Reflection.getInstance().getClass(this.getClassName());
                thisClass.hintShow(this);
            }

            return this.value;
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            let enabled : boolean = super.Enabled();
            if (ObjectValidator.IsSet($value) && enabled !== $value) {
                enabled = super.Enabled($value);
                if (ElementManager.IsVisible(this.Id())) {
                    const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
                    thisClass.resize(this);
                    thisClass.hintShow(this);
                }
            }
            return enabled;
        }

        /**
         * @param {number} [$value] Specify width value of element.
         * @return {number} Returns element's width value.
         */
        public Width($value? : number) : number {
            if (ObjectValidator.IsSet($value) && this.width !== $value) {
                const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
                this.width = Property.PositiveInteger(this.width, $value, thisClass.minWidth);
                if (this.IsLoaded()) {
                    thisClass.resize(this);
                }
            }
            return this.width;
        }

        /**
         * @param {string} [$value] Specify alternate text or help for filed in case of not selected or empty data.
         * @return {string} Returns element's alternate text or help.
         */
        public Hint($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                this.hint = Property.NullString(this.hint, $value);
            }
            if (!ObjectValidator.IsSet(this.hint)) {
                this.hint = "";
            }
            return this.hint;
        }

        /**
         * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
         * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
         * @return {void}
         */
        public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
            switch ($value.name) {
            case "Value":
                if ($force) {
                    Reflection.getInstance().getClass(this.getClassName()).forceSetValue(this, $value.value);
                } else {
                    this.Value(<string>$value.value);
                }
                break;
            default:
                super.setArg($value, $force);
                break;
            }
        }

        public IsPreventingScroll() : boolean {
            return true;
        }

        protected guiTypeValueSetter($value : any) : any {
            return $value;
        }

        protected statusCss() : string {
            let envelopType : string;
            if (this.Enabled()) {
                if (ObjectValidator.IsSet((<any>this).ReadOnly) && (<any>this).ReadOnly()) {
                    envelopType = GeneralCssNames.READONLY;
                } else {
                    this.Error() ? envelopType = GeneralCssNames.ERROR : envelopType = "";
                }
            } else {
                envelopType = GeneralCssNames.DISABLE;
            }

            return envelopType;
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnClick(
                ($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
                    const element : any = $eventArgs.Owner();
                    if (!ObjectValidator.IsSet(element.ReadOnly) || !element.ReadOnly()) {
                        ElementManager.getElement(element.Id() + "_Input").focus();
                    }
                });

            this.getEvents().setOnMouseOver(
                ($args : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    $reflection.getClass($args.Owner().getClassName()).onHoverEventHandler($args, $manager, $reflection);
                });

            this.getEvents().setOnComplete(
                ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    const element : any = $eventArgs.Owner();
                    const thisClass : any = $reflection.getClass(element.getClassName());
                    element.getSelectorEvents().setEvent(EventType.ON_KEY_DOWN,
                        ($eventArgs : KeyEventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                            $manager.setHovered(element, true);
                            const eventArgs : KeyEventArgs = new KeyEventArgs($eventArgs.NativeEventArgs());
                            eventArgs.Owner($eventArgs.Owner());
                            thisClass.onKeyDownEventHandler(eventArgs, $manager, $reflection);
                        });
                    element.getSelectorEvents().setEvent(EventType.ON_KEY_UP,
                        ($eventArgs : KeyEventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                            const eventArgs : KeyEventArgs = new KeyEventArgs($eventArgs.NativeEventArgs());
                            eventArgs.Owner($eventArgs.Owner());
                            thisClass.onKeyUpEventHandler(eventArgs, $manager, $reflection);
                        });
                    element.getSelectorEvents().setEvent(EventType.ON_KEY_PRESS,
                        ($eventArgs : KeyEventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                            const eventArgs : KeyEventArgs = new KeyEventArgs($eventArgs.NativeEventArgs());
                            eventArgs.Owner($eventArgs.Owner());
                            thisClass.onKeyPressEventHandler(eventArgs, $manager, $reflection);
                        });

                    thisClass.forceSetValue(element, element.Value());
                    thisClass.hintShow(element);
                });

            return super.innerCode();
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();
            this.width = 100;
            this.getEvents().Subscriber(this.Id() + "_Enabled");
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push("width", "value", "hint");
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            if (ObjectValidator.IsEmptyOrNull(this.value)) {
                exclude.push("value");
            }
            if (ObjectValidator.IsEmptyOrNull(this.hint)) {
                exclude.push("hint");
            }
            return exclude;
        }
    }
}
