/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;

    /**
     * BasePanelHolderViewer should be used as abstract class for extending to the panel viewers and
     * it is providing base methods connected with preparation of panel content.
     */
    export class BasePanelHolderViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        protected static getTestViewerArgs() : BasePanelHolderViewerArgs {
            const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            args.HeaderText("Motor parameters");
            args.IsOpened(false);

            return args;
        }

        /**
         * @param {BasePanelHolderViewerArgs} [$args] Specify viewer args with which should be panel instance initiated
         */
        constructor($args? : BasePanelHolderViewerArgs) {
            super($args);

            const instanceClass : any = this.getInstanceClass();
            let instance : BasePanelHolder;

            if (!ObjectValidator.IsEmptyOrNull($args)) {
                instance = new instanceClass($args.BodyViewerClass(), $args.BodyArgs());
            } else {
                instance = new instanceClass();
            }

            if (instance.IsMemberOf(BasePanelHolder)) {
                this.setInstance(instance);
            } else {
                ExceptionsManager.Throw(this.getClassName(),
                    "Instance class provided by the getInstanceClass is not member of '" + BasePanelHolder.ClassName() + "' class.");
            }
        }

        /**
         * @return {BasePanelHolder} Returns Panel instance held by this viewer.
         */
        public getInstance() : BasePanelHolder {
            return <BasePanelHolder>super.getInstance();
        }

        /**
         * @param {BasePanelHolderViewerArgs} [$args] Set panel holder viewer arguments.
         * @return {BasePanelHolderViewerArgs} Returns panel holder viewer arguments.
         */
        public ViewerArgs($args? : BasePanelHolderViewerArgs) : BasePanelHolderViewerArgs {
            return <BasePanelHolderViewerArgs>super.ViewerArgs(<BasePanelHolderViewerArgs>$args);
        }

        protected getInstanceClass() : any {
            return BasePanelHolder;
        }

        protected normalImplementation() : void {
            const instance : BasePanelHolder = this.getInstance();

            if (!ObjectValidator.IsEmptyOrNull(this.ViewerArgs())) {
                const args : BasePanelHolderViewerArgs = this.ViewerArgs();
                instance.headerLabel.Text(args.HeaderText());
                instance.descriptionLabel.Text(args.DescriptionText());
                instance.PrioritySize(args.PrioritySize());
                if (!instance.IsCompleted()) {
                    instance.IsOpened(args.IsOpened());
                }
                if (!ObjectValidator.IsEmptyOrNull(instance.getBody())) {
                    instance.setChildPanelArgs(instance.getBody().Id(), args.BodyArgs());
                }
            }
        }

        protected testImplementation() : string | void {
            const instance : BasePanelHolder = this.getInstance();
            instance.StyleClassName("TestCss");

            this.normalImplementation();

            return "<style>.TestCss {position: relative; top: 100px; left: -10px;}</style>";
        }
    }
}
