/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import IBasePanelHolderViewerArgs = Com.Wui.Framework.Gui.Interfaces.Primitives.IBasePanelHolderViewerArgs;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;

    /**
     * BasePanelHolderViewerArgs is structure handling content of BasePanelHolder.
     */
    export class BasePanelHolderViewerArgs extends BasePanelViewerArgs implements IBasePanelHolderViewerArgs {

        private holderViewerClass : any;
        private bodyViewerClass : any;
        private headerText : string;
        private descriptionText : string;
        private isOpened : boolean;
        private bodyArgs : BasePanelViewerArgs;
        private isExpandable : boolean;
        private prioritySize : PropagableNumber;

        constructor() {
            super();

            this.isOpened = true;
            this.isExpandable = false;
            this.bodyArgs = new BasePanelViewerArgs();
            this.holderViewerClass = BasePanelHolderViewer;
            this.bodyViewerClass = null;
            this.prioritySize = null;
        }

        /**
         * @param {string} [$value] Set value for text of holder's header.
         * @return {string} Returns value of holder's header text.
         */
        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }

        /**
         * @param {string} [$value] Set value for text of holder's description text/tooltip.
         * @return {string} Returns value of holder's description text/tooltip.
         */
        public DescriptionText($value? : string) : string {
            return this.descriptionText = Property.String(this.descriptionText, $value);
        }

        /**
         * @param {boolean} [$value] Set if panel holder should be in opened mode.
         * @return {boolean} Returns true, if panel holder should be in opened mode, otherwise false.
         */
        public IsOpened($value? : boolean) : boolean {
            return this.isOpened = Property.Boolean(this.isOpened, $value);
        }

        /**
         * @param {PropagableNumber} [$value] Specify size of holder that should be normalized to parent and prioritized.
         * @return {PropagableNumber} Return size of holder that should be normalized to parent and prioritized.
         */
        public PrioritySize($value? : PropagableNumber) : PropagableNumber {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.prioritySize = $value;
            }
            return this.prioritySize;
        }

        /**
         * @param {BasePanelViewerArgs} [$value] Set arguments for panel held by the panel holder.
         * @return {BasePanelViewerArgs} Returns arguments for panel held by the panel holder.
         */
        public BodyArgs($value? : BasePanelViewerArgs) : BasePanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsMemberOf(BasePanelViewerArgs)) {
                this.bodyArgs = $value;
            }
            return this.bodyArgs;
        }

        /**
         * @param {BasePanelHolderViewer} [$value] Specify viewer class, which should be used for creation of holder instance.
         * @return {BasePanelHolderViewer} Returns class with type of BasePanelHolderViewer.
         */
        public HolderViewerClass($value? : any) : any {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.holderViewerClass = $value;
            }
            return this.holderViewerClass;
        }

        /**
         * @param {BasePanelViewer} [$value] Specify viewer class, which should be used for creation of held body instance.
         * @return {BasePanelViewer} Returns class with type of BasePanelViewer.
         */
        public BodyViewerClass($value? : any) : any {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.bodyViewerClass = $value;
            }
            return this.bodyViewerClass;
        }
    }
}
