/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import IAbstractGuiObject = Com.Wui.Framework.Gui.Interfaces.Primitives.IAbstractGuiObject;
    import ToolTip = Com.Wui.Framework.UserControls.BaseInterface.Components.ToolTip;
    import Notification = Com.Wui.Framework.UserControls.BaseInterface.Components.Notification;

    /**
     * AbstractGuiObject class provides GUI object, which can be used as temporary element needed
     */
    export class AbstractGuiObject extends Com.Wui.Framework.Gui.Primitives.AbstractGuiObject implements IAbstractGuiObject {

        /**
         * @return {ToolTip} Returns tooltip component associated with current GUI element.
         */
        public Title() : ToolTip {
            return <ToolTip>super.Title();
        }

        /**
         * @return {Notification} Returns notification component associated with current GUI element.
         */
        public Notification() : Notification {
            return <Notification>super.Notification();
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        /**
         * @return {INotification} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.INotification
         */
        protected getNotificationClass() : any {
            return Notification;
        }
    }
}
