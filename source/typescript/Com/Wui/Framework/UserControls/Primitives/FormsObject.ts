/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import Parent = Com.Wui.Framework.Gui.Primitives.FormsObject;
    import IFormsObject = Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ToolTip = Com.Wui.Framework.UserControls.BaseInterface.Components.ToolTip;
    import Notification = Com.Wui.Framework.UserControls.BaseInterface.Components.Notification;
    import ScrollBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ScrollBar;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import DragBar = Com.Wui.Framework.UserControls.BaseInterface.Components.DragBar;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    /**
     * FormsObject should be used as abstract class for extending to the GUI objects type of input
     * and it is providing base methods connected with form elements.
     */
    export abstract class FormsObject extends Com.Wui.Framework.Gui.Primitives.FormsObject implements IFormsObject {

        /**
         * @param {FormsObject} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnOn($element : FormsObject, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!$manager.IsActive(<ClassName>ScrollBar)
                && !$manager.IsActive(<ClassName>ResizeBar)
                && !$manager.IsActive(<ClassName>DragBar)) {
                Parent.TurnOn($element, $manager, $reflection);
            }
        }

        /**
         * @return {void}
         */
        public static Blur() : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            const elements : ArrayList<IGuiCommons> = manager.getActive();
            const reflection : Reflection = Reflection.getInstance();
            elements.foreach(($element : FormsObject) : void => {
                if (ObjectValidator.IsEmptyOrNull($element.Parent()) ||
                    (!ObjectValidator.IsEmptyOrNull($element.Parent())
                        && !reflection.IsMemberOf($element, ScrollBar)
                        && !reflection.IsMemberOf($element, ResizeBar)
                        && !reflection.IsMemberOf($element, DragBar))) {
                    manager.setActive($element, false);

                    const eventArgs : EventArgs = new EventArgs();
                    eventArgs.Owner($element);
                    $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_BLUR, eventArgs);
                    $element.getEventsManager().FireEvent($element, EventType.ON_BLUR, eventArgs);
                    const elementClass : any = reflection.getClass($element.getClassName());
                    if (ObjectValidator.IsSet(elementClass.TurnOff)) {
                        elementClass.TurnOff($element, manager, reflection);
                    }

                    if (reflection.IsMemberOf($element, FormsObject)) {
                        $element.Error($element.Error());
                    }
                }
            });
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        /**
         * @return {INotification} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.INotification
         */
        protected getNotificationClass() : any {
            return Notification;
        }
    }
}
