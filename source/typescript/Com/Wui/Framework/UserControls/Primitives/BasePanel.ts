/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import IBasePanel = Com.Wui.Framework.Gui.Interfaces.Primitives.IBasePanel;
    import ToolTip = Com.Wui.Framework.UserControls.BaseInterface.Components.ToolTip;
    import Notification = Com.Wui.Framework.UserControls.BaseInterface.Components.Notification;
    import ScrollBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ScrollBar;
    import OrientationType = Com.Wui.Framework.Gui.Enums.OrientationType;
    import Label = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Label;
    import Dialog = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Dialog;
    import IconType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.IconType;
    import Icon = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Icon;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ScrollEventArgs = Com.Wui.Framework.Gui.Events.Args.ScrollEventArgs;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeEventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;

    /**
     * BasePanel should be used as abstract class for extending to the GUI panels and
     * it is providing base methods connected with panel implementation and behaviour.
     */
    export class BasePanel extends Com.Wui.Framework.Gui.Primitives.BasePanel implements IBasePanel {

        /**
         * Provide panel resize as reaction on parent element resizing.
         * @param {ResizeEventArgs} $eventArgs Resize event args, which should be processed.
         * @param {GuiObjectManager} [$manager] Provide gui object manager instance.
         * @param {Reflection} [$reflection] Provide reflection instance.
         * @return {void}
         */
        public static ResizeEventHandler($eventArgs : ResizeEventArgs, $manager : GuiObjectManager,
                                         $reflection : Reflection) : void {
            const element : BasePanel = <BasePanel>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, Dialog)) {
                if (!ObjectValidator.IsEmptyOrNull((<any>element).PanelViewer())) {
                    const child : BasePanel = (<any>element).PanelViewer().getInstance();
                    if (!ObjectValidator.IsEmptyOrNull(child) && child.IsLoaded()) {
                        child.Width($eventArgs.AvailableWidth());
                        child.Height($eventArgs.AvailableHeight());
                    }
                }
            } else if ($reflection.IsMemberOf(element, BasePanel)) {
                element.Width($eventArgs.AvailableWidth());
                element.Height($eventArgs.AvailableHeight());
            }
        }

        protected static onUnhoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                                               $reflection? : Reflection) : void {
            $manager.setHovered($eventArgs.Owner(), false);
            const parent : BasePanel = <BasePanel>$eventArgs.Owner().Parent();
            if (!ObjectValidator.IsEmptyOrNull(parent)) {
                const hoverParent : any = ($parent : BasePanel) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($parent)) {
                        if ($reflection.IsMemberOf($parent, Dialog)) {
                            if ($parent.Visible()) {
                                $manager.setActive($parent, true);
                            }
                        } else {
                            if ($reflection.IsMemberOf($parent, BasePanel) && $parent.Scrollable() && (
                                (ObjectValidator.IsSet($parent.verticalScrollBar) &&
                                    $parent.verticalScrollBar.Visible() && ElementManager.IsVisible($parent.verticalScrollBar.Id())) ||
                                (ObjectValidator.IsSet($parent.horizontalScrollBar) &&
                                    $parent.horizontalScrollBar.Visible() && ElementManager.IsVisible($parent.horizontalScrollBar.Id()))
                            )) {
                                $manager.setHovered($parent, true);
                            } else {
                                hoverParent(<BasePanel>$parent.Parent());
                            }
                        }
                    }
                };
                hoverParent(parent);
            }
        }

        private static onScrollBarMoveEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                                   $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>ScrollBar)) {
                let element : any = <ScrollBar>$eventArgs.Owner();
                if (Reflection.getInstance().IsMemberOf(element, ScrollBar) &&
                    ElementManager.Exists(element.Parent().Id())) {
                    element = <BasePanel>element.Parent();
                    if ($reflection.IsMemberOf(element, BasePanel)) {
                        if ($eventArgs.OrientationType() === OrientationType.VERTICAL) {
                            BasePanel.scrollTop(element, $eventArgs.Position());
                        } else if ($eventArgs.OrientationType() === OrientationType.HORIZONTAL) {
                            BasePanel.scrollLeft(element, $eventArgs.Position());
                        }
                    }
                }
            }
        }

        private static scrollEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                          $reflection : Reflection) : void {
            const parent : BasePanel = <BasePanel>$eventArgs.Owner();
            if ($reflection.IsMemberOf(parent, BasePanel)) {
                let child : ScrollBar;
                if ($eventArgs.OrientationType() === OrientationType.VERTICAL) {
                    child = <ScrollBar>parent.verticalScrollBar;
                } else if ($eventArgs.OrientationType() === OrientationType.HORIZONTAL) {
                    child = <ScrollBar>parent.horizontalScrollBar;
                }
                if (ObjectValidator.IsSet(child) && !$manager.IsActive(child) && ElementManager.IsVisible(child.Id())) {
                    ScrollBar.MoveTo(child, $eventArgs.Position());
                }
            }
        }

        /**
         * @param {number} [$value] Specify panel's width.
         * @return {number} Returns panel's width.
         */
        public Width($value? : number) : number {
            if (ObjectValidator.IsSet($value) && ObjectValidator.IsSet(this.horizontalScrollBar)) {
                this.horizontalScrollBar.Size(Math.ceil($value * 0.95));
            }
            return super.Width($value);
        }

        /**
         * @param {number} [$value] Specify panel's height.
         * @return {number} Returns panel's height.
         */
        public Height($value? : number) : number {
            if (ObjectValidator.IsSet($value) && ObjectValidator.IsSet(this.verticalScrollBar)) {
                this.verticalScrollBar.Size(Math.ceil($value * 0.95));
            }
            return super.Height($value);
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        /**
         * @return {INotification} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.INotification
         */
        protected getNotificationClass() : any {
            return Notification;
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon
         */
        protected getLoaderIconClass() : any {
            return Icon;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel
         */
        protected getLoaderTextClass() : any {
            return Label;
        }

        /**
         * @return {IScrollBar} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IScrollBar
         */
        protected getScrollBarClass() : any {
            return ScrollBar;
        }

        protected cssInterfaceName() : string {
            return BasePanel.ClassName();
        }

        protected innerCode() : IGuiElement {
            if (ObjectValidator.IsSet(this.loaderIcon)) {
                this.loaderIcon.IconType(IconType.SPINNER_BIG);
            }

            this.verticalScrollBar.getEvents().setOnButton(BasePanel.onScrollBarMoveEventHandler);
            this.verticalScrollBar.getEvents().setOnChange(BasePanel.onScrollBarMoveEventHandler);
            this.horizontalScrollBar.getEvents().setOnButton(BasePanel.onScrollBarMoveEventHandler);
            this.horizontalScrollBar.getEvents().setOnChange(BasePanel.onScrollBarMoveEventHandler);

            this.getEvents().setOnResize(
                ($eventArgs : ResizeEventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        const parent : BasePanel = $eventArgs.Owner();
                        if (parent.Scrollable()) {
                            ScrollBar.ResizeEventHandler($eventArgs, $manager, $reflection);
                        }
                    }, false);
                });
            this.getEvents().setOnScroll(BasePanel.scrollEventHandler);

            return super.innerCode();
        }
    }
}
