/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IllegalArgumentException = Com.Wui.Framework.Commons.Exceptions.Type.IllegalArgumentException;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IDirectoryBrowser = Com.Wui.Framework.Gui.Interfaces.UserControls.IDirectoryBrowser;
    import FileSystemItemType = Com.Wui.Framework.Commons.Enums.FileSystemItemType;

    /**
     * DirectoryItem should be used as container for DirectoryBrowser item content.
     */
    export class DirectoryItem extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private readonly index : number;
        private readonly owner : IDirectoryBrowser;
        private text : string;
        private value : string;
        private selectedFlag : boolean;
        private readonly events : ElementEventsManager;
        private readonly nested : number;
        private isHidden : boolean;
        private parent : DirectoryItem;
        private readonly childList : ArrayList<DirectoryItem>;
        private isOpen : boolean;
        private type : FileSystemItemType;
        private isComplete : boolean;

        /**
         * Change item status to OFF
         * @param {DirectoryItem} $element Specify element, which should be handled
         * @return {void}
         */
        public static TurnOff($element : DirectoryItem) : void {
            if (!ObjectValidator.IsEmptyOrNull($element) && $element.isComplete) {
                ElementManager.TurnOff($element.Id() + "_Status");
            }
        }

        /**
         * Change item status to ON
         * @param {DirectoryItem} $element Specify element, which should be handled
         * @return {void}
         */
        public static TurnOn($element : DirectoryItem) : void {
            if (!ObjectValidator.IsEmptyOrNull($element) && $element.isComplete) {
                ElementManager.TurnOn($element.Id() + "_Status");
            }
        }

        /**
         * Change item status to ACTIVE
         * @param {DirectoryItem} $element Specify element, which should be handled
         * @return {void}
         */
        public static TurnActive($element : DirectoryItem) : void {
            if (!ObjectValidator.IsEmptyOrNull($element) && $element.isComplete) {
                ElementManager.TurnActive($element.Id() + "_Status");
            }
        }

        /**
         * @param {string} $text Specify visible text value of the item.
         * @param {IDirectoryBrowser} $owner Set item's owner.
         * @param {number} $index Set index of the item in the DirectoryBrowser list of items.
         * @param {number} $nested Specify level of item nesting.
         */
        constructor($text : string, $owner : IDirectoryBrowser, $index : number, $nested : number) {
            super();
            this.Text($text);
            this.Value(null);
            this.index = $index;
            this.owner = $owner;
            this.selectedFlag = false;
            this.events = new ElementEventsManager($owner, this.Id() + "_Status");
            this.nested = $nested;
            this.isHidden = false;
            this.parent = null;
            this.childList = new ArrayList<DirectoryItem>();
            this.isOpen = false;
            this.type = "";
            this.isComplete = false;
        }

        /**
         * @return {ElementEventsManager} Returns events manager subscribed to the item.
         */
        public getEvents() : ElementEventsManager {
            return this.events;
        }

        /**
         * @return {IDirectoryBrowser} Returns item's owner element.
         */
        public getOwner() : IDirectoryBrowser {
            return this.owner;
        }

        /**
         * @param {DirectoryItem} [$value] Specify item's parent object.
         * @return {DirectoryItem} Returns item's parent object.
         */
        public Parent($value? : DirectoryItem) : DirectoryItem {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.parent = $value;
            }
            return this.parent;
        }

        /**
         * @return {string} Returns item's GUI id.
         */
        public Id() : string {
            return this.owner.Id() + "_Item_" + this.index;
        }

        /**
         * @return {number} Returns index of the item in the DirectoryBrowser list of items.
         */
        public getIndex() : number {
            return this.index;
        }

        /**
         * @param {string} [$value] Set text value of the item.
         * @return {string} Returns text value of the item.
         */
        public Text($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                if (ObjectValidator.IsEmptyOrNull($value)) {
                    ExceptionsManager.Throw(this.getClassName(), new IllegalArgumentException("Text attribute can not be null."));
                }
                this.text = Property.String(this.text, $value);
            }

            return this.text;
        }

        /**
         * @param {string|number} [$value] Set value connected with the visible text value.
         * @return {string} Returns item's value connected with the visible text value.
         */
        public Value($value? : string | number) : string {
            if (ObjectValidator.IsSet($value)) {
                this.value = Property.String(this.value, ObjectValidator.IsEmptyOrNull($value) ? this.text : $value.toString());
            }

            return this.value;
        }

        /**
         * @param {boolean} [$value] Specify selection flag.
         * @return {boolean} Returns true, if item has been selected, otherwise false.
         */
        public IsSelected($value? : boolean) : boolean {
            this.selectedFlag = Property.Boolean(this.selectedFlag, $value);
            if (ObjectValidator.IsSet($value)) {
                if (this.selectedFlag && this.isComplete) {
                    DirectoryItem.TurnActive(this);
                } else {
                    DirectoryItem.TurnOff(this);
                }
            }

            return this.selectedFlag;
        }

        /**
         * @param {boolean} [$value] Specify hidden flag.
         * @return {boolean} Returns true, if item has hidden flag, otherwise false.
         */
        public IsHidden($value? : boolean) : boolean {
            return this.isHidden = Property.Boolean(this.isHidden, $value);
        }

        /**
         * @param {FileSystemItemType} [$value] Specify item type.
         * @return {FileSystemItemType} Returns item's type.
         */
        public Type($value? : FileSystemItemType) : FileSystemItemType {
            if (ObjectValidator.IsSet($value) && FileSystemItemType.Contains($value)) {
                this.type = $value;
            }
            return this.type;
        }

        /**
         * @param {DirectoryItem} $value Specify item's child.
         * @return {void}
         */
        public AddChild($value : DirectoryItem) : void {
            if (!this.childList.KeyExists($value.getHash())) {
                this.childList.Add($value, $value.getHash());
            }
        }

        /**
         * @return {ArrayList<DirectoryItem>} Returns list of item's children.
         */
        public getChildren() : ArrayList<DirectoryItem> {
            return this.childList;
        }

        /**
         * @param {boolean} [$value] Specify open flag.
         * @return {boolean} Returns true, if item has open flag, otherwise false.
         */
        public IsOpen($value? : boolean) : boolean {
            this.isOpen = Property.Boolean(this.isOpen, $value);
            if (ObjectValidator.IsSet($value) && this.isComplete) {
                if (this.isOpen) {
                    ElementManager.Show(this.Id() + "_Children");
                } else {
                    ElementManager.Hide(this.Id() + "_Children");
                }
            }
            return this.isOpen;
        }

        /**
         * @return {number} Returns current item's text left offset in pixels including nested width.
         */
        public getTextOffset() : number {
            let output : number =
                ElementManager.getElement(this.Id() + "_Icon").offsetWidth +
                ElementManager.getCssIntegerValue(this.Id() + "_Icon", "margin-right") +
                ElementManager.getCssIntegerValue(this.Id() + "_Icon", "margin-left") +
                ElementManager.getCssIntegerValue(this.Id() + "_Icon", "padding-right") +
                ElementManager.getCssIntegerValue(this.Id() + "_Icon", "padding-left") +
                ElementManager.getCssIntegerValue(this.Id() + "_Text", "padding-right");
            if (this.nested > 0) {
                output += (ElementManager.getElement(this.Id() + "_Nested").offsetWidth * this.nested);
            }
            return output;
        }

        /**
         * @return {number} Returns current item's width.
         */
        public getWidth() : number {
            if (this.isComplete) {
                ElementManager.setWidth(this.Id(), 50000);
                let widthMax : number =
                    this.getTextOffset() +
                    ElementManager.getElement(this.Id() + "_Text").offsetWidth;
                ElementManager.ClearCssProperty(this.Id(), "width");
                if (this.IsOpen()) {
                    this.getChildren().foreach(($child : DirectoryItem) : void => {
                        if ($child.Text() !== "***") {
                            const childWidth : number = $child.getWidth() + 1;
                            if (widthMax < childWidth) {
                                widthMax = childWidth;
                            }
                        }
                    });
                }
                return widthMax;
            }
            return 0;
        }

        /**
         * @return {string} Returns HTML representing DirectoryBrowser item.
         */
        public getInnerHtml() : IGuiElement {
            const id : string = this.Id();
            const guiElementClass : any = this.owner.getGuiElementClass();
            const addElement : ($id? : string) => IGuiElement = ($id? : string) : IGuiElement => {
                return new guiElementClass().Id($id);
            };
            const isPlaceHolder : boolean = this.childList.Length() === 1 && this.childList.getFirst().Text() === "***";

            let status : string = GeneralCssNames.OFF;
            if (this.selectedFlag) {
                status = GeneralCssNames.ACTIVE;
            }
            let attributes : string = "";
            if (this.isHidden) {
                if (this.childList.IsEmpty() && !isPlaceHolder) {
                    attributes = "Last " + FileSystemItemType.HIDDEN;
                } else {
                    attributes = FileSystemItemType.HIDDEN;
                }
            } else if (this.childList.IsEmpty() && !isPlaceHolder) {
                attributes = "Last";
            }

            const childrenOutput : IGuiElement = addElement(id + "_Children").Visible(this.isOpen);
            this.childList.foreach(($child : DirectoryItem) : void => {
                if ($child.Text() !== "***") {
                    childrenOutput.Add($child.getInnerHtml());
                }
            });
            const nestedPrefix : IGuiElement = addElement();
            let index : number;
            for (index = 0; index < this.nested; index++) {
                nestedPrefix.Add(addElement(id + "_Nested").StyleClassName("Nested"));
            }

            return addElement(id).StyleClassName("Item")
                .Add(addElement().StyleClassName(this.type)
                    .Add(addElement().StyleClassName(attributes)
                        .Add(addElement(id + "_Status").StyleClassName(status)
                            .Add(nestedPrefix)
                            .Add(addElement(id + "_Icon").StyleClassName(GeneralCssNames.ICON))
                            .Add(addElement(id + "_Text").StyleClassName(GeneralCssNames.TEXT).Add(this.text))
                        )
                    )
                )
                .Add(childrenOutput);
        }

        /**
         * @param {HTMLElement} $target Specify object, which should be appended by item's content.
         * @param {string} $EOL Specify end of line format for generated html output.
         * @return {void}
         */
        public AppendTo($target : HTMLElement, $EOL : string) : void {
            $target.appendChild(this.getInnerHtml().ToDOMElement($EOL));

            const subscribe : any = ($item : DirectoryItem) : void => {
                const id : string = $item.Id();
                ElementManager.CleanElementCache(id + "*");

                $item.getEvents().setOnMouseOver(() : void => {
                    if (!$item.IsSelected()) {
                        DirectoryItem.TurnOn($item);
                    }
                });
                $item.getEvents().setOnMouseDown(() : void => {
                    DirectoryItem.TurnActive($item);
                });
                $item.getEvents().setOnMouseOut(() : void => {
                    if (!$item.IsSelected()) {
                        DirectoryItem.TurnOff($item);
                    }
                });
                $item.getEvents().Subscribe();
                $item.getChildren().foreach(($child : DirectoryItem) : void => {
                    subscribe($child);
                });
                $item.isComplete = true;
            };
            subscribe(this);
        }

        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            return $prefix + "[" + this.getHash() + "] " + this.text + StringUtils.NewLine($htmlTag) +
                $prefix + StringUtils.Tab(1, $htmlTag) + "index: " + this.index + StringUtils.NewLine($htmlTag) +
                $prefix + StringUtils.Tab(1, $htmlTag) + "value: " + this.value + StringUtils.NewLine($htmlTag);
        }

        public getHash() : number {
            return StringUtils.getCrc(this.Id() + this.value);
        }
    }
}
