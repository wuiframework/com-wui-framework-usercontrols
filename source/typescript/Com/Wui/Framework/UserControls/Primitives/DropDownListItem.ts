/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IllegalArgumentException = Com.Wui.Framework.Commons.Exceptions.Type.IllegalArgumentException;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import IDropDownList = Com.Wui.Framework.Gui.Interfaces.UserControls.IDropDownList;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;

    /**
     * DropDownListItem should be used as container for DropDownList item content.
     */
    export class DropDownListItem extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private index : number;
        private id : string;
        private owner : IDropDownList;
        private text : string;
        private value : string;
        private styleClassName : string;
        private selectedFlag : boolean;
        private showSeparator : boolean;
        private readonly events : ElementEventsManager;

        /**
         * @param {string} $text Specify visible text value of the item.
         * @param {IDropDownList} $owner Set item's owner.
         * @param {number} $index Set index of the item in the DropDownList options array.
         */
        constructor($text : string, $owner : IDropDownList, $index : number) {
            super();
            this.Owner($owner.Id());
            this.Index($index);
            this.Text($text);
            this.Value(null);
            this.owner = $owner;
            this.styleClassName = "";
            this.selectedFlag = false;
            this.showSeparator = true;
            ElementManager.CleanElementCache(this.id);
            ElementManager.CleanElementCache(this.id + "_Item_" + this.index);
            ElementManager.CleanElementCache(this.id + "_Item_" + this.index + "_Envelop");
            ElementManager.CleanElementCache(this.id + "_Item_" + this.index + "_Status");
            ElementManager.CleanElementCache(this.id + "_Item_" + this.index + "_Icon");
            ElementManager.CleanElementCache(this.id + "_Item_" + this.index + "_Text");
            ElementManager.CleanElementCache(this.id + "_Item_" + this.index + "Separator");
            this.events = new ElementEventsManager($owner, this.id + "_Item_" + this.index + "_Status");
        }

        /**
         * @return {ElementEventsManager} Returns events manager subscribed to the item.
         */
        public getEvents() : ElementEventsManager {
            return this.events;
        }

        /**
         * @param {string} [$value] Set item's owner id.
         * @return {string} Returns item's owner id.
         */
        public Owner($value? : string) : string {
            return this.id = Property.String(this.id, $value);
        }

        /**
         * @param {number} [$value] Set index of the item in the DropDownList options array.
         * @return {number} Returns item index in the DropDownList options array.
         */
        public Index($value? : number) : number {
            return this.index = Property.Integer(this.index, $value);
        }

        /**
         * @param {string} [$value] Set text value of the item.
         * @return {string} Returns text value of the item.
         */
        public Text($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                if (ObjectValidator.IsEmptyOrNull($value)) {
                    ExceptionsManager.Throw(this.getClassName(), new IllegalArgumentException("Text attribute can not be null."));
                }
                this.text = Property.String(this.text, $value);
            }

            return this.text;
        }

        /**
         * @param {string|number} [$value] Set value connected with the visible text value.
         * @return {string} Returns item's value connected with the visible text value.
         */
        public Value($value? : string | number) : string {
            if (ObjectValidator.IsSet($value)) {
                this.value = Property.String(this.value, ObjectValidator.IsEmptyOrNull($value) ? this.text : $value.toString());
            }

            return this.value;
        }

        /**
         * @param {string} [$value] Set type of css class name connected with the object instance.
         * @return {string} Returns css class name connected with the object instance.
         */
        public StyleClassName($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                this.styleClassName = $value;
            }

            return this.styleClassName;
        }

        /**
         * @param {boolean} [$value] Specify selection flag.
         * @return {boolean} Returns true, if element has been selected, otherwise false.
         */
        public IsSelected($value? : boolean) : boolean {
            if (ObjectValidator.IsSet($value)) {
                this.selectedFlag = Property.Boolean(this.selectedFlag, $value);
            }

            return this.selectedFlag;
        }

        /**
         * @param {boolean} [$value] Specify, if rendered item should contains separator element.
         * @return {boolean} Returns true, if element's separator is visible, otherwise false.
         */
        public SeparatorVisible($value? : boolean) : boolean {
            this.showSeparator = Property.Boolean(this.showSeparator, $value);
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.id)) {
                if (this.showSeparator) {
                    ElementManager.Show(this.id + "_Item_" + this.index + "Separator");
                } else {
                    ElementManager.Hide(this.id + "_Item_" + this.index + "Separator");
                }
            }
            return this.showSeparator;
        }

        /**
         * @return {IGuiElement} Returns GuiElement representing DropDownList item.
         */
        public getInnerHtml() : IGuiElement {
            const guiElementClass : any = this.owner.getGuiElementClass();
            const addElement : ($id? : string) => IGuiElement = ($id? : string) : IGuiElement => {
                return new guiElementClass().Id($id);
            };
            const itemIndex : string = this.Index().toString();
            const output : IGuiElement = addElement(this.id + "_Item_" + itemIndex).StyleClassName(this.styleClassName);
            let cssStyle : string = GeneralCssNames.OFF;
            if (this.selectedFlag) {
                cssStyle = GeneralCssNames.ACTIVE;
            }
            output
                .Add(addElement(this.id + "_Item_" + itemIndex + "_Envelop")
                    .StyleClassName("Item")
                    .Add(addElement(this.id + "_Item_" + itemIndex + "_Status")
                        .StyleClassName(cssStyle)
                        .Add(addElement(this.id + "_Item_" + itemIndex + "_Icon").StyleClassName(GeneralCssNames.ICON))
                        .Add(addElement(this.id + "_Item_" + itemIndex + "_Text").StyleClassName(GeneralCssNames.TEXT).Add(this.text))
                    )
                )
                .Add(addElement(this.id + "_Item_" + itemIndex + "_Separator").StyleClassName("Separator").Visible(this.showSeparator));
            return output;
        }

        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            return $prefix + "[" + this.getHash() + "] " + this.text + StringUtils.NewLine($htmlTag) +
                $prefix + StringUtils.Tab(1, $htmlTag) + "index: " + this.index + StringUtils.NewLine($htmlTag) +
                $prefix + StringUtils.Tab(1, $htmlTag) + "value: " + this.value + StringUtils.NewLine($htmlTag) +
                $prefix + StringUtils.Tab(1, $htmlTag) + "style: " + this.styleClassName + StringUtils.NewLine($htmlTag) +
                $prefix + StringUtils.Tab(1, $htmlTag) + "withSeparator: " + Convert.BooleanToString(this.showSeparator) +
                StringUtils.NewLine($htmlTag) +
                $prefix + StringUtils.Tab(1, $htmlTag) + "isSelected: " + Convert.BooleanToString(this.selectedFlag);
        }

        /**
         * @return {number} Returns CRC calculated from data, which represents current object.
         */
        public getHash() : number {
            return StringUtils.getCrc(
                this.index + this.id +
                this.text + this.value +
                this.styleClassName + this.selectedFlag + this.showSeparator);
        }
    }
}
