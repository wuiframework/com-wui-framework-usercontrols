/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import ILabel = Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel;
    import IImageButton = Com.Wui.Framework.Gui.Interfaces.UserControls.IImageButton;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IBasePanelHolderEvents = Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelHolderEvents;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import ValueProgressEventArgs = Com.Wui.Framework.Gui.Events.Args.ValueProgressEventArgs;
    import ValueProgressManager = Com.Wui.Framework.Gui.Utils.ValueProgressManager;
    import BasePanelHolderEventType = Com.Wui.Framework.Gui.Enums.Events.BasePanelHolderEventType;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;

    /**
     * BasePanelHolder should be used as abstract class for extending to the GUI panels holder
     * suitable for external handling eg. by Accordion user control.
     */
    export abstract class BasePanelHolder extends BasePanel implements Com.Wui.Framework.Gui.Interfaces.Primitives.IBasePanelHolder {

        public headerLabel : ILabel;
        public descriptionLabel : ILabel;
        public arrowButton : IImageButton;
        protected minWidth : number;
        protected minHeight : number;
        private guiType : any;
        private isOpened : boolean;
        private body : BasePanel;
        private headerEvents : ElementEventsManager;
        private prioritySize : PropagableNumber;
        private openedSize : number;
        private targetSize : number;
        private currentSize : number;
        private currentBodySize : number;
        private isResizeDelegated : boolean;
        private queue : any;
        private animIterationNum : number;

        protected static resizeHolder($element : BasePanelHolder, size? : number) : void {
            // placeholder for overriding
        }

        protected static open($element : BasePanelHolder, $animate : boolean = true) : void {
            if (!$element.getGuiManager().IsActive($element) || !$animate) {
                $element.getGuiManager().setActive($element, true);
                const thisClass : any = Reflection.getInstance().getClass($element.getClassName());
                const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get($element.Id());
                manipulatorArgs.Owner($element);

                $element.getBody().Visible(true);

                thisClass.beforeOpenHolderHandler($element);
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner($element);
                $element.getEvents().FireAsynchronousMethod(() : void => {
                    $element.getEventsManager().FireEvent($element, BasePanelHolderEventType.BEFORE_OPEN, eventArgs, false);
                    manipulatorArgs.RangeEnd($element.TargetSize() - $element.getHeaderSize());
                    manipulatorArgs.RangeStart(0);
                    manipulatorArgs.CurrentValue(manipulatorArgs.RangeStart());
                    manipulatorArgs.ProgressType(ProgressType.FAST_END);
                    manipulatorArgs.DirectionType(DirectionType.UP);
                    manipulatorArgs.Step($animate ? $element.animIterationNum : 0);

                    manipulatorArgs.ChangeEventType($element.Id() + "_ShowBody_" + EventType.ON_CHANGE);
                    manipulatorArgs.CompleteEventType($element.Id() + "_ShowBody_" + EventType.ON_COMPLETE);
                    ElementManager.setCssProperty($element.getBodyId(), "position", "relative");

                    $element.getEventsManager().setEvent($element, $element.Id() + "_ShowBody_" + EventType.ON_CHANGE,
                        ($eventArgs : ValueProgressEventArgs, $manager : GuiObjectManager) : void => {
                            thisClass.onChangeBodyHandler($eventArgs.Owner(), $eventArgs);
                        });

                    $element.getEventsManager().setEvent($element, $element.Id() + "_ShowBody_" + EventType.ON_COMPLETE,
                        ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                            const element : BasePanelHolder = <BasePanelHolder>$eventArgs.Owner();
                            thisClass.onShowCompleteBodyHandler(element);
                            (<any>element).isOpened = true;

                            $manager.setActive(element, false);
                            $element.getEventsManager().FireEvent(element, BasePanelHolderEventType.ON_OPEN);

                            if (!$element.IsResizeDelegated() && !ObjectValidator.IsEmptyOrNull($element.queue)) {
                                $element.queue.handler();
                            }
                        });
                    ValueProgressManager.Execute(manipulatorArgs);

                }, false, 30);
            }
        }

        protected static hide($element : BasePanelHolder, $animate : boolean = true) : void {
            if (!$element.getGuiManager().IsActive($element)) {
                $element.getGuiManager().setActive($element, true);
                const thisClass : any = Reflection.getInstance().getClass($element.getClassName());
                const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get($element.Id());
                manipulatorArgs.Owner($element);

                thisClass.beforeCloseHolderHandler($element, manipulatorArgs);
                $element.getEvents().FireAsynchronousMethod(() : void => {
                    if (!$animate) {
                        manipulatorArgs.Step(0);
                    }

                    manipulatorArgs.ChangeEventType($element.Id() + "_HideBody_" + EventType.ON_CHANGE);
                    manipulatorArgs.CompleteEventType($element.Id() + "_HideBody_" + EventType.ON_COMPLETE);

                    ElementManager.setCssProperty($element.getBodyId(), "position", "relative");

                    $element.getEventsManager().FireEvent($element, BasePanelHolderEventType.BEFORE_CLOSE, false);
                    (<any>$element).isOpened = false;

                    $element.getEventsManager().setEvent($element, $element.Id() + "_HideBody_" + EventType.ON_CHANGE,
                        ($eventArgs : ValueProgressEventArgs, $manager : GuiObjectManager) : void => {
                            thisClass.onChangeBodyHandler($eventArgs.Owner(), $eventArgs);
                        });

                    $element.getEventsManager().setEvent($element, $element.Id() + "_HideBody_" + EventType.ON_COMPLETE,
                        ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                            const element : BasePanelHolder = <BasePanelHolder>$eventArgs.Owner();
                            thisClass.onHideCompleteBodyHandler(element);
                            element.getBody().Visible(false);

                            $manager.setActive(element, false);
                            $element.getEventsManager().FireEvent(element, BasePanelHolderEventType.ON_CLOSE);

                            if (!$element.IsResizeDelegated() && !ObjectValidator.IsEmptyOrNull($element.queue)) {
                                $element.queue.handler();
                            }
                        }
                    );
                    ValueProgressManager.Execute(manipulatorArgs);
                }, false, 30);
            }
        }

        protected static beforeOpenHolderHandler($element : BasePanelHolder) : void {
            ElementManager.StopOpacityChange($element.Id());
            ElementManager.setOpacity($element.Id(), 75);

            if (ObjectValidator.IsEmptyOrNull($element.PrioritySize())) {
                $element.OpenedSize($element.getContentBasedSize());
            } else {
                $element.OpenedSize($element.PrioritySize().Normalize($element.getParentSize(), UnitType.PX));
            }

            $element.TargetSize($element.OpenedSize());
        }

        protected static beforeCloseHolderHandler($element : BasePanelHolder,
                                                  $manipulatorArgs : ValueProgressEventArgs) : void {
            ElementManager.StopOpacityChange($element.Id());
            ElementManager.setOpacity($element.Id(), 75);

            $manipulatorArgs.RangeStart(0);
            $manipulatorArgs.RangeEnd($element.TargetSize() - $element.getHeaderSize());
            $manipulatorArgs.Step($element.animIterationNum);
            $manipulatorArgs.CurrentValue($manipulatorArgs.RangeEnd());
            $manipulatorArgs.ProgressType(ProgressType.FAST_END);
            $manipulatorArgs.DirectionType(DirectionType.DOWN);

            ElementManager.setClassName($element.Id() + "_Type", $element.getTypeCssClassName(false));
        }

        protected static onChangeBodyHandler($element : BasePanelHolder,
                                             $manipulatorArgs : ValueProgressEventArgs) : void {
            const thisClass : any = Reflection.getInstance().getClass($element.getClassName());
            $element.CurrentSize($manipulatorArgs.CurrentValue() + $element.getHeaderSize());
            $element.CurrentBodySize($manipulatorArgs.CurrentValue());

            if (!$element.IsResizeDelegated()) {
                thisClass.resizeHolder($element, $element.CurrentSize());
            } else {
                $element.getEventsManager().FireEvent($element, EventType.ON_CHANGE);
            }
        }

        protected static onShowCompleteBodyHandler($element : BasePanelHolder) : void {
            ElementManager.setClassName($element.Id() + "_Type", $element.getTypeCssClassName(true));
            ElementManager.ChangeOpacity($element, DirectionType.UP, 10);
        }

        protected static onHideCompleteBodyHandler($element : BasePanelHolder) : void {
            ElementManager.ChangeOpacity($element, DirectionType.UP, 10);
        }

        protected static toggleOpenHide($element : BasePanelHolder, $manager : GuiObjectManager) : void {
            $element.IsOpened(!$element.IsOpened());
        }

        protected static onMouseOverEventHandler($element : BasePanelHolder, $manager : GuiObjectManager,
                                                 $reflection : Reflection) : void {
            if (!ObjectValidator.IsEmptyOrNull($element.getImageButtonClass())) {
                const arrowClass : any = $reflection.getClass($element.arrowButton.getClassName());
                arrowClass.TurnOn($element.arrowButton, $manager, $reflection);
            }
            if (!ObjectValidator.IsEmptyOrNull($element.getLabelClass())) {
                $element.headerLabel.Title().Enabled(false);
            }
        }

        protected static onMouseOutEventHandler($element : BasePanelHolder, $manager : GuiObjectManager,
                                                $reflection : Reflection) : void {
            if (!ObjectValidator.IsEmptyOrNull($element.getImageButtonClass())) {
                const arrowClass : any = $reflection.getClass($element.arrowButton.getClassName());
                arrowClass.TurnOff($element.arrowButton, $manager, $reflection);
            }
            if (!ObjectValidator.IsEmptyOrNull($element.getLabelClass()) && $element.IsOpened()) {
                $element.headerLabel.Title().Enabled(true);
            }
        }

        /**
         * @param {BasePanelViewer} $bodyClass Specify class name of panel viewer, which should be handled.
         * @param {BasePanelViewerArgs} [$args] Specify arguments for handled panel object viewer.
         * @param {any} [$holderType] Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        protected constructor($bodyClass : BasePanelViewer, $args? : BasePanelViewerArgs, $holderType? : any, $id? : string) {
            super($id);

            this.minWidth = 100;
            this.minHeight = 100;
            this.guiType = this.guiTypeValueSetter($holderType);
            this.isOpened = true;
            this.prioritySize = null;
            this.currentSize = 38;
            this.currentBodySize = 0;
            this.isResizeDelegated = false;
            this.queue = null;
            this.animIterationNum = 10;

            const labelClass : any = this.getLabelClass();
            if (!ObjectValidator.IsEmptyOrNull(labelClass)) {
                this.headerLabel = new labelClass();
                this.descriptionLabel = new labelClass();
            }

            const imageButtonClass : any = this.getImageButtonClass();
            if (!ObjectValidator.IsEmptyOrNull(imageButtonClass)) {
                this.arrowButton = new imageButtonClass();
            }

            if (!ObjectValidator.IsEmptyOrNull($args)) {
                $args.AsyncEnabled(false);
            }

            if (ObjectValidator.IsEmptyOrNull($bodyClass)) {
                this.body = new BasePanel();
                this.body.StyleClassName("Empty");
            } else {
                this.addChildPanel($bodyClass, $args, ($parent : BasePanelHolder, $child : BasePanel) : void => {
                    $parent.body = $child;
                });
            }
        }

        /**
         * @return {IBasePanelHolderEvents} Returns events connected with the element.
         */
        public getEvents() : IBasePanelHolderEvents {
            return <IBasePanelHolderEvents>super.getEvents();
        }

        /**
         * @param {any} [$holderType] Specify type of element look and feel.
         * @return {any} Returns type of element's look and feel.
         */
        public GuiType($holderType ? : any) : any {
            if (ObjectValidator.IsSet($holderType)) {
                this.guiType = this.guiTypeValueSetter($holderType);
                if (this.IsLoaded()) {
                    ElementManager.setClassName(this.Id() + "_Type", this.getTypeCssClassName(this.isOpened));
                }
            }

            return this.guiType;
        }

        /**
         * @param {boolean} [$value] Specify if holder should be in opened state.
         * @param {boolean} [$animate] Specify if animation should be executed.
         * @return {boolean} Returns true, if holder is in opened mode, otherwise false.
         */
        public IsOpened($value? : boolean, $animate : boolean = true) : boolean {
            if (this.IsLoaded() && $value !== this.isOpened) {
                if (ObjectValidator.IsBoolean($value) && this.IsCompleted()) {
                    const manager : GuiObjectManager = this.getGuiManager();
                    if (!manager.IsActive(this) && !manager.IsActive(this.Parent())) {
                        const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
                        if ($value) {
                            thisClass.open(this, $animate);
                        } else {
                            thisClass.hide(this, $animate);
                        }
                    } else {
                        this.queue = {
                            handler: () : void => {
                                this.queue = null;
                                this.IsOpened($value, $animate);
                            },
                            time   : new Date().getTime()
                        };
                    }
                }
            } else {
                this.isOpened = Property.Boolean(this.isOpened, $value);
            }
            return this.isOpened;
        }

        /**
         * @return {number} Returns current header size.
         */
        public getHeaderSize() : number {
            return this.CurrentSize() - this.CurrentBodySize();
        }

        /**
         * @param {number} [$value] Specify default, non-scaled open holder primary axis size.
         * @return {number} Returns default, non-scaled open holder primary axis size.
         */
        public OpenedSize($value? : number) : number {
            return this.openedSize = Property.PositiveInteger(this.openedSize, $value);
        }

        /**
         * @param {number} [$value] Specify scaled open holder primary axis size.
         * @return {number} Returns scaled open holder primary axis size.
         */
        public TargetSize($value? : number) : number {
            return this.targetSize = Property.PositiveInteger(this.targetSize, $value);
        }

        /**
         * @param {number} [$value] Specify current holder primary axis size.
         * @return {number} Returns current holder primary axis size.
         */
        public CurrentSize($value? : number) : number {
            return this.currentSize = Property.PositiveInteger(this.currentSize, $value);
        }

        /**
         * @param {number} [$value] Specify current holder primary axis body size.
         * @return {number} Returns current holder primary axis body size.
         */
        public CurrentBodySize($value? : number) : number {
            return this.currentBodySize = Property.PositiveInteger(this.currentBodySize, $value);
        }

        /**
         * @param {PropagableNumber} [$value] Specify prioritized size.
         * @return {PropagableNumber} Returns PropagableNumber instance specifying prioritized size.
         */
        public PrioritySize($value? : PropagableNumber) : PropagableNumber {
            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
            if (ObjectValidator.IsSet($value)) {
                this.prioritySize = $value;
                if (this.IsLoaded() && this.IsOpened()) {
                    thisClass.resizeHolder(this);
                }
            }
            return this.prioritySize;
        }

        public IsScalable() : boolean {
            return ObjectValidator.IsEmptyOrNull(this.PrioritySize()) ? true :
                this.PrioritySize().Normalize(9999, UnitType.PX) !==
                this.PrioritySize().Normalize(0, UnitType.PX);
        }

        public AnimIterationNum($value? : number) : number {
            return this.animIterationNum = Property.PositiveInteger(this.animIterationNum, $value);
        }

        /**
         * @return {any}
         */
        public getQueue() : any {
            return this.queue;
        }

        public StopAnimation() : void {
            const manager : GuiObjectManager = this.getGuiManager();
            if (manager.IsActive(this)) {
                manager.setActive(this, false);
                this.queue = null;
                ValueProgressManager.Remove(this.Id());
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner(this);
                this.getEventsManager().FireEvent(this, this.Id() + "_ShowBody_" +
                    EventType.ON_COMPLETE, eventArgs, false);
            }
        }

        /**
         * @method getBody
         * @memberOf Com.Wui.Framework.UserControls.Primitives.BasePanelHolder#
         * @return {BasePanel} Returns BasePanel instance held by the holder element, if instance has been set, otherwise null.
         */
        public getBody() : BasePanel {
            return this.body;
        }

        /**
         * @return {ElementEventsManager} Returns events connected with holder header.
         */
        public getHeaderEvents() : ElementEventsManager {
            if (!ObjectValidator.IsSet(this.headerEvents)) {
                this.headerEvents = new ElementEventsManager(this, this.Id() + "_Header");
            }
            return this.headerEvents;
        }

        /**
         * @param {boolean} [$value] Specify whether or not resize during animation is delegated to subscriber of onChange event
         * - used for animation grouping.
         * @return {boolean} Returns whether or not resize during animation is delegated to subscriber of onChange event
         * - used for animation grouping..
         */
        public IsResizeDelegated($value? : boolean) : boolean {
            return this.isResizeDelegated = Property.Boolean(this.isResizeDelegated, $value);
        }

        /**
         * @return {number} Returns holder content width or height based on holder type
         */
        public getContentBasedSize() : number {
            // placeholder for overriding
            return undefined;
        }

        protected getParentSize() : number {
            // placeholder for overriding
            return undefined;
        }

        protected getBodyId() : string {
            if (!ObjectValidator.IsEmptyOrNull(this.getBody())) {
                return this.getBody().Id();
            } else {
                return this.Id() + "_EmptyContent";
            }
        }

        protected innerCode() : IGuiElement {
            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
            this.body.Visible(true);

            if (!ObjectValidator.IsEmptyOrNull(this.getLabelClass())) {
                this.headerLabel.Visible(true);
                this.descriptionLabel.Visible(true);
            }

            if (!ObjectValidator.IsEmptyOrNull(this.getImageButtonClass())) {
                this.arrowButton.Visible(true);
                this.arrowButton.getEvents().setOnClick(
                    ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                        $eventArgs.StopAllPropagation();
                        thisClass.toggleOpenHide(<BasePanelHolder>$eventArgs.Owner().Parent(), $manager);
                    });

                this.arrowButton.getEvents().setOnMouseOver(
                    ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                        thisClass.onMouseOverEventHandler(<BasePanelHolder>$eventArgs.Owner().Parent(), $manager, $reflection);
                    });

                this.arrowButton.getEvents().setOnMouseOut(
                    ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                        thisClass.onMouseOutEventHandler(<BasePanelHolder>$eventArgs.Owner().Parent(), $manager, $reflection);
                    });
            }

            this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                const element : BasePanelHolder = <BasePanelHolder>$eventArgs.Owner();
                element.getHeaderEvents().Subscribe();
                if (!ObjectValidator.IsEmptyOrNull(this.getLabelClass())) {
                    element.getHeaderEvents().Subscribe(element.headerLabel.Id());
                    element.getHeaderEvents().Subscribe(element.descriptionLabel.Id());
                }
            });

            this.getHeaderEvents().setOnClick(
                ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                    $eventArgs.StopAllPropagation();
                    thisClass.toggleOpenHide(<BasePanelHolder>$eventArgs.Owner(), $manager);
                });

            this.getHeaderEvents().setOnMouseOver(
                ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    thisClass.onMouseOverEventHandler(<BasePanelHolder>$eventArgs.Owner(), $manager, $reflection);
                });

            this.getHeaderEvents().setOnMouseOut(
                ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    thisClass.onMouseOutEventHandler(<BasePanelHolder>$eventArgs.Owner(), $manager, $reflection);
                });

            const resizeHandler : any = ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                const element : BasePanel = <BasePanel>$eventArgs.Owner();
                const holder : BasePanelHolder =
                    <BasePanelHolder>($reflection.IsMemberOf(element, BasePanelHolder) ? element : element.Parent());
                if (holder.IsCompleted() && !$manager.IsActive(holder)) {
                    if ($eventArgs.Type() === EventType.ON_COMPLETE) {
                        if (ObjectValidator.IsEmptyOrNull(holder.TargetSize())) {
                            thisClass.resizeHolder(holder, holder.CurrentSize());
                        }
                        if (holder.IsOpened()) {
                            if (!holder.IsResizeDelegated()) {
                                thisClass.open(holder);
                            }
                        } else {
                            holder.getBody().Visible(false);
                        }
                        ElementManager.setOpacity(holder.Id(), 100);
                    } else {
                        if (holder.IsOpened() && ObjectValidator.IsEmptyOrNull(holder.PrioritySize())
                            && holder.CurrentBodySize() !== 0 && $eventArgs.Owner() === holder.getBody()) {
                            const targetSize = holder.getContentBasedSize();
                            holder.TargetSize(targetSize);
                            thisClass.resizeHolder(holder, targetSize);
                        } else if (!holder.IsResizeDelegated()) {
                            thisClass.resizeHolder(holder, holder.IsOpened() ? holder.TargetSize() : holder.CurrentSize());
                        }
                    }
                }
            };

            this.getBody().getEvents().setOnResize(resizeHandler);
            this.getEvents().setOnResize(resizeHandler);
            this.getEvents().setOnComplete(resizeHandler);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Type")
                .StyleClassName(this.getTypeCssClassName(this.IsOpened()))
                .GuiTypeTag(this.getGuiTypeTag())
                .Add(this.addElement(this.Id() + "_Header")
                    .StyleClassName("Header")
                    .Add(this.addElement(this.Id() + "_HeaderLeft")
                        .StyleClassName(GeneralCssNames.LEFT)
                        .Add(this.arrowButton)
                    )
                    .Add(this.addElement(this.Id() + "_HeaderCenter")
                        .StyleClassName(GeneralCssNames.CENTER)
                        .Add(this.headerLabel)
                    )
                    .Add(this.addElement(this.Id() + "_HeaderRight")
                        .StyleClassName(GeneralCssNames.RIGHT)
                        .Add(this.descriptionLabel)
                    )
                )
                .Add(this.getBody());
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel
         */
        protected getLabelClass() : any {
            return null;
        }

        /**
         * @return {IImageButton} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IImageButton
         */
        protected getImageButtonClass() : any {
            return null;
        }

        protected guiTypeValueSetter($value : any) : any {
            return "";
        }

        protected getTypeCssClassName($opened : boolean) : string {
            if ($opened) {
                return "Opened";
            } else {
                if (ObjectValidator.IsEmptyOrNull(this.GuiType())) {
                    return "Closed";
                } else {
                    return "Closed " + this.GuiType();
                }
            }
        }

        protected cssInterfaceName() : string {
            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
            if (this.getClassNameWithoutNamespace() !== thisClass.ClassNameWithoutNamespace()) {
                return thisClass.ClassNameWithoutNamespace() + " " + this.getClassNameWithoutNamespace();
            }
            return thisClass.NamespaceName();
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push(
                "headerEvents"
            );
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push(
                "headerEvents"
            );
            return exclude;
        }
    }
}
