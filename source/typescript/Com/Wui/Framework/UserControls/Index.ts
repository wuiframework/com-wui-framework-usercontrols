/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import StaticPageContentManger = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import Viewers = Com.Wui.Framework.UserControls.BaseInterface.Viewers;

    /**
     * Index request resolver class provides handling of web index page.
     */
    export class Index extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            let output : string =
                "<div class=\"GuiInterface\">" +
                "<h1>WUI Framework User Controls Library</h1>" +
                "<h3>Basic GUI User Controls mainly focused on implementation of business logic and " +
                "extendability to graphical user controls.</h3>" +
                "<div class=\"Index\">";

            output +=
                "<H3>Components</H3>" +
                "<a href=\"" + Viewers.Components.ToolTipViewer.CallbackLink(true) + "\">ToolTip</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.NotificationViewer.CallbackLink(true) + "\">Notification</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.ScrollBarViewer.CallbackLink(true) + "\">ScrollBar</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.DragBarViewer.CallbackLink(true) + "\">DragBar</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.ResizeBarViewer.CallbackLink(true) + "\">ResizeBar</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.SelectBoxViewer.CallbackLink(true) + "\">SelectBox</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.CropBoxViewer.CallbackLink(true) + "\">CropBox</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.FileUploadViewer.CallbackLink(true) + "\">FileUpload</a>" +
                StringUtils.NewLine() +

                "<H3>User Controls</H3>" +
                "<a href=\"" + Viewers.UserControls.LabelViewer.CallbackLink(true) + "\">Label</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.LabelListViewer.CallbackLink(true) + "\">Labels list</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.InputLabelViewer.CallbackLink(true) + "\">InputLabel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.LinkViewer.CallbackLink(true) + "\">Link</a>" +
                StringUtils.NewLine() +
                StringUtils.NewLine() +

                "<a href=\"" + Viewers.UserControls.CheckBoxViewer.CallbackLink(true) + "\">CheckBox</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.RadioBoxViewer.CallbackLink(true) + "\">RadioBox</a>" +
                StringUtils.NewLine() +
                StringUtils.NewLine() +

                "<a href=\"" + Viewers.UserControls.IconViewer.CallbackLink(true) + "\">Icon</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.ImageViewer.CallbackLink(true) + "\">Image</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.ImageButtonViewer.CallbackLink(true) + "\">ImageButton</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.ButtonViewer.CallbackLink(true) + "\">Button</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.TabsViewer.CallbackLink(true) + "\">Tabs</a>" +
                StringUtils.NewLine() +
                StringUtils.NewLine() +

                "<a href=\"" + Viewers.UserControls.TextFieldViewer.CallbackLink(true) + "\">TextField</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.TextAreaViewer.CallbackLink(true) + "\">TextArea</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.DropDownListViewer.CallbackLink(true) + "\">DropDownList</a>" +
                StringUtils.NewLine() +
                StringUtils.NewLine() +

                "<a href=\"" + Viewers.UserControls.ProgressBarViewer.CallbackLink(true) + "\">ProgressBar</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.NumberPickerViewer.CallbackLink(true) + "\">NumberPicker</a>" +
                StringUtils.NewLine() +
                StringUtils.NewLine() +

                "<a href=\"" + Viewers.UserControls.FormInputViewer.CallbackLink(true) + "\">FormInput</a>" +
                StringUtils.NewLine() +
                StringUtils.NewLine() +

                "<a href=\"" + Viewers.UserControls.VerticalPanelHolderViewer.CallbackLink(true) + "\">Vertical panel holder</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.HorizontalPanelHolderViewer.CallbackLink(true) + "\">Horizontal panel holder</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.AccordionViewer.CallbackLink(true) + "\">Accordion</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.DialogViewer.CallbackLink(true) + "\">Dialog</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.DirectoryBrowserViewer.CallbackLink(true) + "\">DirectoryBrowser</a>" +
                StringUtils.NewLine();

            /* dev:start */
            output +=
                "<H3>Runtime tests</H3>" +
                "<a href=\"" + RuntimeTests.PerformanceTestPanelViewer.CallbackLink() + "\">Performance test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.UserControlsTestPanelViewer.CallbackLink() + "\">User Controls test panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.UserControlsTestPanelResponsiveViewer.CallbackLink() + "\">" +
                "User Controls test panel responsive</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.UserControlsTestPanelResponsiveSimpleViewer.CallbackLink() + "\">" +
                "User Controls test panel responsive simple</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.UserControlsTestDialogViewer.CallbackLink(true) + "\">User Controls test dialog</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.MultiPanelTestPanelViewer.CallbackLink() + "\">Multi panel test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.VerticalTestPanelHolderViewer.CallbackLink(true) + "\">Vertical panel holder test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.HorizontalTestPanelHolderViewer.CallbackLink(true) + "\">Horizontal panel holder test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.MultiAccordionTestPanelViewer.CallbackLink() + "\">Multi accordion test</a>" +
                StringUtils.NewLine();

            /* dev:end */

            output +=
                "</div>" +
                "</div>";

            output +=
                "<div class=\"Note\">" +
                "version: " + this.getEnvironmentArgs().getProjectVersion() +
                ", build: " + this.getEnvironmentArgs().getBuildTime() +
                "</div>" + StringUtils.NewLine(false) +
                "<div class=\"Logo\">" + StringUtils.NewLine(false) +
                "   <div class=\"WUI\"></div>" + StringUtils.NewLine(false) +
                "</div>";

            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("WUI - UserControls Index");
            StaticPageContentManger.BodyAppend(output);
            StaticPageContentManger.Draw();
        }
    }
}
