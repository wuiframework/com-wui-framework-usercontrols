/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.HttpProcessor {
    "use strict";
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;

    export class HttpResolver extends Com.Wui.Framework.Gui.HttpProcessor.HttpResolver {

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            resolvers["/"] = Com.Wui.Framework.UserControls.Index;
            resolvers["/index"] = Com.Wui.Framework.UserControls.Index;
            resolvers["/web/"] = Com.Wui.Framework.UserControls.Index;
            return resolvers;
        }

        protected getEventsManagerClass() : any {
            return EventsManager;
        }
    }
}
