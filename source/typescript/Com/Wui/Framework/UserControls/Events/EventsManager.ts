/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Events {
    "use strict";
    import ScrollBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ScrollBar;
    import DragBar = Com.Wui.Framework.UserControls.BaseInterface.Components.DragBar;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import TextField = Com.Wui.Framework.UserControls.BaseInterface.UserControls.TextField;
    import TextArea = Com.Wui.Framework.UserControls.BaseInterface.UserControls.TextArea;
    import NumberPicker = Com.Wui.Framework.UserControls.BaseInterface.UserControls.NumberPicker;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    /**
     * EventsManager class provides handling of all registered custom events.
     */
    export class EventsManager extends Com.Wui.Framework.Gui.Events.EventsManager {

        protected isOnLoadKey($eventArgs : KeyboardEvent) : boolean {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            return (super.isOnLoadKey($eventArgs)
                && !manager.IsActive(<ClassName>TextField) && !manager.IsActive(<ClassName>TextArea));
        }

        protected isElementMoveAllowed() : boolean {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            return !(manager.IsActive(<ClassName>ScrollBar) ||
                manager.IsActive(<ClassName>DragBar) ||
                manager.IsActive(<ClassName>ResizeBar) ||
                manager.IsActive(<ClassName>NumberPicker));
        }
    }
}
