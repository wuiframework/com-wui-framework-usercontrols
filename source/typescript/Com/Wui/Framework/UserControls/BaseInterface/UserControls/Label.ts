/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import ILabel = Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import ILabelEvents = Com.Wui.Framework.Gui.Interfaces.Events.ILabelEvents;

    /**
     * Label class renders simple text.
     */
    export class Label extends Com.Wui.Framework.UserControls.Primitives.BaseGuiObject implements ILabel {
        private text : string;

        /**
         * @param {string} [$text] Specify label content.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($text? : string, $id? : string) {
            super($id);
            if (ObjectValidator.IsSet($text) && $text !== null) {
                this.Text($text);
            } else {
                this.Text(this.Id());
            }
        }

        /**
         * @return {ILabelEvents} Returns events manager subscribed to the item.
         */
        public getEvents() : ILabelEvents {
            return <ILabelEvents>super.getEvents();
        }

        /**
         * @param {string} [$value] Set text value, which should be displayed as element context.
         * @return {string} Returns element's text value.
         */
        public Text($value? : string) : string {
            if (ObjectValidator.IsSet($value) && this.text !== $value) {
                this.text = Property.NullString(this.text, $value);
                if (this.IsLoaded()) {
                    ElementManager.setInnerHtml(this.Id() + "_Text", $value);
                    ElementManager.setInnerHtml(this.Id() + "_DisabledText", $value);

                    const eventArgs : EventArgs = new EventArgs();
                    eventArgs.Owner(this);
                    const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
                    this.getEventsManager().FireEvent(thisClass.ClassName(), EventType.ON_CHANGE, eventArgs);
                    this.getEventsManager().FireEvent(this, EventType.ON_CHANGE, eventArgs);
                }
            }
            return this.text;
        }

        /**
         * @param {string} [$value] Set text value, which should be displayed as element context.
         * @return {string} Returns element's text value, if value is set, otherwise null.
         */
        public Value($value? : string) : any {
            if (ObjectValidator.IsSet($value)) {
                this.Text($value);
            }
            return null;
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            const enabled : boolean = super.Enabled($value);
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                if (enabled || this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                    ElementManager.Enabled(this.Id(), enabled);
                }
            }
            return enabled;
        }

        /**
         * @return {IGuiCommonsArg[]} Returns array of element's attributes.
         */
        public getArgs() : IGuiCommonsArg[] {
            const args : IGuiCommonsArg[] = super.getArgs();
            let index : number;
            for (index = 0; index < args.length; index++) {
                if (args[index].name === "Value") {
                    args[index].value = this.Text();
                    break;
                }
            }
            return args;
        }

        protected innerHtml() : IGuiElement {
            const disabledOption : IGuiElement = this.addElement();
            if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                disabledOption
                    .Id(this.Id() + "_Disabled")
                    .StyleClassName(GeneralCssNames.DISABLE)
                    .Visible(!this.Enabled())
                    .Add(this.addElement(this.Id() + "_DisabledText")
                        .StyleClassName(GeneralCssNames.TEXT)
                        .Add(this.Text())
                    );
            }

            return this.addElement()
                .Add(this.addElement(this.Id() + "_Enabled")
                    .Visible(this.Enabled())
                    .Add(this.addElement(this.Id() + "_Text")
                        .StyleClassName(GeneralCssNames.TEXT)
                        .Add(this.Text())
                    ))
                .Add(disabledOption);
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();
            this.getEvents().Subscriber(this.Id() + "_Enabled");
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push("text");
            return exclude;
        }
    }
}
