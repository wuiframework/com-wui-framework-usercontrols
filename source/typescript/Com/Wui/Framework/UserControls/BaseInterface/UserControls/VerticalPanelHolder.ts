/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;

    /**
     * VerticalPanelHolder class renders BasePanel holder,
     * which is suitable for external manipulation at vertical orientation.
     */
    export class VerticalPanelHolder extends Com.Wui.Framework.UserControls.Primitives.BasePanelHolder {

        protected static resizeHolder($element : VerticalPanelHolder, size? : number) : void {
            let newWidth : number;
            let newHeight : number;

            newWidth = ObjectValidator.IsEmptyOrNull($element.Parent()) ? $element.Width() :
                ElementManager.getCssIntegerValue($element.Parent().Id() + "_PanelContentEnvelop", "width");
            if (ObjectValidator.IsSet(size)) {
                newHeight = size;
            } else if (!ObjectValidator.IsEmptyOrNull($element.PrioritySize())) {
                const parentHeight : number = ObjectValidator.IsEmptyOrNull($element.Parent()) ? 0 : $element.Parent().Height();
                newHeight = $element.PrioritySize().Normalize(parentHeight, UnitType.PX);
                if (ObjectValidator.IsEmptyOrNull($element.TargetSize())) {
                    $element.OpenedSize(newHeight);
                    $element.TargetSize(newHeight);
                }
            } else {
                return;
            }

            const headerOffset : number = $element.minHeight;

            let widthChanged : boolean = false;
            let heightChanged : boolean = false;
            const innerPanel : BasePanel = $element.getBody();

            const innerWrapperHeight : number = Math.max(0, newHeight - headerOffset);
            const innerHeight : number = Math.max(Math.max(0, $element.TargetSize() - headerOffset), innerWrapperHeight);

            if (innerPanel.Height() !== innerHeight) {
                innerPanel.Height(innerHeight - ElementManager.getHeightOffset($element.Id()));
                heightChanged = true;
            } else if ($element.Height() !== newHeight) {
                heightChanged = true;
            }

            if (innerPanel.Width() !== newWidth - ElementManager.getWidthOffset($element.Id())) {
                innerPanel.Width(newWidth - ElementManager.getWidthOffset($element.Id()));
                widthChanged = true;
            }

            if (widthChanged || heightChanged) {
                newWidth = Math.max($element.minWidth, newWidth);
                newHeight = Math.max($element.minHeight, newHeight);

                if (widthChanged) {
                    $element.Width(newWidth);
                    const headerLeftWidth : number = ElementManager.getOffsetWidth($element.Id() + "_HeaderLeft");
                    const headerRightWidth : number = ElementManager.getOffsetWidth($element.Id() + "_HeaderRight");

                    const centerWidth : number = newWidth - headerLeftWidth - headerRightWidth;
                    ElementManager.setWidth($element.Id() + "_HeaderCenter", centerWidth);
                    ElementManager.setCssProperty($element.Id() + "_Header", "min-width", newWidth + "px");

                    if (!ObjectValidator.IsEmptyOrNull($element.getLabelClass()) &&
                        !ObjectValidator.IsEmptyOrNull($element.descriptionLabel.Text())) {
                        $element.descriptionLabel.Visible(true);
                        const headerLabelWidth : number = ElementManager.getOffsetWidth($element.headerLabel.Id());
                        const maxWidth : number = newWidth - ElementManager.getElement($element.headerLabel.Id()).offsetLeft
                            - headerLeftWidth;
                        if (maxWidth < headerLabelWidth ||
                            maxWidth - headerLabelWidth < ElementManager.getOffsetWidth($element.descriptionLabel.Id())
                            + ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "padding-left")
                            + ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "right")) {
                            $element.descriptionLabel.Visible(false);
                        }
                    }
                }

                if (heightChanged) {
                    $element.Height(newHeight);
                    ElementManager.setCssProperty(innerPanel.Id(), "top", newHeight - $element.TargetSize());
                }
            }
        }

        /**
         * @param {BasePanelViewer} $bodyClass Specify class name of panel viewer, which should be handled.
         * @param {BasePanelViewerArgs} [$args] Specify arguments for handled panel object viewer.
         * @param {any} [$holderType] Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($bodyClass : BasePanelViewer, $args? : BasePanelViewerArgs, $holderType? : any, $id? : string) {
            super($bodyClass, $args, $holderType, $id);
            this.minWidth = 200;
            this.minHeight = 38;
            this.Height(this.minHeight);
            this.CurrentSize(this.minHeight);
        }

        public getContentBasedSize() : number {
            const body : BasePanel = this.getBody();
            let size : number = body.Height();
            if (ElementManager.Exists(body.Id() + "_PanelContent")) {
                ElementManager.ClearCssProperty(body.Id() + "_PanelContentEnvelop", "height");
                size = ElementManager.getEnvelopHeight(body.Id() + "_PanelContent", true) +
                    ElementManager.getHeightOffset(body.Id()) +
                    (ElementManager.IsVisible(body.Id() + "_Horizontal_ScrollBar_ContentOffset") ?
                        ElementManager.getCssIntegerValue(body.Id() + "_Horizontal_ScrollBar_ContentOffset", "height") : 0);
            }
            size += this.getHeaderSize();
            return size;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel
         */
        protected getLabelClass() : any {
            return Label;
        }

        /**
         * @return {IImageButton} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IImageButton
         */
        protected getImageButtonClass() : any {
            return ImageButton;
        }

        protected getParentSize() : number {
            return ObjectValidator.IsEmptyOrNull(this.Parent()) ?
                0 : this.Parent().Height();
        }

        protected innerCode() : IGuiElement {
            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return super.innerHtml();
        }
    }
}
