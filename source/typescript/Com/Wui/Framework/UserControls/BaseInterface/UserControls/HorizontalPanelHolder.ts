/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;

    /**
     * HorizontalPanelHolder class renders BasePanel holder,
     * which is suitable for external manipulation at horizontal orientation.
     */
    export class HorizontalPanelHolder extends Com.Wui.Framework.UserControls.Primitives.BasePanelHolder {

        protected static resizeHolder($element : HorizontalPanelHolder, size? : number) : void {
            let newWidth : number;
            let newHeight : number;

            newHeight = ObjectValidator.IsEmptyOrNull($element.Parent()) ? $element.Height() :
                $element.Parent().Height() - ElementManager.getHeightOffset($element.Parent().Id()) -
                (ElementManager.IsVisible($element.Parent().Id() + "_Horizontal_ScrollBar") ?
                    ElementManager.getCssIntegerValue($element.Parent().Id() + "_Horizontal_ScrollBar_ContentOffset", "height") : 0);
            if (ObjectValidator.IsSet(size)) {
                newWidth = size;
            } else if (!ObjectValidator.IsEmptyOrNull($element.PrioritySize())) {
                const parentWidth : number = ObjectValidator.IsEmptyOrNull($element.Parent()) ? 0 : $element.Parent().Width();
                newWidth = $element.PrioritySize().Normalize(parentWidth, UnitType.PX);
                if (ObjectValidator.IsEmptyOrNull($element.TargetSize())) {
                    $element.OpenedSize(newWidth);
                    $element.TargetSize(newWidth);
                }
            } else {
                return;
            }

            let widthChanged : boolean = false;
            let heightChanged : boolean = false;
            const innerPanel : BasePanel = $element.getBody();

            const innerWrapperWidth : number = Math.max(0, newWidth);
            const innerWidth : number = Math.max(Math.max(0, $element.TargetSize()), innerWrapperWidth);

            if (innerPanel.Width() !== innerWidth) {
                innerPanel.Width(innerWidth - ElementManager.getWidthOffset($element.Id()));
                widthChanged = true;
            } else if ($element.Width() !== Math.max($element.minWidth, newWidth)) {
                widthChanged = true;
            }

            if ($element.Height() !== newHeight || innerPanel.Height() !== newHeight - ElementManager.getHeightOffset($element.Id())
                - $element.minWidth) {
                innerPanel.Height(newHeight - $element.minWidth - ElementManager.getHeightOffset($element.Id()));
                heightChanged = true;
            }

            if (widthChanged || heightChanged) {
                const mult : number = (newWidth - $element.minWidth) / ($element.TargetSize() - $element.minWidth);
                let width : number;
                let height : number;
                if (mult < 0.5) {
                    width = $element.minWidth;
                    height = newHeight * (1 - (Math.max(0, mult) * 2));
                } else {
                    width = newWidth * (Math.max(0, mult - 0.5) * 2);
                    height = $element.minHeight;
                }

                const headerLeftWidth : number = ElementManager.getOffsetWidth($element.Id() + "_HeaderLeft");
                const headerRightWidth : number = ElementManager.getOffsetWidth($element.Id() + "_HeaderRight");

                ElementManager.setWidth($element.Id() + "_HeaderCenter",
                    Math.max(width, $element.minWidth) - headerLeftWidth - headerRightWidth);

                height = Math.max(height, $element.minHeight) - ElementManager.getHeightOffset($element.Id() + "_HeaderCenter");

                ElementManager.setHeight($element.Id() + "_HeaderLeft", height);
                ElementManager.setHeight($element.Id() + "_HeaderCenter", height);
                ElementManager.setHeight($element.Id() + "_HeaderRight", height);

                ElementManager.setCssProperty(innerPanel.Id(), "left", newWidth - $element.TargetSize());
                ElementManager.setCssProperty(innerPanel.Id(), "top", $element.minWidth);

                newWidth = Math.max($element.minWidth, newWidth);
                newHeight = Math.max($element.minHeight, newHeight);

                if (!ObjectValidator.IsEmptyOrNull($element.getLabelClass()) &&
                    !ObjectValidator.IsEmptyOrNull($element.descriptionLabel.Text())) {
                    $element.descriptionLabel.Visible(true);
                    if (width > height) {
                        ElementManager.setClassName($element.Id() + "_Type", $element.getTypeCssClassName(true));
                        const headerLabelWidth : number = ElementManager.getOffsetWidth($element.headerLabel.Id());
                        const maxWidth : number = width - ElementManager.getCssIntegerValue($element.headerLabel.Id(), "left")
                            - headerLeftWidth;
                        if (maxWidth < headerLabelWidth ||
                            maxWidth - headerLabelWidth < ElementManager.getOffsetWidth($element.descriptionLabel.Id())
                            + ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "padding-left")
                            + ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "right")) {
                            $element.descriptionLabel.Visible(false);
                        }
                    } else {
                        ElementManager.setClassName($element.Id() + "_Type", $element.getTypeCssClassName(false));
                        const headerLabelHeight : number = ElementManager.getOffsetHeight($element.headerLabel.Id());
                        const maxHeight : number = height - ElementManager.getCssIntegerValue($element.headerLabel.Id(), "top");
                        if (maxHeight < headerLabelHeight ||
                            maxHeight - headerLabelHeight < ElementManager.getOffsetHeight($element.descriptionLabel.Id())
                            + ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "padding-top")
                            + ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "bottom")) {
                            $element.descriptionLabel.Visible(false);
                        }
                    }
                }

                if (widthChanged) {
                    $element.Width(newWidth);
                }

                if (heightChanged) {
                    $element.Height(newHeight);
                }
            }
        }

        /**
         * @param {BasePanelViewer} $bodyClass Specify class name of panel viewer, which should be handled.
         * @param {BasePanelViewerArgs} [$args] Specify arguments for handled panel object viewer.
         * @param {HorizontalPanelHolderType} [$holderType] Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($bodyClass : BasePanelViewer, $args? : BasePanelViewerArgs, $holderType? : any, $id? : string) {
            super($bodyClass, $args, $holderType, $id);
            this.minWidth = 38;
            this.minHeight = 38;
            this.Width(this.minWidth);
            this.CurrentSize(this.minWidth);
        }

        public getContentBasedSize() : number {
            const body : BasePanel = this.getBody();
            let size : number = body.Width();
            if (ElementManager.Exists(body.Id() + "_PanelContent")) {
                ElementManager.ClearCssProperty(body.Id() + "_PanelContentEnvelop", "width");
                ElementManager.ClearCssProperty(body.Id() + "_PanelContent", "min-width");
                size = ElementManager.getEnvelopWidth(body.Id() + "_PanelContent") +
                    ElementManager.getWidthOffset(body.Id()) +
                    (ElementManager.IsVisible(body.Id() + "_Vertical_ScrollBar_ContentOffset") ?
                        ElementManager.getCssIntegerValue(body.Id() + "_Vertical_ScrollBar_ContentOffset", "width") : 0);
            }
            return size;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel
         */
        protected getLabelClass() : any {
            return Label;
        }

        /**
         * @return {IImageButton} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IImageButton
         */
        protected getImageButtonClass() : any {
            return ImageButton;
        }

        protected getParentSize() : number {
            return ObjectValidator.IsEmptyOrNull(this.Parent()) ?
                0 : this.Parent().Width();
        }

        protected innerCode() : IGuiElement {
            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return super.innerHtml();
        }
    }
}
