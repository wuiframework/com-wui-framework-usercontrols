/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import BasePanel = Com.Wui.Framework.UserControls.Primitives.BasePanel;
    import BasePanelHolderViewerArgs = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewerArgs;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import BasePanelHolder = Com.Wui.Framework.UserControls.Primitives.BasePanelHolder;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import BasePanelHolderViewer = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewer;
    import OrientationType = Com.Wui.Framework.Gui.Enums.OrientationType;
    import AccordionType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionType;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import AccordionResizeType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionResizeType;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import ResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeEventArgs;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import VerticalPanelHolderViewer = Com.Wui.Framework.UserControls.BaseInterface.Viewers.UserControls.VerticalPanelHolderViewer;
    import HorizontalPanelHolderViewer = Com.Wui.Framework.UserControls.BaseInterface.Viewers.UserControls.HorizontalPanelHolderViewer;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    /**
     * Accordion class renders accordion type of element.
     */
    export class Accordion extends BasePanel implements Com.Wui.Framework.Gui.Interfaces.UserControls.IAccordion {
        private static defaultWidth : number = 785;
        private static defaultHeight : number = 530;

        protected orientation : OrientationType;
        private guiType : AccordionType;
        private holders : ArrayList<string>;
        private resizeType : AccordionResizeType;
        private activeHolders : ArrayList<BasePanelHolder>;
        private queue : () => void;
        private animIterationNum : number;

        public static recomputeContentSizes($element : Accordion) : void {
            let child : BasePanelHolder = null;
            $element.holders.foreach(($propertyName : string) : void => {
                if ($element.hasOwnProperty($propertyName)) {
                    child = <BasePanelHolder>$element[$propertyName];
                    if (child.IsOpened() && ObjectValidator.IsEmptyOrNull(child.PrioritySize())) {
                        const contentSize : number = child.getContentBasedSize();
                        child.CurrentBodySize(contentSize - child.getHeaderSize());
                        child.CurrentSize(contentSize);
                        child.TargetSize(contentSize);
                    }
                }
            });
            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner($element);
            Accordion.resizeBody(eventArgs);
        }

        private static getSizeInfo($element : Accordion) : ISizeInfo {
            let child : BasePanelHolder;
            let scalableSize : number = 0;
            let totalSize : number = 0;
            const parentSize : number = $element.orientation === OrientationType.HORIZONTAL ?
                ElementManager.getCssIntegerValue($element.Id(), "width") :
                ElementManager.getCssIntegerValue($element.Id(), "height");
            $element.holders.foreach(($propertyName : string) : void => {
                if ($element.hasOwnProperty($propertyName)) {
                    child = <BasePanelHolder>$element[$propertyName];
                    if (ElementManager.IsVisible(child.Id())) {
                        const separatorSize : number = ElementManager.IsVisible(child.Id() + "_Separator") ?
                            ($element.orientation === OrientationType.HORIZONTAL ?
                                ElementManager.getEnvelopWidth(child.Id() + "_Separator") :
                                ElementManager.getEnvelopHeight(child.Id() + "_Separator")) : 0;
                        if ($element.ResizeType() === AccordionResizeType.RESPONSIVE) {
                            if (ObjectValidator.IsEmptyOrNull(child.PrioritySize())) {
                                child.PrioritySize(new PropagableNumber({number: 100, unitType: UnitType.PCT}));
                            }
                            child.OpenedSize(child.PrioritySize().Normalize(parentSize, UnitType.PX));
                        }
                        if (child.IsOpened() ||
                            GuiObjectManager.getInstanceSingleton().IsActive(child) && child.CurrentBodySize() === 0) {
                            if (child.IsScalable() && $element.ResizeType() === AccordionResizeType.RESPONSIVE) {
                                scalableSize += child.OpenedSize() - child.getHeaderSize();
                            }
                            totalSize += child.OpenedSize();
                        } else {
                            totalSize += child.CurrentSize();
                        }
                        totalSize += separatorSize;
                    }
                }
            });
            const mult : number = scalableSize === 0 ? 1 : Math.min(1, 1 - (totalSize - parentSize) / scalableSize);

            return {totalSize, parentSize, scalableSize, mult};
        }

        private static normalizeTargetSize($element : BasePanelHolder) : void {
            const parent : Accordion = <Accordion>$element.Parent();
            const sizeInfo : ISizeInfo = Accordion.getSizeInfo(parent);
            const bodySize : number = $element.OpenedSize() - $element.getHeaderSize();
            $element.TargetSize(Math.ceil(bodySize * ($element.IsScalable() &&
                (<Accordion>$element.Parent()).ResizeType() === AccordionResizeType.RESPONSIVE ? sizeInfo.mult : 1)
                + $element.getHeaderSize()));
        }

        private static resizeBody($eventArgs : EventArgs) : void {
            const element : BasePanelHolder = <BasePanelHolder>$eventArgs.Owner();
            let parent : Accordion;
            if (element.IsMemberOf(Accordion)) {
                parent = <Accordion>$eventArgs.Owner();
            } else {
                parent = <Accordion>element.Parent();
            }

            let firstScalableId : string = null;
            let child : BasePanelHolder = null;

            const sizeInfo : ISizeInfo = Accordion.getSizeInfo(parent);

            const normalizedSizes : any = {};
            let remainingSize : number = sizeInfo.parentSize;
            parent.holders.foreach(($propertyName : string) : void => {
                if (parent.hasOwnProperty($propertyName)) {
                    child = <BasePanelHolder>parent[$propertyName];
                    if (ElementManager.IsVisible(child.Id())) {
                        if (child.IsScalable() && parent.ResizeType() === AccordionResizeType.RESPONSIVE &&
                            ObjectValidator.IsEmptyOrNull(parent.activeHolders.getItem(child.Id()))) {
                            normalizedSizes[$propertyName] = Math.ceil(((child.IsOpened() ? child.OpenedSize() :
                                child.CurrentSize()) - child.getHeaderSize()) * sizeInfo.mult + child.getHeaderSize());
                        } else {
                            normalizedSizes[$propertyName] = child.CurrentSize();
                        }
                        firstScalableId = ObjectValidator.IsEmptyOrNull(firstScalableId) &&
                        normalizedSizes[child.Id()] !== child.getHeaderSize() && child.IsScalable() ? child.Id() : firstScalableId;
                        remainingSize -= normalizedSizes[$propertyName] + (ElementManager.IsVisible(child.Id() + "_Separator") ?
                            (parent.orientation === OrientationType.HORIZONTAL ?
                                ElementManager.getEnvelopWidth(child.Id() + "_Separator") :
                                ElementManager.getEnvelopHeight(child.Id() + "_Separator")) : 0);
                    }
                }
            });

            let offsetApplyId : string = null;

            if (parent.ResizeType() === AccordionResizeType.RESPONSIVE) {
                const sizeOverflow : number = sizeInfo.totalSize - sizeInfo.parentSize;
                if (sizeInfo.mult < 1 && Math.abs(remainingSize) <= 1 && sizeOverflow > 0) {
                    offsetApplyId = firstScalableId;
                } else {
                    remainingSize = 0;
                }
            } else if (parent.orientation === OrientationType.HORIZONTAL) {
                ElementManager.setCssProperty(parent.Id() + "_PanelContent", "min-width", sizeInfo.totalSize + "px");
                ElementManager.setCssProperty(parent.Id() + "_Type", "width", sizeInfo.totalSize + "px");
            }

            parent.holders.foreach(($propertyName : string) : void => {
                if (parent.hasOwnProperty($propertyName)) {
                    child = <BasePanelHolder>parent[$propertyName];
                    if (ElementManager.IsVisible(child.Id()) && !ObjectValidator.IsEmptyOrNull(normalizedSizes[$propertyName])) {
                        const targetSize : number = normalizedSizes[$propertyName] + ($propertyName === offsetApplyId ? remainingSize : 0);
                        if (parent.ResizeType() === AccordionResizeType.RESPONSIVE || ObjectValidator.IsEmptyOrNull
                        (parent.activeHolders) || !ObjectValidator.IsEmptyOrNull(parent.activeHolders.getItem(child.Id()))) {
                            if ((child.IsOpened() || $propertyName === offsetApplyId) &&
                                (ObjectValidator.IsEmptyOrNull(parent.activeHolders.getItem(child.Id()))
                                    || child.CurrentSize() === child.TargetSize())) {
                                child.TargetSize(targetSize);
                            }
                            Reflection.getInstance().getClass(child.getClassName()).resizeHolder(child, targetSize);
                        }
                    }
                }
            });

            if (parent.Scrollable()) {
                parent.getEvents().FireAsynchronousMethod(() : void => {
                    BasePanel.scrollBarVisibilityHandler(parent);
                    BasePanel.scrollToCurrentPosition(parent);
                }, true, 500);
            }

            if (parent.activeHolders.Length() === 1 && parent.activeHolders.getFirst().StyleClassName() === "Last" &&
                parent.ResizeType() === AccordionResizeType.DEFAULT) {
                if (parent.orientation === OrientationType.HORIZONTAL) {
                    BasePanel.scrollLeft(parent, 100);
                } else {
                    BasePanel.scrollTop(parent, 100);
                }
            }
        }

        private static setOpened($element : Accordion, $value : boolean, $filter : Array<number | ClassName>,
                                 $animate : boolean = true) : void {
            let index : number = 0;
            if (!$element.getGuiManager().IsActive($element) || !$animate) {
                if (!$animate) {
                    $element.StopAnimation();
                }
                const filterList : ArrayList<number | ClassName> = ArrayList.ToArrayList($filter);
                $element.holders.foreach(($propertyName : string) : void => {
                    if ($element.hasOwnProperty($propertyName)) {
                        const holder : BasePanelHolder = (<BasePanelHolder>$element[$propertyName]);
                        if ((ObjectValidator.IsEmptyOrNull($filter) ||
                            filterList.Contains(index) ||
                            filterList.Contains(Reflection.getInstance().getClass(holder.getBody().getClassName())))) {
                            holder.IsOpened($value, $animate);
                        }
                        index++;
                    }
                });
            } else {
                $element.queue = () : void => {
                    this.setOpened($element, $value, $filter, $animate);
                    $element.queue = null;
                };
            }
        }

        private static showScrollbars($element : Accordion, $value : boolean = true) : void {
            let child : BasePanelHolder = null;
            $element.holders.foreach(($propertyName : string) : void => {
                if ($element.hasOwnProperty($propertyName)) {
                    child = <BasePanelHolder>$element[$propertyName];
                    if (!child.IsScalable() && !(ObjectValidator.IsEmptyOrNull($element.activeHolders.getItem(child.Id())) &&
                        ObjectValidator.IsEmptyOrNull(child.PrioritySize()) && !$value)) {
                        child.getBody().Scrollable($value);
                        BasePanel.scrollBarVisibilityHandler(child.getBody());
                    }
                }
            });
        }

        /**
         * @param {ArrayList<BasePanelHolderViewerArgs>} $args Specify arguments for panel holders,
         * which should be registered by the constructor.
         * @param {AccordionType} [$accordionType] Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($args : ArrayList<BasePanelHolderViewerArgs>, $accordionType? : AccordionType, $id? : string) {
            super($id);

            this.animIterationNum = 10;
            this.guiType = this.guiTypeValueSetter($accordionType);
            this.holders = new ArrayList<string>();
            this.activeHolders = new ArrayList<BasePanelHolder>();
            const holderViewerClass : BasePanelHolderViewer = this.getPanelHolderViewerClass();

            if (!ObjectValidator.IsEmptyOrNull($args)) {
                $args.foreach(($holderArgs : BasePanelHolderViewerArgs, $key? : number) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($holderArgs) && $holderArgs.IsMemberOf(BasePanelHolderViewerArgs)
                        && !ObjectValidator.IsEmptyOrNull($holderArgs.HolderViewerClass())) {
                        this.holders.Add(null, $key.toString());
                        this.addChildPanel(holderViewerClass, $holderArgs, ($parent : Accordion, $child : BasePanelHolder,
                                                                            $childIndex? : number) : void => {
                            const propertyName : string = $child.Id();
                            $parent[propertyName] = $child;
                            $child.IsResizeDelegated(true);
                            $parent.holders.Add(propertyName, $childIndex.toString());
                        });
                    }
                });
            }

            this.horizontalScrollBar.StyleClassName(this.getClassNameWithoutNamespace() + "Bar");
            this.verticalScrollBar.StyleClassName(this.getClassNameWithoutNamespace() + "Bar");
            this.resizeType = AccordionResizeType.DEFAULT;
            this.Scrollable(true);
        }

        public Width($value? : number) : number {
            if (ObjectValidator.IsSet($value) && $value !== super.Width()) {
                super.Width($value);
                if (this.IsCompleted()) {
                    const eventArgs : EventArgs = new EventArgs();
                    eventArgs.Owner(this);
                    Accordion.resizeBody(eventArgs);
                }
            }
            return super.Width();
        }

        public Height($value? : number) : number {
            if (ObjectValidator.IsSet($value) && $value !== super.Height()) {
                super.Height($value);
                if (this.IsCompleted()) {
                    const eventArgs : EventArgs = new EventArgs();
                    eventArgs.Owner(this);
                    Accordion.resizeBody(eventArgs);
                }
            }
            return super.Height();
        }

        /**
         * @param {any} [$accordionType] Specify type of element look and feel.
         * @return {any} Returns type of element's look and feel.
         */
        public GuiType($accordionType ? : AccordionType) : AccordionType {
            if (ObjectValidator.IsSet($accordionType)) {
                this.guiType = this.guiTypeValueSetter($accordionType);
                if (this.IsLoaded()) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                }
            }
            return this.guiType;
        }

        /**
         * @param {ResizeType} [$value] Specify how resizing should be handled.
         * @return {ResizeType} Returns type of resize that should be applied.
         */
        public ResizeType($value? : AccordionResizeType) : AccordionResizeType {
            if (ObjectValidator.IsSet($value)) {
                this.resizeType = $value;
            }
            return this.resizeType;
        }

        /**
         * @param {ArrayList<BasePanelHolderViewerArgs>} $args Specify panel holder's arguments,
         * which should be passed to the registered panel holders.
         * @return {void}
         */
        public setPanelHoldersArgs($args : ArrayList<BasePanelHolderViewerArgs>) : void {
            if (!ObjectValidator.IsEmptyOrNull($args)) {
                let childIndex : number = 0;
                this.holders.foreach(($propertyName : string) : boolean => {
                    if (this.hasOwnProperty($propertyName)) {
                        if ($args.KeyExists(childIndex)) {
                            this.setChildPanelArgs(
                                (<BasePanelHolder>this[$propertyName]).Id(), $args.getItem(childIndex));
                            childIndex++;
                            return true;
                        } else {
                            return false;
                        }
                    }
                });
            }
        }

        /**
         * Clean up all registered panel holders
         * @return {void}
         */
        public Clear() : void {
            this.holders.foreach(($propertyName : string) : void => {
                if (this.hasOwnProperty($propertyName)) {
                    this[$propertyName] = null;
                }
            });
            this.holders.Clear();
        }

        /**
         * Collapses all registered or index/ClassName defined panel holders
         * @return {void}
         */
        public Collapse($filter? : Array<number | ClassName>, $animate : boolean = true) : void {
            Accordion.setOpened(this, false, $filter, $animate);
        }

        /**
         * Opens all registered or index/ClassName defined, panel holders
         * @return {void}
         */
        public Expand($filter? : Array<number | ClassName>, $animate : boolean = true) : void {
            Accordion.setOpened(this, true, $filter, $animate);
        }

        /**
         * Opens single holder by index/ClassName
         * @return {void}
         */
        public ExpandSingle($filter? : number | ClassName, $animate : boolean = true) : void {
            if (!this.getGuiManager().IsActive(this) || !$animate) {
                if (!$animate) {
                    this.StopAnimation();
                }
                let index : number = 0;
                this.holders.foreach(($propertyName : string) : void => {
                    if (this.hasOwnProperty($propertyName)) {
                        const holder : BasePanelHolder = (<BasePanelHolder>this[$propertyName]);
                        if (holder.IsOpened()) {
                            if (!($filter === index || $filter === Reflection.getInstance().getClass(holder.getBody().getClassName()))) {
                                holder.IsOpened(false, $animate);
                            }
                        } else {
                            if (($filter === index || $filter === Reflection.getInstance().getClass(holder.getBody().getClassName()))) {
                                if (this.activeHolders.Length() === 0) {
                                    holder.IsOpened(true, $animate);
                                } else {
                                    this.queue = () : void => {
                                        this.queue = null;
                                        holder.IsOpened(true, $animate);
                                    };
                                }
                            }
                        }
                        index++;
                    }
                });
            } else {
                this.queue = () : void => {
                    this.queue = null;
                    this.ExpandSingle($filter, $animate);
                };
            }
        }

        /**
         * Returns panel tied to index or ClassName
         * @return {void}
         */
        public getPanel($filter : (number | ClassName)) : any {
            let index : number = 0;
            let panel : any = null;
            this.holders.foreach(($propertyName : string) : boolean => {
                if (this.hasOwnProperty($propertyName)) {
                    const holder : BasePanelHolder = (<BasePanelHolder>this[$propertyName]);
                    if ($filter === index || $filter === Reflection.getInstance().getClass(holder.getBody().getClassName())) {
                        panel = holder.getBody();
                        return false;
                    }
                    index++;
                }
            });
            return panel;
        }

        public AnimIterationNum($value? : number) : number {
            if (ObjectValidator.IsSet($value)) {
                this.animIterationNum = Property.PositiveInteger(this.animIterationNum, $value);
                this.holders.foreach(($propertyName : string) : void => {
                    if (this.hasOwnProperty($propertyName)) {
                        const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                        holder.AnimIterationNum($value);
                    }
                });
            }
            return this.animIterationNum;
        }

        protected StopAnimation() : void {
            this.getGuiManager().setActive(this, false);
            this.holders.foreach(($propertyName : string) : void => {
                if (this.hasOwnProperty($propertyName)) {
                    const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                    this.activeHolders.RemoveAt(this.activeHolders.IndexOf(holder));
                    holder.StopAnimation();
                }
            });
        }

        protected getPanelHolderViewerClass() : any {
            switch (this.guiType) {
            case AccordionType.VERTICAL:
                return VerticalPanelHolderViewer;
            case AccordionType.HORIZONTAL:
                return HorizontalPanelHolderViewer;
            default :
                return BasePanelHolderViewer;
            }
        }

        protected guiTypeValueSetter($value : any) : any {
            this.guiType = Property.EnumType(this.guiType, $value, AccordionType, AccordionType.HORIZONTAL);

            if (ObjectValidator.IsSet($value) && !this.IsLoaded()) {
                switch (this.guiType) {
                case AccordionType.HORIZONTAL:
                    this.orientation = OrientationType.HORIZONTAL;
                    break;
                case AccordionType.VERTICAL:
                    this.orientation = OrientationType.VERTICAL;
                    break;
                default:
                    this.orientation = OrientationType.HORIZONTAL;
                }
            }

            return this.guiType;
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!AccordionType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of accordion type instead of StyleClassName method.");
            return false;
        }

        protected cssInterfaceName() : string {
            return Reflection.getInstance().getClass(this.getClassName()).NamespaceName();
        }

        protected innerCode() : IGuiElement {
            let holderIndex : number = 0;
            let completedPanelNum : number = 0;
            const openedHolders : number[] = [];
            const lastHolderIndex : number = this.holders.Length() - 1;

            this.DisableAsynchronousDraw();

            this.holders.foreach(($propertyName : string) : void => {
                if (this.hasOwnProperty($propertyName)) {
                    const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                    holder.getBody().Scrollable(false);

                    if (holder.IsOpened() && holder.CurrentBodySize() === 0) {
                        openedHolders.push(holderIndex);
                        holder.IsOpened(false);
                    }

                    if (!holder.IsLoaded()) {
                        if (holderIndex === 0) {
                            if (ObjectValidator.IsEmptyOrNull(holder.StyleClassName())) {
                                holder.StyleClassName("First");
                            } else {
                                holder.StyleClassName(holder.StyleClassName() + " First");
                            }
                        } else if (holderIndex === lastHolderIndex) {
                            if (ObjectValidator.IsEmptyOrNull(holder.StyleClassName())) {
                                holder.StyleClassName("Last");
                            } else {
                                holder.StyleClassName(holder.StyleClassName() + " Last");
                            }
                        }
                    }

                    if (this.orientation === OrientationType.VERTICAL) {
                        holder.Width(this.Width());
                    } else {
                        holder.Height(this.Height());
                    }

                    holder.getEvents().setOnComplete(() : void => {
                        completedPanelNum++;
                        if (completedPanelNum === this.holders.Length()) {
                            ElementManager.Hide(this.Id() + "_LoaderIcon");
                            const hideLoader : any = () : void => {
                                ElementManager.ChangeOpacity(this.Id() + "_Loader", DirectionType.DOWN, 10,
                                    () : void => {
                                        ElementManager.Hide(this.Id() + "_Loader");
                                    });
                            };
                            if (openedHolders.length !== 0) {
                                this.getEventsManager().FireAsynchronousMethod(() : void => {
                                    hideLoader();
                                    this.Expand.apply(this, openedHolders);
                                }, 100);
                            } else {
                                hideLoader();
                            }
                        }
                    });

                    holder.getEvents().setBeforeOpen(() : void => {
                        this.activeHolders.Add(holder, holder.Id());
                        Accordion.showScrollbars(this, false);
                        this.getGuiManager().setActive(this, true);
                        Accordion.normalizeTargetSize(holder);
                    });

                    holder.getEvents().setBeforeClose(() : void => {
                        this.activeHolders.Add(holder, holder.Id());
                        Accordion.showScrollbars(this, false);
                        this.getGuiManager().setActive(this, true);
                    });

                    const onChangeEnd : any = ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                        const parent : Accordion = $eventArgs.Owner().Parent();

                        const isAnyActive : any = () : boolean => {
                            let isActive : boolean = false;
                            parent.holders.foreach(($propertyName : string) : boolean => {
                                if (this.hasOwnProperty($propertyName)) {
                                    const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                                    if ($manager.IsActive(holder)) {
                                        isActive = true;
                                        return false;
                                    }
                                }
                            });
                            return isActive;
                        };

                        const getFirstQueuedHolder : any = () : BasePanelHolder => {
                            let minTime : number = Number.POSITIVE_INFINITY;
                            let queuedHolder : BasePanelHolder = null;
                            parent.holders.foreach(($propertyName : string) : void => {
                                if (this.hasOwnProperty($propertyName)) {
                                    const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                                    const queue : any = holder.getQueue();
                                    if (!ObjectValidator.IsEmptyOrNull(holder.getQueue())) {
                                        if (queue.time < minTime) {
                                            minTime = queue.time;
                                            queuedHolder = holder;
                                        }
                                    }
                                }
                            });
                            return queuedHolder;
                        };

                        if (!isAnyActive()) {
                            const queuedHolder : BasePanelHolder = getFirstQueuedHolder();
                            parent.getGuiManager().setActive(parent, false);
                            parent.activeHolders.Clear();

                            if (ObjectValidator.IsEmptyOrNull(queuedHolder) && ObjectValidator.IsEmptyOrNull(parent.queue)) {
                                if (parent.Scrollable()) {
                                    BasePanel.scrollBarVisibilityHandler(parent);
                                }
                                Accordion.showScrollbars(parent);
                            }

                            if (!ObjectValidator.IsEmptyOrNull(queuedHolder)) {
                                queuedHolder.getQueue().handler();
                            } else if (ObjectValidator.IsFunction(parent.queue)) {
                                parent.queue();
                            }
                        }

                        this.getEventsManager().FireEvent(this, EventType.ON_CHANGE);
                    };

                    holder.getEvents().setOnOpen(onChangeEnd);
                    holder.getEvents().setOnClose(onChangeEnd);
                    holder.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                        if (this.activeHolders.IsEmpty() || this.activeHolders.getFirst() === holder) {
                            Accordion.resizeBody($eventArgs);
                        }
                    });

                    holderIndex++;
                }
            });

            this.getEvents().setOnShow(Accordion.resizeBody);
            this.getEvents().setOnResize(($eventArgs : ResizeEventArgs) : void => {
                const child : BasePanel = this.getPanel(0);
                if (!ObjectValidator.IsEmptyOrNull(child) &&
                    (this.guiType === AccordionType.VERTICAL && $eventArgs.AvailableWidth() !== child.Width() ||
                        this.guiType === AccordionType.HORIZONTAL && $eventArgs.AvailableHeight() !== child.Height())) {
                    Accordion.resizeBody($eventArgs);
                }
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            const output : IGuiElement = this.addElement(this.Id() + "_Type").StyleClassName(this.guiType).GuiTypeTag(this.getGuiTypeTag());
            let index : number = 0;
            this.holders.foreach(($propertyName : string) : void => {
                if (this.hasOwnProperty($propertyName)) {
                    const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                    output
                        .Add(this.addElement(holder.Id() + "_Separator")
                            .StyleClassName("Separator")
                            .Visible(index > 0)
                        )
                        .Add(holder);
                    index++;
                }
            });
            output.Add(this.addElement(this.Id() + "_Loader").StyleClassName("Loader")
                .Add(this.addElement(this.Id() + "_LoaderBackground").StyleClassName(GeneralCssNames.BACKGROUND))
                .Add(this.addElement(this.Id() + "_LoaderIcon").StyleClassName(GeneralCssNames.ICON)));
            return output;
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();
            this.Width(Accordion.defaultWidth);
            this.Height(Accordion.defaultHeight);
            if (!ObjectValidator.IsSet(this.holders)) {
                this.holders = new ArrayList<string>();
            }
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            if (ObjectValidator.IsEmptyOrNull(this.holders)) {
                exclude.push("holders");
            }
            return exclude;
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon
         */
        protected getLoaderIconClass() : any {
            return null;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel
         */
        protected getLoaderTextClass() : any {
            return null;
        }

    }

    export class ISizeInfo {
        public totalSize : number;
        public parentSize : number;
        public scalableSize : number;
        public mult : number;
    }
}
