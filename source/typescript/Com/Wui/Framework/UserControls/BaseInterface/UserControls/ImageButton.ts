/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import IImageButton = Com.Wui.Framework.Gui.Interfaces.UserControls.IImageButton;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ImageButtonType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import IconType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.IconType;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import ScrollBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ScrollBar;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import DragBar = Com.Wui.Framework.UserControls.BaseInterface.Components.DragBar;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;

    /**
     * ImageButton class renders element with button behaviour, but without possibility to set the element's size.
     */
    export class ImageButton extends Com.Wui.Framework.UserControls.Primitives.FormsObject implements IImageButton {
        private guiType : ImageButtonType;
        private iconType : IconType;
        private isSelected : boolean;

        /**
         * @param {ImageButton} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnOn($element : ImageButton, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!$manager.IsActive($element)
                && !$manager.IsActive(<ClassName>ScrollBar)
                && !$manager.IsActive(<ClassName>ResizeBar)
                && !$manager.IsActive(<ClassName>DragBar)) {
                ElementManager.TurnOn($element.Id() + "_Active");
            }
        }

        /**
         * @param {ImageButton} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnOff($element : ImageButton, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!$manager.IsActive($element)) {
                ElementManager.TurnOff($element.Id() + "_Active");
            }
        }

        /**
         * @param {ImageButton} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnActive($element : ImageButton, $manager? : GuiObjectManager,
                                 $reflection? : Reflection) : void {
            ElementManager.TurnActive($element.Id() + "_Active");
        }

        /**
         * @param {ImageButton} $element Specify element, which should be handled.
         * @param {boolean} [$value] Specify state of button selection.
         * @return {void}
         */
        public static TurnSelected($element : ImageButton, $value : boolean) : void {
            if ($value) {
                ElementManager.Hide($element.Id() + "_Active");
                ElementManager.Show($element.Id() + "_Static");
            } else {
                ElementManager.Hide($element.Id() + "_Static");
                ElementManager.Show($element.Id() + "_Active");
            }
        }

        protected static onHoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void {
            $eventArgs.StopAllPropagation();
            $manager.setHovered($eventArgs.Owner(), true);
        }

        /**
         * @param {ImageButtonType} [$imageButtonType] Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($imageButtonType? : ImageButtonType, $id? : string) {
            super($id);
            this.guiType = this.guiTypeValueSetter($imageButtonType);
        }

        /**
         * @param {ImageButtonType} [$imageButtonType] Specify type of element look and feel.
         * @return {ImageButtonType} Returns type of element's look and feel.
         */
        public GuiType($imageButtonType? : ImageButtonType) : ImageButtonType {
            if (ObjectValidator.IsSet($imageButtonType)) {
                this.guiType = this.guiTypeValueSetter($imageButtonType);
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                }
            }
            return this.guiType;
        }

        /**
         * @param {IconType} [$value] Specify type of icon, which should be rendered with the element.
         * @return {IconType} Returns type of icon, which belongs to the element.
         */
        public IconName($value? : IconType) : IconType {
            if (ObjectValidator.IsSet($value)) {
                this.iconType = $value;
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_IconEnvelop", this.iconType.toString());
                    if (this.getGuiOptions().Contains(GuiOptionType.ACTIVED, GuiOptionType.SELECTED)) {
                        ElementManager.setClassName(this.Id() + "_StaticIconEnvelop", this.iconType.toString());
                    }
                    if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                        ElementManager.setClassName(this.Id() + "_DisabledIconEnvelop", this.iconType.toString());
                    }
                }
            }
            if (!ObjectValidator.IsSet(this.iconType)) {
                this.iconType = "";
            }
            return this.iconType;
        }

        /**
         * @param {boolean} [$value] Specify element's look and feel status.
         * @return {boolean} Returns element's look and feel status.
         */
        public IsSelected($value? : boolean) : boolean {
            this.isSelected = Property.Boolean(this.isSelected, $value);
            if (this.isSelected && !this.IsLoaded()) {
                this.getGuiOptions().Add(GuiOptionType.ACTIVED);
            }
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())
                && this.getGuiOptions().Contains(GuiOptionType.ACTIVED, GuiOptionType.SELECTED)) {
                Reflection.getInstance().getClass(this.getClassName()).TurnSelected(this, this.isSelected);
            }
            if (ObjectValidator.IsEmptyOrNull(this.isSelected)) {
                this.isSelected = false;
            }
            return this.isSelected;
        }

        public IsPreventingScroll() : boolean {
            return true;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.guiType, $value, ImageButtonType, ImageButtonType.GENERAL);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!ImageButtonType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of imageButton type instead of StyleClassName method.");
            return false;
        }

        protected availableGuiOptions() : ArrayList<GuiOptionType> {
            const options : ArrayList<GuiOptionType> = super.availableGuiOptions();
            options.Add(GuiOptionType.ACTIVED);
            options.Add(GuiOptionType.SELECTED);
            return options;
        }

        protected innerCode() : IGuiElement {
            this.getEvents().Subscriber(this.Id() + "_Active");

            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
            this.getEvents().setOnMouseOver(thisClass.onHoverEventHandler);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            const selectedOption : IGuiElement = this.addElement();
            if (this.getGuiOptions().Contains(GuiOptionType.ACTIVED, GuiOptionType.SELECTED)) {
                selectedOption
                    .Id(this.Id() + "_Static").StyleClassName(GeneralCssNames.ON).Visible(this.IsSelected())
                    .Add(this.addElement(this.Id() + "_StaticIconEnvelop")
                        .StyleClassName(this.IconName())
                        .Add(this.addElement(this.Id() + "_StaticIcon").StyleClassName(GeneralCssNames.ICON))
                    );
            }

            const disabledOption : IGuiElement = this.addElement();
            if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                disabledOption
                    .Id(this.Id() + "_Disabled").Visible(!this.Enabled())
                    .Add(this.addElement(this.Id() + "_DisabledIconEnvelop")
                        .StyleClassName(this.IconName())
                        .Add(this.addElement(this.Id() + "_DisabledIcon").StyleClassName(GeneralCssNames.ICON))
                    );
            }

            return this.addElement(this.Id() + "_Type")
                .StyleClassName(this.GuiType()).GuiTypeTag(this.getGuiTypeTag())
                .Add(this.addElement(this.Id() + "_Status")
                    .StyleClassName(this.statusCss())
                    .Add(this.addElement(this.Id() + "_Enabled")
                        .Visible(this.Enabled())
                        .Add(this.addElement(this.Id() + "_Active")
                            .StyleClassName(GeneralCssNames.OFF).Visible(!this.IsSelected())
                            .Add(this.selectorElement())
                            .Add(this.addElement(this.Id() + "_IconEnvelop")
                                .StyleClassName(this.IconName())
                                .Add(this.addElement(this.Id() + "_Icon").StyleClassName(GeneralCssNames.ICON))
                            )
                        )
                        .Add(selectedOption)
                    )
                    .Add(disabledOption)
                );
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push("isSelected", "iconType");
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            if (this.IsSelected() === false) {
                exclude.push("isSelected");
            }
            if (ObjectValidator.IsEmptyOrNull(this.iconType)) {
                exclude.push("iconType");
            }
            return exclude;
        }
    }
}
