/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import DirectoryItem = Com.Wui.Framework.UserControls.Primitives.DirectoryItem;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import TextSelectionManager = Com.Wui.Framework.Gui.Utils.TextSelectionManager;
    import IDirectoryBrowser = Com.Wui.Framework.Gui.Interfaces.UserControls.IDirectoryBrowser;
    import IFileSystemItemProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemItemProtocol;
    import DirectoryBrowserEventType = Com.Wui.Framework.Gui.Enums.Events.DirectoryBrowserEventType;
    import FileSystemItemType = Com.Wui.Framework.Commons.Enums.FileSystemItemType;
    import FileSystemFilter = Com.Wui.Framework.Commons.Utils.FileSystemFilter;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import ScrollBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ScrollBar;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import DragBar = Com.Wui.Framework.UserControls.BaseInterface.Components.DragBar;
    import Parent = Com.Wui.Framework.UserControls.Primitives.BasePanel;
    import IDirectoryBrowserEvents = Com.Wui.Framework.Gui.Interfaces.Events.IDirectoryBrowserEvents;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import IEventsHandler = Com.Wui.Framework.Commons.Interfaces.IEventsHandler;
    import DirectoryBrowserEventArgs = Com.Wui.Framework.Gui.Events.Args.DirectoryBrowserEventArgs;

    /**
     * DirectoryBrowser class renders GUI element suitable for browsing of structure suitable as file system.
     */
    export class DirectoryBrowser extends Parent implements IDirectoryBrowser {
        private static tabPressed : boolean = false;
        private static rollUpEnabled : boolean = false;

        private structure : IFileSystemItemProtocol[];
        private rootItems : ArrayList<DirectoryItem>;
        private path : string;
        private filter : string[];
        private openedItems : ArrayList<string>;
        private visibleItems : ArrayList<DirectoryItem>;
        private selectedItem : DirectoryItem;
        private mouseSelectedItem : DirectoryItem;
        private keyboardSelectedItem : DirectoryItem;
        private verticalScrollBarPosition : number;
        private renameField : TextField;

        /**
         * @param {DirectoryBrowser} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @return {void}
         */
        public static TurnOn($element : DirectoryBrowser, $manager? : GuiObjectManager) : void {
            if (!$manager.IsActive($element)
                && !$manager.IsActive(<ClassName>ScrollBar)
                && !$manager.IsActive(<ClassName>ResizeBar)
                && !$manager.IsActive(<ClassName>DragBar)) {
                $manager.setActive($element, true);
                ElementManager.TurnOn($element.Id() + "_Status");
            }
        }

        /**
         * @param {DirectoryBrowser} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @return {void}
         */
        public static TurnOff($element : DirectoryBrowser, $manager? : GuiObjectManager) : void {
            if (!$manager.IsActive($element) && !$manager.IsActive($element.renameField)) {
                ElementManager.TurnOff($element.Id() + "_Status");
            }
        }

        /**
         * @param {DirectoryBrowser} $element Specify element, which should be handled.
         * @return {void}
         */
        public static TurnActive($element : DirectoryBrowser) : void {
            if (DirectoryBrowser.tabPressed) {
                ElementManager.TurnActive($element.Id() + "_Status");
                DirectoryBrowser.tabPressed = false;
            }
        }

        protected static resize($element : BasePanel, $width : number, $height : number) : void {
            if (Reflection.getInstance().IsMemberOf($element, BasePanel)) {
                const width : number = $width
                    - ElementManager.getCssIntegerValue($element.Id() + "_Status", "border-left-width")
                    - ElementManager.getCssIntegerValue($element.Id() + "_Status", "border-right-width");
                const height : number = $height
                    - ElementManager.getCssIntegerValue($element.Id() + "_Status", "border-top-width")
                    - ElementManager.getCssIntegerValue($element.Id() + "_Status", "border-bottom-width");
                ElementManager.setSize($element.Id() + "_Status", width, height);
                Parent.resize($element, width, height);
            }
        }

        protected static onKeyEventHandler($eventArgs : KeyEventArgs, $manager : GuiObjectManager,
                                           $reflection? : Reflection) : void {
            if (!$manager.IsActive(TextField)) {
                const keyCode : number = $eventArgs.getKeyCode();
                if (keyCode === KeyMap.UP_ARROW ||
                    keyCode === KeyMap.DOWN_ARROW ||
                    keyCode === KeyMap.LEFT_ARROW ||
                    keyCode === KeyMap.RIGHT_ARROW ||
                    keyCode === KeyMap.ENTER ||
                    keyCode === KeyMap.DELETE ||
                    keyCode === KeyMap.F2 ||
                    keyCode === KeyMap.ESC ||
                    $eventArgs.NativeEventArgs().ctrlKey && $eventArgs.getKeyChar() === "N"
                ) {
                    if ($manager.IsActive(<ClassName>DirectoryBrowser)) {
                        $eventArgs.PreventDefault();
                        const elements : ArrayList<DirectoryBrowser> =
                            <ArrayList<DirectoryBrowser>>$manager.getActive(<ClassName>DirectoryBrowser);
                        elements.foreach(($element : DirectoryBrowser) : void => {
                            if (!$element.renameField.Visible()) {
                                if (keyCode === KeyMap.UP_ARROW || keyCode === KeyMap.DOWN_ARROW) {
                                    let position : number = -1;
                                    if (ObjectValidator.IsEmptyOrNull($element.keyboardSelectedItem)) {
                                        $element.keyboardSelectedItem = $element.selectedItem;
                                    }
                                    if (!ObjectValidator.IsEmptyOrNull($element.keyboardSelectedItem)) {
                                        position = $element.visibleItems.IndexOf($element.keyboardSelectedItem);
                                    }
                                    if (keyCode === KeyMap.DOWN_ARROW) {
                                        position++;
                                    } else if (keyCode === KeyMap.UP_ARROW) {
                                        position--;
                                    }
                                    if (DirectoryBrowser.rollUpEnabled) {
                                        if (position < 0) {
                                            position = $element.visibleItems.Length() - 1;
                                        } else if (position >= $element.visibleItems.Length()) {
                                            position = 0;
                                        }
                                        DirectoryBrowser.rollUpEnabled = false;
                                    }
                                    if (position >= 0 && position < $element.visibleItems.Length()) {
                                        if (!ObjectValidator.IsEmptyOrNull($element.mouseSelectedItem)) {
                                            DirectoryItem.TurnOff($element.mouseSelectedItem);
                                        }
                                        if (!ObjectValidator.IsEmptyOrNull($element.keyboardSelectedItem)) {
                                            if (!$element.keyboardSelectedItem.IsSelected()) {
                                                DirectoryItem.TurnOff($element.keyboardSelectedItem);
                                            } else {
                                                DirectoryItem.TurnActive($element.keyboardSelectedItem);
                                            }
                                        }
                                        $element.keyboardSelectedItem = $element.visibleItems.getItem(position);
                                        DirectoryItem.TurnOn($element.keyboardSelectedItem);

                                        const element : HTMLElement = ElementManager.getElement($element.keyboardSelectedItem.Id());
                                        const content : HTMLElement = ElementManager.getElement($element.Id() + "_PanelContentEnvelop");
                                        if ((element.offsetTop + element.offsetHeight - content.scrollTop) > $element.Height()) {
                                            content.scrollTop = element.offsetTop + element.offsetHeight - $element.Height();
                                            DirectoryBrowser.scrollToCurrentPosition($element);
                                        } else if (element.offsetTop - content.scrollTop < 0) {
                                            content.scrollTop = element.offsetTop;
                                            DirectoryBrowser.scrollToCurrentPosition($element);
                                        }
                                    }
                                } else if (keyCode === KeyMap.LEFT_ARROW) {
                                    if (!ObjectValidator.IsEmptyOrNull($element.keyboardSelectedItem) &&
                                        $element.keyboardSelectedItem.IsOpen()) {
                                        $element.select($element.keyboardSelectedItem);
                                    }
                                } else if (keyCode === KeyMap.RIGHT_ARROW || keyCode === KeyMap.ENTER) {
                                    if (!ObjectValidator.IsEmptyOrNull($element.keyboardSelectedItem)) {
                                        $element.select($element.keyboardSelectedItem, !$element.keyboardSelectedItem.IsOpen());
                                    }
                                } else if ($eventArgs.getKeyCode() === KeyMap.DELETE) {
                                    if ($element.selectedItem.Type() === FileSystemItemType.DIRECTORY ||
                                        $element.selectedItem.Type() === FileSystemItemType.FILE ||
                                        $element.selectedItem.Type() === FileSystemItemType.LINK) {
                                        $element.fireEvent(DirectoryBrowserEventType.ON_DELETE_REQUEST, $element.path);
                                    }
                                } else if ($eventArgs.NativeEventArgs().ctrlKey && $eventArgs.getKeyChar() === "N") {
                                    if ($element.selectedItem.Type() === FileSystemItemType.DIRECTORY ||
                                        $element.selectedItem.Type() === FileSystemItemType.FILE ||
                                        $element.selectedItem.Type() === FileSystemItemType.LINK) {
                                        $element.fireEvent(DirectoryBrowserEventType.ON_CREATE_DIRECTORY_REQUEST, $element.path);
                                    }
                                } else if ($eventArgs.getKeyCode() === KeyMap.F2) {
                                    if ($element.selectedItem.Type() === FileSystemItemType.DIRECTORY ||
                                        $element.selectedItem.Type() === FileSystemItemType.FILE ||
                                        $element.selectedItem.Type() === FileSystemItemType.LINK) {
                                        $element.showRenameField($element.selectedItem);
                                    }
                                }
                            }
                        });
                    }
                } else if ($eventArgs.getKeyCode() === KeyMap.TAB) {
                    DirectoryBrowser.tabPressed = true;
                } else {
                    Parent.onKeyEventHandler($eventArgs, $manager, $reflection);
                }
            }
        }

        private static showLoader($element : DirectoryBrowser, $state : boolean) : void {
            if ($element.IsCompleted()) {
                if ($state) {
                    $element.Scrollable(false);
                    DirectoryBrowser.scrollBarVisibilityHandler($element);
                    ElementManager.Show($element.Id() + "_Loader");
                } else {
                    ElementManager.Hide($element.Id() + "_Loader");
                    $element.Scrollable(true);
                    DirectoryBrowser.scrollBarVisibilityHandler($element);
                }
            }
        }

        /**
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($id? : string) {
            super($id);

            this.structure = null;
            this.rootItems = new ArrayList<DirectoryItem>();
            this.visibleItems = new ArrayList<DirectoryItem>();
            this.path = "";
            this.filter = [];
            this.openedItems = new ArrayList<string>();
            this.selectedItem = null;
            this.keyboardSelectedItem = null;
            this.verticalScrollBarPosition = 0;
            let renameFieldClass : any = this.getRenameFieldClass();
            if (ObjectValidator.IsEmptyOrNull(renameFieldClass)) {
                renameFieldClass = TextField;
            }
            this.renameField = new renameFieldClass();
        }

        /**
         * @return {IDirectoryBrowserEvents} Returns events manager subscribed to the item.
         */
        public getEvents() : IDirectoryBrowserEvents {
            return <IDirectoryBrowserEvents>super.getEvents();
        }

        /**
         * @param {string} [$value] Specify selected path value.
         * @return {string} Returns value of currently selected path.
         */
        public Value($value? : string) : string {
            return this.Path($value);
        }

        /**
         * @param {string} [$value] Specify selected path value.
         * @return {string} Returns value of currently selected path.
         */
        public Path($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                this.path = Property.String(this.path, $value);
                this.path = StringUtils.Replace(this.path, "\\", "/");
                const pathRequest : IEventsHandler = () : void => {
                    this.fireEvent(DirectoryBrowserEventType.ON_PATH_REQUEST, this.path);
                };
                if (this.IsCompleted()) {
                    let pathExists : boolean = false;
                    const validatePath : any = ($item : DirectoryItem) : boolean => {
                        if (!ObjectValidator.IsEmptyOrNull($item)) {
                            if ($item.Value() !== this.path) {
                                $item.getChildren().foreach(($item : DirectoryItem) : void => {
                                    validatePath($item);
                                });
                            } else {
                                pathExists = true;
                                this.select($item, !$item.IsOpen(), false);
                            }
                        }
                        return !pathExists;
                    };
                    this.rootItems.foreach(($item : DirectoryItem) : boolean => {
                        validatePath($item);
                        return !pathExists;
                    });
                    if (!pathExists) {
                        this.verticalScrollBarPosition = this.getScrollTop();
                        DirectoryBrowser.showLoader(this, true);
                        pathRequest();
                    }
                } else {
                    this.getEvents().setOnComplete(pathRequest);
                }
            }
            return this.path;
        }

        /**
         * @param {...(string|FileSystemFilter|FileSystemItemType)} $value Specify filter attributes,
         * which should be applied on current structure.
         * @return {string} Returns value of currently applied filter.
         */
        public Filter(...$value : Array<string | FileSystemFilter | FileSystemItemType>) : string {
            if (ObjectValidator.IsSet($value)) {
                const value : string = $value.join("|");
                if (ObjectValidator.IsEmptyOrNull(value) || value === "*" || value === "*.*") {
                    this.filter = [];
                } else {
                    this.filter = StringUtils.Split(value, "|");
                }
                this.generateItems();
            }
            return this.filter.join("|");
        }

        /**
         * @param {IFileSystemItemProtocol[]} $data Specify data structure, which should be rendered.
         * @param {string} [$path] Specify path value, where should be data inserted.
         * @return {void}
         */
        public setStructure($data : IFileSystemItemProtocol[], $path? : string) : void {
            if (ObjectValidator.IsEmptyOrNull(this.structure) || ObjectValidator.IsEmptyOrNull($path)) {
                this.structure = $data;
                this.openedItems.Clear();
                this.generateItems();
            } else {
                let generate : boolean = false;
                const dataLookup : ($structure : IFileSystemItemProtocol[], $structurePath : string) => void =
                    ($structure : IFileSystemItemProtocol[], $structurePath : string) : void => {
                        if (ObjectValidator.IsNativeArray($structure)) {
                            $structure.forEach(($item : IFileSystemItemProtocol) : boolean => {
                                let path : string = $structurePath + "/" + $item.name;
                                if ($item.type === FileSystemItemType.DRIVE) {
                                    path = $item.value;
                                }
                                if (($item.type === FileSystemItemType.DRIVE ? path + "/" : path) === $path) {
                                    $item.map = $data;
                                    generate = true;
                                    return false;
                                }

                                if (!ObjectValidator.IsEmptyOrNull($item.map)) {
                                    dataLookup(<IFileSystemItemProtocol[]>$item.map, path);
                                }
                                return true;
                            });
                        }
                    };
                dataLookup(this.structure, "");
                if (generate) {
                    this.generateItems();
                } else {
                    this.resizeContent();
                    DirectoryBrowser.showLoader(this, false);
                }
            }
        }

        /**
         * Clean up currently rendered structure.
         * @return {void}
         */
        public Clear() : void {
            ElementManager.setInnerHtml(this.Id() + "_Items", "");
            this.rootItems.Clear();
            this.openedItems.Clear();
            DirectoryBrowser.showLoader(this, false);
        }

        protected cssInterfaceName() : string {
            return this.getNamespaceName();
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.getEvents().setOnLoad(() : void => {
                const element : HTMLElement = ElementManager.getElement(this.Id() + "_Status").parentElement;
                element.parentElement.parentElement.insertBefore(element, element.parentElement.parentElement.firstChild);
            });
            this.getEvents().setOnComplete(() : void => {
                DirectoryBrowser.showLoader(this, true);
                this.generateItems();
            });
            this.getEvents().setOnShow(() : void => {
                ElementManager.Show(this.Id() + "_Status");
                DirectoryBrowser.scrollBarVisibilityHandler(this);
            });
            this.getEvents().setOnHide(() : void => {
                ElementManager.Hide(this.Id() + "_Status");
            });
            this.getEvents().setOnMouseDown(() : void => {
                DirectoryBrowser.tabPressed = false;
            });

            WindowManager.getEvents().setOnMouseDown(
                ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    DirectoryBrowser.tabPressed = false;
                    if ($manager.IsActive(this)
                        && !$manager.IsActive(<ClassName>ScrollBar)
                        && !$manager.IsActive(<ClassName>ResizeBar)
                        && !$manager.IsActive(<ClassName>DragBar)) {
                        $manager.setActive(this, false);
                        const eventArgs : EventArgs = new EventArgs();
                        eventArgs.Owner(this);
                        this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_BLUR, eventArgs);
                        this.getEventsManager().FireEvent(this, EventType.ON_BLUR, eventArgs);
                        const thisClass : any = $reflection.getClass(this.getClassName());
                        thisClass.TurnOff(this, $manager, $reflection);
                    }
                });

            WindowManager.getEvents().setOnKeyUp(() : void => {
                DirectoryBrowser.rollUpEnabled = true;
            });

            this.renameField.DisableAsynchronousDraw();
            this.renameField.setAutocompleteDisable();
            this.renameField.LengthLimit(256);
            this.renameField.Visible(false);
            this.renameField.getEvents().setEvent(EventType.ON_KEY_PRESS, ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                const eventArgs : KeyEventArgs = new KeyEventArgs(<KeyboardEvent>$eventArgs.NativeEventArgs());
                if (eventArgs.getKeyCode() === KeyMap.ENTER) {
                    this.renameConfirm();
                    $manager.setHovered(this, true);
                    $manager.setActive(this, true);
                } else if (eventArgs.getKeyCode() === KeyMap.ESC) {
                    this.hideRenameField();
                    $manager.setHovered(this, true);
                    $manager.setActive(this, true);
                } else {
                    const key : string = eventArgs.getKeyChar();
                    if (key === "\\" || key === "/" || key === ":" || key === "*" || key === "?" || key === "\"" || key === "<" ||
                        key === ">" || key === "|") {
                        eventArgs.PreventDefault();
                    }
                }
            });
            this.renameField.getSelectorEvents().setOnBlur(() : void => {
                this.renameConfirm();
            });

            return super.innerCode().Add(this.selectorElement()).Add(this.renameField);
        }

        protected innerHtml() : IGuiElement {
            return this.addElement().Add(this.addElement(this.Id() + "_Items").StyleClassName("Items"));
        }

        protected guiContent() : IGuiElement {
            return this.addElement()
                .Add(this.addElement().StyleClassName(this.cssContainerName())
                    .Add(this.addElement(this.Id() + "_Status").StyleClassName(GeneralCssNames.OFF))
                )
                .Add(super.guiContent())
                .Add(this.addElement(this.Id() + "_Loader").StyleClassName("Loader")
                    .Add(this.addElement(this.Id() + "_LoaderBackground").StyleClassName(GeneralCssNames.BACKGROUND))
                    .Add(this.addElement(this.Id() + "_LoaderIcon").StyleClassName(GeneralCssNames.ICON))
                );
        }

        /**
         * @return {ITextField} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ITextField
         */
        protected getRenameFieldClass() : any {
            return TextField;
        }

        private generateItems() : void {
            if (this.IsCompleted()) {
                this.rootItems.Clear();
                if (!ObjectValidator.IsEmptyOrNull(this.structure)) {
                    DirectoryBrowser.showLoader(this, true);

                    let itemIndex : number = 0;
                    const applyFilter : boolean = !ObjectValidator.IsEmptyOrNull(this.filter);
                    const dataLookup : ($data : IFileSystemItemProtocol[], $nesting : number, $path : string,
                                        $parent : DirectoryItem) => void =
                        ($data : IFileSystemItemProtocol[], $nesting : number, $path : string, $parent : DirectoryItem) : void => {
                            if (ObjectValidator.IsNativeArray($data)) {
                                const groups : ArrayList<IFileSystemItemProtocol> = new ArrayList<IFileSystemItemProtocol>();
                                const directories : ArrayList<IFileSystemItemProtocol> = new ArrayList<IFileSystemItemProtocol>();
                                const files : ArrayList<IFileSystemItemProtocol> = new ArrayList<IFileSystemItemProtocol>();

                                $data.forEach(($item : IFileSystemItemProtocol) : void => {
                                    let passed : boolean = !applyFilter;
                                    if (!passed) {
                                        passed = this.filter.indexOf(<string>$item.type) !== -1;
                                    }
                                    if (passed && $item.type === FileSystemItemType.FILE && applyFilter) {
                                        passed = false;
                                        this.filter.forEach(($pattern : string) : boolean => {
                                            passed = StringUtils.PatternMatched($pattern, $item.name);
                                            return !passed;
                                        });
                                    }
                                    let isHidden : boolean = false;
                                    if (passed && !ObjectValidator.IsEmptyOrNull($item.attributes)) {
                                        isHidden = $item.attributes.indexOf(FileSystemItemType.HIDDEN) !== -1;
                                        passed =
                                            !isHidden ||
                                            (isHidden && (!applyFilter || this.filter.indexOf(FileSystemItemType.HIDDEN) !== -1));
                                    }
                                    if (passed) {
                                        passed =
                                            $item.type !== FileSystemItemType.DRIVE ||
                                            $item.type === FileSystemItemType.DRIVE
                                            && !ObjectValidator.IsEmptyOrNull($item.value);
                                    }
                                    if (passed) {
                                        if ($item.type === FileSystemItemType.DIRECTORY) {
                                            directories.Add($item, StringUtils.ToLowerCase($item.name));
                                        } else if ($item.type === FileSystemItemType.FILE || $item.type === FileSystemItemType.LINK) {
                                            files.Add($item, StringUtils.ToLowerCase($item.name));
                                        } else {
                                            groups.Add($item);
                                        }
                                    }
                                });

                                $data = [];
                                let dataIndex : number = 0;
                                const itemHandler : ($item : IFileSystemItemProtocol) => void =
                                    ($item : IFileSystemItemProtocol) : void => {
                                        $data[dataIndex++] = $item;
                                    };
                                groups.foreach(itemHandler);
                                directories.SortByKeyUp();
                                directories.foreach(itemHandler);
                                files.SortByKeyUp();
                                files.foreach(itemHandler);

                                $data.forEach(($item : IFileSystemItemProtocol) : void => {
                                    const newItem : DirectoryItem = new DirectoryItem($item.name, this, itemIndex++, $nesting);
                                    let path : string = $path + "/" + $item.name;
                                    if ($item.type === FileSystemItemType.DRIVE) {
                                        path = $item.value;
                                        newItem.Value(path + "/");
                                        newItem.Text($item.name + " (" + $item.value + ")");
                                    } else if (!ObjectValidator.IsEmptyOrNull($item.value)) {
                                        newItem.Value($item.value);
                                    } else {
                                        newItem.Value(path);
                                    }
                                    newItem.Type($item.type);
                                    if (!ObjectValidator.IsEmptyOrNull($item.attributes)) {
                                        newItem.IsHidden(
                                            $item.attributes.indexOf(FileSystemItemType.HIDDEN) !== -1 ||
                                            $item.attributes.indexOf(FileSystemItemType.PERMISSION_DENIED) !== -1);
                                    }

                                    let clickCount : number = 0;
                                    newItem.getEvents().setOnClick(() : void => {
                                        clickCount++;
                                        this.getEvents().FireAsynchronousMethod(() : void => {
                                            if (clickCount > 1) {
                                                this.select(newItem, false);
                                                if (newItem.Type() === FileSystemItemType.DIRECTORY ||
                                                    newItem.Type() === FileSystemItemType.FILE ||
                                                    newItem.Type() === FileSystemItemType.LINK) {
                                                    this.showRenameField(newItem);
                                                }
                                            } else {
                                                this.select(newItem, newItem.Type() !== FileSystemItemType.READONLY || !newItem.IsOpen());
                                                TextSelectionManager.Clear();
                                            }
                                            clickCount = 0;
                                            // todo: add element inside directory browser item that will show on hover?
                                            // - single click delay needs to be removed
                                        }, true, newItem.Type() === FileSystemItemType.READONLY ? 0 : 250);
                                    });

                                    newItem.getEvents().setOnDoubleClick(() : void => {
                                        if (newItem.Type() === FileSystemItemType.READONLY) {
                                            this.getEvents().FireAsynchronousMethod(() : void => {
                                                this.select(newItem, newItem.IsOpen(), false);
                                            }, 50);
                                        }
                                    });

                                    newItem.getEvents().setOnMouseOver(() : void => {
                                        if (!ObjectValidator.IsEmptyOrNull(this.keyboardSelectedItem)) {
                                            if (!this.keyboardSelectedItem.IsSelected()) {
                                                DirectoryItem.TurnOff(this.keyboardSelectedItem);
                                            } else {
                                                DirectoryItem.TurnActive(this.keyboardSelectedItem);
                                            }
                                        }
                                        if (!newItem.IsSelected()) {
                                            this.mouseSelectedItem = newItem;
                                        }
                                        this.keyboardSelectedItem = null;
                                    });

                                    newItem.IsOpen(this.openedItems.Contains(newItem.Value()));

                                    if (!ObjectValidator.IsEmptyOrNull($parent)) {
                                        newItem.Parent($parent);
                                        $parent.AddChild(newItem);
                                    } else {
                                        this.rootItems.Add(newItem);
                                    }

                                    if (StringUtils.StartsWith(this.path, path)) {
                                        const openPath : any = ($item : DirectoryItem) : void => {
                                            if (!ObjectValidator.IsEmptyOrNull($item)) {
                                                $item.IsOpen(true);
                                                if (!this.openedItems.Contains($item.Value())) {
                                                    this.openedItems.Add($item.Value());
                                                }
                                                openPath($item.Parent());
                                            }
                                        };

                                        if (this.path === path || this.path === path + "/") {
                                            newItem.IsSelected(true);
                                            this.selectedItem = newItem;
                                            this.keyboardSelectedItem = this.selectedItem;
                                        }
                                        openPath(newItem);
                                    }

                                    if (!ObjectValidator.IsEmptyOrNull($item.map)) {
                                        if (<string>$item.map === "***") {
                                            newItem.AddChild(new DirectoryItem("***", this, itemIndex++, $nesting + 1));
                                        } else {
                                            dataLookup(<IFileSystemItemProtocol[]>$item.map, $nesting + 1, path, newItem);
                                        }
                                    }
                                });
                            }
                        };
                    dataLookup(this.structure, 0, "", null);
                }

                this.resizeContent();
            }
        }

        private collectVisibleItems() : void {
            this.visibleItems.Clear();
            this.rootItems.foreach(($item : DirectoryItem) : void => {
                this.visibleItems.Add($item);
                if ($item.IsOpen()) {
                    const addChild : any = ($item : DirectoryItem) : void => {
                        if ($item.IsOpen()) {
                            $item.getChildren().foreach(($child : DirectoryItem) : void => {
                                this.visibleItems.Add($child);
                                addChild($child);
                            });
                        }
                    };
                    $item.getChildren().foreach(($child : DirectoryItem) : void => {
                        this.visibleItems.Add($child);
                        addChild($child);
                    });
                }
            });
            this.visibleItems.Reindex();
        }

        private resizeContent() : void {
            const target : HTMLElement = ElementManager.getElement(this.Id() + "_Items");
            ElementManager.setInnerHtml(target, "");
            if (!this.rootItems.IsEmpty()) {
                if (this.renameField.IsCompleted() && this.renameField.Visible()) {
                    this.renameField.Visible(false);
                }
                this.collectVisibleItems();

                let width : number = this.Width();
                this.rootItems.foreach(($item : DirectoryItem) : void => {
                    $item.AppendTo(target, this.outputEndOfLine + "                ");
                    const itemWidth : number = $item.getWidth() + 1;
                    if (width < itemWidth) {
                        width = itemWidth;
                    }
                });

                ElementManager.setWidth(this.Id() + "_PanelContent", width);
                DirectoryBrowser.showLoader(this, false);
                DirectoryBrowser.scrollTop(this, this.verticalScrollBarPosition);
                this.scrollToSelected();
            }
        }

        private select($item : DirectoryItem, $handleChildren : boolean = true, $fireEvent : boolean = true) : void {
            let parent : DirectoryItem = $item.Parent();
            while (!ObjectValidator.IsEmptyOrNull(parent)) {
                parent.IsOpen(true);
                parent = parent.Parent();
            }

            let directoryRequest : boolean = false;
            if ($handleChildren) {
                const elementHash : string = $item.Value();
                if ($item.IsOpen()) {
                    this.openedItems.RemoveAt(this.openedItems.IndexOf(elementHash));
                    $item.IsOpen(false);
                } else {
                    $item.IsOpen(true);
                    if (!this.openedItems.Contains(elementHash)) {
                        this.openedItems.Add(elementHash);
                        if ($item.Type() !== FileSystemItemType.FILE &&
                            $item.Type() !== FileSystemItemType.LINK &&
                            $item.Type() !== FileSystemItemType.RECENT &&
                            $item.Type() !== FileSystemItemType.READONLY) {
                            directoryRequest = true;
                        }
                    }
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(this.selectedItem)) {
                this.selectedItem.IsSelected(false);
            }
            $item.IsSelected(true);
            this.selectedItem = $item;
            this.keyboardSelectedItem = this.selectedItem;
            DirectoryBrowser.scrollBarVisibilityHandler(this);
            if ($fireEvent) {
                if (!this.rootItems.Contains($item)) {
                    this.path = $item.Value();
                    const eventArgs : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
                    eventArgs.Owner(this);
                    eventArgs.Value(this.path);
                    this.getEventsManager().FireEvent(this, EventType.ON_CHANGE, eventArgs);
                    this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_CHANGE, eventArgs);
                    if (directoryRequest) {
                        this.verticalScrollBarPosition = this.getScrollTop();
                        DirectoryBrowser.showLoader(this, true);
                        this.getEventsManager().FireEvent(this, DirectoryBrowserEventType.ON_DIRECTORY_REQUEST, eventArgs);
                        this.getEventsManager().FireEvent(this.getClassName(), DirectoryBrowserEventType.ON_DIRECTORY_REQUEST, eventArgs);
                    }
                }
            }
            this.collectVisibleItems();
            DirectoryBrowser.scrollBarVisibilityHandler(this);
            this.scrollToSelected();
        }

        private scrollToSelected() : void {
            // todo : scrolls to bottom of list - undesired in certain cases
            // if (!ObjectValidator.IsEmptyOrNull(this.selectedItem)) {
            //     const content : HTMLElement = ElementManager.getElement(this.Id() + "_PanelContentEnvelop");
            //     if (ElementManager.Exists(content)) {
            //         const element : HTMLElement = ElementManager.getElement(this.selectedItem.Id());
            //         if (!ObjectValidator.IsEmptyOrNull(element)) {
            //             if ((element.offsetTop + element.offsetHeight - content.scrollTop) > this.Height()) {
            //                 content.scrollTop = element.offsetTop + element.offsetHeight - this.Height();
            //                 DirectoryBrowser.scrollToCurrentPosition(this);
            //             } else if (element.offsetTop - content.scrollTop < 0) {
            //                 content.scrollTop = element.offsetTop;
            //                 DirectoryBrowser.scrollToCurrentPosition(this);
            //             }
            //         }
            //     }
            // }
        }

        private showRenameField($item : DirectoryItem) : void {
            const field : HTMLElement = <HTMLElement>ElementManager.getElement(this.renameField, true)
                .parentNode.parentNode;
            const item : HTMLElement = ElementManager.getElement($item.Id() + "_Text");
            item.parentNode.appendChild(field.cloneNode(true));
            if (!ObjectValidator.IsEmptyOrNull(field.parentNode)) {
                field.parentNode.removeChild(field);
            }
            ElementManager.Hide(item);
            ElementManager.CleanElementCache(this.renameField.Id() + "*");
            const onShowHandler : any = () : void => {
                this.renameField.Width(this.Width() - $item.getTextOffset());
                this.renameField.getEvents().Subscribe();
                this.renameField.getSelectorEvents().Subscribe();
                this.renameField.Value($item.Text());
                DirectoryBrowser.scrollBarVisibilityHandler(this);
                TextSelectionManager.SelectAll(this.renameField.Id() + "_Input");
                this.getEvents().FireAsynchronousMethod(() : void => {
                    ElementManager.getElement(this.renameField.Id() + "_Input").focus();
                });
            };
            if (!this.renameField.IsCompleted()) {
                this.renameField.getEvents().setOnComplete(() : void => {
                    onShowHandler();
                });
                this.renameField.Visible(true);
            } else {
                this.renameField.Visible(true);
                onShowHandler();
            }
        }

        private hideRenameField() : void {
            this.renameField.Visible(false);
            ElementManager.Show(this.selectedItem.Id() + "_Text");
            DirectoryBrowser.scrollBarVisibilityHandler(this);
        }

        private renameConfirm() : void {
            if (this.renameField.Visible()) {
                if (this.selectedItem.Text() !== this.renameField.Value()
                    && !StringUtils.Contains(this.renameField.Value(), "\\", "/", ":", "*", "?", "\"", "<", ">", "|")) {
                    this.fireEvent(DirectoryBrowserEventType.ON_RENAME_REQUEST, (<any>(this.renameField.Value() + "")).trim());
                } else {
                    this.hideRenameField();
                }
            }
        }

        private fireEvent($type : DirectoryBrowserEventType, $value : string) : void {
            const eventArgs : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
            eventArgs.Owner(this);
            eventArgs.Value($value);
            DirectoryBrowser.showLoader(this, true);
            this.getEventsManager().FireEvent(this, <string>$type, eventArgs);
            this.getEventsManager().FireEvent(this.getClassName(), <string>$type, eventArgs);
        }
    }
}
