/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import IDialog = Com.Wui.Framework.Gui.Interfaces.UserControls.IDialog;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import DialogType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.DialogType;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import DragBar = Com.Wui.Framework.UserControls.BaseInterface.Components.DragBar;
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import IllegalArgumentException = Com.Wui.Framework.Commons.Exceptions.Type.IllegalArgumentException;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import DialogEventType = Com.Wui.Framework.Gui.Enums.Events.DialogEventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import ResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeEventArgs;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import ResizeBarEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeBarEventArgs;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import Borders = Com.Wui.Framework.Gui.Structures.Borders;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import ScrollBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ScrollBar;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IDialogEvents = Com.Wui.Framework.Gui.Interfaces.Events.IDialogEvents;
    import IArrayList = Com.Wui.Framework.Commons.Interfaces.IArrayList;

    /**
     * Dialog class renders floating panel container with modal or modeless behaviour.
     */
    export class Dialog extends Com.Wui.Framework.UserControls.Primitives.FormsObject implements IDialog {
        private static minWidth : number = 100;
        private static minHeight : number = 100;
        private static tabPressed : boolean = false;
        private static modalOpened : boolean = false;

        public closeButton : ImageButton;
        public headerIcon : Icon;
        public headerText : Label;

        protected top : number;
        protected left : number;
        private guiType : DialogType;
        private isModal : boolean;
        private isDraggable : boolean;
        private topOffset : number;
        private width : number;
        private height : number;
        private maxWidth : number;
        private maxHeight : number;
        private startWidth : number;
        private startHeight : number;
        private autoResize : boolean;
        private autoCenter : boolean;
        private readonly cornerResizeBar : ResizeBar;
        private rightResizeBar : ResizeBar;
        private bottomResizeBar : ResizeBar;
        private dragBar : DragBar;
        private panelViewerInstance : BasePanelViewer;
        private scrollable : boolean;
        private backgroundEvents : ElementEventsManager;

        /**
         * Move the specified dialog to the window center.
         * @param {Dialog} $element Specify element, which should be handled.
         * @return {void}
         */
        public static Center($element : Dialog) : void {
            const id : string = $element.Id();
            const dialogSize : Size = new Size(id + "_Envelop", true);
            const windowSize : Size = WindowManager.getSize();

            if ($element.TopOffset() === -1 || $element.Modal()) {
                $element.TopOffset(Math.ceil(windowSize.Height() / 2 - dialogSize.Height() / 2));
                $element.top = $element.TopOffset();
            }

            let top : number = $element.top;
            let left : number = Math.ceil(windowSize.Width() / 2 - dialogSize.Width() / 2);

            if (!$element.Modal()) {
                const elements : ArrayList<IGuiCommons> = $element.getGuiManager().getType(Dialog);
                elements.foreach(($element : Dialog) : void => {
                    if ($element.Id() !== id && $element.Visible() && !$element.Modal()) {
                        if (top === $element.top) {
                            top += 25;
                            if (top + $element.Height() > windowSize.Height() - 50) {
                                top -= 50;
                            }
                        }
                        if (top < 25) {
                            top = 25;
                        }
                        if (left === $element.left) {
                            left += 25;
                            if (left + $element.Width() > windowSize.Width() - 50) {
                                left -= 50;
                            }
                        }
                        if (left < 25) {
                            left = 25;
                        }
                    }
                });
                $element.TopOffset(top);
            }
            ElementManager.setCssProperty(id + "_Envelop", "top", top);
            ElementManager.setCssProperty(id + "_Envelop", "left", left);

            $element.top = top;
            $element.left = left;
        }

        /**
         * Open/Show the specified dialog.
         * @param {Dialog} $element Specify element, which should be handled.
         * @return {void}
         */
        public static Open($element : Dialog) : void {
            if (!$element.getGuiManager().IsActive($element)) {
                if (!Dialog.modalOpened) {
                    if ($element.Enabled()) {
                        $element.Visible(true);
                        const eventArgs : EventArgs = new EventArgs();
                        eventArgs.Owner($element);
                        $element.getEventsManager().FireEvent($element, DialogEventType.ON_OPEN, eventArgs);
                        $element.getEventsManager().FireEvent(Dialog.ClassName(), DialogEventType.ON_OPEN, eventArgs);
                        if (!ObjectValidator.IsEmptyOrNull($element.PanelViewer())
                            && !ObjectValidator.IsEmptyOrNull($element.PanelViewer().getInstance()) &&
                            $element.PanelViewer().getInstance().IsLoaded()) {
                            $element.getGuiManager().setHovered($element.PanelViewer().getInstance(), true);
                        }
                    }
                } else {
                    if ($element.IsLoaded()) {
                        ElementManager.Hide($element.Id());
                    }
                }
            }
        }

        /**
         * Close/Hide dialog with desired element id or all opened dialogs.
         * @param {Dialog} [$element] Specify element, which should be handled.
         * @return {void}
         */
        public static Close($element? : Dialog) : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            const reflection : Reflection = Reflection.getInstance();
            const hideDialog : any = ($element : Dialog) : void => {
                const id : string = $element.Id();
                ElementManager.Hide(id);
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner($element);
                $element.getEventsManager().FireEvent($element, DialogEventType.ON_CLOSE, eventArgs);
                $element.getEventsManager().FireEvent(Dialog.ClassName(), DialogEventType.ON_CLOSE, eventArgs);
                manager.setActive($element, false);
                reflection.getClass($element.getClassName()).TurnOff($element, manager, reflection);
                if ($element.Modal()) {
                    Dialog.modalOpened = false;
                }
                document.body.style.overflow = "visible";
                if (!ObjectValidator.IsEmptyOrNull($element.PanelViewer())) {
                    manager.setHovered($element.PanelViewer().getInstance(), false);
                }
            };

            if (!ObjectValidator.IsEmptyOrNull($element)) {
                hideDialog($element);
            } else {
                const elements : ArrayList<IGuiCommons> = manager.getActive(<ClassName>Dialog);
                elements.foreach(($element : Dialog) : void => {
                    hideDialog($element);
                });
            }
        }

        /**
         * @param {Dialog} $element Specify element, which should be handled.
         * @return {void}
         */
        public static Show($element : Dialog) : void {
            if (!$element.getGuiManager().IsActive($element)) {
                const id : string = $element.Id();
                const thisClass : any = Reflection.getInstance().getClass($element.getClassName());
                if (!Dialog.modalOpened) {
                    if ($element.Enabled()) {
                        ElementManager.Show(id);
                        thisClass.Focus($element);
                        Dialog.modalOpened = $element.Modal();

                        if (!$element.IsLoaded()) {
                            const contentSize : Size = new Size(id + "_Content", true);
                            if ($element.Width() === -1) {
                                $element.Width(contentSize.Width());
                            }
                            if ($element.Height() === -1) {
                                $element.Height(contentSize.Height());
                            }

                            const windowSize : Size = WindowManager.getSize();
                            ElementManager.setCssProperty(id + "_Envelop", "top", (-1) * windowSize.Height());
                            ElementManager.setCssProperty(id + "_Envelop", "left", (-1) * windowSize.Width());

                            if (ObjectValidator.IsEmptyOrNull($element.PanelViewer()) ||
                                (!ObjectValidator.IsEmptyOrNull($element.PanelViewer())
                                    && !ObjectValidator.IsEmptyOrNull($element.PanelViewer().getInstance())
                                    && !$element.PanelViewer().getInstance().IsLoaded())) {
                                $element.getEvents().FireAsynchronousMethod(() : void => {
                                    if ($element.AutoResize()) {
                                        ElementManager.setCssProperty($element.Id() + "_Envelop", "top", 0);
                                        ElementManager.setCssProperty($element.Id() + "_Envelop", "left", 0);
                                        Dialog.resize($element, $element.Width(), $element.Height());
                                    }
                                    if ($element.AutoCenter()) {
                                        thisClass.Center($element);
                                    }
                                    if (!$element.Modal() && $element.top !== -1 && $element.left !== -1) {
                                        ElementManager.setCssProperty(id + "_Envelop", "top", $element.top);
                                        ElementManager.setCssProperty(id + "_Envelop", "left", $element.left);
                                    }
                                }, false);
                            }
                        } else if ($element.Modal()) {
                            document.body.style.overflowX = "hidden";
                            document.body.style.overflowY = "hidden";
                            ElementManager.setOpacity(id + "_Background", 10);
                            ElementManager.ChangeOpacity(id + "_Background", DirectionType.UP, 3);
                        }
                        const eventArgs : EventArgs = new EventArgs();
                        eventArgs.Owner($element);
                        $element.getEventsManager().FireEvent($element, EventType.ON_SHOW, eventArgs);
                        $element.getEventsManager().FireEvent(Dialog.ClassName(), EventType.ON_SHOW, eventArgs);
                    }
                }
            }
        }

        /**
         * @param {GuiCommons} $element Specify element, which should be handled.
         * @return {void}
         */
        public static Hide($element : Dialog) : void {
            Reflection.getInstance().getClass($element.getClassName()).Close($element);
            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner($element);
            $element.getEventsManager().FireEvent($element, EventType.ON_HIDE, eventArgs);
            $element.getEventsManager().FireEvent(Dialog.ClassName(), EventType.ON_HIDE, eventArgs);
        }

        /**
         * @param {Dialog} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnOn($element : Dialog, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!$manager.IsActive($element)
                && !$manager.IsActive(<ClassName>ScrollBar)
                && !$manager.IsActive(<ClassName>ResizeBar)
                && !$manager.IsActive(<ClassName>DragBar)) {
                ElementManager.TurnOn($element.Id() + "_Status");
            }
        }

        /**
         * @param {Dialog} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnOff($element : Dialog, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!$manager.IsActive($element)) {
                ElementManager.TurnOff($element.Id() + "_Status");
            }
        }

        /**
         * @param {Dialog} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnActive($element : Dialog, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (Dialog.tabPressed) {
                ElementManager.TurnActive($element.Id() + "_Status");
                Dialog.tabPressed = false;
            }
        }

        /**
         * @param {Dialog} $element Specify element, which should be handled.
         * @return {void}
         */
        public static Focus($element : Dialog) : void {
            const reflection : Reflection = Reflection.getInstance();
            const thisClass : any = reflection.getClass($element.getClassName());
            thisClass.Blur();
            const manager : GuiObjectManager = $element.getGuiManager();
            if ($element.Enabled() && (!$element.Modal() && !Dialog.modalOpened || !Dialog.modalOpened)) {
                ElementManager.TopMost($element.Id());
                if (!manager.IsActive(<ClassName>Dialog)) {
                    thisClass.TurnActive($element, manager, reflection);
                    manager.setActive($element, true);
                }
            }
        }

        /**
         * @return {void}
         */
        public static Blur() : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            const reflection : Reflection = Reflection.getInstance();
            const elements : ArrayList<IGuiCommons> = manager.getActive(<ClassName>Dialog);
            elements.foreach(($element : Dialog) : void => {
                if (!$element.Modal()) {
                    manager.setActive($element, false);
                    reflection.getClass($element.getClassName()).TurnOff($element, manager, reflection);
                    if (!manager.IsActive($element.dragBar)
                        && !manager.IsActive($element.cornerResizeBar)
                        && !manager.IsActive($element.rightResizeBar)
                        && !manager.IsActive($element.bottomResizeBar)) {
                        ElementManager.SendToBack($element.Id());
                    }
                }
            });
        }

        protected static onHoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void {
            // do not handle hover event
        }

        protected static onKeyEventHandler($eventArgs : KeyEventArgs, $manager : GuiObjectManager, $reflection? : Reflection) : void {
            if ($eventArgs.getKeyCode() === KeyMap.ESC) {
                if ($manager.IsActive(<ClassName>Dialog)) {
                    const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>Dialog);
                    elements.foreach(($element : Dialog) : void => {
                        $reflection.getClass($element.getClassName()).Close($element);
                        $eventArgs.PreventDefault();
                    });
                }
            } else if ($eventArgs.getKeyCode() === KeyMap.TAB) {
                Dialog.tabPressed = true;
            } else if ($eventArgs.getKeyCode() === KeyMap.LEFT_ARROW ||
                $eventArgs.getKeyCode() === KeyMap.RIGHT_ARROW ||
                $eventArgs.getKeyCode() === KeyMap.UP_ARROW ||
                $eventArgs.getKeyCode() === KeyMap.DOWN_ARROW) {
                if ($manager.IsActive(<ClassName>Dialog)) {
                    const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>Dialog);
                    elements.foreach(($element : Dialog) : boolean => {
                        if ($element.Visible() && $element.Modal()) {
                            $eventArgs.PreventDefault();
                            return false;
                        }
                        return true;
                    });
                }
            }
        }

        protected static resize($element : Dialog, $width : number, $height : number) : void {
            const id : string = $element.Id();
            if (ElementManager.IsVisible(id)) {
                const windowSize : Size = WindowManager.getSize();
                $element.Width($width);
                $element.Height($height);

                if ($element.Modal()) {
                    ElementManager.setSize(id + "_Background", windowSize.Width(), windowSize.Height());
                }

                const dialogBorders : Borders = new Borders();
                dialogBorders.Left(ElementManager.getElement(id + "_MiddleLeft").offsetWidth +
                    ElementManager.getCssIntegerValue(id + "_MiddleLeft", "border-left-width"));
                dialogBorders.Right(ElementManager.getElement(id + "_MiddleRight").offsetWidth +
                    ElementManager.getCssIntegerValue(id + "_MiddleRight", "border-right-width"));
                const contentBorders : Borders = new Borders(id + "_Content");

                $element.Width(
                    $element.Width()
                    - dialogBorders.Left() - dialogBorders.Right()
                    + contentBorders.Left() + contentBorders.Right());
                if ($element.Width() > windowSize.Width() - 100) {
                    $element.Width(windowSize.Width() - 100);
                }
                if ($element.Height() > windowSize.Height() - 100) {
                    $element.Height(windowSize.Height() - 100);
                }

                if ($element.Width() > $element.MaxWidth()) {
                    $element.Width($element.MaxWidth());
                }
                if ($element.Height() > $element.MaxHeight()) {
                    $element.Height($element.MaxHeight());
                }

                if ($element.Width() < Dialog.minWidth) {
                    $element.Width(Dialog.minWidth);
                }
                if ($element.Height() < Dialog.minHeight) {
                    $element.Height(Dialog.minHeight);
                }

                const envelop : HTMLElement = ElementManager.getElement(id + "_Envelop");
                if (envelop.offsetLeft + $element.Width() > windowSize.Width() - 50) {
                    $element.Width(Math.ceil(windowSize.Width() - 50 - envelop.offsetLeft));
                }
                if (envelop.offsetTop + $element.Height() > windowSize.Height() - 50) {
                    $element.Height(Math.ceil(windowSize.Height() - 50 - envelop.offsetTop));
                }

                const eventArgs : ResizeEventArgs = new ResizeEventArgs();
                eventArgs.Owner($element);
                eventArgs.Width($element.Width());
                eventArgs.Height($element.Height());
                eventArgs.AvailableWidth(
                    $element.Width()
                    - ElementManager.getCssIntegerValue(id + "_MiddleLeft", "border-left-width")
                    - ElementManager.getCssIntegerValue(id + "_MiddleRight", "border-right-width"));
                eventArgs.AvailableHeight($element.Height());
                eventArgs.ScrollBarWidth(0);
                eventArgs.ScrollBarHeight(0);
                $element.getEventsManager().FireEvent($element, EventType.BEFORE_RESIZE, eventArgs, false);
                $element.getEventsManager().FireEvent(Dialog.ClassName(), EventType.BEFORE_RESIZE, eventArgs, false);

                ElementManager.setWidth(id + "_TopCenter", $element.Width());
                ElementManager.setWidth(id + "_BottomCenter", $element.Width());
                ElementManager.setSize(id + "_MiddleCenter", $element.Width(), $element.Height());
                ElementManager.setHeight(id + "_MiddleLeft", $element.Height());
                ElementManager.setHeight(id + "_MiddleRight", $element.Height());
                ElementManager.setSize(id + "_Content",
                    $element.Width() +
                    ElementManager.getElement(id + "_MiddleLeft").offsetWidth +
                    ElementManager.getElement(id + "_MiddleRight").offsetWidth,
                    $element.Height());
                ElementManager.setCssProperty(id + "_Content", "left",
                    (-1) * (ElementManager.getElement(id + "_MiddleLeft").offsetWidth));
                ElementManager.setWidth(id + "_Envelop",
                    $element.Width() +
                    dialogBorders.Left() +
                    dialogBorders.Right()
                    - contentBorders.Left()
                    - contentBorders.Right()
                );

                const contentSize : Size = new Size(id + "_Content", true);
                if (ObjectValidator.IsEmptyOrNull($element.PanelViewer()) ||
                    ObjectValidator.IsEmptyOrNull($element.PanelViewer().getInstance())) {
                    const emptyBorders : Borders = new Borders();
                    emptyBorders.Top(
                        ElementManager.getCssIntegerValue(id + "_EmptyContent", "border-top-width") +
                        ElementManager.getCssIntegerValue(id + "_EmptyContent", "margin-top"));
                    emptyBorders.Left(
                        ElementManager.getCssIntegerValue(id + "_EmptyContent", "border-left-width") +
                        ElementManager.getCssIntegerValue(id + "_EmptyContent", "margin-left"));
                    emptyBorders.Right(
                        ElementManager.getCssIntegerValue(id + "_EmptyContent", "border-right-width") +
                        ElementManager.getCssIntegerValue(id + "_EmptyContent", "margin-right"));
                    emptyBorders.Bottom(
                        ElementManager.getCssIntegerValue(id + "_EmptyContent", "border-bottom-width") +
                        ElementManager.getCssIntegerValue(id + "_EmptyContent", "margin-bottom"));
                    ElementManager.setSize(id + "_EmptyContent",
                        contentSize.Width() - emptyBorders.Left() - emptyBorders.Right(),
                        contentSize.Height() - emptyBorders.Top() - emptyBorders.Bottom());
                }

                ElementManager.setWidth(id + "_Header",
                    $element.Width() +
                    dialogBorders.Left()
                    - ElementManager.getElement($element.closeButton.Id()).offsetWidth);

                $element.Width($element.Width() + dialogBorders.Left() + dialogBorders.Right());

                ElementManager.setCssProperty($element.dragBar.Id(), "width",
                    $element.Width()
                    - ElementManager.getCssIntegerValue(id + "_TopLeft", "border-left-width")
                    - ElementManager.getCssIntegerValue(id + "_TopRight", "border-right-width"));

                ElementManager.setWidth(id + "_TopForeground",
                    $element.Width()
                    - ElementManager.getCssIntegerValue(id + "_TopLeft", "border-left-width")
                    - ElementManager.getCssIntegerValue(id + "_TopRight", "border-right-width")
                    - ElementManager.getCssIntegerValue(id + "_TopForeground", "border-left-width")
                    - ElementManager.getCssIntegerValue(id + "_TopForeground", "border-right-width"));

                ElementManager.setWidth(id + "_BottomForeground",
                    $element.Width()
                    - ElementManager.getCssIntegerValue(id + "_BottomLeft", "border-left-width")
                    - ElementManager.getCssIntegerValue(id + "_BottomRight", "border-right-width")
                    - ElementManager.getCssIntegerValue(id + "_BottomForeground", "border-left-width")
                    - ElementManager.getCssIntegerValue(id + "_BottomForeground", "border-right-width"));

                eventArgs.Width($element.Width());
                eventArgs.AvailableWidth(
                    $element.Width()
                    - ElementManager.getCssIntegerValue(id + "_MiddleLeft", "border-left-width")
                    - ElementManager.getCssIntegerValue(id + "_MiddleRight", "border-right-width"));
                $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE, eventArgs, false);
                $element.getEventsManager().FireEvent(Dialog.ClassName(), EventType.ON_RESIZE, eventArgs, false);
            }
        }

        private static onResizeStartEventHandler($eventArgs : ResizeBarEventArgs, $manager : GuiObjectManager,
                                                 $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>ResizeBar)) {
                let element : any = <ResizeBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, ResizeBar)) {
                    element = <Dialog>element.Parent();
                    if ($reflection.IsMemberOf(element, Dialog)) {
                        $reflection.getClass(element.getClassName(element)).Focus(element);
                        element.startWidth = element.Width();
                        element.startHeight = element.Height();
                        if (!ObjectValidator.IsEmptyOrNull(element.PanelViewer())) {
                            element.scrollable = element.PanelViewer().getInstance().Scrollable();
                            element.PanelViewer().getInstance().Scrollable(false);
                        }
                    }
                }
            }
        }

        private static onResizeChangeEventHandler($eventArgs : ResizeBarEventArgs, $manager : GuiObjectManager,
                                                  $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>ResizeBar)) {
                let element : any = <ResizeBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, ResizeBar)) {
                    element = <Dialog>element.Parent();
                    if ($reflection.IsMemberOf(element, Dialog)) {
                        const newSize : Size = new Size();
                        newSize.Width(element.startWidth);
                        newSize.Height(element.startHeight);

                        if ($eventArgs.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                            $eventArgs.ResizeableType() === ResizeableType.HORIZONTAL) {
                            let width : number = newSize.Width() + $eventArgs.getDistanceX();
                            if (width < Dialog.minWidth) {
                                width = Dialog.minWidth;
                            }
                            newSize.Width(width);
                        }

                        if ($eventArgs.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                            $eventArgs.ResizeableType() === ResizeableType.VERTICAL) {
                            let height : number = newSize.Height() + $eventArgs.getDistanceY();
                            if (height < Dialog.minHeight) {
                                height = Dialog.minHeight;
                            }
                            newSize.Height(height);
                        }

                        if ($eventArgs.ResizeableType() !== ResizeableType.NONE) {
                            Dialog.resize(element, newSize.Width(), newSize.Height());
                        }
                    }
                }
            }
        }

        private static onResizeCompleteEventHandler($eventArgs : ResizeBarEventArgs, $manager : GuiObjectManager,
                                                    $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>ResizeBar)) {
                let element : any = <ResizeBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, ResizeBar)) {
                    element = <Dialog>element.Parent();
                    if ($reflection.IsMemberOf(element, Dialog)) {
                        const thisClass : any = $reflection.getClass(element.getClassName());
                        thisClass.Focus(element);
                        if (!ObjectValidator.IsEmptyOrNull(element.PanelViewer())
                            && !ObjectValidator.IsEmptyOrNull(element.PanelViewerArgs())) {
                            if (element.AutoResize()) {
                                element.PanelViewer().getInstance().Scrollable(element.scrollable);
                                Dialog.resize(element, element.Width(), element.Height());
                            }
                        }

                        if (!element.Draggable() && element.AutoCenter()) {
                            thisClass.Center(element);
                        }
                    }
                }
            }
        }

        private static onDragStartEventHandler($eventArgs : MoveEventArgs, $manager : GuiObjectManager,
                                               $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>DragBar)) {
                let element : any = <DragBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, DragBar)) {
                    element = <Dialog>element.Parent();
                    if ($reflection.IsMemberOf(element, Dialog)) {
                        $reflection.getClass(element.getClassName()).Focus(element);
                        const parentOffset : ElementOffset = ElementManager.getAbsoluteOffset(element.Id() + "_Envelop");
                        element.top = parentOffset.Top();
                        element.left = parentOffset.Left();
                    }
                }
            }
        }

        private static onDragChangeEventHandler($eventArgs : MoveEventArgs, $manager : GuiObjectManager,
                                                $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>DragBar)) {
                let element : any = <DragBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, DragBar)) {
                    element = <Dialog>element.Parent();
                    if ($reflection.IsMemberOf(element, Dialog)) {
                        const windowSize : Size = WindowManager.getSize();
                        let top : number = element.top + $eventArgs.getDistanceY();
                        let left : number = element.left + $eventArgs.getDistanceX();

                        if (left < 50) {
                            left = 50;
                        }
                        if (top < 50) {
                            top = 50;
                        }
                        if (element.Width() + left > windowSize.Width() - 50) {
                            left = windowSize.Width() - element.Width() - 50;
                        }
                        if (element.Height() + top > windowSize.Height() - 50) {
                            top = windowSize.Height() - element.Height() - 50;
                        }

                        ElementManager.setCssProperty(element.Id() + "_Envelop", "top", top);
                        ElementManager.setCssProperty(element.Id() + "_Envelop", "left", left);

                        const eventArgs : MoveEventArgs = new MoveEventArgs();
                        eventArgs.Owner(element);
                        eventArgs.NativeEventArgs($eventArgs.NativeEventArgs());
                        element.getEventsManager().FireEvent(element, EventType.ON_DRAG, eventArgs);
                        element.getEventsManager().FireEvent(Dialog.ClassName(), EventType.ON_DRAG, eventArgs);
                    }
                }
            }
        }

        private static onDragCompleteEventHandler($eventArgs : MoveEventArgs, $manager : GuiObjectManager,
                                                  $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>DragBar)) {
                let element : any = <DragBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, DragBar)) {
                    element = <Dialog>element.Parent();
                    if ($reflection.IsMemberOf(element, Dialog)) {
                        const parentOffset : ElementOffset = ElementManager.getAbsoluteOffset(element.Id() + "_Envelop");
                        element.top = parentOffset.Top();
                        element.left = parentOffset.Left();
                        $reflection.getClass(element.getClassName()).Focus(element);
                    }
                }
            }
        }

        private static resizeHeaderEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager,
                                                $reflection : Reflection) : void {
            let element : Dialog = <Dialog>$eventArgs.Owner();
            if (!$reflection.IsMemberOf(element, Dialog) && !ObjectValidator.IsEmptyOrNull(element.Parent())) {
                element = <Dialog>element.Parent();
            }
            if ($reflection.IsMemberOf(element, Dialog) && ElementManager.IsVisible(element.Id())) {
                const id : string = element.Id();
                ElementManager.setCssProperty(id + "_HeaderContent", "position", "fixed");
                ElementManager.ClearCssProperty(id + "_HeaderContent", "width");
                let width : number = ElementManager.getElement(id + "_HeaderContent").offsetWidth + 1;
                ElementManager.setCssProperty(id + "_HeaderContent", "position", "relative");
                const closeButtonWidth : number = ElementManager.getElement(element.closeButton.Id()).offsetWidth;
                if (width > element.Width() - closeButtonWidth) {
                    width = element.Width() - closeButtonWidth;
                }
                ElementManager.setWidth(id + "_HeaderContent", width);
                ElementManager.setCssProperty(id + "_Header", "left",
                    (-1) * ElementManager.getElement(id + "_MiddleLeft").offsetWidth);

                ElementManager.setWidth(id + "_Header", element.Width() -
                    ElementManager.getElement(id + "_MiddleRight").offsetWidth -
                    ElementManager.getCssIntegerValue(id + "_MiddleRight", "border-left-width")
                    - closeButtonWidth);
            }
        }

        /**
         * @param {DialogType} $dialogType Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($dialogType : DialogType, $id? : string) {
            super($id);

            this.cornerResizeBar = new ResizeBar(ResizeableType.NONE);
            this.rightResizeBar = new ResizeBar(ResizeableType.NONE);
            this.bottomResizeBar = new ResizeBar(ResizeableType.NONE);
            this.dragBar = new DragBar();

            let closeButtonClass : any = this.getCloseButtonClass();
            if (ObjectValidator.IsEmptyOrNull(closeButtonClass)) {
                closeButtonClass = ImageButton;
            }
            this.closeButton = new closeButtonClass();
            this.closeButton.Parent(this);

            let headerIconClass : any = this.getHeaderIconClass();
            if (ObjectValidator.IsEmptyOrNull(headerIconClass)) {
                headerIconClass = Icon;
            }
            this.headerIcon = new headerIconClass();
            this.headerIcon.Visible(false);

            let headerLabelClass : any = this.getHeaderLabelClass();
            if (ObjectValidator.IsEmptyOrNull(headerLabelClass)) {
                headerLabelClass = Label;
            }
            this.headerText = new headerLabelClass();
            this.headerText.Visible(false);

            this.autoCenter = true;
            this.autoResize = true;

            this.GuiType($dialogType);
        }

        /**
         * @return {IDialogEvents} Returns events manager subscribed to the item.
         */
        public getEvents() : IDialogEvents {
            return <IDialogEvents>super.getEvents();
        }

        /**
         * @param {number} [$value] Specify width value of element.
         * @return {number} Returns element's width value.
         */
        public Width($value? : number) : number {
            this.width = Property.PositiveInteger(this.width, $value);
            return this.width;
        }

        /**
         * @param {number} [$value] Specify height value of element.
         * @return {number} Returns element's height value.
         */
        public Height($value? : number) : number {
            this.height = Property.PositiveInteger(this.height, $value);
            return this.height;
        }

        /**
         * @param {number} [$value] Specify initial top offset for modeless dialog.
         * @return {number} Returns element's top offset.
         */
        public TopOffset($value? : number) : number {
            if (ObjectValidator.IsSet($value) && $value === -1 || !ObjectValidator.IsSet(this.topOffset)) {
                this.topOffset = -1;
            } else {
                this.topOffset = Property.PositiveInteger(this.topOffset, $value);
            }
            return this.topOffset;
        }

        /**
         * @param {number} [$value] Specify maximal width value of element.
         * @return {number} Returns maximal element's width value.
         */
        public MaxWidth($value? : number) : number {
            if (ObjectValidator.IsSet($value) && $value === -1 || !ObjectValidator.IsSet(this.maxWidth)) {
                this.maxWidth = -1;
            } else {
                this.maxWidth = Property.PositiveInteger(this.maxWidth, $value, Dialog.minWidth);
            }
            return this.maxWidth;
        }

        /**
         * @param {number} [$value] Specify maximal height value of element.
         * @return {number} Returns maximal element's height value.
         */
        public MaxHeight($value? : number) : number {
            if (ObjectValidator.IsSet($value) && $value === -1 || !ObjectValidator.IsSet(this.maxHeight)) {
                this.maxHeight = -1;
            } else {
                this.maxHeight = Property.PositiveInteger(this.maxHeight, $value, Dialog.minHeight);
            }
            return this.maxHeight;
        }

        /**
         * @param {boolean} [$value] Specify, if dialog will be automatically resized on each page refresh.
         * @return {boolean} Returns true, if dialog is automatically resized, otherwise false.
         */
        public AutoResize($value? : boolean) : boolean {
            if (!ObjectValidator.IsSet(this.autoResize)) {
                this.autoResize = false;
            }
            this.autoResize = Property.Boolean(this.autoResize, $value);
            return this.autoResize;
        }

        /**
         * @param {boolean} [$value] Specify, if dialog should be automatically moved to the window center
         * on each page refresh.
         * @return {boolean} Returns true, if dialog is automatically moved to the window center, otherwise false.
         */
        public AutoCenter($value? : boolean) : boolean {
            if (!ObjectValidator.IsSet(this.autoCenter)) {
                this.autoCenter = false;
            }
            this.autoCenter = Property.Boolean(this.autoCenter, $value);
            return this.autoCenter;
        }

        /**
         * @param {ResizeableType} [$type] Specify type of element's resize direction.
         * @return {ResizeableType} Returns type of element's resize direction.
         */
        public ResizeableType($type? : ResizeableType) : ResizeableType {
            if (ObjectValidator.IsSet($type) && ResizeableType.Contains($type)) {
                if ($type === ResizeableType.HORIZONTAL_AND_VERTICAL) {
                    this.rightResizeBar.ResizeableType(ResizeableType.HORIZONTAL);
                    this.bottomResizeBar.ResizeableType(ResizeableType.VERTICAL);
                } else if ($type === ResizeableType.HORIZONTAL) {
                    this.rightResizeBar.ResizeableType(ResizeableType.HORIZONTAL);
                    this.bottomResizeBar.ResizeableType(ResizeableType.NONE);
                } else if ($type === ResizeableType.VERTICAL) {
                    this.rightResizeBar.ResizeableType(ResizeableType.NONE);
                    this.bottomResizeBar.ResizeableType(ResizeableType.VERTICAL);
                } else if ($type === ResizeableType.NONE) {
                    this.rightResizeBar.ResizeableType(ResizeableType.NONE);
                    this.bottomResizeBar.ResizeableType(ResizeableType.NONE);
                }
            }

            return this.cornerResizeBar.ResizeableType($type);
        }

        /**
         * @param {boolean} [$value] Specify, if dialog should have modal behaviour - user has to interact with
         * the dialog first, before another interaction.
         * @return {boolean} Returns true, if dialog is modal, otherwise false.
         */
        public Modal($value? : boolean) : boolean {
            if (!ObjectValidator.IsSet(this.isModal)) {
                this.isModal = true;
            }
            this.isModal = Property.Boolean(this.isModal, $value);
            return this.isModal;
        }

        /**
         * @param {boolean} [$value] Specify, if dialog can be moved by the user. Modal dialog can not be draggable.
         * @return {boolean} Returns true, if dialog is draggable, otherwise false.
         */
        public Draggable($value? : boolean) : boolean {
            if (!ObjectValidator.IsSet(this.isDraggable)) {
                this.isDraggable = false;
            }
            if (ObjectValidator.IsSet($value) && this.Modal()) {
                $value = false;
            }
            this.isDraggable = Property.Boolean(this.isDraggable, $value);
            return this.isDraggable;
        }

        public Value($value? : BasePanelViewerArgs) : BasePanelViewerArgs {
            return this.PanelViewerArgs($value);
        }

        /**
         * @param {BasePanelViewer} [$instance] Specify dialog content by panel viewer instance.
         * @return {BasePanelViewer} Returns panel viewer instance, if dialog content has been specified, otherwise null.
         */
        public PanelViewer($instance? : BasePanelViewer) : BasePanelViewer {
            if (ObjectValidator.IsSet($instance)) {
                if (!Reflection.getInstance().IsMemberOf($instance, BasePanelViewer)) {
                    ExceptionsManager.Throw(this.getClassName(), new IllegalArgumentException(
                        "Into the " + Dialog.ClassName() + " content can be passed only member of " +
                        BasePanelViewer.ClassName() + " class."));
                }

                $instance.getInstance().Parent(this);
                if (this.IsCompleted()) {
                    const childList : IArrayList<IGuiCommons> = this.getChildElements();
                    childList.Add($instance.getInstance(), $instance.getInstance().Id());
                    if (!ObjectValidator.IsEmptyOrNull(this.panelViewerInstance)) {
                        childList.RemoveAt(childList.IndexOf(this.panelViewerInstance.getInstance()));
                    }
                }
                this.panelViewerInstance = $instance;
            }
            return this.panelViewerInstance;
        }

        /**
         * @param {BasePanelViewerArgs} [$value] Specify arguments for the dialog's panel viewer instance.
         * @return {BasePanelViewerArgs} Returns panel viewer args, if args has been specified, otherwise null.
         */
        public PanelViewerArgs($value? : BasePanelViewerArgs) : BasePanelViewerArgs {
            if (ObjectValidator.IsSet($value)) {
                if (ObjectValidator.IsEmptyOrNull(this.panelViewerInstance)) {
                    ExceptionsManager.Throw(this.getClassName(), new IllegalArgumentException(
                        "PanelArgs of the " + this.getClassName() + "  content can be set, because panel " +
                        "instance is empty."));
                }
                if (!Reflection.getInstance().IsMemberOf($value, BasePanelViewerArgs)) {
                    ExceptionsManager.Throw(this.getClassName(), new IllegalArgumentException(
                        "Into the PanelViewerArgs method can be passed as argument only member of " +
                        BasePanelViewerArgs.ClassName() + " class."));
                }

                this.panelViewerInstance.ViewerArgs($value);
            }
            if (ObjectValidator.IsEmptyOrNull(this.panelViewerInstance)) {
                return null;
            } else {
                return this.panelViewerInstance.ViewerArgs();
            }
        }

        /**
         * @param {DialogType} [$dialogType] Specify type of element look and feel.
         * @return {DialogType} Returns type of element's look and feel.
         */
        public GuiType($dialogType? : DialogType) : DialogType {
            if (ObjectValidator.IsSet($dialogType)) {
                this.guiType = this.guiTypeValueSetter($dialogType);
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                }
            }
            return this.guiType;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.guiType, $value, DialogType, DialogType.GENERAL);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!DialogType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Dialog type can not be set by StyleClassName method.");
            return false;
        }

        protected innerCode() : IGuiElement {
            this.getEvents().Subscriber(this.Id() + "_Content");
            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());

            if (this.Modal() || !this.Draggable()) {
                this.isDraggable = false;
                this.dragBar.Visible(false);
            }

            this.getEvents().setOnMouseDown(($eventArgs : MouseEventArgs) : void => {
                thisClass.Focus($eventArgs.Owner());
            });

            this.getBackgroundEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                thisClass.Close($eventArgs.Owner());
            });

            this.closeButton.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                const element : ImageButton = <ImageButton>$eventArgs.Owner();
                thisClass.Focus(<Dialog>element.Parent());
                thisClass.Close(<Dialog>element.Parent());
            });

            this.closeButton.getEvents().setOnFocus(($eventArgs : EventArgs) : void => {
                const element : ImageButton = <ImageButton>$eventArgs.Owner();
                thisClass.Focus(<Dialog>element.Parent());
            });
            this.closeButton.getEvents().setOnBlur(() : void => {
                thisClass.Blur();
            });

            if (this.ResizeableType() === ResizeableType.NONE) {
                this.cornerResizeBar.Visible(false);
                this.rightResizeBar.Visible(false);
                this.bottomResizeBar.Visible(false);
            }

            this.rightResizeBar.getEvents().setOnResizeStart(Dialog.onResizeStartEventHandler);
            this.rightResizeBar.getEvents().setOnResizeChange(Dialog.onResizeChangeEventHandler);
            this.rightResizeBar.getEvents().setOnResizeComplete(Dialog.onResizeCompleteEventHandler);
            this.cornerResizeBar.getEvents().setOnResizeStart(Dialog.onResizeStartEventHandler);
            this.cornerResizeBar.getEvents().setOnResizeChange(Dialog.onResizeChangeEventHandler);
            this.cornerResizeBar.getEvents().setOnResizeComplete(Dialog.onResizeCompleteEventHandler);
            this.bottomResizeBar.getEvents().setOnResizeStart(Dialog.onResizeStartEventHandler);
            this.bottomResizeBar.getEvents().setOnResizeChange(Dialog.onResizeChangeEventHandler);
            this.bottomResizeBar.getEvents().setOnResizeComplete(Dialog.onResizeCompleteEventHandler);

            this.dragBar.getEvents().setOnDragStart(Dialog.onDragStartEventHandler);
            this.dragBar.getEvents().setOnDragChange(Dialog.onDragChangeEventHandler);
            this.dragBar.getEvents().setOnDragComplete(Dialog.onDragCompleteEventHandler);

            this.getEvents().setOnResize(ResizeBar.ResizeEventHandler);
            this.getEvents().setOnResize(Com.Wui.Framework.UserControls.Primitives.BasePanel.ResizeEventHandler);

            const windowResizeEventHandler : () => void = () : void => {
                if (this.AutoResize()) {
                    ElementManager.setCssProperty(this.Id() + "_Envelop", "top", 0);
                    ElementManager.setCssProperty(this.Id() + "_Envelop", "left", 0);
                    Dialog.resize(this, this.Width(), this.Height());
                }
                if (this.AutoCenter()) {
                    thisClass.Center(this);
                }
            };
            this.getEvents().FireAsynchronousMethod(windowResizeEventHandler, 100);

            WindowManager.getEvents().setOnResize(() : void => {
                if (this.Visible()) {
                    this.getEvents().FireAsynchronousMethod(windowResizeEventHandler, false, 50);
                }
            });
            WindowManager.getEvents().setOnMouseDown(() : void => {
                Dialog.tabPressed = false;
            });

            this.headerText.getEvents().setOnStart(Dialog.resizeHeaderEventHandler);
            this.headerText.getEvents().setOnChange(Dialog.resizeHeaderEventHandler);
            this.headerIcon.getEvents().setOnStart(Dialog.resizeHeaderEventHandler);
            this.getEvents().setOnResize(Dialog.resizeHeaderEventHandler);
            this.getEvents().setOnOpen(Dialog.resizeHeaderEventHandler);

            if (!ObjectValidator.IsEmptyOrNull(this.panelViewerInstance)
                && !ObjectValidator.IsEmptyOrNull(this.panelViewerInstance.getInstance())) {

                if (!this.IsCached()) {
                    this.panelViewerInstance.getInstance().Visible(false);
                    this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                        const element : Dialog = <Dialog>$eventArgs.Owner();
                        element.getEvents().FireAsynchronousMethod(() : void => {
                            element.panelViewerInstance.getInstance().Visible(true);
                        }, false, 100);
                    });
                }

                this.panelViewerInstance.getInstance().getEvents().setOnLoad(
                    ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                        const element : BasePanel = <BasePanel>$eventArgs.Owner();
                        if (!ObjectValidator.IsEmptyOrNull(element.Parent())) {
                            const parent : Dialog = <Dialog>element.Parent();
                            if ($reflection.IsMemberOf(parent, Dialog)) {
                                Dialog.resize(parent, element.Width(), element.Height());
                                if (parent.Modal()) {
                                    thisClass.Center(parent);
                                } else {
                                    if (parent.top !== -1 && parent.left !== -1) {
                                        ElementManager.setCssProperty(parent.Id() + "_Envelop", "top", parent.top);
                                        ElementManager.setCssProperty(parent.Id() + "_Envelop", "left", parent.left);
                                    } else {
                                        thisClass.Center(parent);
                                    }
                                }
                            }
                        }
                    });

                this.panelViewerInstance.getInstance().getEvents().setOnComplete(
                    ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                        $manager.setHovered($eventArgs.Owner(), true);
                    });
            }

            this.getEvents().setOnStart(
                ($eventArgs : EventArgs) : void => {
                    const element : Dialog = <Dialog>$eventArgs.Owner();
                    if (element.Visible() && element.Modal()) {
                        document.body.style.overflowX = "hidden";
                        document.body.style.overflowY = "hidden";
                        ElementManager.setOpacity(element.Id() + "_Background", 10);
                        ElementManager.ChangeOpacity(element.Id() + "_Background", DirectionType.UP, 3);
                    }
                });

            this.getEvents().setOnLoad(
                ($eventArgs : EventArgs) : void => {
                    const element : Dialog = <Dialog>$eventArgs.Owner();
                    if (element.Visible()) {
                        thisClass.Open(element);
                    }
                });

            this.getEvents().setOnComplete(
                ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    const element : Dialog = <Dialog>$eventArgs.Owner();
                    element.getBackgroundEvents().Subscribe();

                    const resizeEvents : ResizeEventArgs = new ResizeEventArgs();
                    resizeEvents.Owner(element);
                    resizeEvents.Width(element.Width());
                    resizeEvents.Height(element.Height());
                    resizeEvents.AvailableWidth(
                        element.Width()
                        - ElementManager.getCssIntegerValue(element.Id() + "_MiddleLeft", "border-left-width")
                        - ElementManager.getCssIntegerValue(element.Id() + "_MiddleRight", "border-right-width"));
                    resizeEvents.AvailableHeight(element.Height());
                    resizeEvents.ScrollBarWidth(0);
                    resizeEvents.ScrollBarHeight(0);
                    ResizeBar.ResizeEventHandler(resizeEvents, $manager, $reflection);
                });

            return super.innerCode()
                .Add(this.addElement(this.Id() + "_Background").StyleClassName(GeneralCssNames.BACKGROUND).Visible(this.Modal()))
                .Add(super.selectorElement());
        }

        protected innerHtml() : IGuiElement {
            const content : IGuiElement = this.addElement();
            if (!ObjectValidator.IsEmptyOrNull(this.panelViewerInstance)) {
                this.panelViewerInstance.PrepareImplementation();
                this.panelViewerInstance.getInstance().getChildPanelList().foreach(($child : BasePanelViewer) : void => {
                    $child.PrepareImplementation();
                });
                content.Add(this.panelViewerInstance);
            } else {
                content.Id(this.Id() + "_EmptyContent").StyleClassName("Empty");
            }

            return this.addElement(this.Id() + "_Type").StyleClassName(this.guiType)
                .Add(this.addElement(this.Id() + "_Status")
                    .StyleClassName(GeneralCssNames.OFF)
                    .GuiTypeTag(this.getGuiTypeTag())
                    .Add(this.addElement(this.Id() + "_Envelop")
                        .StyleClassName("Envelop")
                        .Add(this.addElement(this.Id() + "_Top")
                            .StyleClassName(GeneralCssNames.TOP)
                            .Add(this.addElement(this.Id() + "_TopLeft").StyleClassName(GeneralCssNames.LEFT))
                            .Add(this.addElement(this.Id() + "_TopCenter")
                                .StyleClassName(GeneralCssNames.CENTER)
                                .Add(this.addElement(this.Id() + "_TopForeground").StyleClassName(GeneralCssNames.FOREGROUND))
                                .Add(this.dragBar)
                                .Add(this.addElement(this.Id() + "_Header")
                                    .StyleClassName("Header")
                                    .Add(this.addElement(this.Id() + "_HeaderContent")
                                        .StyleClassName("Content")
                                        .Add(this.headerIcon)
                                        .Add(this.headerText)
                                    )
                                )
                            )
                            .Add(this.addElement(this.Id() + "_TopRight")
                                .StyleClassName(GeneralCssNames.RIGHT)
                                .Add(this.closeButton)
                            )
                        )
                        .Add(this.addElement(this.Id() + "_Middle")
                            .StyleClassName(GeneralCssNames.MIDDLE)
                            .Add(this.addElement(this.Id() + "_MiddleLeft").StyleClassName(GeneralCssNames.LEFT))
                            .Add(this.addElement(this.Id() + "_MiddleCenter")
                                .StyleClassName(GeneralCssNames.CENTER)
                                .Add(this.addElement(this.Id() + "_Content")
                                    .StyleClassName("Content")
                                    .Add(content)
                                )
                            )
                            .Add(this.addElement(this.Id() + "_MiddleRight")
                                .StyleClassName(GeneralCssNames.RIGHT)
                                .Add(this.rightResizeBar)
                            )
                        )
                        .Add(this.addElement(this.Id() + "_Bottom")
                            .StyleClassName(GeneralCssNames.BOTTOM)
                            .Add(this.addElement(this.Id() + "_BottomLeft").StyleClassName(GeneralCssNames.LEFT))
                            .Add(this.addElement(this.Id() + "_BottomCenter")
                                .StyleClassName(GeneralCssNames.CENTER)
                                .Add(this.addElement(this.Id() + "_BottomForeground").StyleClassName(GeneralCssNames.FOREGROUND))
                                .Add(this.bottomResizeBar)
                            )
                            .Add(this.addElement(this.Id() + "_BottomRight")
                                .StyleClassName(GeneralCssNames.RIGHT)
                                .Add(this.cornerResizeBar)
                            )
                        )
                    )
                );
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();

            this.top = -1;
            this.left = -1;

            this.width = -1;
            this.height = -1;

            this.Visible(false);
            if (ObjectValidator.IsSet(this.headerIcon)) {
                this.headerIcon.Visible(false);
            }
            if (ObjectValidator.IsSet(this.headerText)) {
                this.headerText.Visible(false);
            }
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push(
                "isModal", "isDraggable",
                "topOffset", "top", "left",
                "width", "height",
                "maxWidth", "maxHeight",
                "startWidth", "startHeight",
                "autoResize", "autoCenter", "scrollable",
                "backgroundEvents"
            );
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push(
                "backgroundEvents", "scrollable",
                "startWidth", "startHeight");

            if (this.Modal() === true) {
                exclude.push("isModal");
            }
            if (this.Draggable() === false) {
                exclude.push("isDraggable");
            }
            if (this.TopOffset() === -1) {
                exclude.push("topOffset");
            }
            if (this.AutoResize() === false) {
                exclude.push("autoResize");
            }
            if (this.AutoResize() === false) {
                exclude.push("autoCenter");
            }
            if (this.MaxWidth() === -1) {
                exclude.push("maxWidth");
            }
            if (this.MaxHeight() === -1) {
                exclude.push("maxHeight");
            }
            return exclude;
        }

        /**
         * @return {IImageButton} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IImageButton
         */
        protected getCloseButtonClass() : any {
            return ImageButton;
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IIcon
         */
        protected getHeaderIconClass() : any {
            return Icon;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.ILabel
         */
        protected getHeaderLabelClass() : any {
            return Label;
        }

        private getBackgroundEvents() : ElementEventsManager {
            if (!ObjectValidator.IsSet(this.backgroundEvents)) {
                this.backgroundEvents = new ElementEventsManager(this, this.Id() + "_Background");
            }
            return this.backgroundEvents;
        }
    }
}
