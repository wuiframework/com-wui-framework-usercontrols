/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import ILabelList = Com.Wui.Framework.Gui.Interfaces.UserControls.ILabelList;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import IconType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.IconType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import IGuiCommonsListArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsListArg;

    /**
     * LabelList class renders list of simple text.
     */
    export class LabelList extends Com.Wui.Framework.UserControls.Primitives.BaseGuiObject implements ILabelList {
        private items : ArrayList<Label>;
        private iconType : string;

        /**
         * @param {IconType} [$iconType] Specify icon type, which should be rendered at each line of the list.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($iconType? : IconType, $id? : string) {
            super($id);
            this.IconName($iconType);
        }

        /**
         * @param {IconType} [$value] Specify type of icon, which should be rendered at each line of the list.
         * @return {IconType} Returns type of icon, which belongs to the list item.
         */
        public IconName($value? : IconType) : IconType {
            if (ObjectValidator.IsSet($value)) {
                this.iconType = <string>$value;
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_IconType", this.iconType.toString());
                }
            }
            if (!ObjectValidator.IsSet(this.iconType)) {
                this.iconType = "";
            }
            return this.iconType;
        }

        /**
         * @param {string|Label} $value Push this type of value to the list.
         * @return {boolean} Returns true, if value has been added to the list, otherwise false.
         */
        public Add($value : string | Label) : boolean {
            if (ObjectValidator.IsSet($value) && (
                ObjectValidator.IsString($value) ||
                Reflection.getInstance().IsMemberOf(<Label>$value, Label))) {
                if (ObjectValidator.IsString($value)) {
                    $value = new Label(<string>$value);
                }
                (<Label>$value).Parent(this);
                (<Label>$value).getGuiOptions().Add(GuiOptionType.DISABLE);
                (<Label>$value).DisableAsynchronousDraw();
                this.items.Add(<Label>$value);
                this.getChildElements().Add(<Label>$value, (<Label>$value).Id());
                return true;
            }

            return false;
        }

        /**
         * @param {number} $index Specify item index, which should be returned. Items indexes starts by 0.
         * @return {Label} Returns item at desired index, if item index has been found, otherwise null.
         */
        public getItem($index : number) : Label {
            if (this.items.KeyExists($index)) {
                return this.items.getItem($index);
            }
            return null;
        }

        /**
         * Clean up all registered items.
         * @return {void}
         */
        public Clear() : void {
            this.items.Clear();
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            const enabled : boolean = super.Enabled($value);
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                this.items.foreach(($item : Label) : void => {
                    if (!enabled || (enabled && $item.Enabled())) {
                        ElementManager.Enabled($item.Id(), enabled);
                    }
                });
            }
            return enabled;
        }

        /**
         * @return {IGuiCommonsArg[]} Returns array of element's attributes.
         */
        public getArgs() : IGuiCommonsArg[] {
            const args : IGuiCommonsArg[] = super.getArgs();
            args.push(<IGuiCommonsListArg>{
                items: IconType.getProperties(),
                name : "IconName",
                type : GuiCommonsArgType.LIST,
                value: IconType.getKey(<string>this.IconName())
            });
            return args;
        }

        /**
         * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
         * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
         * @return {void}
         */
        public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
            switch ($value.name) {
            case "IconName":
                this.IconName(IconType[<string>$value.value]);
                break;
            default:
                super.setArg($value, $force);
                break;
            }
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setBeforeLoad(($eventArgs : EventArgs) : void => {
                const element : LabelList = <LabelList>$eventArgs.Owner();
                element.Enabled(element.Enabled());
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            const output : IGuiElement = this.addElement(this.Id() + "_IconType").StyleClassName(this.IconName());
            this.items.foreach(($item : Label) : void => {
                output.Add(this.addElement()
                    .StyleClassName("Item")
                    .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                    .Add($item)
                );
            });
            return output;
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();
            if (ObjectValidator.IsEmptyOrNull(this.items)) {
                this.items = new ArrayList<Label>();
            }
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            if (this.items.IsEmpty()) {
                exclude.push("items");
            }
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push("iconType");
            return exclude;
        }
    }
}
