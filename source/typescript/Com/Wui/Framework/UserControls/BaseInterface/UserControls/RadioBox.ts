/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;

    /**
     * RadioBox class renders switch type of element, which belongs to elements group,
     * where only of the element from the group can be selected.
     */
    export class RadioBox extends CheckBox implements Com.Wui.Framework.Gui.Interfaces.UserControls.IRadioBox {
        private groupName : string;

        /**
         * @param {RadioBox} $element Specify element, which should be handled.
         * @return {void}
         */
        public static ToggleChecked($element : RadioBox) : void {
            $element.Checked(!$element.Checked());
        }

        /**
         * @param {string} $groupName Specify group name, which should be handled.
         * @param {boolean} $value Switch type of group mode between enabled and disabled.
         * @return {void}
         */
        public static EnabledGroup($groupName : string, $value : boolean) : void {
            GuiObjectManager.getInstanceSingleton().getType(RadioBox).foreach(($element : RadioBox) : void => {
                if ($element.GroupName() === $groupName) {
                    $element.Enabled($value);
                }
            });
        }

        /**
         * @param {string} $groupName Specify group name, which should be handled.
         * @param {boolean} $value Specify, if group is in error status or not.
         * @return {void}
         */
        public static ErrorGroup($groupName : string, $value : boolean) : void {
            GuiObjectManager.getInstanceSingleton().getType(RadioBox).foreach(($element : RadioBox) : void => {
                if ($element.GroupName() === $groupName) {
                    $element.Error($value);
                }
            });
        }

        protected static checked($element : RadioBox) : void {
            let eventArgs : EventArgs;
            const elements : ArrayList<RadioBox> = $element.getGroup();
            elements.foreach(($groupElement : any) : void => {
                if ($groupElement.Id() !== $element.Id()) {
                    $groupElement.setChanged();
                    $groupElement.Checked(false);

                    eventArgs = new EventArgs();
                    eventArgs.Owner($groupElement);
                    $element.getEventsManager().FireEvent($groupElement, EventType.ON_CHANGE, eventArgs);
                    $element.getEventsManager().FireEvent($groupElement.getClassName(), EventType.ON_CHANGE, eventArgs);
                }
            });
            $element.setChanged();
            $element.Checked(true);

            eventArgs = new EventArgs();
            eventArgs.Owner($element);
            $element.getEventsManager().FireEvent($element, EventType.ON_CHANGE, eventArgs);
            $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_CHANGE, eventArgs);
        }

        protected static onClickEventHandler($eventArgs : MouseEventArgs) : void {
            RadioBox.checked($eventArgs.Owner());
        }

        /**
         * @param {string} $groupName Specify group name, to which the element should belongs to.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($groupName : string, $id? : string) {
            super($id);
            this.GroupName($groupName);
        }

        /**
         * @param {string} [$value] Set text value, which should be displayed as element context.
         * @return {string} Returns element's text value.
         */
        public GroupName($value? : string) : string {
            return this.groupName = Property.String(this.groupName, $value);
        }

        /**
         * @return {ArrayList<RadioBox>} Returns list of elements, which belong to the same group as current element.
         */
        public getGroup() : ArrayList<RadioBox> {
            const group : ArrayList<RadioBox> = new ArrayList<RadioBox>();
            const groupName : string = this.GroupName();
            const elements : ArrayList<IGuiCommons> = this.getGuiManager().getType(RadioBox);
            elements.foreach(($element : RadioBox) : void => {
                if ($element.GroupName() === groupName) {
                    group.Add($element);
                }
            });
            return group;
        }

        /**
         * @param {boolean} [$value] Specify element's check state.
         * @return {boolean} Returns element's check state.
         */
        public Checked($value? : boolean) : boolean {
            const checked : boolean = super.Checked($value);
            if (!this.IsCompleted() && !this.IsPersistent() ||
                ElementManager.Exists(this.Id())) {
                if (ObjectValidator.IsSet($value) && checked) {
                    this.getGroup().foreach(($element : RadioBox) : void => {
                        if ($element.Id() !== this.Id()) {
                            $element.Checked(false);
                        }
                    });
                }
            }
            return checked;
        }
    }
}
