/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ButtonType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import ScrollBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ScrollBar;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import DragBar = Com.Wui.Framework.UserControls.BaseInterface.Components.DragBar;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import IGuiCommonsListArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsListArg;
    import IButtonEvents = Com.Wui.Framework.Gui.Interfaces.Events.IButtonEvents;

    /**
     * Button class renders button type of element.
     */
    export class Button extends ImageButton implements Com.Wui.Framework.Gui.Interfaces.UserControls.IButton {
        private text : string;
        private width : number;

        /**
         * @param {Button} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnOn($element : Button, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!$manager.IsActive($element)
                && !$manager.IsActive(<ClassName>ScrollBar)
                && !$manager.IsActive(<ClassName>ResizeBar)
                && !$manager.IsActive(<ClassName>DragBar)) {
                ElementManager.TurnOn($element.Id() + "_Enabled");
            }
        }

        /**
         * @param {Button} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnOff($element : Button, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!$manager.IsActive($element)) {
                ElementManager.TurnOff($element.Id() + "_Enabled");
            }
        }

        /**
         * @param {Button} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnActive($element : Button, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            ElementManager.TurnActive($element.Id() + "_Enabled");
        }

        /**
         * @param {Button} $element Specify element, which should be handled.
         * @param {boolean} [$value] Specify state of button selection.
         * @return {void}
         */
        public static TurnSelected($element : Button, $value : boolean) : void {
            ImageButton.TurnSelected($element, $value);
            Button.resize($element);
        }

        private static resize($element : Button) : void {
            if ($element.Width() !== -1) {
                const resize : any = ($id : string) : void => {
                    const width : number = $element.Width();
                    const centerWidth : number = width
                        - ElementManager.getOffsetWidth($id + "Left")
                        - ElementManager.getOffsetWidth($id + "Right");

                    const contentEnvelopWidth : number = width
                        - ElementManager.getCssIntegerValue($id + "Left", "border-left-width")
                        - ElementManager.getCssIntegerValue($id + "Right", "border-right-width");

                    const maxTextWidth : number = contentEnvelopWidth -
                        ElementManager.getWidthOffset($id + "Text") -
                        ElementManager.getEnvelopWidth($id + "IconEnvelop") -
                        ElementManager.getEnvelopWidth($id + "Splitter");

                    ElementManager.setWidth($id + "ContentEnvelop", contentEnvelopWidth);
                    ElementManager.setWidth($id + "Center", centerWidth);
                    ElementManager.setCssProperty($id + "Text", "max-width", maxTextWidth);
                };

                const id : string = $element.Id();
                if (ElementManager.IsVisible($element.Id() + "_Enabled")) {
                    if ($element.IsSelected() && $element.getGuiOptions().Contains(GuiOptionType.SELECTED, GuiOptionType.ACTIVED)
                        && ElementManager.Exists(id + "_StaticContent")) {
                        resize(id + "_Static");
                    } else {
                        resize(id + "_");
                    }
                } else if ($element.getGuiOptions().Contains(GuiOptionType.DISABLE)
                    && ElementManager.Exists(id + "_DisabledContent")) {
                    resize(id + "_Disabled");
                }

                $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE);
                $element.getEventsManager().FireEvent(Button.ClassName(), EventType.ON_RESIZE);
            }
        }

        /**
         * @param {ButtonType} [$buttonType] Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($buttonType? : ButtonType, $id? : string) {
            super("", $id);
            this.GuiType($buttonType);
        }

        /**
         * @return {IButtonEvents} Returns events manager subscribed to the item.
         */
        public getEvents() : IButtonEvents {
            return <IButtonEvents>super.getEvents();
        }

        /**
         * @param {ButtonType} [$buttonType] Specify type of element look and feel.
         * @return {ButtonType} Returns type of element's look and feel.
         */
        public GuiType($buttonType? : ButtonType) : ButtonType {
            return <ButtonType>super.GuiType($buttonType);
        }

        /**
         * @param {string} [$value] Set text value, which should be displayed as element context.
         * @return {string} Returns element's text value.
         */
        public Text($value? : string) : string {
            if (ObjectValidator.IsSet($value) && this.text !== $value) {
                this.text = Property.String(this.text, $value);
                if (this.IsLoaded()) {
                    ElementManager.setInnerHtml(this.Id() + "_Text", $value);
                    ElementManager.setInnerHtml(this.Id() + "_StaticText", $value);
                    ElementManager.setInnerHtml(this.Id() + "_DisabledText", $value);
                    this.getEventsManager().FireEvent(this, EventType.ON_CHANGE);
                    this.getEventsManager().FireEvent(Button.ClassName(), EventType.ON_CHANGE);
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        Button.resize(this);
                    }, false);
                }
            }
            return this.text;
        }

        /**
         * @param {number} [$value] Specify element's width value.
         * If value is -1, element will has auto size, otherwise width has to be more that 30.
         * @return {number} Returns element's width value.
         */
        public Width($value? : number) : number {
            if (ObjectValidator.IsSet($value)) {
                if (this.width !== $value) {
                    this.width = Property.PositiveInteger(this.width, $value, 30);
                    if ($value === -1) {
                        if (!this.IsLoaded()) {
                            this.width = $value;
                        } else {
                            this.width = -1;
                            this.width = this.Width();
                        }
                    }

                    if (ElementManager.IsVisible(this.Id())) {
                        Button.resize(this);
                    }
                }
            } else {
                if (this.width === -1 && this.IsLoaded()) {
                    this.width = ElementManager.getElement(this.Id()).offsetWidth;
                    const getWidth : any = ($id : string) : number => {
                        ElementManager.ClearCssProperty($id + "Text", "max-width");
                        ElementManager.setCssProperty($id + "Content", "position", "fixed");
                        let contentWidth : number = ElementManager.getElement($id + "Content").offsetWidth;
                        if (contentWidth === 0) {
                            this.unhide(($elementId : string, $element : HTMLElement) : boolean => {
                                if ($elementId === $id + "Content") {
                                    contentWidth = 1 + $element.offsetWidth;
                                    return false;
                                }
                                return true;
                            });
                        }
                        const width : number = contentWidth +
                            ElementManager.getCssIntegerValue($id + "Left", "border-left-width") +
                            ElementManager.getCssIntegerValue($id + "Right", "border-right-width");
                        ElementManager.setCssProperty($id + "Content", "position", "relative");
                        return width;
                    };
                    if (this.IsSelected() && this.getGuiOptions().Contains(GuiOptionType.SELECTED, GuiOptionType.ACTIVED)) {
                        this.width = getWidth(this.Id() + "_Static");
                    } else if (!this.Enabled() && this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                        this.width = getWidth(this.Id() + "_Disabled");
                    } else if (this.Enabled()) {
                        this.width = getWidth(this.Id() + "_");
                    }
                }
            }
            return this.width;
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            let enabled : boolean = super.Enabled();
            if (ObjectValidator.IsSet($value)) {
                if (enabled !== $value) {
                    enabled = super.Enabled($value);
                    if (ElementManager.IsVisible(this.Id())) {
                        Button.resize(this);
                    }
                }
            }
            return enabled;
        }

        /**
         * @return {Size} Returns current element's width and height based on available information.
         */
        public getSize() : Size {
            const size : Size = super.getSize();
            size.Width(this.Width());
            if (this.IsLoaded()) {
                size.Height(ElementManager.getElement(this.guiContentId()).offsetHeight);
            }
            return size;
        }

        /**
         * @return {IGuiCommonsArg[]} Returns array of element's attributes.
         */
        public getArgs() : IGuiCommonsArg[] {
            const args : IGuiCommonsArg[] = super.getArgs();
            let index : number;
            for (index = 0; index < args.length; index++) {
                if (args[index].name === "Value") {
                    args[index].value = this.Text();
                    break;
                }
            }
            args.push({
                name : "Text",
                type : GuiCommonsArgType.TEXT,
                value: this.Text()
            });
            args.push({
                name : "Width",
                type : GuiCommonsArgType.NUMBER,
                value: this.Width()
            });
            args.push(<IGuiCommonsListArg>{
                items: ButtonType.getProperties(),
                name : "GuiType",
                type : GuiCommonsArgType.LIST,
                value: ButtonType.getKey(<string>this.GuiType())
            });
            return args;
        }

        /**
         * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
         * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
         * @return {void}
         */
        public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
            switch ($value.name) {
            case "Value":
            case "Text":
                this.Text(<string>$value.value);
                break;
            case "Width":
                this.Width(<number>$value.value);
                break;
            case "Height":
                ElementManager.setCssProperty(this.Id(), "height", <number>$value.value);
                break;
            case "GuiType":
                this.GuiType(ButtonType[<string>$value.value]);
                break;
            default:
                super.setArg($value, $force);
                break;
            }
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, ButtonType, ButtonType.GENERAL);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!ButtonType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of button type instead of StyleClassName method.");
            return false;
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                Button.resize($eventArgs.Owner());
            });

            this.getEvents().setBeforeLoad(($eventArgs : EventArgs) : void => {
                Button.resize($eventArgs.Owner());
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            const selectedOption : IGuiElement = this.addElement();
            if (this.getGuiOptions().Contains(GuiOptionType.ACTIVED, GuiOptionType.SELECTED)) {
                selectedOption
                    .Id(this.Id() + "_Static")
                    .StyleClassName(GeneralCssNames.ON).GuiTypeTag(this.getGuiTypeTag()).Visible(this.IsSelected())
                    .Add(this.addElement()
                        .StyleClassName(GeneralCssNames.BACKGROUND)
                        .Add(this.addElement(this.Id() + "_StaticLeft").StyleClassName(GeneralCssNames.LEFT))
                        .Add(this.addElement(this.Id() + "_StaticCenter")
                            .StyleClassName(GeneralCssNames.CENTER)
                            .Width(this.Width())
                            .Add(this.addElement(this.Id() + "_StaticContentEnvelop")
                                .StyleClassName("Envelop")
                                .Add(this.addElement(this.Id() + "_StaticContent")
                                    .StyleClassName("Content")
                                    .Add(this.addElement(this.Id() + "_StaticIconEnvelop")
                                        .StyleClassName(this.IconName())
                                        .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                                    )
                                    .Add(this.addElement(this.Id() + "_StaticSplitter").StyleClassName(GeneralCssNames.SPLITTER))
                                    .Add(this.addElement(this.Id() + "_StaticText")
                                        .StyleClassName(GeneralCssNames.TEXT)
                                        .Add(this.text)
                                    )
                                )
                            )
                        )
                        .Add(this.addElement(this.Id() + "_StaticRight").StyleClassName(GeneralCssNames.RIGHT))
                    );
            }

            const disabledOption : IGuiElement = this.addElement();
            if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                disabledOption
                    .Id(this.Id() + "_Disabled")
                    .StyleClassName(GeneralCssNames.DISABLE).GuiTypeTag(this.getGuiTypeTag()).Visible(!this.Enabled())
                    .Add(this.addElement()
                        .StyleClassName(GeneralCssNames.BACKGROUND)
                        .Add(this.addElement(this.Id() + "_DisabledLeft").StyleClassName(GeneralCssNames.LEFT))
                        .Add(this.addElement(this.Id() + "_DisabledCenter")
                            .StyleClassName(GeneralCssNames.CENTER)
                            .Width(this.Width())
                            .Add(this.addElement(this.Id() + "_DisabledContentEnvelop")
                                .StyleClassName("Envelop")
                                .Add(this.addElement(this.Id() + "_DisabledContent")
                                    .StyleClassName("Content")
                                    .Add(this.addElement(this.Id() + "_DisabledIconEnvelop")
                                        .StyleClassName(this.IconName())
                                        .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                                    )
                                    .Add(this.addElement(this.Id() + "_DisabledSplitter").StyleClassName(GeneralCssNames.SPLITTER))
                                    .Add(this.addElement(this.Id() + "_DisabledText")
                                        .StyleClassName(GeneralCssNames.TEXT)
                                        .Add(this.text)
                                    )
                                )
                            )
                        )
                        .Add(this.addElement(this.Id() + "_DisabledRight").StyleClassName(GeneralCssNames.RIGHT))
                    );
            }

            return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType())
                .Add(this.addElement(this.Id() + "_Status")
                    .StyleClassName(this.errorCssName())
                    .Add(this.addElement(this.Id() + "_Enabled")
                        .StyleClassName(GeneralCssNames.OFF)
                        .Visible(this.Enabled())
                        .Add(this.addElement(this.Id() + "_Active")
                            .Visible(!this.IsSelected()).GuiTypeTag(this.getGuiTypeTag())
                            .Add(this.selectorElement())
                            .Add(this.addElement()
                                .StyleClassName(GeneralCssNames.BACKGROUND)
                                .Add(this.addElement(this.Id() + "_Left").StyleClassName(GeneralCssNames.LEFT))
                                .Add(this.addElement(this.Id() + "_Center")
                                    .StyleClassName(GeneralCssNames.CENTER)
                                    .Width(this.Width())
                                    .Add(this.addElement(this.Id() + "_ContentEnvelop")
                                        .StyleClassName("Envelop")
                                        .Add(this.addElement(this.Id() + "_Content")
                                            .StyleClassName("Content")
                                            .Add(this.addElement(this.Id() + "_IconEnvelop")
                                                .StyleClassName(this.IconName())
                                                .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                                            )
                                            .Add(this.addElement(this.Id() + "_Splitter").StyleClassName(GeneralCssNames.SPLITTER))
                                            .Add(this.addElement(this.Id() + "_Text")
                                                .StyleClassName(GeneralCssNames.TEXT)
                                                .Add(this.text)
                                            )
                                        )
                                    )
                                )
                                .Add(this.addElement(this.Id() + "_Right").StyleClassName(GeneralCssNames.RIGHT))
                            )
                        )
                        .Add(selectedOption)
                    )
                    .Add(disabledOption)
                );
        }

        protected guiContentId() : string {
            if (this.IsSelected() && this.getGuiOptions().Contains(GuiOptionType.SELECTED, GuiOptionType.ACTIVED)) {
                return this.Id() + "_StaticContentEnvelop";
            } else if (!this.Enabled() && this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                return this.Id() + "_DisabledContentEnvelop";
            } else if (this.Enabled()) {
                return this.Id() + "_ContentEnvelop";
            }
            return this.Id();
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();
            this.text = this.Id();
            this.width = -1;
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push("text", "width");
            return exclude;
        }
    }
}
