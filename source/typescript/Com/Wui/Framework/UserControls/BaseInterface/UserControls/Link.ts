/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import LinkSelector = Com.Wui.Framework.UserControls.Utils.LinkSelector;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;

    /**
     * Link class renders clickable simple text.
     */
    export class Link extends Label implements Com.Wui.Framework.Gui.Interfaces.UserControls.ILink {
        private selector : LinkSelector;

        /**
         * @param {Link} $element Specify element, which should be handled.
         * @param {boolean} [$force=false] If true, than do not care about TAB navigation state.
         * @return {void}
         */
        public static Focus($element : Link, $force : boolean = false) : void {
            LinkSelector.Focus($element, $force);
        }

        /**
         * @return {void}
         */
        public static Blur() : void {
            LinkSelector.Blur(<ClassName>Link);
        }

        /**
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($id? : string) {
            super("", $id);
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            return this.getSelector().Enabled(super.Enabled($value));
        }

        /**
         * @param {string} [$value] Set link value, which will be used for redirect in case of element click.
         * @return {string} Returns element's link value.
         */
        public ReloadTo($value? : string) : string {
            return this.getSelector().ReloadTo($value);
        }

        /**
         * @param {boolean} [$value] Specify, if reloaded content should be opened in new window.
         * @return {boolean} Returns true, if reloaded content will be opened in new window, otherwise false.
         */
        public OpenInNewWindow($value? : boolean) : boolean {
            return this.getSelector().OpenInNewWindow($value);
        }

        /**
         * @param {number} [$value] Set index for TAB key navigation in the page.
         * @return {number} Returns index of element in the page based on TAB key elements register.
         */
        public TabIndex($value? : number) : number {
            return this.getSelector().TabIndex($value);
        }

        public IsPreventingScroll() : boolean {
            return true;
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();
            this.getEvents().Subscriber(this.Id());
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push("selector");
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push("selector");
            return exclude;
        }

        protected innerCode() : IGuiElement {
            if (ObjectValidator.IsEmptyOrNull(this.getSelector().getValue())) {
                this.getSelector().ReloadTo(this.getHttpManager().CreateLink(""));
            }
            return super.innerCode().Add(this.getSelector().getInnerCode());
        }

        protected innerHtml() : IGuiElement {
            const innerHtml : IGuiElement = super.innerHtml();
            (<IGuiElement>innerHtml.getChildElements().getFirst()).StyleClassName(GeneralCssNames.OFF);
            return innerHtml;
        }

        private getSelector() : LinkSelector {
            if (!ObjectValidator.IsSet(this.selector)) {
                this.selector = new LinkSelector(this);
            }
            return this.selector;
        }
    }
}
