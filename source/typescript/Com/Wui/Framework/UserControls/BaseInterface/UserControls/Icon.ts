/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import IIcon = Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IconType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.IconType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;

    /**
     * Icon class renders icon element as independent user control.
     */
    export class Icon extends Com.Wui.Framework.UserControls.Primitives.FormsObject implements IIcon {
        private iconType : IconType;

        /**
         * @param {IconType} $iconType Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($iconType : IconType, $id? : string) {
            super($id);
            this.iconType = this.iconTypeValueSetter($iconType);
        }

        /**
         * @param {IconType} [$type] Specify type of element look and feel.
         * @return {IconType} Returns type of element's look and feel.
         */
        public IconType($type? : IconType) : IconType {
            if (ObjectValidator.IsSet($type)) {
                this.iconType = this.iconTypeValueSetter($type);
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_Type", this.iconType.toString());
                }
            }
            return this.iconType;
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!IconType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use IconType method for set of icon type instead of StyleClassName method.");
            return false;
        }

        protected iconTypeValueSetter($value : any) : any {
            return Property.EnumType(this.iconType, $value, IconType, IconType.GENERAL);
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Status")
                .StyleClassName(this.statusCss())
                .Add(this.addElement(this.Id() + "_Type")
                    .StyleClassName(this.iconType).GuiTypeTag("Icon")
                    .Add(this.addElement(this.Id() + "_Icon").StyleClassName(GeneralCssNames.ICON))
                );
        }

        protected cssContainerName() : string {
            return "Icons";
        }
    }
}
