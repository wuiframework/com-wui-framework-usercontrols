/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import ICheckBox = Com.Wui.Framework.Gui.Interfaces.UserControls.ICheckBox;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;

    /**
     * CheckBox class renders switch type of element.
     */
    export class CheckBox extends Com.Wui.Framework.UserControls.Primitives.FormsObject implements ICheckBox {
        private text : string;
        private checked : boolean;

        /**
         * @param {CheckBox} $element Specify element, which should be handled.
         * @return {void}
         */
        public static ToggleChecked($element : CheckBox) : void {
            CheckBox.checked($element, !$element.Checked());
        }

        protected static onHoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void {
            $eventArgs.StopAllPropagation();
            $manager.setHovered($eventArgs.Owner(), true);
        }

        protected static checked($element : CheckBox, $value : boolean) : void {
            if ($element.Checked() !== $value) {
                $element.setChanged();
                $element.Checked($value);

                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner($element);
                $element.getEventsManager().FireEvent($element, EventType.ON_CHANGE, eventArgs);
                $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_CHANGE, eventArgs);
            }
            if ($value) {
                ElementManager.setClassName($element.Id() + "_Check", "Checked");
            } else {
                ElementManager.setClassName($element.Id() + "_Check", "Unchecked");
            }
        }

        protected static onClickEventHandler($eventArgs : MouseEventArgs) : void {
            CheckBox.ToggleChecked($eventArgs.Owner());
        }

        /**
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($id? : string) {
            super($id);
        }

        /**
         * @param {string} [$value] Set text value, which should be displayed as element context.
         * @return {string} Returns element's text value.
         */
        public Text($value? : string) : string {
            this.text = Property.NullString(this.text, $value);
            if (ObjectValidator.IsSet($value)) {
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setInnerHtml(this.Id() + "_Text", $value);
                    ElementManager.setInnerHtml(this.Id() + "_DisabledText", $value);
                }
            }
            return this.text;
        }

        /**
         * @param {boolean} [$value] Specify element's check state.
         * @return {boolean} Returns element's check state.
         */
        public Checked($value? : boolean) : boolean {
            if (!this.IsCompleted() && this.IsPersistent()) {
                this.checked = this.valuesPersistence.Variable(this.InstancePath());
            } else {
                this.checked = Property.Boolean(this.checked, $value);
            }
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                CheckBox.checked(this, this.checked);
            }
            if (!ObjectValidator.IsSet(this.checked)) {
                this.checked = false;
            }
            return this.checked;
        }

        public Value($value? : boolean) : boolean {
            return this.Checked($value);
        }

        /**
         * @return {IGuiCommonsArg[]} Returns array of element's attributes.
         */
        public getArgs() : IGuiCommonsArg[] {
            const args : IGuiCommonsArg[] = super.getArgs();
            args.push({
                name : "Text",
                type : GuiCommonsArgType.TEXT,
                value: this.Text()
            });
            return args;
        }

        /**
         * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
         * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
         * @return {void}
         */
        public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
            switch ($value.name) {
            case "Text":
                this.Text(<string>$value.value);
                break;
            default:
                super.setArg($value, $force);
                break;
            }
        }

        public IsPreventingScroll() : boolean {
            return true;
        }

        protected checkedCss() : string {
            return this.Checked() ? "Checked" : "Unchecked";
        }

        protected innerCode() : IGuiElement {
            this.getEvents().Subscriber(this.Id() + "_Enabled");

            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
            this.getEvents().setOnMouseOver(thisClass.onHoverEventHandler);

            this.getEvents().setOnClick(
                ($eventArgs : MouseEventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    const element : CheckBox = <CheckBox>$eventArgs.Owner();
                    $reflection.getClass(element.getClassName()).onClickEventHandler($eventArgs);
                });
            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            const disabledOption : IGuiElement = this.addElement();
            if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                disabledOption
                    .Id(this.Id() + "_Disabled")
                    .StyleClassName(GeneralCssNames.DISABLE)
                    .Visible(!this.Enabled())
                    .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                    .Add(this.addElement(this.Id() + "_DisabledText")
                        .StyleClassName(GeneralCssNames.TEXT)
                        .Add(this.text)
                    );
            }

            return this.addElement(this.Id() + "_Status").StyleClassName(this.statusCss())
                .Add(this.addElement(this.Id() + "_Check")
                    .StyleClassName(this.checkedCss())
                    .GuiTypeTag(this.getGuiTypeTag())
                    .Add(this.addElement(this.Id() + "_Enabled")
                        .StyleClassName(GeneralCssNames.OFF)
                        .Visible(this.Enabled())
                        .Add(this.selectorElement())
                        .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                        .Add(this.addElement(this.Id() + "_Text")
                            .StyleClassName(GeneralCssNames.TEXT)
                            .Add(this.text)
                        )
                    )
                    .Add(disabledOption)
                );
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();
            this.text = this.Id();
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push("text", "checked");
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            if (this.Checked() === false) {
                exclude.push("checked");
            }
            return exclude;
        }
    }
}
