/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import INumberPicker = Com.Wui.Framework.Gui.Interfaces.UserControls.INumberPicker;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import NumberPickerType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.NumberPickerType;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import NumberPickerEventArgs = Com.Wui.Framework.Gui.Events.Args.NumberPickerEventArgs;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import KeyEventHandler = Com.Wui.Framework.Gui.Utils.KeyEventHandler;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import Borders = Com.Wui.Framework.Gui.Structures.Borders;
    import ScrollBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ScrollBar;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import DragBar = Com.Wui.Framework.UserControls.BaseInterface.Components.DragBar;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import TextSelectionManager = Com.Wui.Framework.Gui.Utils.TextSelectionManager;

    /**
     * NumberPicker class renders element for easy pick of number from specified range of numbers.
     */
    export class NumberPicker extends Com.Wui.Framework.UserControls.Primitives.FormsObject implements INumberPicker {
        private static threadCallback : any;
        private static threadTick : number;

        private value : number;
        private valueStep : number;
        private rangeStart : number;
        private rangeEnd : number;
        private width : number;
        private text : string;
        private guiType : NumberPickerType;
        private pointerEvents : ElementEventsManager;
        private trackerEvents : ElementEventsManager;
        private isTriggeredState : boolean;
        private pointerPosition : number;
        private stopPointerPosition : number;
        private decimalPlacesCount : number;

        /**
         * @param {NumberPicker} $element Specify element, which should be handled.
         * @return {void}
         */
        public static NotificationShow($element : NumberPicker) : void {
            const manager : GuiObjectManager = $element.getGuiManager();
            if (!manager.IsActive(<ClassName>ScrollBar)
                && !manager.IsActive(<ClassName>ResizeBar)
                && !manager.IsActive(<ClassName>DragBar)) {
                const id : string = $element.Id() + "_NotificationEnvelop";
                if (!ElementManager.IsVisible(id)) {
                    ElementManager.setOpacity(id, 0);
                }
                ElementManager.Show(id);
                NumberPicker.centerNotification($element);
                if (!manager.IsActive(<ClassName>NumberPicker) || manager.IsActive($element) || manager.IsHovered($element)) {
                    ElementManager.ChangeOpacity(id, DirectionType.UP, 11);
                }
            }
        }

        /**
         * @return {void}
         */
        public static NotificationHide() : void {
            Loader.getInstance().getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
                const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                const elements : ArrayList<IGuiCommons> = manager.getType(NumberPicker);
                elements.foreach(($element : NumberPicker) : void => {
                    if (!manager.IsActive($element) && !manager.IsHovered($element)) {
                        ElementManager.Hide($element.Id() + "_NotificationEnvelop");
                    }
                });
            }, 100);
        }

        /**
         * @param {NumberPicker} $element Specify element, which should be handled.
         * @return {void}
         */
        public static PointerOn($element : NumberPicker) : void {
            const manager : GuiObjectManager = $element.getGuiManager();
            if (!manager.IsActive(<ClassName>NumberPicker)
                && !manager.IsActive(<ClassName>ScrollBar)
                && !manager.IsActive(<ClassName>ResizeBar)
                && !manager.IsActive(<ClassName>DragBar)) {
                if ($element.Enabled()) {
                    ElementManager.TurnOn($element.Id() + "_Pointer");
                }
            }
        }

        /**
         * @param {NumberPicker} $element Specify element, which should be handled.
         * @return {void}
         */
        public static PointerOff($element : NumberPicker) : void {
            if (!$element.getGuiManager().IsActive($element)) {
                ElementManager.TurnOff($element.Id() + "_Pointer");
            }
        }

        /**
         * @param {NumberPicker} $element Specify element, which should be handled.
         * @return {void}
         */
        public static Focus($element : NumberPicker) : void {
            $element.getGuiManager().setActive($element, true);
            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner($element);
            $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_FOCUS, eventArgs);
            $element.getEventsManager().FireEvent($element, EventType.ON_FOCUS, eventArgs);
            $element.isTriggered(true);
            if ($element.Enabled()) {
                ElementManager.TurnActive($element.Id() + "_Pointer");
            }
            NumberPicker.NotificationShow($element);
        }

        /**
         * @return {void}
         */
        public static Blur() : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            const elements : ArrayList<IGuiCommons> = manager.getActive(<ClassName>NumberPicker);
            elements.foreach(($element : NumberPicker) : void => {
                manager.setActive($element, false);
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner($element);
                $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_BLUR, eventArgs);
                $element.getEventsManager().FireEvent($element, EventType.ON_BLUR, eventArgs);
                if ($element.Enabled()) {
                    ElementManager.TurnOff($element.Id() + "_Pointer");
                }
                NumberPicker.moveStop();
                $element.isTriggered(false);
                NumberPicker.NotificationHide();
            });
        }

        protected static onKeyEventHandler($eventArgs : KeyEventArgs, $manager : GuiObjectManager) : void {
            if (KeyEventHandler.IsNavigate($eventArgs.NativeEventArgs()) && $manager.IsActive(<ClassName>NumberPicker)) {
                const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>NumberPicker);
                const keyCode : number = $eventArgs.getKeyCode();
                Loader.getInstance().getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
                    elements.foreach(($element : NumberPicker) : void => {
                        if ($element.Enabled()) {
                            if (keyCode === KeyMap.LEFT_ARROW || keyCode === KeyMap.DOWN_ARROW) {
                                NumberPicker.moveMinus($element, 5);
                            } else if (keyCode === KeyMap.RIGHT_ARROW || keyCode === KeyMap.UP_ARROW) {
                                NumberPicker.movePlus($element, 5);
                            }

                            clearTimeout(NumberPicker.threadCallback);
                            NumberPicker.threadTick = 0;
                        }
                    });
                }, 10);
            }
        }

        private static resize($element : NumberPicker) : void {
            const id : string = $element.Id();
            ElementManager.setWidth(id + "_Center",
                $element.Width() -
                ElementManager.getOffsetWidth(id + "_Left") -
                ElementManager.getOffsetWidth(id + "_Right"));
            ElementManager.setWidth(id + "_BarCenter",
                $element.Width() -
                ElementManager.getOffsetWidth(id + "_BarLeft") -
                ElementManager.getOffsetWidth(id + "_BarRight"));
            ElementManager.setWidth(id + "_BarEnvelop", $element.Width());
            NumberPicker.move($element);
        }

        private static moveInit($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void {
            const element : NumberPicker = <NumberPicker>$eventArgs.Owner();
            if (element.Enabled()) {
                $manager.setActive(element, true);
                element.isTriggered(false);

                let pointerEvents : string;
                let onStartHandler : any;
                const onChangeHandler : any = ($eventArgs : MoveEventArgs, $manager : GuiObjectManager) : void => {
                    if ($manager.IsActive(<ClassName>NumberPicker)) {
                        $eventArgs.StopAllPropagation();
                        document.body.style.cursor = "default";
                        document.body.style.pointerEvents = "none";

                        const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>NumberPicker);
                        elements.foreach(($element : NumberPicker) : void => {
                            if (!$element.isTriggered() && $element.Enabled()) {
                                ElementManager.TurnActive($element.Id() + "_Pointer");
                                let percentage : number = 1 /
                                    ($element.Width() - ElementManager.getElement($element.Id() + "_Pointer").offsetWidth) *
                                    ($element.position() + $eventArgs.getDistanceX());
                                if (percentage < 0) {
                                    percentage = 0;
                                } else if (percentage > 1) {
                                    percentage = 1;
                                }
                                const newValue : number =
                                    $element.RangeStart() + ($element.RangeEnd() - $element.RangeStart()) * percentage;
                                if ($element.ValueStep() !== -1) {
                                    if (Math.abs(element.Value() - newValue) >= $element.ValueStep() / 2) {
                                        let newValueWithStep : number;
                                        if (element.Value() < newValue) {
                                            newValueWithStep = Math.ceil(newValue / $element.ValueStep()) * $element.ValueStep();
                                        } else {
                                            newValueWithStep = Math.floor(newValue / $element.ValueStep()) * $element.ValueStep();
                                        }
                                        element.Value(newValueWithStep);
                                    }
                                } else {
                                    element.Value(newValue);
                                }
                            }
                        });
                    }
                };
                const onCompleteHandler : any = () : void => {
                    document.body.style.pointerEvents = pointerEvents;
                    TextSelectionManager.Disable(false);
                    TextSelectionManager.Clear();
                    NumberPicker.moveStop();
                    element.getEventsManager().RemoveHandler(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START, onStartHandler);
                    element.getEventsManager().RemoveHandler(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE, onChangeHandler);
                    element.getEventsManager().RemoveHandler(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE, onCompleteHandler);
                };
                onStartHandler = ($eventArgs : MoveEventArgs, $manager : GuiObjectManager) : void => {
                    $manager.setActive(element, true);
                    element.getEvents().FireAsynchronousMethod(() : void => {
                        if ($manager.IsActive(<ClassName>NumberPicker)) {
                            pointerEvents = document.body.style.pointerEvents;
                            TextSelectionManager.Disable(true);
                            $eventArgs.StopAllPropagation();
                            const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>NumberPicker);
                            elements.foreach(($element : NumberPicker) : void => {
                                if (!$element.isTriggered() && $element.Enabled()) {
                                    ElementManager.TurnActive($element.Id() + "_Pointer");
                                    NumberPicker.NotificationShow($element);
                                    element.position(
                                        ElementManager.getElement($element.Id() + "_Notification").offsetLeft
                                        - ElementManager.getCssIntegerValue($element.Id(), "margin-left")
                                        - ElementManager.getCssIntegerValue($element.Id(), "padding-left"));
                                }
                            });
                            element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE, onChangeHandler);
                            element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE, onCompleteHandler);
                        }
                    });
                };
                element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START, onStartHandler);
            }
        }

        private static move($element : NumberPicker) : void {
            const id : string = $element.Id();
            let position : number = 100 / ($element.RangeEnd() - $element.RangeStart()) * ($element.Value() - $element.RangeStart());

            if (position < 0) {
                position = 0;
            } else if (position > 100) {
                position = 100;
            }
            position = position / 100;

            const pointerWidth : number = ElementManager.getOffsetWidth(id + "_Pointer");
            let size : number = $element.Width() * position;
            if (position > 0.5) {
                size -= (pointerWidth / 4);
            }
            if (position < 0.5) {
                size += (pointerWidth / 4);
            }

            if (size < 0) {
                size = 0;
            }

            let left : number = ($element.Width() - pointerWidth) * position;
            if (left < 0) {
                left = 0;
            }

            ElementManager.setWidth(id + "_Bar", size);
            ElementManager.setCssProperty(id + "_Notification", "left", left);
            $element.updateNotificationText();

            if (ElementManager.IsVisible($element.Id() + "_NotificationEnvelop")) {
                NumberPicker.centerNotification($element);
            }

            const eventArgs : NumberPickerEventArgs = new NumberPickerEventArgs();
            eventArgs.Owner($element);
            eventArgs.CurrentValue($element.Value());
            eventArgs.RangeStart($element.RangeStart());
            eventArgs.RangeEnd($element.RangeEnd());
            eventArgs.Percentage(position * 100);
            $element.getEventsManager().FireEvent($element, EventType.ON_CHANGE, eventArgs);
            $element.getEventsManager().FireEvent(NumberPicker.ClassName(), EventType.ON_CHANGE, eventArgs);
        }

        private static moveStop() : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            if (manager.IsActive(<ClassName>NumberPicker)) {
                const elements : ArrayList<IGuiCommons> = manager.getActive(<ClassName>NumberPicker);
                elements.foreach(($element : NumberPicker) : void => {
                    $element.stopPosition(-1);
                    ElementManager.TurnOff($element.Id() + "_Pointer");
                    manager.setActive($element, false);
                });
                clearTimeout(NumberPicker.threadCallback);
                NumberPicker.threadTick = 0;
            }
        }

        private static moveMinus($element : NumberPicker, $step : number) : void {
            if ($element.Enabled()) {
                const getStepValue : any = ($percentage : number) : number => {
                    return ($element.RangeEnd() - $element.RangeStart()) / 100 * $percentage;
                };

                let newValue : number = $element.Value() - ($element.valueStep === -1 ? getStepValue($step) : $element.valueStep);
                let stopMove : boolean = false;
                if ($element.stopPosition() !== -1 && $element.stopPosition() > newValue) {
                    newValue = $element.stopPosition();
                    stopMove = true;
                }

                $element.getGuiManager().setActive($element, true);
                $element.isTriggered(true);
                $element.Value(newValue);

                if (!stopMove) {
                    NumberPicker.threadTick++;
                    if (ObjectValidator.IsSet(NumberPicker.threadCallback)) {
                        clearTimeout(NumberPicker.threadCallback);
                    }
                    NumberPicker.threadCallback = $element.getEvents().FireAsynchronousMethod(() : void => {
                        NumberPicker.moveMinus($element, getStepValue(3));
                    }, Math.sqrt(10000 / NumberPicker.threadTick));
                } else {
                    NumberPicker.moveStop();
                }
            }
        }

        private static movePlus($element : NumberPicker, $step : number) : void {
            if ($element.Enabled()) {
                const getStepValue : any = ($percentage : number) : number => {
                    return ($element.RangeEnd() - $element.RangeStart()) / 100 * $percentage;
                };

                let newValue : number = $element.Value() + ($element.valueStep === -1 ? getStepValue($step) : $element.valueStep);
                let stopMove : boolean = false;
                if ($element.stopPosition() !== -1 && $element.stopPosition() < newValue) {
                    newValue = $element.stopPosition();
                    stopMove = true;
                }

                $element.getGuiManager().setActive($element, true);
                $element.isTriggered(true);
                $element.Value(newValue);

                if (!stopMove) {
                    NumberPicker.threadTick++;
                    if (ObjectValidator.IsSet(NumberPicker.threadCallback)) {
                        clearTimeout(NumberPicker.threadCallback);
                    }
                    NumberPicker.threadCallback = $element.getEvents().FireAsynchronousMethod(() : void => {
                        NumberPicker.movePlus($element, getStepValue(3));
                    }, Math.sqrt(10000 / NumberPicker.threadTick));
                } else {
                    NumberPicker.moveStop();
                }
            }
        }

        private static pointerMoveEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void {
            if (!$manager.IsActive($eventArgs.Owner())) {
                const element : NumberPicker = <NumberPicker>$eventArgs.Owner();
                if (element.Enabled()) {
                    NumberPicker.NotificationShow(element);

                    const offset : ElementOffset = element.getScreenPosition();
                    let percentage : number = 100 / (ElementManager.getElement(element.Id() + "_Tracker").offsetWidth) *
                        Math.abs(
                            ElementManager.getCssIntegerValue(element.Id(), "margin-left") +
                            ElementManager.getCssIntegerValue(element.Id(), "padding-left") +
                            offset.Left()
                            - WindowManager.getMouseX($eventArgs.NativeEventArgs()));
                    if (percentage < 0) {
                        percentage = 0;
                    } else if (percentage > 100) {
                        percentage = 100;
                    }

                    let position : number = element.RangeStart() + (element.RangeEnd() - element.RangeStart()) / 100 * percentage;
                    let step : number = 5;
                    if (element.valueStep !== -1) {
                        position = Math.round(position / element.valueStep) * element.valueStep;
                        step = element.valueStep;
                    }

                    element.stopPosition(position);
                    if (element.stopPosition() < element.Value()) {
                        NumberPicker.moveMinus(element, step);
                    } else {
                        NumberPicker.movePlus(element, step);
                    }
                }
            }
        }

        private static centerNotification($element : NumberPicker) : void {
            const id : string = $element.Id();
            const notificationSize : Size = new Size(id + "_NotificationEnvelop", true);
            ElementManager.ClearCssProperty(id + "_Text", "width");
            const textWidth : number = ElementManager.getElement(id + "_Text").offsetWidth;
            if (textWidth > 0) {
                const borders : Borders = new Borders();
                borders.Left(
                    ElementManager.getElement(id + "_NotificationLeft").offsetWidth +
                    ElementManager.getCssIntegerValue(id + "_NotificationLeft", "border-left-width"));
                borders.Right(
                    ElementManager.getElement(id + "_NotificationRight").offsetWidth +
                    ElementManager.getCssIntegerValue(id + "_NotificationRight", "border-right-width"));

                notificationSize.Width(textWidth);
                const centerWidth : number = textWidth - borders.Left() - borders.Right();
                ElementManager.setWidth(id + "_NotificationEnvelop", Math.ceil(centerWidth > 0 ? centerWidth : 0)
                    + borders.Left() + borders.Right());
                ElementManager.setWidth(id + "_NotificationCenter", Math.floor(centerWidth > 0 ? centerWidth : 0));

                const pointerWidth : number = ElementManager.getElement(id + "_Pointer").offsetWidth;
                const arrowWidth : number = ElementManager.getElement(id + "_NotificationArrow").offsetWidth;
                ElementManager.setPosition($element.Id() + "_NotificationFixedHolder", $element.getScreenPosition());
                const notificationLeft : number = ElementManager.getElement(id + "_Notification").offsetLeft
                    - ((notificationSize.Width() - pointerWidth) / 2);
                ElementManager.setCssProperty(id + "_NotificationEnvelop", "left", notificationLeft);
                ElementManager.setCssProperty(id + "_Text", "left", (notificationSize.Width() - textWidth) / 2);
                ElementManager.setCssProperty(id + "_NotificationArrow", "left",
                    (notificationSize.Width() - pointerWidth) / 2 + (pointerWidth - arrowWidth) / 2);
            }
        }

        /**
         * @param {NumberPickerType} [$numberPickerType] Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($numberPickerType? : NumberPickerType, $id? : string) {
            super($id);
            this.guiType = this.guiTypeValueSetter($numberPickerType);
        }

        /**
         * @param {NumberPickerType} [$numberPickerType] Specify type of element look and feel.
         * @return {NumberPickerType} Returns type of element's look and feel.
         */
        public GuiType($numberPickerType? : NumberPickerType) : NumberPickerType {
            if (ObjectValidator.IsSet($numberPickerType)) {
                this.guiType = this.guiTypeValueSetter($numberPickerType);
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                }
            }
            return this.guiType;
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            const enabled : boolean = super.Enabled($value);
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                if (enabled) {
                    ElementManager.Show(this.Id() + "_Input");
                } else {
                    ElementManager.Hide(this.Id() + "_Input");
                }
            }
            return enabled;
        }

        /**
         * @param {number} [$value]  Value or arguments object, which should be set to the NumberPicker#
         * @return {number} Returns  object value.
         */
        public Value($value? : number) : number {
            if (this.Enabled()) {
                if (ObjectValidator.IsSet($value) && this.IsLoaded() && ElementManager.IsVisible(this.Id())) {
                    if (this.value !== $value) {
                        this.setChanged();

                        const eventArgs : EventArgs = new EventArgs();
                        eventArgs.Owner(this);
                        this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_CHANGE, eventArgs);
                    }
                }

                if (!this.IsCompleted() && this.IsPersistent()) {
                    this.value = this.valuesPersistence.Variable(this.InstancePath());
                } else if (ObjectValidator.IsDigit($value)) {
                    if ($value < this.rangeStart) {
                        $value = this.rangeStart;
                    }
                    if ($value > this.rangeEnd) {
                        $value = this.rangeEnd;
                    }
                    this.value = $value;

                    if (ElementManager.IsVisible(this.Id())) {
                        NumberPicker.move(this);
                    }
                }
            }
            return this.value;
        }

        /**
         * @param {number} [$value] Specify minimal positive and non-zero step for change of current value.
         * @return {number} Returns minimal step value, if value step has been specified, otherwise -1.
         */
        public ValueStep($value? : number) : number {
            if (ObjectValidator.IsDigit($value) && $value > 0) {
                this.valueStep = $value;
            }
            if (!ObjectValidator.IsSet(this.valueStep)) {
                this.valueStep = -1;
            }
            return this.valueStep;
        }

        /**
         * @param {number} [$value] Specify start value of numbers range.
         * @return {number} Returns start value of numbers range.
         */
        public RangeStart($value? : number) : number {
            if (ObjectValidator.IsDigit($value)) {
                this.rangeStart = $value;
            }
            if (this.value < this.rangeStart) {
                this.value = this.rangeStart;
            }
            if (this.rangeEnd < this.rangeStart) {
                this.rangeEnd = this.rangeStart;
            }
            return this.rangeStart;
        }

        /**
         * @param {number} [$value] Specify end value of numbers range.
         * @return {number} Returns end value of numbers range.
         */
        public RangeEnd($value? : number) : number {
            if (ObjectValidator.IsDigit($value) && $value >= this.rangeStart) {
                this.rangeEnd = $value;
            }
            if (this.value > this.rangeEnd) {
                this.value = this.rangeEnd;
            }
            return this.rangeEnd;
        }

        /**
         * @param {number} [$value] Specify element's width value.
         * @return {number} Returns element's width value.
         */
        public Width($value? : number) : number {
            this.width = Property.PositiveInteger(this.width, $value, 50);
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                NumberPicker.resize(this);
            }
            return this.width;
        }

        /**
         * @param {string} [$value] Set text value, which should be displayed as notification context.
         * @return {string} Returns element's text value.
         */
        public Text($value? : string) : string {
            this.text = Property.String(this.text, $value);
            if (ObjectValidator.IsSet($value)) {
                if (StringUtils.Contains($value, "{0}")) {
                    this.text = StringUtils.Format(this.text, "<span id=\"" + this.Id() + "_TextValue\"></span>");
                }
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setInnerHtml(this.Id() + "_Text", this.text);
                    this.updateNotificationText(true);
                }
            }
            return this.text;
        }

        /**
         * @param {number} [$value] Specify number of decimal places.
         * @return {number} Returns number of decimal places, which will be displayed.
         */
        public DecimalPlaces($value? : number) : number {
            if (!ObjectValidator.IsSet(this.decimalPlacesCount)) {
                this.decimalPlacesCount = 0;
            }
            this.decimalPlacesCount = Property.PositiveInteger(this.decimalPlacesCount, $value);
            return this.decimalPlacesCount;
        }

        public IsPreventingScroll() : boolean {
            return true;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.guiType, $value, NumberPickerType, NumberPickerType.GENERAL);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!NumberPickerType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of NumberPicker type instead of StyleClassName method.");
            return false;
        }

        protected innerCode() : IGuiElement {
            this.getPointerEvents().setOnMouseOver(($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
                $eventArgs.StopAllPropagation();
                $manager.setHovered($eventArgs.Owner(), true);
                NumberPicker.PointerOn($eventArgs.Owner());
                NumberPicker.NotificationShow($eventArgs.Owner());
            });
            this.getPointerEvents().setOnMouseDown(NumberPicker.moveInit);
            this.getPointerEvents().setOnMouseOut(($eventArgs : MouseEventArgs) : void => {
                NumberPicker.PointerOff($eventArgs.Owner());
                NumberPicker.NotificationHide();
            });

            this.getTrackerEvents().setOnMouseOver(($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
                $eventArgs.StopAllPropagation();
                $manager.setHovered($eventArgs.Owner(), true);
                NumberPicker.NotificationShow($eventArgs.Owner());
            });
            this.getTrackerEvents().setOnMouseDown(NumberPicker.pointerMoveEventHandler);
            this.getTrackerEvents().setOnMouseUp(() : void => {
                NumberPicker.moveStop();
            });
            this.getTrackerEvents().setOnMouseOut(() : void => {
                NumberPicker.NotificationHide();
            });

            this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_DOWN, NumberPicker.onKeyEventHandler);
            this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_SCROLL,
                ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                    const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>NumberPicker);
                    elements.foreach(($element : NumberPicker) : void => {
                        ElementManager.Hide($element.Id() + "_NotificationEnvelop");
                    });
                });

            this.getEvents().setOnStart(($eventArgs : EventArgs) : void => {
                NumberPicker.resize($eventArgs.Owner());
            });

            const notificationEvents : ElementEventsManager = new ElementEventsManager(this, this.Id() + "_NotificationFixedHolder");
            notificationEvents.setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                $eventArgs.StopAllPropagation();
                $manager.setHovered($eventArgs.Owner(), true);
                NumberPicker.PointerOn($eventArgs.Owner());
            });

            this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                const element : NumberPicker = <NumberPicker>$eventArgs.Owner();
                element.getPointerEvents().Subscribe();
                element.getTrackerEvents().Subscribe();
                notificationEvents.Subscribe();
                element.position(0);
                element.stopPosition(-1);
                element.isTriggered(false);
            });

            this.getEvents().setBeforeLoad(($eventArgs : EventArgs) : void => {
                NumberPicker.resize($eventArgs.Owner());
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            let statusCss : string = "";
            if (!this.Enabled()) {
                statusCss = GeneralCssNames.DISABLE;
            }
            const width90 : number = Math.ceil(this.Width() * 0.9);

            return this.addElement(this.Id() + "_Type").StyleClassName(this.guiType)
                .Add(this.addElement(this.Id() + "_Status")
                    .StyleClassName(statusCss)
                    .GuiTypeTag(this.getGuiTypeTag())
                    .Add(this.selectorElement())
                    .Add(this.addElement(this.Id() + "_Notification")
                        .StyleClassName("Notification")
                        .Add(this.addElement()
                            .StyleClassName("Pointer")
                            .Add(this.addElement(this.Id() + "_Pointer").StyleClassName(GeneralCssNames.OFF))
                        )
                        .Add(this.addElement(this.Id() + "_NotificationFixedHolder")
                            .StyleClassName(GeneralCssNames.GUI_FIXED_HOLDER)
                            .Add(this.addElement(this.Id() + "_NotificationEnvelop")
                                .StyleClassName(GeneralCssNames.BACKGROUND)
                                .Visible(false)
                                .Add(this.addElement(this.Id() + "_NotificationLeft").StyleClassName(GeneralCssNames.LEFT))
                                .Add(this.addElement(this.Id() + "_NotificationCenter").StyleClassName(GeneralCssNames.CENTER))
                                .Add(this.addElement(this.Id() + "_NotificationRight").StyleClassName(GeneralCssNames.RIGHT))
                                .Add(this.addElement(this.Id() + "_NotificationArrow").StyleClassName("Arrow"))
                                .Add(this.addElement(this.Id() + "_Text").StyleClassName(GeneralCssNames.TEXT).Add(this.text))
                            )
                        )
                    )
                    .Add(this.addElement(this.Id() + "_Tracker")
                        .StyleClassName(GeneralCssNames.BACKGROUND)
                        .Add(this.addElement(this.Id() + "_Left").StyleClassName(GeneralCssNames.LEFT))
                        .Add(this.addElement(this.Id() + "_Center")
                            .StyleClassName(GeneralCssNames.CENTER)
                            .Width(width90)
                            .Add(this.addElement(this.Id() + "_Bar")
                                .StyleClassName("Bar")
                                .Add(this.addElement(this.Id() + "_BarEnvelop")
                                    .StyleClassName("Envelop")
                                    .Add(this.addElement(this.Id() + "_BarLeft").StyleClassName(GeneralCssNames.LEFT))
                                    .Add(this.addElement(this.Id() + "_BarCenter").StyleClassName(GeneralCssNames.CENTER))
                                    .Add(this.addElement(this.Id() + "_BarRight").StyleClassName(GeneralCssNames.RIGHT))
                                )
                            )
                        )
                        .Add(this.addElement(this.Id() + "_Right").StyleClassName(GeneralCssNames.RIGHT))
                    )
                );
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();
            this.value = 0;
            this.rangeStart = 0;
            this.rangeEnd = 100;
            this.width = 250;
            this.valueStep = -1;
            this.Text("{0}");
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push(
                "value", "valueStep", "rangeStart", "rangeEnd",
                "width",
                "text",
                "pointerEvents", "trackerEvents",
                "isTriggeredState",
                "pointerPosition", "stopPointerPosition",
                "decimalPlacesCount"
            );

            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push(
                "pointerEvents", "trackerEvents",
                "pointerPosition", "stopPointerPosition", "isTriggeredState"
            );
            if (this.DecimalPlaces() === 0) {
                exclude.push("decimalPlacesCount");
            }
            return exclude;
        }

        private getPointerEvents() : ElementEventsManager {
            if (!ObjectValidator.IsSet(this.pointerEvents)) {
                this.pointerEvents = new ElementEventsManager(this, this.Id() + "_Pointer");
            }
            return this.pointerEvents;
        }

        private getTrackerEvents() : ElementEventsManager {
            if (!ObjectValidator.IsSet(this.trackerEvents)) {
                this.trackerEvents = new ElementEventsManager(this, this.Id() + "_Tracker");
            }
            return this.trackerEvents;
        }

        private isTriggered($value? : boolean) : boolean {
            return this.isTriggeredState = Property.Boolean(this.isTriggeredState, $value);
        }

        private position($value? : number) : number {
            return this.pointerPosition = Property.Integer(this.pointerPosition, $value);
        }

        private stopPosition($value? : number) : number {
            return this.stopPointerPosition = Property.Integer(this.stopPointerPosition, $value);
        }

        private updateNotificationText($force : boolean = false) : void {
            let text : string = Convert.ToFixed(this.value, this.DecimalPlaces()) + "";
            if (this.DecimalPlaces() > 0) {
                if (!StringUtils.Contains(text, ".")) {
                    text += ".";
                }
                const decimalPart : string = StringUtils.Substring(text, StringUtils.IndexOf(text, "."));
                let index : number;
                for (index = StringUtils.Length(decimalPart); index <= this.DecimalPlaces(); index++) {
                    text += "0";
                }
            }
            ElementManager.setInnerHtml(ElementManager.getElement(this.Id() + "_TextValue", $force), text);
        }
    }
}
