/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.Components {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ResizeBarEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeBarEventArgs;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import CropBoxType = Com.Wui.Framework.UserControls.BaseInterface.Enums.Components.CropBoxType;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Borders = Com.Wui.Framework.Gui.Structures.Borders;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import ICropBox = Com.Wui.Framework.Gui.Interfaces.Components.ICropBox;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import IGuiCommonsListArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsListArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import ICropBoxEvents = Com.Wui.Framework.Gui.Interfaces.Events.ICropBoxEvents;
    import CropBoxEventArgs = Com.Wui.Framework.Gui.Events.Args.CropBoxEventArgs;

    /**
     * CropBox class provides component with ability to create viewpoint with desired dimensions.
     */
    export class CropBox extends Com.Wui.Framework.Gui.Primitives.GuiCommons implements ICropBox {
        private guiType : CropBoxType;
        private size : Size;
        private startOffset : ElementOffset;
        private topOffset : ElementOffset;
        private bottomOffset : ElementOffset;
        private autoCenter : boolean;
        private topResizeBar : ResizeBar;
        private bottomResizeBar : ResizeBar;
        private envelopOwner : string;

        private static onResizeStartEventHandler($eventArgs : ResizeBarEventArgs, $manager : GuiObjectManager,
                                                 $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>ResizeBar)) {
                const element : ResizeBar = <ResizeBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, ResizeBar)) {
                    const parent : CropBox = <CropBox>element.Parent();
                    if ($reflection.IsMemberOf(parent, CropBox)) {
                        $manager.setActive(parent, true);
                        if (element === parent.topResizeBar) {
                            if (parent.autoCenter) {
                                parent.startOffset.Top(ElementManager.getElement(parent.Id() + "_BorderTop").offsetHeight);
                                parent.startOffset.Left(ElementManager.getElement(parent.Id() + "_BorderMiddleLeft").offsetWidth);
                                parent.topOffset.Top(parent.startOffset.Top());
                                parent.topOffset.Left(parent.startOffset.Left());
                                parent.bottomOffset.Top(
                                    ElementManager.getElement(parent.Id() + "_BorderTop").offsetHeight +
                                    ElementManager.getElement(parent.Id() + "_BorderMiddleCenter").offsetHeight);
                                parent.bottomOffset.Left(
                                    ElementManager.getElement(parent.Id() + "_BorderMiddleLeft").offsetWidth +
                                    ElementManager.getElement(parent.Id() + "_BorderMiddleCenter").offsetWidth);
                            } else {
                                parent.startOffset.Top(parent.topOffset.Top());
                                parent.startOffset.Left(parent.topOffset.Left());
                            }
                        } else if (element === parent.bottomResizeBar) {
                            if (parent.autoCenter) {
                                parent.startOffset.Top(
                                    ElementManager.getElement(parent.Id() + "_BorderTop").offsetHeight +
                                    ElementManager.getElement(parent.Id() + "_BorderMiddleCenter").offsetHeight);
                                parent.startOffset.Left(
                                    ElementManager.getElement(parent.Id() + "_BorderMiddleLeft").offsetWidth +
                                    ElementManager.getElement(parent.Id() + "_BorderMiddleCenter").offsetWidth);
                                parent.bottomOffset.Top(parent.startOffset.Top());
                                parent.bottomOffset.Left(parent.startOffset.Left());
                                parent.topOffset.Top(ElementManager.getElement(parent.Id() + "_BorderTop").offsetHeight);
                                parent.topOffset.Left(ElementManager.getElement(parent.Id() + "_BorderMiddleLeft").offsetWidth);
                            } else {
                                parent.startOffset.Top(parent.bottomOffset.Top());
                                parent.startOffset.Left(parent.bottomOffset.Left());
                            }
                        }

                        const eventArgs : CropBoxEventArgs = new CropBoxEventArgs();
                        eventArgs.Owner(parent);
                        eventArgs.Height(parent.size.Height());
                        eventArgs.Width(parent.size.Width());
                        eventArgs.OffsetLeft(parent.topOffset.Left());
                        eventArgs.OffsetTop(parent.topOffset.Top());
                        parent.getEventsManager().FireEvent(parent, EventType.ON_RESIZE_START, eventArgs);
                        parent.getEventsManager().FireEvent(CropBox.ClassName(), EventType.ON_RESIZE_START, eventArgs);
                    }
                }
            }
        }

        private static onResizeChangeEventHandler($eventArgs : ResizeBarEventArgs, $manager : GuiObjectManager,
                                                  $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>ResizeBar)) {
                const element : ResizeBar = <ResizeBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, ResizeBar)) {
                    const parent : CropBox = <CropBox>element.Parent();
                    if ($reflection.IsMemberOf(parent, CropBox)) {
                        let top : number = parent.startOffset.Top() + $eventArgs.DistanceY();
                        let left : number = parent.startOffset.Left() + $eventArgs.DistanceX();
                        if (element === parent.topResizeBar) {
                            if (top < 0) {
                                top = 0;
                            }
                            if (left < 0) {
                                left = 0;
                            }
                            if (top > parent.bottomOffset.Top()) {
                                top = parent.bottomOffset.Top();
                            }
                            if (left > parent.bottomOffset.Left()) {
                                left = parent.bottomOffset.Left();
                            }
                            parent.topOffset.Top(top);
                            parent.topOffset.Left(left);
                        } else if (element === parent.bottomResizeBar) {
                            if (top < parent.topOffset.Top()) {
                                top = parent.topOffset.Top();
                            }
                            if (left < parent.topOffset.Left()) {
                                left = parent.topOffset.Left();
                            }
                            const envelopSize : Size = new Size(parent.Id(), true);
                            if (top > envelopSize.Height()) {
                                top = envelopSize.Height();
                            }
                            if (left > envelopSize.Width()) {
                                left = envelopSize.Width();
                            }
                            parent.bottomOffset.Top(top);
                            parent.bottomOffset.Left(left);
                        }

                        const eventArgs : CropBoxEventArgs = new CropBoxEventArgs();
                        eventArgs.Owner(parent);
                        eventArgs.Height(parent.size.Height());
                        eventArgs.Width(parent.size.Width());
                        eventArgs.OffsetLeft(parent.topOffset.Left());
                        eventArgs.OffsetTop(parent.topOffset.Top());
                        parent.getEventsManager().FireEvent(parent, EventType.ON_RESIZE_CHANGE, eventArgs);
                        parent.getEventsManager().FireEvent(CropBox.ClassName(), EventType.ON_RESIZE_CHANGE, eventArgs);

                        CropBox.resize(parent, false);
                    }
                }
            }
        }

        private static onResizeCompleteEventHandler($eventArgs : ResizeBarEventArgs, $manager : GuiObjectManager,
                                                    $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>ResizeBar)) {
                let element : any = <ResizeBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, ResizeBar)) {
                    element = <CropBox>element.Parent();
                    if ($reflection.IsMemberOf(element, CropBox)) {
                        $manager.setActive(element, false);
                        element.size = new Size(element.Id() + "_BorderMiddleCenter", true);
                        CropBox.resize(element);
                        const eventArgs : CropBoxEventArgs = new CropBoxEventArgs();
                        eventArgs.Owner(element);
                        eventArgs.Height(element.size.Height());
                        eventArgs.Width(element.size.Width());
                        eventArgs.OffsetLeft(element.topOffset.Left());
                        eventArgs.OffsetTop(element.topOffset.Top());
                        element.getEventsManager().FireEvent(element, EventType.ON_RESIZE_COMPLETE, eventArgs);
                        element.getEventsManager().FireEvent(CropBox.ClassName(), EventType.ON_RESIZE_COMPLETE, eventArgs);
                        element.getEventsManager().FireEvent(element, EventType.ON_RESIZE, eventArgs);
                        element.getEventsManager().FireEvent(CropBox.ClassName(), EventType.ON_RESIZE, eventArgs);
                    }
                }
            }
        }

        private static resize($element : CropBox, $autoCenter? : boolean) : void {
            if (!ObjectValidator.IsSet($autoCenter)) {
                $autoCenter = $element.autoCenter;
            }
            const borders : Borders = new Borders();
            borders.Top(ElementManager.getElement($element.Id() + "_TopCenter").offsetHeight);
            borders.Bottom(ElementManager.getElement($element.Id() + "_BottomCenter").offsetHeight);
            borders.Left(ElementManager.getElement($element.Id() + "_MiddleLeft").offsetWidth);
            borders.Right(ElementManager.getElement($element.Id() + "_MiddleRight").offsetWidth);

            let parentSize : Size;
            if (ObjectValidator.IsEmptyOrNull($element.EnvelopOwner())) {
                parentSize = WindowManager.getSize();
            } else {
                parentSize = new Size($element.EnvelopOwner(), true);
            }
            ElementManager.setSize($element.Id(), parentSize.Width(), parentSize.Height());

            let leftWidth : number;
            let rightWidth : number;
            let topHeight : number;
            let bottomHeight : number;
            let middleWidth : number;
            let middleHeight : number;

            if ($autoCenter) {
                middleWidth = $element.size.Width();
                middleHeight = $element.size.Height();
            } else {
                middleWidth = $element.bottomOffset.Left() - $element.topOffset.Left();
                middleHeight = $element.bottomOffset.Top() - $element.topOffset.Top();
            }
            if (middleWidth + borders.Left() + borders.Right() > parentSize.Width()) {
                middleWidth = parentSize.Width() - borders.Left() - borders.Right();
            }
            if (middleHeight + borders.Top() + borders.Bottom() > parentSize.Height()) {
                middleHeight = parentSize.Height() - borders.Top() - borders.Bottom();
            }
            if ($autoCenter) {
                leftWidth = Math.ceil((parentSize.Width() - middleWidth) / 2) - borders.Left();
                if (leftWidth < 0) {
                    leftWidth = 0;
                }
                rightWidth = parentSize.Width() - leftWidth - borders.Left() - middleWidth - borders.Right();
                topHeight = Math.ceil((parentSize.Height() - middleHeight) / 2) - borders.Top();
                if (topHeight < 0) {
                    topHeight = 0;
                }
                bottomHeight = parentSize.Height() - topHeight - borders.Top() - middleHeight - borders.Bottom();
            } else {
                leftWidth = $element.topOffset.Left() - borders.Left();
                rightWidth = parentSize.Width() - $element.bottomOffset.Left() - borders.Right();
                topHeight = $element.topOffset.Top() - borders.Top();
                bottomHeight = parentSize.Height() - $element.bottomOffset.Top() - borders.Bottom();
            }
            if (rightWidth > parentSize.Width()) {
                rightWidth = parentSize.Width();
            }
            if (leftWidth < 0) {
                rightWidth -= borders.Left();
            }
            if (rightWidth < 0) {
                leftWidth -= borders.Right();
            }
            if (leftWidth < 0) {
                leftWidth = 0;
            }
            if (rightWidth < 0) {
                rightWidth = 0;
            }
            if (topHeight < 0) {
                topHeight = 0;
            }
            if (bottomHeight > parentSize.Height()) {
                bottomHeight = parentSize.Height();
            }
            if (bottomHeight < 0) {
                bottomHeight = 0;
            }
            if (middleWidth + leftWidth + borders.Left() + rightWidth + borders.Right() > parentSize.Width()) {
                middleWidth = parentSize.Width() - leftWidth - borders.Left() - rightWidth - borders.Right();
            }
            if (middleHeight + topHeight + borders.Top() + bottomHeight + borders.Bottom() > parentSize.Height()) {
                middleHeight = parentSize.Height() - topHeight - borders.Top() - bottomHeight - borders.Bottom();
            }
            if (middleWidth >= 0 && middleHeight >= 0) {
                const eventArgs : CropBoxEventArgs = new CropBoxEventArgs();
                eventArgs.Owner($element);
                eventArgs.Height(middleHeight);
                eventArgs.Width(middleWidth);
                eventArgs.OffsetLeft($element.topOffset.Left());
                eventArgs.OffsetTop($element.topOffset.Top());

                ElementManager.setSize($element.Id() + "_BorderTop", parentSize.Width(), topHeight);
                ElementManager.setSize($element.Id() + "_BorderMiddle", parentSize.Width(), middleHeight);
                ElementManager.setSize($element.Id() + "_BorderBottom", parentSize.Width(), bottomHeight);

                ElementManager.setWidth($element.Id() + "_TopCenter", middleWidth);
                ElementManager.setHeight($element.Id() + "_MiddleLeft", middleHeight);
                ElementManager.setSize($element.Id() + "_MiddleCenter", middleWidth, middleHeight);
                ElementManager.setHeight($element.Id() + "_MiddleRight", middleHeight);
                ElementManager.setWidth($element.Id() + "_BottomCenter", middleWidth);

                middleWidth += borders.Left() + borders.Right();
                middleHeight += borders.Top() + borders.Bottom();
                ElementManager.setSize($element.Id() + "_BorderMiddleLeft", leftWidth, middleHeight);
                ElementManager.setSize($element.Id() + "_BorderMiddleCenter", middleWidth, middleHeight);
                ElementManager.setSize($element.Id() + "_BorderMiddleRight", rightWidth, middleHeight);

                $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE, eventArgs);
                $element.getEventsManager().FireEvent(CropBox.ClassName(), EventType.ON_RESIZE, eventArgs);
            }
        }

        /**
         * @param {CropBoxType} [$cropBoxType] Specify element's look and feel.
         * @param {string|IGuiCommons} [$owner] Specify owner of CropBox envelop suitable for calculation of envelop dimensions.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($cropBoxType? : CropBoxType, $owner? : string | IGuiCommons, $id? : string) {
            super($id);
            this.guiType = this.guiTypeValueSetter($cropBoxType);
            this.size = new Size();
            this.startOffset = new ElementOffset();
            this.topOffset = new ElementOffset();
            this.bottomOffset = new ElementOffset();
            this.autoCenter = true;
            this.topResizeBar = new ResizeBar(ResizeableType.HORIZONTAL_AND_VERTICAL);
            this.bottomResizeBar = new ResizeBar(ResizeableType.HORIZONTAL_AND_VERTICAL);
            this.EnvelopOwner($owner);
        }

        /**
         * @return {ICropBoxEvents} Returns events manager subscribed to the item.
         */
        public getEvents() : ICropBoxEvents {
            return <ICropBoxEvents>super.getEvents();
        }

        /**
         * @param {CropBoxType} [$cropBoxType] Specify type of element's look and feel.
         * @return {CropBoxType} Returns type of element's look and feel.
         */
        public GuiType($cropBoxType? : CropBoxType) : CropBoxType {
            if (ObjectValidator.IsSet($cropBoxType)) {
                this.guiType = this.guiTypeValueSetter($cropBoxType);
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                }
            }
            return this.guiType;
        }

        /**
         * @param {string|IGuiCommons} [$owner] Specify owner of CropBox envelop suitable for calculation of envelop dimensions.
         * @return {string} Returns envelop owner id in case of that owner has been specified, othewise null.
         */
        public EnvelopOwner($owner? : string | IGuiCommons) : string {
            if (!ObjectValidator.IsEmptyOrNull($owner)) {
                if (ObjectValidator.IsString($owner)) {
                    this.envelopOwner = <string>$owner;
                } else if (ObjectValidator.IsObject($owner) && (<IGuiCommons>$owner).IsMemberOf(GuiCommons)) {
                    this.envelopOwner = (<IGuiCommons>$owner).Id();
                } else {
                    this.envelopOwner = null;
                }
            }
            return this.envelopOwner;
        }

        /**
         * @param {number} $topX Specify top X position value.
         * @param {number} $topY Specify top Y position value.
         * @param {number} $bottomX Specify bottom X position value.
         * @param {number} $bottomY Specify bottom Y position value.
         * @return {void}
         */
        public setDimensions($topX : number, $topY : number, $bottomX : number, $bottomY : number) : void {
            this.topOffset.Top($topY);
            this.topOffset.Left($topX);
            this.bottomOffset.Top($bottomY);
            this.bottomOffset.Left($bottomX);
            this.autoCenter = false;

            if (this.IsLoaded() && (
                ObjectValidator.IsSet($topX) || ObjectValidator.IsSet($topY) ||
                ObjectValidator.IsSet($bottomX) || ObjectValidator.IsSet($bottomY))) {
                CropBox.resize(this);
            }
        }

        /**
         * @param {number} $width Specify crop width.
         * @param {number} $height Specify crop height.
         * @return {void}
         */
        public setSize($width : number, $height : number) : void {
            this.size.Width($width);
            this.size.Height($height);
            this.autoCenter = true;

            if (this.IsLoaded() && (ObjectValidator.IsSet($width) || ObjectValidator.IsSet($height))) {
                CropBox.resize(this);
            }
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            const enabled : boolean = super.Enabled($value);
            this.topResizeBar.Visible(enabled);
            this.bottomResizeBar.Visible(enabled);
            return enabled;
        }

        /**
         * @return {IGuiCommonsArg[]} Returns array of element's attributes.
         */
        public getArgs() : IGuiCommonsArg[] {
            const args : IGuiCommonsArg[] = super.getArgs();
            args.push({
                name : "Width",
                type : GuiCommonsArgType.NUMBER,
                value: this.size.Width()
            });
            args.push({
                name : "Height",
                type : GuiCommonsArgType.NUMBER,
                value: this.size.Height()
            });
            args.push(<IGuiCommonsListArg>{
                items: CropBoxType.getProperties(),
                name : "GuiType",
                type : GuiCommonsArgType.LIST,
                value: CropBoxType.getKey(<string>this.GuiType())
            });
            return args;
        }

        /**
         * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
         * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
         * @return {void}
         */
        public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
            switch ($value.name) {
            case "Width":
                this.setSize(<number>$value.value, this.size.Height());
                break;
            case "Height":
                this.setSize(this.size.Width(), <number>$value.value);
                break;
            case "GuiType":
                this.GuiType(CropBoxType[<string>$value.value]);
                break;
            default:
                super.setArg($value, $force);
                break;
            }
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.guiType, $value, CropBoxType, CropBoxType.GENERAL);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!CropBoxType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of cropbox type instead of StyleClassName method.");
            return false;
        }

        protected innerCode() : IGuiElement {
            this.topResizeBar.getEvents().setOnResizeStart(CropBox.onResizeStartEventHandler);
            this.topResizeBar.getEvents().setOnResizeChange(CropBox.onResizeChangeEventHandler);
            this.topResizeBar.getEvents().setOnResizeComplete(CropBox.onResizeCompleteEventHandler);
            this.bottomResizeBar.getEvents().setOnResizeStart(CropBox.onResizeStartEventHandler);
            this.bottomResizeBar.getEvents().setOnResizeChange(CropBox.onResizeChangeEventHandler);
            this.bottomResizeBar.getEvents().setOnResizeComplete(CropBox.onResizeCompleteEventHandler);

            if (ObjectValidator.IsEmptyOrNull(this.Parent())) {
                WindowManager.getEvents().setOnResize(() : void => {
                    CropBox.resize(this);
                });
            } else {
                this.Parent().getEvents().setEvent(EventType.ON_RESIZE, () : void => {
                    CropBox.resize(this);
                });
            }

            this.getEvents().setOnLoad(($eventArgs : EventArgs) : void => {
                CropBox.resize(<CropBox>$eventArgs.Owner());
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType()).GuiTypeTag(this.getGuiTypeTag())
                .Add(this.addElement(this.Id() + "_BorderTop").StyleClassName(GeneralCssNames.TOP))
                .Add(this.addElement(this.Id() + "_BorderMiddle")
                    .StyleClassName(GeneralCssNames.MIDDLE)
                    .Add(this.addElement(this.Id() + "_BorderMiddleLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_BorderMiddleCenter")
                        .StyleClassName(GeneralCssNames.CENTER)
                        .Add(this.addElement(this.Id() + "_Top")
                            .StyleClassName(GeneralCssNames.TOP)
                            .Add(this.addElement(this.Id() + "_TopLeft")
                                .StyleClassName(GeneralCssNames.LEFT)
                                .Add(this.topResizeBar)
                            )
                            .Add(this.addElement(this.Id() + "_TopCenter").StyleClassName(GeneralCssNames.CENTER))
                            .Add(this.addElement(this.Id() + "_TopRight").StyleClassName(GeneralCssNames.RIGHT))
                        )
                        .Add(this.addElement(this.Id() + "_Middle")
                            .StyleClassName(GeneralCssNames.MIDDLE)
                            .Add(this.addElement(this.Id() + "_MiddleLeft").StyleClassName(GeneralCssNames.LEFT))
                            .Add(this.addElement(this.Id() + "_MiddleCenter").StyleClassName(GeneralCssNames.CENTER))
                            .Add(this.addElement(this.Id() + "_MiddleRight").StyleClassName(GeneralCssNames.RIGHT))
                        )
                        .Add(this.addElement(this.Id() + "_Bottom")
                            .StyleClassName(GeneralCssNames.BOTTOM)
                            .Add(this.addElement(this.Id() + "_BottomLeft").StyleClassName(GeneralCssNames.LEFT))
                            .Add(this.addElement(this.Id() + "_BottomCenter").StyleClassName(GeneralCssNames.CENTER))
                            .Add(this.addElement(this.Id() + "_BottomRight")
                                .StyleClassName(GeneralCssNames.RIGHT)
                                .Add(this.bottomResizeBar)
                            )
                        )
                    )
                    .Add(this.addElement(this.Id() + "_BorderMiddleRight").StyleClassName(GeneralCssNames.RIGHT))
                )
                .Add(this.addElement(this.Id() + "_BorderBottom").StyleClassName(GeneralCssNames.BOTTOM));
        }
    }
}
