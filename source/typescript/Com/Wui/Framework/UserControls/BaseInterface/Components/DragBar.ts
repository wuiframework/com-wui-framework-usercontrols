/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.Components {
    "use strict";
    import IDragBar = Com.Wui.Framework.Gui.Interfaces.Components.IDragBar;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IDragBarEvents = Com.Wui.Framework.Gui.Interfaces.Events.IDragBarEvents;
    import TextSelectionManager = Com.Wui.Framework.Gui.Utils.TextSelectionManager;

    /**
     * DragBar class provides element's component enabling drag of parent element.
     */
    export class DragBar extends Com.Wui.Framework.Gui.Primitives.GuiCommons implements IDragBar {

        private static moveInit($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                                $reflection : Reflection) : void {
            const element : DragBar = <DragBar>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, DragBar)) {
                $manager.setActive(element, true);

                let pointerEvents : string;
                element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START,
                    ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                        element.getEvents().FireAsynchronousMethod(() : void => {
                            if ($manager.IsActive(<ClassName>DragBar)) {
                                pointerEvents = document.body.style.pointerEvents;
                                TextSelectionManager.Disable(true);
                                $args.StopAllPropagation();
                                const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>DragBar);
                                elements.foreach(($element : DragBar) : void => {
                                    $args.Owner($element);
                                    $element.getEventsManager().FireEvent($element, EventType.ON_DRAG_START, $args, false);
                                    $element.getEventsManager().FireEvent(DragBar.ClassName(), EventType.ON_DRAG_START, $args, false);
                                    document.body.style.cursor = "move";
                                });
                            }
                        });
                    });

                element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                    ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                        element.getEvents().FireAsynchronousMethod(() : void => {
                            if ($manager.IsActive(<ClassName>DragBar)) {
                                document.body.style.pointerEvents = "none";
                                $args.StopAllPropagation();
                                const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>DragBar);
                                elements.foreach(($element : DragBar) : void => {
                                    $args.Owner($element);
                                    $element.getEventsManager().FireEvent($element, EventType.ON_DRAG_CHANGE, $args);
                                    $element.getEventsManager().FireEvent(DragBar.ClassName(), EventType.ON_DRAG_CHANGE, $args);
                                });
                            }
                        });
                    });

                element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
                    ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                        element.getEvents().FireAsynchronousMethod(() : void => {
                            if ($manager.IsActive(<ClassName>DragBar)) {
                                document.body.style.pointerEvents = pointerEvents;
                                TextSelectionManager.Disable(false);
                                TextSelectionManager.Clear();
                                const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>DragBar);
                                elements.foreach(($element : DragBar) : void => {
                                    $args.Owner($element);
                                    $element.getEventsManager().FireEvent($element, EventType.ON_DRAG_COMPLETE, $args, false);
                                    $element.getEventsManager().FireEvent(DragBar.ClassName(), EventType.ON_DRAG_COMPLETE, $args, false);
                                    $manager.setActive($element, false);
                                });
                            }
                        });
                    });
            }
        }

        /**
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($id? : string) {
            super($id);
        }

        /**
         * @return {IDragBarEvents} Returns events manager subscribed to the item.
         */
        public getEvents() : IDragBarEvents {
            return <IDragBarEvents>super.getEvents();
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnMouseDown(DragBar.moveInit);

            return super.innerCode();
        }
    }
}
