/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.Components {
    "use strict";
    import IResizeBar = Com.Wui.Framework.Gui.Interfaces.Components.IResizeBar;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import ResizeBarEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeBarEventArgs;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import EventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import ResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeEventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import IGuiCommonsListArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsListArg;
    import IResizeBarEvents = Com.Wui.Framework.Gui.Interfaces.Events.IResizeBarEvents;
    import CursorType = Com.Wui.Framework.Gui.Enums.CursorType;

    /**
     * DragBar class provides element's component enabling resize of parent element.
     */
    export class ResizeBar extends Com.Wui.Framework.Gui.Primitives.GuiCommons implements IResizeBar {
        private resizeType : ResizeableType;
        private cursorType : CursorType;

        /**
         * @param {ResizeBar} $element Specify element, which should be handled.
         * @return {void}
         */
        public static TurnOn($element : ResizeBar) : void {
            if (!$element.getGuiManager().IsActive(<ClassName>ResizeBar)) {
                ElementManager.TurnOn($element.Id() + "_Status");
            }
        }

        /**
         * @param {ResizeBar} $element Specify element, which should be handled.
         * @return {void}
         */
        public static TurnOff($element : ResizeBar) : void {
            if (!$element.getGuiManager().IsActive(<ClassName>ResizeBar)) {
                ElementManager.setClassName($element.Id() + "_Status", "");
                document.body.style.cursor = "default";
            }
        }

        /**
         * @param {ResizeBar} $element Specify element, which should be handled.
         * @return {void}
         */
        public static TurnActive($element : ResizeBar) : void {
            ElementManager.TurnActive($element.Id() + "_Status");
        }

        /**
         * @param {ResizeBar} $element Specify element, which should be handled.
         * @param {number} $value Specify new width value for handled element.
         * @return {void}
         */
        public static setWidth($element : ResizeBar, $value : number) : void {
            ElementManager.setWidth($element.guiContentId(), $value);
        }

        /**
         * @param {ResizeBar} $element Specify element, which should be handled.
         * @param {number} $value Specify new height value for handled element.
         * @return {void}
         */
        public static setHeight($element : ResizeBar, $value : number) : void {
            ElementManager.setHeight($element.guiContentId(), $value);
        }

        /**
         * @param {ResizeBar} $element Specify element, which should be handled.
         * @param {number} $distanceX Specify distance from current width, which should be applied.
         * @param {number} $distanceY Specify distance from current height, which should be applied.
         * @return {void}
         */
        public static ResizeTo($element : ResizeBar, $distanceX : number, $distanceY : number) : void {
            const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
            eventArgs.Owner($element);
            eventArgs.DistanceX($distanceX);
            eventArgs.DistanceY($distanceY);
            $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE, eventArgs);
            $element.getEventsManager().FireEvent(ResizeBar.ClassName(), EventType.ON_RESIZE, eventArgs);
        }

        /**
         * Handle resize event
         * @param {ResizeEventArgs} $eventArgs Specify event args of resize event.
         * @param {GuiObjectManager} [$manager] Provide gui object manager instance.
         * @param {Reflection} [$reflection] Provide reflection instance.
         * @return {void}
         */
        public static ResizeEventHandler($eventArgs : ResizeEventArgs, $manager : GuiObjectManager,
                                         $reflection : Reflection) : void {
            if ($reflection.IsMemberOf($eventArgs, ResizeEventArgs)) {
                const element : ResizeBar = <ResizeBar>$eventArgs.Owner();
                element.getChildElements().foreach(($child : ResizeBar) : void => {
                    if ($reflection.IsMemberOf($child, ResizeBar) &&
                        ElementManager.IsVisible($child.Id())) {
                        if ($child.ResizeableType() === ResizeableType.HORIZONTAL) {
                            ResizeBar.setHeight($child, $eventArgs.AvailableHeight());
                        }
                        if ($child.ResizeableType() === ResizeableType.VERTICAL) {
                            ResizeBar.setWidth($child, $eventArgs.AvailableWidth());
                        }
                    }
                });
            }
        }

        private static moveInit($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                                $reflection : Reflection) : void {
            const element : ResizeBar = <ResizeBar>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, ResizeBar)) {
                $manager.setActive(element, true);
                ResizeBar.TurnActive(element);

                element.getEventsManager().setEvent(EventOwner.MOUSE_MOVE, EventType.ON_START, ResizeBar.onMoveStartEventHandler);
                element.getEventsManager().setEvent(EventOwner.MOUSE_MOVE, EventType.ON_CHANGE, ResizeBar.onMoveChangeEventHandler);
                element.getEventsManager().setEvent(EventOwner.MOUSE_MOVE, EventType.ON_COMPLETE, ResizeBar.onMoveCompleteEventHandler);
            }
        }

        private static setMouseCursor($resizeableType : ResizeableType, $cursorType? : CursorType) : void {
            if (ObjectValidator.IsEmptyOrNull($cursorType)) {
                switch ($resizeableType) {
                case ResizeableType.HORIZONTAL_AND_VERTICAL:
                    document.body.style.cursor = "nw-resize";
                    break;
                case ResizeableType.HORIZONTAL:
                    document.body.style.cursor = "w-resize";
                    break;
                case ResizeableType.VERTICAL:
                    document.body.style.cursor = "n-resize";
                    break;
                default :
                    document.body.style.cursor = "default";
                    break;
                }
            } else {
                document.body.style.cursor = $cursorType.toString();
            }
        }

        private static onMoveStartEventHandler($args : MoveEventArgs, $manager : GuiObjectManager) : void {
            Loader.getInstance().getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
                if ($manager.IsActive(<ClassName>ResizeBar)) {
                    $args.StopAllPropagation();
                    const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>ResizeBar);
                    elements.foreach(($element : ResizeBar) : void => {
                        if ($element.Enabled()) {
                            const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                            eventArgs.Owner($element);
                            eventArgs.ResizeableType($element.ResizeableType());
                            $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE_START, eventArgs, false);
                            $element.getEventsManager().FireEvent(ResizeBar.ClassName(), EventType.ON_RESIZE_START, eventArgs, false);

                            ResizeBar.setMouseCursor($element.ResizeableType(), $element.CursorType());
                        }
                    });
                }
            });
        }

        private static onMoveChangeEventHandler($args : MoveEventArgs, $manager : GuiObjectManager) : void {
            Loader.getInstance().getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
                if ($manager.IsActive(<ClassName>ResizeBar)) {
                    $args.StopAllPropagation();
                    const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>ResizeBar);
                    elements.foreach(($element : ResizeBar) : void => {
                        if ($element.Enabled()) {
                            ResizeBar.setMouseCursor($element.ResizeableType(), $element.CursorType());

                            let distanceX : number = 0;
                            let distanceY : number = 0;

                            if ($element.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                                $element.ResizeableType() === ResizeableType.HORIZONTAL) {
                                distanceX = $args.getDistanceX();
                            }
                            if ($element.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                                $element.ResizeableType() === ResizeableType.VERTICAL) {
                                distanceY = $args.getDistanceY();
                            }

                            const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                            eventArgs.Owner($element);
                            eventArgs.ResizeableType($element.ResizeableType());
                            eventArgs.DistanceX(distanceX);
                            eventArgs.DistanceY(distanceY);
                            $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE_CHANGE, eventArgs, false);
                            $element.getEventsManager().FireEvent(ResizeBar.ClassName(), EventType.ON_RESIZE_CHANGE, eventArgs, false);
                        }
                    });
                }
            });
        }

        private static onMoveCompleteEventHandler($args : MoveEventArgs, $manager : GuiObjectManager) : void {
            Loader.getInstance().getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
                if ($manager.IsActive(<ClassName>ResizeBar)) {
                    $args.StopAllPropagation();
                    const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>ResizeBar);
                    elements.foreach(($element : ResizeBar) : void => {
                        if ($element.Enabled()) {
                            const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                            eventArgs.Owner($element);
                            eventArgs.ResizeableType($element.ResizeableType());
                            $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE_COMPLETE, eventArgs, false);
                            $element.getEventsManager().FireEvent(ResizeBar.ClassName(), EventType.ON_RESIZE_COMPLETE, eventArgs, false);
                            $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE, eventArgs, false);
                            $element.getEventsManager().FireEvent(ResizeBar.ClassName(), EventType.ON_RESIZE, eventArgs, false);
                            $manager.setActive($element, false);
                            ResizeBar.TurnOff($element);
                        }
                    });
                    Loader.getInstance().getHttpResolver().getEvents().RemoveHandler(EventOwner.MOUSE_MOVE, EventType.ON_START,
                        ResizeBar.onMoveStartEventHandler);
                    Loader.getInstance().getHttpResolver().getEvents().RemoveHandler(EventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                        ResizeBar.onMoveChangeEventHandler);
                }
            });
        }

        /**
         * @param {ResizeableType} [$resizeableType] Specify element's look and feel.
         * @param {CursorType} [$cursorType] Specify element's cursor handling type during resize.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($resizeableType? : ResizeableType, $cursorType? : CursorType, $id? : string) {
            super($id);
            this.ResizeableType($resizeableType);
            this.CursorType($cursorType);
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            if (ObjectValidator.IsSet($value)) {
                if ($value) {
                    ElementManager.ClearCssProperty(this.Id(), "pointer-events");
                } else {
                    ElementManager.setCssProperty(this.Id(), "pointer-events", "none");
                }
            }
            return super.Enabled($value);
        }

        /**
         * @return {IResizeBarEvents} Returns events manager subscribed to the item.
         */
        public getEvents() : IResizeBarEvents {
            return <IResizeBarEvents>super.getEvents();
        }

        /**
         * @param {ResizeableType} [$type] Specify element's look and feel.
         * @return {ResizeableType}
         */
        public ResizeableType($type? : ResizeableType) : ResizeableType {
            if (ObjectValidator.IsSet($type) && ResizeableType.Contains($type)) {
                this.resizeType = $type;
                if (ElementManager.Exists(this.Id())) {
                    if ($type === ResizeableType.NONE) {
                        ResizeBar.Hide(this);
                    } else {
                        ResizeBar.Show(this);
                    }
                }
            }
            return this.resizeType;
        }

        /**
         * @param {CursorType} [$type] Specify element's cursor look and feel.
         * @return {CursorType}
         */
        public CursorType($type? : CursorType) : CursorType {
            if (ObjectValidator.IsSet($type) && CursorType.Contains($type)) {
                this.cursorType = $type;
            }
            return this.cursorType;
        }

        /**
         * @return {IGuiCommonsArg[]} Returns array of element's attributes.
         */
        public getArgs() : IGuiCommonsArg[] {
            const args : IGuiCommonsArg[] = super.getArgs();
            args.push(<IGuiCommonsListArg>{
                items: ResizeableType.getProperties(),
                name : "ResizeableType",
                type : GuiCommonsArgType.LIST,
                value: ResizeableType.getKey(<string>this.ResizeableType())
            });
            return args;
        }

        /**
         * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
         * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
         * @return {void}
         */
        public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
            switch ($value.name) {
            case "Width":
                Reflection.getInstance().getClass(this.getClassName()).setWidth(this, <number>$value.value);
                break;
            case "Height":
                Reflection.getInstance().getClass(this.getClassName()).setHeight(this, <number>$value.value);
                break;
            case "ResizeableType":
                this.ResizeableType(ResizeableType[<string>$value.value]);
                break;
            default:
                super.setArg($value, $force);
                break;
            }
        }

        protected innerCode() : IGuiElement {
            this.getEvents().Subscriber(this.Id() + "_Position");

            if (this.resizeType === ResizeableType.NONE) {
                this.Visible(false);
            }

            this.getEvents().setOnMouseOver(($eventArgs : MouseEventArgs) : void => {
                ResizeBar.TurnOn($eventArgs.Owner());
            });
            this.getEvents().setOnMouseOut(($eventArgs : MouseEventArgs) : void => {
                ResizeBar.TurnOff($eventArgs.Owner());
            });
            this.getEvents().setOnMouseDown(ResizeBar.moveInit);
            this.getEvents().setOnMouseUp(($eventArgs : MouseEventArgs) : void => {
                ResizeBar.TurnOff($eventArgs.Owner());
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Status").Add(this.addElement(this.Id() + "_Position").StyleClassName(this.resizeType));
        }

        protected guiContentId() : string {
            return this.Id() + "_Position";
        }
    }
}
