/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.Components {
    "use strict";
    import IScrollBar = Com.Wui.Framework.Gui.Interfaces.Components.IScrollBar;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import OrientationType = Com.Wui.Framework.Gui.Enums.OrientationType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import ResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeEventArgs;
    import ScrollEventArgs = Com.Wui.Framework.Gui.Events.Args.ScrollEventArgs;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ScrollBarEventType = Com.Wui.Framework.Gui.Enums.Events.ScrollBarEventType;
    import EventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import IGuiCommonsListArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsListArg;
    import IScrollBarEvents = Com.Wui.Framework.Gui.Interfaces.Events.IScrollBarEvents;
    import TextSelectionManager = Com.Wui.Framework.Gui.Utils.TextSelectionManager;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import ValueProgressEventArgs = Com.Wui.Framework.Gui.Events.Args.ValueProgressEventArgs;
    import ValueProgressManager = Com.Wui.Framework.Gui.Utils.ValueProgressManager;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;

    /**
     * ScrollBar class provides element's component for scrolling of parent's content.
     */
    export class ScrollBar extends Com.Wui.Framework.Gui.Primitives.GuiCommons implements IScrollBar {
        private static threadCallback : any;
        private static threadTick : number;
        private size : number;
        private guiType : OrientationType;
        private upArrowEvents : ElementEventsManager;
        private downArrowEvents : ElementEventsManager;
        private trackerEvents : ElementEventsManager;
        private trackerButtonEvents : ElementEventsManager;
        private hoverExpandEvents : ElementEventsManager;
        private isInnerTriggeredState : boolean;
        private isOutsideTriggeredState : boolean;
        private elementPosition : ElementOffset;
        private buttonPositionValue : ElementOffset;

        private defaultSize : number;
        private expandSize : number;
        private currentSize : number;
        private upArrowSize : Size;
        private downArrowSize : Size;
        private upArrowIconSize : Size;
        private buttonIconSize : Size;
        private downArrowIconSize : Size;
        private upArrowWidthOffset : number;
        private buttonWidthOffset : number;
        private downArrowWidthOffset : number;
        private upArrowHeightOffset : number;
        private buttonHeightOffset : number;
        private downArrowHeightOffset : number;
        private upArrowIconOffset : ElementOffset;
        private buttonIconOffset : ElementOffset;
        private downArrowIconOffset : ElementOffset;
        private isExpandable : boolean;

        private isMouseOver : boolean;
        private hideEventHandle : number;

        /**
         * Change class name of arrow element to general type ON
         * @param {ScrollBar} $element Specify of arrow element owner.
         * @param {string} $arrowId Specify id of arrow element.
         * @return {void}
         */
        public static ArrowOn($element : ScrollBar, $arrowId : string) : void {
            if (!$element.getGuiManager().IsActive(<ClassName>ScrollBar)) {
                ElementManager.TurnOn($arrowId);
            }

            const eventArgs : ScrollEventArgs = new ScrollEventArgs();
            eventArgs.Owner($element);
            eventArgs.OrientationType($element.OrientationType());
            eventArgs.Position($element.OrientationType() === OrientationType.VERTICAL ?
                $element.position().Top() : $element.position().Left());
            $element.getEventsManager().FireEvent($element, ScrollBarEventType.ON_ARROW, eventArgs);
            $element.getEventsManager().FireEvent(ScrollBar.ClassName(), ScrollBarEventType.ON_ARROW, eventArgs);
        }

        /**
         * Change class name of arrow element to general type OFF
         * @param {ScrollBar} $element Specify of arrow element owner.
         * @param {string} $arrowId Specify id of arrow element.
         * @return {void}
         */
        public static ArrowOff($element : ScrollBar, $arrowId : string) : void {
            if (!$element.isInnerTriggered()) {
                ElementManager.TurnOff($arrowId);
            }
        }

        /**
         * Change class name of button element to general type ON
         * @param {ScrollBar} $element Specify scrollbar element.
         * @return {void}
         */
        public static ButtonOn($element : ScrollBar) : void {
            if (!$element.isInnerTriggered() && !$element.getGuiManager().IsActive(<ClassName>ScrollBar)) {
                ElementManager.TurnOn($element.Id() + "_Button");
            }
        }

        /**
         * Change class name of button element to general type OFF
         * @param {ScrollBar} $element Specify scrollbar element.
         * @return {void}
         */
        public static ButtonOff($element : ScrollBar) : void {
            if (!$element.getGuiManager().IsActive($element)) {
                ElementManager.TurnOff($element.Id() + "_Button");
            }
        }

        /**
         * Move scrollbar button in minus direction based on scrollbar orientation
         * @param {ScrollBar} $element Specify scrollbar element.
         * @param {number} $position Specify new position as percentage of scrollbar size.
         * @return {void}
         */
        public static MoveMinus($element : ScrollBar, $position : number) : void {
            if (Reflection.getInstance().IsMemberOf($element, ScrollBar)) {
                const id : string = $element.Id();
                $element.getGuiManager().setActive($element, true);
                $element.isInnerTriggered(true);

                const tracker : HTMLElement = ElementManager.getElement(id + "_Tracker");
                let step : number = 2;
                if ($position < 100) {
                    if ($element.OrientationType() === OrientationType.VERTICAL) {
                        step = Math.ceil(tracker.offsetHeight / 100 * $position);
                        ScrollBar.move($element, ElementManager.getElement(id + "_ButtonEnvelop").offsetTop - step);
                    } else {
                        step = Math.ceil(tracker.offsetWidth / 100 * $position);
                        ScrollBar.move($element, ElementManager.getElement(id + "_ButtonEnvelop").offsetLeft - step);
                    }

                    ScrollBar.threadCallback = $element.getEvents().FireAsynchronousMethod(() : void => {
                        ScrollBar.MoveMinus($element, ScrollBar.threadTick < 5 ? 1 : 5);
                    }, ScrollBar.threadTick < 3 ? 100 : 30);
                    ScrollBar.threadTick++;
                }
            }
        }

        /**
         * Move scrollbar button in plus direction based on scrollbar orientation
         * @param {ScrollBar} $element Specify scrollbar element.
         * @param {number} $position Specify new position as percentage of scrollbar size.
         * @return {void}
         */
        public static MovePlus($element : ScrollBar, $position : number) : void {
            if (Reflection.getInstance().IsMemberOf($element, ScrollBar)) {
                const id : string = $element.Id();
                $element.getGuiManager().setActive($element, true);
                $element.isInnerTriggered(true);

                const tracker : HTMLElement = ElementManager.getElement(id + "_Tracker");
                let step : number = 2;
                if ($position < 100) {
                    if ($element.OrientationType() === OrientationType.VERTICAL) {
                        step = Math.ceil(tracker.offsetHeight / 100 * $position);
                        ScrollBar.move($element, ElementManager.getElement(id + "_ButtonEnvelop").offsetTop + step);
                    } else {
                        step = Math.ceil(tracker.offsetWidth / 100 * $position);
                        ScrollBar.move($element, ElementManager.getElement(id + "_ButtonEnvelop").offsetLeft + step);
                    }

                    ScrollBar.threadCallback = $element.getEvents().FireAsynchronousMethod(() : void => {
                        ScrollBar.MovePlus($element, ScrollBar.threadTick < 5 ? 1 : 5);
                    }, ScrollBar.threadTick < 3 ? 100 : 30);
                    ScrollBar.threadTick++;
                }
            }
        }

        /**
         * Move scrollbar button to desired position
         * @param {ScrollBar} $element Specify scrollbar element.
         * @param {number} $position Specify new position as percentage of scrollbar size.
         * @return {void}
         */
        public static MoveTo($element : ScrollBar, $position : number) : void {
            const id : string = $element.Id();
            if (ElementManager.IsVisible(id)) {
                if (Reflection.getInstance().IsMemberOf($element, ScrollBar)) {
                    $element.getGuiManager().setActive($element, true);
                    $element.isOutsideTriggered(true);

                    const buttonSize : Size = new Size(id + "_ButtonEnvelop", true);
                    const maxSize : Size = new Size(id + "_Tracker", true);
                    if ($element.OrientationType() === OrientationType.VERTICAL) {
                        $position = Math.ceil((maxSize.Height() - buttonSize.Height()) / 100 * $position);
                    } else {
                        $position = Math.ceil((maxSize.Width() - buttonSize.Width()) / 100 * $position);
                    }

                    ScrollBar.move($element, $position);
                    ScrollBar.moveStop();
                }
            }
        }

        /**
         * Handle scroll event
         * @param {ScrollEventArgs} $eventArgs Specify event args of scroll event.
         * @param {GuiObjectManager} [$manager] Provide gui object manager instance.
         * @param {Reflection} [$reflection] Provide reflection instance.
         * @return {void}
         */
        public static ScrollEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                         $reflection : Reflection) : void {
            $manager.setHovered($eventArgs.Owner(), true);
            const parent : IGuiCommons = $eventArgs.Owner();
            parent.getChildElements().foreach(($child : ScrollBar) : void => {
                if ($reflection.IsMemberOf($child, ScrollBar)
                    && !$manager.IsActive($child) && ElementManager.IsVisible($child.Id())
                    && $child.OrientationType() === $eventArgs.OrientationType()) {
                    ScrollBar.MoveTo($child, $eventArgs.Position());
                }
            });
        }

        /**
         * Handle resize event
         * @param {ResizeEventArgs} $eventArgs Specify event args of resize event.
         * @param {GuiObjectManager} [$manager] Provide gui object manager instance.
         * @param {Reflection} [$reflection] Provide reflection instance.
         * @return {void}
         */
        public static ResizeEventHandler($eventArgs : ResizeEventArgs, $manager : GuiObjectManager,
                                         $reflection : Reflection) : void {
            if ($reflection.IsMemberOf($eventArgs, ResizeEventArgs)) {
                const element : IGuiCommons = $eventArgs.Owner();
                if ($reflection.IsMemberOf(element, ScrollBar)) {
                    if (ElementManager.IsVisible(element.Id())) {
                        ScrollBar.resize(<ScrollBar>element, (<ScrollBar>element).OrientationType() === OrientationType.VERTICAL ?
                            $eventArgs.ScrollBarHeight() : $eventArgs.ScrollBarWidth());
                    }
                } else {
                    element.getChildElements().foreach(($child : ScrollBar) : void => {
                        if ($reflection.IsMemberOf($child, ScrollBar)
                            && ElementManager.IsVisible($child.Id())) {
                            ScrollBar.resize($child, $child.OrientationType() === OrientationType.VERTICAL ?
                                $eventArgs.ScrollBarHeight() : $eventArgs.ScrollBarWidth());
                        }
                    });
                }
            }
        }

        private static resize($element : ScrollBar, $size : number) : void {
            const id : string = $element.Id();
            if (ElementManager.IsVisible(id)) {
                if (Reflection.getInstance().IsMemberOf($element, ScrollBar)) {
                    const upArrowSize : Size = new Size(id + "_UpArrow", true);
                    const downArrowSize : Size = new Size(id + "_DownArrow", true);
                    const buttonTopSize : Size = new Size(id + "_ButtonTop", true);
                    const buttonBottomSize : Size = new Size(id + "_ButtonBottom", true);
                    const buttonIconSize : Size = new Size(id + "_ButtonIcon", true);

                    $element.Size($size);
                    if (ObjectValidator.IsEmptyOrNull($element.currentSize)) {
                        $element.getInitialLayoutInfo();
                    }

                    if ($element.OrientationType() === OrientationType.VERTICAL) {
                        const minHeight : number = buttonTopSize.Height() + buttonBottomSize.Height();
                        const trackerSize : number = $size - upArrowSize.Height() - downArrowSize.Height();
                        if ($size > minHeight) {
                            ElementManager.setHeight(id + "_Tracker", trackerSize);
                            const centerHeight : number = Math.floor((trackerSize - minHeight) * 0.25);
                            ElementManager.setCssProperty(id + "_ButtonEnvelop", "min-height", centerHeight + minHeight);
                            ElementManager.setHeight(id + "_ButtonCenter", centerHeight);
                            ElementManager.setCssProperty(id + "_ButtonIcon", "top",
                                Math.ceil((centerHeight - buttonIconSize.Height()) / 2));
                            ElementManager.setCssProperty(id + "_ButtonEnvelop", "top",
                                Math.floor($element.position().Top() / 100 * (trackerSize - centerHeight - minHeight)));
                        }
                        ElementManager.setCssProperty($element.Id(), "right", 0);
                    } else {
                        const minWidth : number = buttonTopSize.Width() + buttonBottomSize.Width();
                        const trackerSize : number = $size - upArrowSize.Width() - downArrowSize.Width();
                        if ($size > minWidth) {
                            ElementManager.setWidth(id + "_Tracker", trackerSize);
                            const centerWidth : number = Math.floor((trackerSize - minWidth) * 0.25);
                            ElementManager.setCssProperty(id + "_ButtonEnvelop", "min-width", centerWidth + minWidth);
                            ElementManager.setWidth(id + "_ButtonCenter", centerWidth);
                            ElementManager.setCssProperty(id + "_ButtonIcon", "left",
                                Math.ceil((centerWidth - buttonIconSize.Width()) / 2));
                            ElementManager.setCssProperty(id + "_ButtonEnvelop", "left",
                                Math.floor($element.position().Left() / 100 * (trackerSize - centerWidth - minWidth)));
                        }
                        ElementManager.setCssProperty(id, "min-width", $size);
                        const parent : GuiCommons = $element.Parent();
                        if (!ObjectValidator.IsEmptyOrNull(parent)) {
                            ElementManager.setCssProperty($element.Id(), "top",
                                parent.Height() - ElementManager.getHeightOffset(parent.Id())
                                - ElementManager.getOffsetHeight($element.Id(), true));
                        }
                    }
                }
            }
        }

        private static moveInit($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                                $reflection : Reflection) : void {
            const element : ScrollBar = <ScrollBar>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, ScrollBar)) {
                $manager.setActive(element, true);

                let pointerEvents : string;
                element.getEventsManager().setEvent(EventOwner.MOUSE_MOVE, EventType.ON_START,
                    ($args : MouseEventArgs) : void => {
                        if ($manager.IsActive(<ClassName>ScrollBar)) {
                            pointerEvents = document.body.style.pointerEvents;
                            document.body.style.pointerEvents = "none";
                            $args.StopAllPropagation();
                            TextSelectionManager.Disable(true);
                            const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>ScrollBar);
                            elements.foreach(($element : ScrollBar) : void => {
                                if (!$element.isInnerTriggered()) {
                                    ElementManager.TurnActive($element.Id() + "_Button");
                                    $element.buttonPosition().Top(
                                        ElementManager.getElement($element.Id() + "_ButtonEnvelop").offsetTop);
                                    $element.buttonPosition().Left(
                                        ElementManager.getElement($element.Id() + "_ButtonEnvelop").offsetLeft);

                                    const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                                    eventArgs.Owner($element);
                                    eventArgs.OrientationType($element.OrientationType());
                                    eventArgs.Position($element.OrientationType() === OrientationType.VERTICAL ?
                                        $element.position().Top() : $element.position().Left());
                                    $element.getEventsManager().FireEvent($element, ScrollBarEventType.ON_BUTTON, eventArgs);
                                    $element.getEventsManager().FireEvent(ScrollBar.ClassName(), ScrollBarEventType.ON_BUTTON, eventArgs);
                                }
                            });
                        }
                    });

                element.getEventsManager().setEvent(EventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                    ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                        if ($manager.IsActive(<ClassName>ScrollBar)) {
                            $args.StopAllPropagation();
                            const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>ScrollBar);
                            elements.foreach(($element : ScrollBar) : void => {
                                if (!$element.isInnerTriggered()) {
                                    let position : number;
                                    $element.OrientationType() === OrientationType.VERTICAL ?
                                        position = $element.buttonPosition().Top() + $args.getDistanceY() :
                                        position = $element.buttonPosition().Left() + $args.getDistanceX();
                                    ScrollBar.move($element, position);
                                }
                            });
                        }
                    });

                element.getEventsManager().setEvent(EventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
                    ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                        if ($manager.IsActive(<ClassName>ScrollBar)) {
                            document.body.style.pointerEvents = pointerEvents;
                            TextSelectionManager.Disable(false);
                            TextSelectionManager.Clear();
                            const elements : ArrayList<IGuiCommons> = $manager.getType(<ClassName>ScrollBar);
                            elements.foreach(($element : ScrollBar) : void => {
                                ElementManager.TurnOff($element.Id() + "_Button");
                                ElementManager.TurnOff($element.Id() + "_UpArrow");
                                ElementManager.TurnOff($element.Id() + "_DownArrow");
                            });
                        }
                        ScrollBar.moveStop();
                    });
            }
        }

        private static move($element : ScrollBar, $position : number) : void {
            const id : string = $element.Id();
            if ($element.getGuiManager().IsActive($element)) {
                if (Reflection.getInstance().IsMemberOf($element, ScrollBar)) {
                    const buttonSize : Size = new Size(id + "_ButtonEnvelop", true);
                    const trackerSize : Size = new Size(id + "_Tracker", true);
                    $element.position().Top(0);
                    $element.position().Left(0);
                    if ($element.OrientationType() === OrientationType.VERTICAL) {
                        const heightTracker : number = trackerSize.Height();
                        const heightButton : number = buttonSize.Height();
                        if ($position < 0) {
                            $position = 0;
                        } else if ($position > heightTracker - heightButton) {
                            $position = heightTracker - heightButton;
                        }
                        ElementManager.setCssProperty(id + "_ButtonEnvelop", "top", $position);
                        $element.position().Top(Math.ceil(100 / (heightTracker - heightButton) * $position));
                    } else {
                        const widthTracker : number = trackerSize.Width();
                        const widthButton : number = buttonSize.Width();
                        if ($position < 0) {
                            $position = 0;
                        } else if ($position > widthTracker - widthButton) {
                            $position = widthTracker - widthButton;
                        }
                        ElementManager.setCssProperty(id + "_ButtonEnvelop", "left", $position);
                        $element.position().Left(Math.ceil(100 / (widthTracker - widthButton) * $position));
                    }

                    if (!$element.isOutsideTriggered()) {
                        const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                        eventArgs.Owner($element);
                        eventArgs.OrientationType($element.OrientationType());
                        eventArgs.Position($element.OrientationType() === OrientationType.VERTICAL ?
                            $element.position().Top() : $element.position().Left());
                        $element.getEventsManager().FireEvent($element, EventType.ON_CHANGE, eventArgs);
                        $element.getEventsManager().FireEvent(ScrollBar.ClassName(), EventType.ON_CHANGE, eventArgs);
                    }
                }
            }
        }

        private static buttonMove($eventArgs : MouseEventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void {
            if (!$manager.IsActive($eventArgs.Owner())) {
                const id : string = $eventArgs.Owner().Id();
                const element : ScrollBar = <ScrollBar>$eventArgs.Owner();

                let offset : number = 0;
                if (element.OrientationType() === OrientationType.VERTICAL) {
                    offset = element.getScreenPosition().Top();
                } else {
                    offset = element.getScreenPosition().Left();
                }

                const upArrow : HTMLElement = ElementManager.getElement(id + "_UpArrow");
                const button : HTMLElement = ElementManager.getElement(id + "_ButtonEnvelop");
                const event : MouseEvent = $eventArgs.NativeEventArgs();
                if (element.OrientationType() === OrientationType.VERTICAL) {
                    if (WindowManager.getMouseY(event) >
                        offset + ElementManager.getElement(id).offsetTop + upArrow.offsetHeight + button.offsetTop) {
                        ScrollBar.MovePlus(element, 5);
                    } else {
                        ScrollBar.MoveMinus(element, 5);
                    }
                } else {
                    if (WindowManager.getMouseX(event) <
                        offset + ElementManager.getElement(id).offsetLeft + upArrow.offsetWidth + button.offsetLeft) {
                        ScrollBar.MoveMinus(element, 5);
                    } else {
                        ScrollBar.MovePlus(element, 5);
                    }
                }
                const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                eventArgs.Owner(element);
                eventArgs.OrientationType(element.OrientationType());
                eventArgs.Position(element.OrientationType() === OrientationType.VERTICAL ?
                    element.position().Top() : element.position().Left());
                element.getEventsManager().FireEvent(element, ScrollBarEventType.ON_TRACKER, eventArgs);
                element.getEventsManager().FireEvent(ScrollBar.ClassName(), ScrollBarEventType.ON_TRACKER, eventArgs);
            }
        }

        private static moveStop() : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            if (manager.IsActive(<ClassName>ScrollBar)) {
                const elements : ArrayList<IGuiCommons> = manager.getActive(<ClassName>ScrollBar);
                elements.foreach(($element : ScrollBar) : void => {
                    const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                    eventArgs.Owner($element);
                    eventArgs.OrientationType($element.OrientationType());
                    eventArgs.Position($element.OrientationType() === OrientationType.VERTICAL ?
                        $element.position().Top() : $element.position().Left());
                    $element.getEventsManager().FireEvent($element, EventType.ON_SCROLL, eventArgs);
                    $element.getEventsManager().FireEvent(ScrollBar.ClassName(), EventType.ON_SCROLL, eventArgs);
                    manager.setActive($element, false);
                    $element.isInnerTriggered(false);
                    $element.isOutsideTriggered(false);
                    $element.clearExpand(true);
                    clearTimeout(ScrollBar.threadCallback);
                    ScrollBar.threadTick = 0;
                });
            }
        }

        /**
         * @param {OrientationType} $orientationType Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($orientationType : OrientationType, $id? : string) {
            super($id);
            this.GuiType($orientationType);
        }

        /**
         * @return {IScrollBarEvents} Returns events manager subscribed to the item.
         */
        public getEvents() : IScrollBarEvents {
            return <IScrollBarEvents>super.getEvents();
        }

        /**
         * @param {OrientationType} [$orientationType] Specify type of element look and feel.
         * @return {OrientationType} Returns type of element's look and feel.
         */
        public GuiType($orientationType? : OrientationType) : OrientationType {
            this.guiType = Property.EnumType(this.guiType, $orientationType, OrientationType, OrientationType.VERTICAL);
            if (ObjectValidator.IsSet($orientationType)) {
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                }
            }
            return this.guiType;
        }

        /**
         * @param {OrientationType} [$orientationType] Specify type of element orientation.
         * @return {OrientationType} Returns type of element's orientation.
         */
        public OrientationType($orientationType? : OrientationType) : OrientationType {
            return this.GuiType($orientationType);
        }

        /**
         * @param {number} [$value] Specify element's width or height value based on element's orientation.
         * @return {number} Returns element's size value.
         */
        public Size($value? : number) : number {
            this.size = Property.PositiveInteger(this.size, $value, 30);
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                ScrollBar.Show(this);
            }
            return this.size;
        }

        /**
         * @return {IGuiCommonsArg[]} Returns array of element's attributes.
         */
        public getArgs() : IGuiCommonsArg[] {
            const args : IGuiCommonsArg[] = super.getArgs();
            args.push({
                name : "Size",
                type : GuiCommonsArgType.NUMBER,
                value: this.Size()
            });
            args.push({
                name : this.GuiType() === OrientationType.HORIZONTAL ? "Width" : "Height",
                type : GuiCommonsArgType.NUMBER,
                value: this.Size()
            });
            args.push(<IGuiCommonsListArg>{
                items: OrientationType.getProperties(),
                name : "GuiType",
                type : GuiCommonsArgType.LIST,
                value: OrientationType.getKey(<string>this.GuiType())
            });
            return args;
        }

        /**
         * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
         * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
         * @return {void}
         */
        public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
            switch ($value.name) {
            case "Size":
                this.Size(<number>$value.value);
                break;
            case "Width":
                if (this.GuiType() === OrientationType.HORIZONTAL) {
                    this.Size(<number>$value.value);
                } else {
                    super.setArg($value, $force);
                }
                break;
            case "Height":
                if (this.GuiType() === OrientationType.VERTICAL) {
                    this.Size(<number>$value.value);
                } else {
                    super.setArg($value, $force);
                }
                break;
            case "GuiType":
                this.GuiType(OrientationType[<string>$value.value]);
                break;
            default:
                super.setArg($value, $force);
                break;
            }
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!OrientationType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType or OrientationType method for set of scrollbar type instead of StyleClassName method.");
            return false;
        }

        protected innerCode() : IGuiElement {
            this.upArrowEvents = new ElementEventsManager(this, this.Id() + "_UpArrow");
            this.downArrowEvents = new ElementEventsManager(this, this.Id() + "_DownArrow");
            this.trackerEvents = new ElementEventsManager(this, this.Id() + "_Tracker");
            this.trackerButtonEvents = new ElementEventsManager(this, this.Id() + "_Button");
            this.hoverExpandEvents = new ElementEventsManager(this, this.Id() + "_HoverExpand");

            this.upArrowEvents.setOnMouseDown(($eventArgs : MouseEventArgs) : void => {
                ElementManager.TurnActive($eventArgs.Owner().Id() + "_UpArrow");
                ScrollBar.MoveMinus($eventArgs.Owner(), 2);
            });
            this.upArrowEvents.setOnMouseOver(($eventArgs : MouseEventArgs) : void => {
                ScrollBar.ArrowOn($eventArgs.Owner(), $eventArgs.Owner().Id() + "_UpArrow");
            });
            this.upArrowEvents.setOnMouseOut(($eventArgs : MouseEventArgs) : void => {
                ScrollBar.ArrowOff($eventArgs.Owner(), $eventArgs.Owner().Id() + "_UpArrow");
            });

            this.downArrowEvents.setOnMouseDown(($eventArgs : MouseEventArgs) : void => {
                ElementManager.TurnActive($eventArgs.Owner().Id() + "_DownArrow");
                ScrollBar.MovePlus($eventArgs.Owner(), 2);
            });
            this.downArrowEvents.setOnMouseOver(($eventArgs : MouseEventArgs) : void => {
                ScrollBar.ArrowOn($eventArgs.Owner(), $eventArgs.Owner().Id() + "_DownArrow");
            });
            this.downArrowEvents.setOnMouseOut(($eventArgs : MouseEventArgs) : void => {
                ScrollBar.ArrowOff($eventArgs.Owner(), $eventArgs.Owner().Id() + "_DownArrow");
            });

            this.trackerEvents.setOnMouseDown(ScrollBar.buttonMove);

            this.trackerButtonEvents.setOnMouseDown(ScrollBar.moveInit);
            this.trackerButtonEvents.setOnMouseOver(($eventArgs : MouseEventArgs) : void => {
                ScrollBar.ButtonOn($eventArgs.Owner());
            });
            this.trackerButtonEvents.setOnMouseOut(($eventArgs : MouseEventArgs) : void => {
                ScrollBar.ButtonOff($eventArgs.Owner());
            });

            this.getEvents().setOnMouseOver(() : void => {
                this.animateExpand();
            });
            this.getEvents().setOnMouseOut(() : void => {
                this.isMouseOver = false;
                this.clearExpand();
            });
            this.getEvents().setOnMouseUp(() : void => {
                this.isMouseOver = true;
                ScrollBar.moveStop();
            });

            this.hoverExpandEvents.setOnMouseOver(() : void => {
                this.animateExpand();
            });
            this.hoverExpandEvents.setOnMouseOut(() : void => {
                this.isMouseOver = false;
                this.clearExpand();
            });
            this.hoverExpandEvents.setOnMouseUp(() : void => {
                this.isMouseOver = true;
                ScrollBar.moveStop();
            });

            this.getEvents().setOnComplete(
                ($eventArgs : EventArgs) : void => {
                    const element : ScrollBar = <ScrollBar>$eventArgs.Owner();
                    element.upArrowEvents.Subscribe();
                    element.downArrowEvents.Subscribe();
                    element.trackerEvents.Subscribe();
                    element.trackerButtonEvents.Subscribe();
                    element.hoverExpandEvents.Subscribe();
                    element.isInnerTriggered(false);
                    element.isOutsideTriggered(false);
                });

            if (ObjectValidator.IsEmptyOrNull(this.Parent())) {
                this.getEvents().setOnLoad(
                    ($eventArgs : EventArgs) : void => {
                        const element : ScrollBar = <ScrollBar>$eventArgs.Owner();
                        ScrollBar.resize(element, element.Size());
                    });
            }

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            let sizeType : string = "width";
            if (this.guiType === OrientationType.VERTICAL) {
                sizeType = "height";
            }

            return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType()).GuiTypeTag(this.getGuiTypeTag())
                .Add(this.addElement(this.Id() + "_HoverExpand").StyleClassName("HoverExpand"))
                .Add(this.addElement(this.Id() + "_ContentOffset").StyleClassName("ContentOffset"))
                .Add(this.addElement()
                    .StyleClassName("UpArrow")
                    .Add(this.addElement(this.Id() + "_UpArrow")
                        .StyleClassName(GeneralCssNames.OFF)
                        .Add(this.addElement(this.Id() + "_UpArrowIcon").StyleClassName(GeneralCssNames.ICON))
                    )
                )
                .Add(this.addElement(this.Id() + "_Tracker")
                    .StyleClassName("Tracker").setAttribute(sizeType, this.size + "px")
                    .Add(this.addElement(this.Id() + "_ButtonEnvelop")
                        .StyleClassName("Button")
                        .Add(this.addElement(this.Id() + "_Button")
                            .StyleClassName(GeneralCssNames.OFF)
                            .Add(this.addElement(this.Id() + "_ButtonTop").StyleClassName(GeneralCssNames.TOP))
                            .Add(this.addElement(this.Id() + "_ButtonCenter")
                                .StyleClassName(GeneralCssNames.CENTER)
                                .Add(this.addElement(this.Id() + "_ButtonIcon").StyleClassName(GeneralCssNames.ICON))
                            )
                            .Add(this.addElement(this.Id() + "_ButtonBottom").StyleClassName(GeneralCssNames.BOTTOM))
                        )
                    )
                )
                .Add(this.addElement()
                    .StyleClassName("DownArrow")
                    .Add(this.addElement(this.Id() + "_DownArrow")
                        .StyleClassName(GeneralCssNames.OFF)
                        .Add(this.addElement(this.Id() + "_DownArrowIcon").StyleClassName(GeneralCssNames.ICON))
                    )
                );
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();
            this.size = 300;
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push(
                "size",
                "upArrowEvents", "downArrowEvents",
                "trackerEvents", "trackerButtonEvents",
                "isInnerTriggeredState", "isOutsideTriggeredState",
                "elementPosition", "buttonPositionValue"
            );
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push(
                "upArrowEvents", "downArrowEvents", "trackerEvents", "trackerButtonEvents",
                "isInnerTriggeredState", "isOutsideTriggeredState",
                "elementPosition", "buttonPositionValue");
            return exclude;
        }

        private isInnerTriggered($value? : boolean) : boolean {
            this.isInnerTriggeredState = Property.Boolean(this.isInnerTriggeredState, $value);
            return this.isInnerTriggeredState;
        }

        private isOutsideTriggered($value? : boolean) : boolean {
            this.isOutsideTriggeredState = Property.Boolean(this.isOutsideTriggeredState, $value);
            return this.isOutsideTriggeredState;
        }

        private position($value? : ElementOffset) : ElementOffset {
            if (ObjectValidator.IsSet($value)) {
                this.elementPosition = $value;
            }
            if (!ObjectValidator.IsSet(this.elementPosition)) {
                this.elementPosition = new ElementOffset();
            }
            return this.elementPosition;
        }

        private buttonPosition($value? : ElementOffset) : ElementOffset {
            if (ObjectValidator.IsSet($value)) {
                this.buttonPositionValue = $value;
            }
            if (!ObjectValidator.IsSet(this.buttonPositionValue)) {
                this.buttonPositionValue = new ElementOffset();
            }
            return this.buttonPositionValue;
        }

        private animateExpand($directionType : DirectionType = DirectionType.UP) : void {
            if (this.isExpandable) {
                this.isMouseOver = true;
                const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(this.Id() + "_Expand");
                if ($directionType === DirectionType.UP && manipulatorArgs.CurrentValue() === 0
                    || $directionType === DirectionType.DOWN && manipulatorArgs.CurrentValue() !== 0) {
                    manipulatorArgs.Owner(this.Id());
                    manipulatorArgs.DirectionType($directionType);
                    manipulatorArgs.ProgressType($directionType === DirectionType.UP ? ProgressType.LINEAR : ProgressType.SLOW_START);
                    manipulatorArgs.Step(20);
                    manipulatorArgs.RangeStart(0);
                    manipulatorArgs.RangeEnd(100);
                    manipulatorArgs.ChangeEventType(this.Id() + "_ExpandChange");
                    this.getEventsManager().setEvent(this.Id() + "_Expand", this.Id() + "_ExpandChange",
                        (eventArgs : ValueProgressEventArgs) : void => {
                            this.setExpandPercentage(eventArgs.CurrentValue());
                        });
                    if ($directionType === DirectionType.UP) {
                        const elements : ArrayList<ScrollBar> = <ArrayList<ScrollBar>>this.getGuiManager().getType(<ClassName>ScrollBar);
                        let waitForHide : boolean = false;
                        elements.foreach(($element : ScrollBar) : boolean => {
                            if ($element.Visible() && this !== $element && this.Parent() === $element.Parent() && $element.isExpanded()) {
                                waitForHide = true;
                                $element.animateExpand(DirectionType.DOWN);
                                return false;
                            }
                        });
                        this.getEvents().FireAsynchronousMethod(() : void => {
                            ValueProgressManager.Execute(manipulatorArgs);
                        }, waitForHide ? 200 : 0);
                    } else {
                        ValueProgressManager.Execute(manipulatorArgs);
                    }
                }
            }
        }

        private clearExpand($force : boolean = false) : void {
            if (this.isExpandable) {
                clearTimeout(this.hideEventHandle);
                this.hideEventHandle = this.getEvents().FireAsynchronousMethod(() : void => {
                    if (!this.isMouseOver && ($force || !this.getGuiManager().IsActive(this))) {
                        this.animateExpand(DirectionType.DOWN);
                    }
                }, false, 600);
            }
        }

        private setExpandPercentage($percentage : number) : void {
            if (this.isExpandable) {
                this.currentSize = Math.ceil(this.defaultSize + (this.expandSize - this.defaultSize) * $percentage / 100);
                const percentageMult : number = this.currentSize / this.defaultSize;
                const propertyName : string = this.guiType === OrientationType.VERTICAL ? "width" : "height";
                const propertyValue : number = this.currentSize - (this.guiType === OrientationType.VERTICAL
                    ? this.buttonWidthOffset : this.buttonHeightOffset);
                ElementManager.setCssProperty(this.Id() + "_ButtonTop", propertyName, propertyValue);
                ElementManager.setCssProperty(this.Id() + "_ButtonCenter", propertyName, propertyValue);
                ElementManager.setCssProperty(this.Id() + "_ButtonBottom", propertyName, propertyValue);
                ElementManager.setCssProperty(this.Id() + "_Tracker", propertyName, this.currentSize);
                ElementManager.setCssProperty(this.Id() + "_ButtonIcon", "left",
                    (this.buttonIconOffset.Left() + this.buttonIconSize.Width()) * percentageMult - this.buttonIconSize.Width());
                ElementManager.setCssProperty(this.Id() + "_ButtonIcon", "top",
                    (this.buttonIconOffset.Top() + this.buttonIconSize.Height()) * percentageMult - this.buttonIconSize.Height());

                const upArrowWidth : number =
                    Math.floor((this.guiType === OrientationType.VERTICAL
                        ? this.currentSize : this.upArrowSize.Width() * percentageMult) - this.upArrowWidthOffset);
                const upArrowHeight : number =
                    Math.floor((this.guiType === OrientationType.VERTICAL
                        ? this.upArrowSize.Height() * percentageMult : this.currentSize) - this.upArrowWidthOffset);
                const downArrowWidth : number =
                    Math.floor((this.guiType === OrientationType.VERTICAL
                        ? this.currentSize : this.downArrowSize.Width() * percentageMult) - this.downArrowWidthOffset);
                const downArrowHeight : number =
                    Math.floor((this.guiType === OrientationType.VERTICAL
                        ? this.downArrowSize.Height() * percentageMult : this.currentSize) - this.downArrowHeightOffset);

                if (!(upArrowHeight === 0 || upArrowWidth === 0)) {
                    ElementManager.setCssProperty(this.Id() + "_UpArrow", "width", upArrowWidth);
                    ElementManager.setCssProperty(this.Id() + "_UpArrow", "height", upArrowHeight);
                    ElementManager.setCssProperty(this.Id() + "_UpArrowIcon", "left",
                        (this.upArrowIconOffset.Left() + this.upArrowIconSize.Width())
                        * percentageMult - this.upArrowIconSize.Width());
                    ElementManager.setCssProperty(this.Id() + "_UpArrowIcon", "top",
                        (this.upArrowIconOffset.Top() + this.upArrowIconSize.Height())
                        * percentageMult - this.upArrowIconSize.Height());
                }
                if (!(downArrowHeight === 0 || downArrowWidth === 0)) {
                    ElementManager.setCssProperty(this.Id() + "_DownArrow", "width", downArrowWidth);
                    ElementManager.setCssProperty(this.Id() + "_DownArrow", "height", downArrowHeight);
                    ElementManager.setCssProperty(this.Id() + "_DownArrowIcon", "left",
                        (this.downArrowIconOffset.Left() + this.downArrowIconSize.Width())
                        * percentageMult - this.downArrowIconSize.Width());
                    ElementManager.setCssProperty(this.Id() + "_DownArrowIcon", "top",
                        (this.downArrowIconOffset.Top() + this.downArrowIconSize.Height())
                        * percentageMult - this.downArrowIconSize.Height());
                }
                ScrollBar.resize(this, this.Size());
            }
        }

        private getInitialLayoutInfo() : void {
            const handler : any = () : void => {
                this.defaultSize = this.OrientationType() === OrientationType.VERTICAL ?
                    ElementManager.getCssIntegerValue(this.Id() + "_Tracker", "width")
                    : ElementManager.getCssIntegerValue(this.Id() + "_Tracker", "height");
                this.currentSize = this.defaultSize;
                this.expandSize = ElementManager.getCssIntegerValue(this.Id() + "_HoverExpand",
                    this.OrientationType() === OrientationType.VERTICAL ? "width" : "height");
                if (this.expandSize <= this.currentSize) {
                    this.isExpandable = false;
                } else {
                    this.isExpandable = true;
                    this.upArrowSize = new Size(this.Id() + "_UpArrow");
                    this.upArrowIconSize = new Size(this.Id() + "_UpArrowIcon");
                    this.buttonIconSize = new Size(this.Id() + "_ButtonIcon");
                    this.downArrowSize = new Size(this.Id() + "_DownArrow");
                    this.downArrowIconSize = new Size(this.Id() + "_DownArrowIcon");
                    this.upArrowIconOffset = new ElementOffset(
                        ElementManager.getCssIntegerValue(this.Id() + "_UpArrowIcon", "top"),
                        ElementManager.getCssIntegerValue(this.Id() + "_UpArrowIcon", "left"));
                    this.buttonIconOffset = new ElementOffset(
                        ElementManager.getCssIntegerValue(this.Id() + "_ButtonIcon", "top"),
                        ElementManager.getCssIntegerValue(this.Id() + "_ButtonIcon", "left"));
                    this.downArrowIconOffset = new ElementOffset(
                        ElementManager.getCssIntegerValue(this.Id() + "_DownArrowIcon", "top"),
                        ElementManager.getCssIntegerValue(this.Id() + "_DownArrowIcon", "left"));
                    this.upArrowWidthOffset = ElementManager.getWidthOffset(this.Id() + "_UpArrow");
                    this.upArrowHeightOffset = ElementManager.getHeightOffset(this.Id() + "_UpArrow");
                    this.buttonWidthOffset = ElementManager.getWidthOffset(this.Id() + "_ButtonCenter");
                    this.buttonHeightOffset = ElementManager.getHeightOffset(this.Id() + "_ButtonCenter");
                    this.downArrowWidthOffset = ElementManager.getWidthOffset(this.Id() + "_DownArrow");
                    this.downArrowHeightOffset = ElementManager.getHeightOffset(this.Id() + "_DownArrow");
                }
            };
            if (this.IsCompleted()) {
                handler();
            } else {
                this.getEvents().setOnComplete(() : void => {
                    handler();
                });
            }
        }

        private isExpanded() : boolean {
            return !this.isExpandable || ValueProgressManager.get(this.Id() + "_Expand").CurrentValue() !== 0;
        }
    }
}
