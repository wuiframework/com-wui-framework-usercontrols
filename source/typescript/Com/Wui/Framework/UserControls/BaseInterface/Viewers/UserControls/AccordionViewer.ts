/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import AccordionViewerArgs = Com.Wui.Framework.UserControls.BaseInterface.ViewersArgs.UserControls.AccordionViewerArgs;
    import Accordion = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Accordion;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import BasePanelHolderViewerArgs = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewerArgs;
    import AccordionType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import AccordionResizeType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionResizeType;

    /* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
    export class AccordionViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : AccordionViewerArgs {
            const args : AccordionViewerArgs = new AccordionViewerArgs(AccordionType.HORIZONTAL);
            args.ResizeType(AccordionResizeType.RESPONSIVE);

            let holderArgs : BasePanelHolderViewerArgs;

            holderArgs = new BasePanelHolderViewerArgs();

            holderArgs.HeaderText("Holder 1");
            holderArgs.DescriptionText("Desc.");
            holderArgs.PrioritySize(new PropagableNumber({number: 200, unitType: UnitType.PCT}));
            holderArgs.IsOpened(false);
            args.AddPanelHoldersArgs(holderArgs);

            holderArgs = new BasePanelHolderViewerArgs();
            holderArgs.HeaderText("Holder 2");
            holderArgs.DescriptionText("Description txt");
            holderArgs.IsOpened(true);
            holderArgs.BodyViewerClass(Com.Wui.Framework.UserControls.RuntimeTests.UserControlsTestPanelViewer);
            args.AddPanelHoldersArgs(holderArgs);

            holderArgs = new BasePanelHolderViewerArgs();
            holderArgs.HeaderText("Holder 3");
            holderArgs.PrioritySize(new PropagableNumber({number: 100, unitType: UnitType.PX}));
            holderArgs.IsOpened(true);

            args.AddPanelHoldersArgs(holderArgs);

            return args;
        }

        /* dev:end */

        constructor($args? : AccordionViewerArgs) {
            super($args);
            if (!ObjectValidator.IsEmptyOrNull($args)) {
                this.setInstance(new Accordion($args.getPanelHoldersArgsList(), $args.getGuiType()));
            } else {
                this.setInstance(new Accordion(new AccordionViewerArgs(AccordionType.VERTICAL).getPanelHoldersArgsList()));
            }
            /* dev:start */
            this.setTestSubscriber(Com.Wui.Framework.UserControls.RuntimeTests.UserControls.AccordionTest);
            /* dev:end */
        }

        public getInstance() : Accordion {
            return <Accordion>super.getInstance();
        }

        /**
         * @param {AccordionViewerArgs} [$args] Set user control viewer arguments.
         * @return {AccordionViewerArgs} Returns user control viewer arguments.
         */
        public ViewerArgs($args? : AccordionViewerArgs) : AccordionViewerArgs {
            return <AccordionViewerArgs>super.ViewerArgs(<AccordionViewerArgs>$args);
        }

        protected normalImplementation() : void {
            const instance : Accordion = this.getInstance();

            if (!ObjectValidator.IsEmptyOrNull(this.ViewerArgs())) {
                const args : AccordionViewerArgs = this.ViewerArgs();
                args.AsyncEnabled(true);
                instance.setPanelHoldersArgs(args.getPanelHoldersArgsList());
                instance.ResizeType(args.ResizeType());
            }
        }
    }
}
