/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import DialogViewerArgs = Com.Wui.Framework.UserControls.BaseInterface.ViewersArgs.UserControls.DialogViewerArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Dialog = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Dialog;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import DialogType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.DialogType;

    /* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
    export class DialogViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DialogViewerArgs {
            const args : DialogViewerArgs = new DialogViewerArgs();
            args.Visible(true);
            return args;
        }

        /* dev:end */

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new Dialog(DialogType.GENERAL));
            /* dev:start */
            this.setTestSubscriber(Com.Wui.Framework.UserControls.RuntimeTests.UserControls.DialogTest);
            /* dev:end */
        }

        public getInstance() : Dialog {
            return <Dialog>super.getInstance();
        }

        public ViewerArgs($args? : DialogViewerArgs) : DialogViewerArgs {
            return <DialogViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : Dialog, $args : DialogViewerArgs) : void {
            if (!ObjectValidator.IsEmptyOrNull($args.PanelArgs())) {
                $instance.Value($args.PanelArgs());
            }
        }
    }
}
