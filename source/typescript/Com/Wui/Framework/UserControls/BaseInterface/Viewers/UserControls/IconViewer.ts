/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Icon = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Icon;
    import IconType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.IconType;

    /* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
    export class IconViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new Icon(IconType.BLUE_SQUARE));
            /* dev:start */
            this.setTestSubscriber(Com.Wui.Framework.UserControls.RuntimeTests.UserControls.IconTest);
            /* dev:end */
        }
    }
}
