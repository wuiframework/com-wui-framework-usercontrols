/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.ViewersArgs.UserControls {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    /**
     * DialogViewerArgs is abstract structure handling data connected with Dialog user control.
     */
    export class DialogViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private panelArgs : BasePanelViewerArgs;
        private headerText : string;

        constructor() {
            super();
            super.Visible(false);
            this.headerText = "";
        }

        /**
         * @param {BasePanelViewerArgs} [$value] Specify arguments, which will be passed to the dialog's panel.
         * @return {BasePanelViewerArgs} Returns dialog's panel arguments.
         */
        public PanelArgs($value? : BasePanelViewerArgs) : BasePanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) &&
                Reflection.getInstance().IsMemberOf($value, BasePanelViewerArgs)) {
                this.panelArgs = $value;
            }
            return this.panelArgs;
        }

        /**
         * @param {string} [$value] Set value for text of dialog header.
         * @return {string} Returns value of dialog's header text.
         */
        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }
    }
}
