/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.ViewersArgs.UserControls {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import BasePanelHolderViewerArgs = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewerArgs;
    import AccordionType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionType;
    import BasePanelHolderViewer = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewer;
    import HorizontalPanelHolderViewer = Com.Wui.Framework.UserControls.BaseInterface.Viewers.UserControls.HorizontalPanelHolderViewer;
    import VerticalPanelHolderViewer = Com.Wui.Framework.UserControls.BaseInterface.Viewers.UserControls.VerticalPanelHolderViewer;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import AccordionResizeType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionResizeType;

    /**
     * AccordionViewerArgs is structure handling BasePanelHolders instances.
     */
    export class AccordionViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private readonly holdersArgsList : ArrayList<BasePanelHolderViewerArgs>;
        private readonly guiType : AccordionType;
        private resizeType : AccordionResizeType;

        /**
         * @param {AccordionType} [$accordionType] Specify Accordion look and feel.
         */
        constructor($accordionType? : AccordionType) {
            super();

            this.holdersArgsList = new ArrayList<BasePanelHolderViewerArgs>();
            this.guiType = Property.EnumType(this.guiType, $accordionType, AccordionType, AccordionType.HORIZONTAL);
        }

        /**
         * @param {BasePanelHolderViewerArgs} [$value] Register panel holder arguments,
         * which should be generated as content of user control body.
         * @return {void}
         */
        public AddPanelHoldersArgs($value : BasePanelHolderViewerArgs) : void {
            this.holdersArgsList.Add($value);
        }

        /**
         * @return {ArrayList<BasePanelHolderViewerArgs>} Returns list of registered arguments for BasePanelHolder user controls.
         */
        public getPanelHoldersArgsList() : ArrayList<BasePanelHolderViewerArgs> {
            return this.holdersArgsList;
        }

        /**
         * @return {AccordionType} Returns type of element's look and feel.
         */
        public getGuiType() : AccordionType {
            return this.guiType;
        }

        /**
         * @param {ResizeType} [$value] Specify how resizing should be handled.
         * @return {ResizeType} Returns type of resize that should be applied.
         */
        public ResizeType($value? : AccordionResizeType) : AccordionResizeType {
            if (ObjectValidator.IsSet($value)) {
                this.resizeType = $value;
            }
            return this.resizeType;
        }

        /**
         * @return {BasePanelHolderViewer} Returns class with type of BasePanelHolderViewer based on desired guiType.
         */
        public getPanelHolderViewerClass() : any {
            switch (this.guiType) {
            case AccordionType.VERTICAL:
                return VerticalPanelHolderViewer;
            case AccordionType.HORIZONTAL:
                return HorizontalPanelHolderViewer;
            default :
                return BasePanelHolderViewer;
            }
        }
    }
}
