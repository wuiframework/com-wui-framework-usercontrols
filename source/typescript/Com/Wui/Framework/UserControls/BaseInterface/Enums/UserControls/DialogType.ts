/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls {
    "use strict";

    export class DialogType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly GENERAL : string = "";
        public static readonly RED : string = "Red";
        public static readonly BLUE : string = "Blue";
        public static readonly GREEN : string = "Green";
    }
}
