/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls {
    "use strict";

    export class AccordionResizeType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly DEFAULT : string = "Default";
        public static readonly RESPONSIVE : string = "Responsive";
    }
}
