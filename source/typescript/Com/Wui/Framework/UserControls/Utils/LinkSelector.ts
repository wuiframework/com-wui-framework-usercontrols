/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Utils {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import TextField = Com.Wui.Framework.UserControls.BaseInterface.UserControls.TextField;
    import TextArea = Com.Wui.Framework.UserControls.BaseInterface.UserControls.TextArea;
    import DropDownList = Com.Wui.Framework.UserControls.BaseInterface.UserControls.DropDownList;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import ScrollBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ScrollBar;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import DragBar = Com.Wui.Framework.UserControls.BaseInterface.Components.DragBar;
    import ILink = Com.Wui.Framework.Gui.Interfaces.UserControls.ILink;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;

    /**
     * LinkSelector class provides methods for element selection by Link HTML element.
     */
    export class LinkSelector extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private static tabPressed : boolean = false;
        private static focusedByTab : boolean = false;
        private owner : IGuiCommons;
        private enabled : boolean;
        private value : string;
        private reloadTo : string;
        private blankTargetAllowed : boolean;
        private tabIndex : number;
        private linkEvents : ElementEventsManager;
        private linkFormatter : ($value : string, $self? : LinkSelector) => string;

        /**
         * @param {IGuiCommons} $element Specify element, which should be handled.
         * @param {boolean} [$force=false] If true, than do not care about TAB navigation state.
         * @return {void}
         */
        public static Focus($element : IGuiCommons, $force : boolean = false) : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            const reflection : Reflection = Reflection.getInstance();
            if (!LinkSelector.isLinkNull($element) && $element.Enabled()) {
                if ($force) {
                    ElementManager.getElement($element.Id() + "_Link").focus();
                }
                if (LinkSelector.tabPressed) {
                    LinkSelector.tabPressed = false;
                    LinkSelector.focusedByTab = true;
                } else {
                    LinkSelector.focusedByTab = false;
                }

                if (LinkSelector.focusedByTab) {
                    const elementClass : any = reflection.getClass($element.getClassName());
                    if (ObjectValidator.IsSet(elementClass.TurnActive)) {
                        elementClass.TurnActive($element, manager, reflection);
                    } else {
                        ElementManager.TurnActive($element.Id() + "_Enabled");
                    }
                }
                manager.setActive($element, true);
            }
        }

        /**
         * @param {ClassName} [$elementType] Specify element type, which should be handled.
         * @return {void}
         */
        public static Blur($elementType : ClassName) : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            const reflection : Reflection = Reflection.getInstance();
            const elements : ArrayList<IGuiCommons> = manager.getActive($elementType);
            elements.foreach(($element : IGuiCommons) : void => {
                manager.setActive($element, false);
                const elementClass : any = reflection.getClass($element.getClassName());
                if (ObjectValidator.IsSet(elementClass.TurnOff)) {
                    elementClass.TurnOff($element, manager, reflection);
                } else {
                    ElementManager.TurnOff($element.Id() + "_Enabled");
                }
            });
        }

        private static isLinkNull($element : any) : boolean {
            if (ObjectValidator.IsSet($element.getSelector)) {
                return ObjectValidator.IsEmptyOrNull($element.getSelector().getValue());
            }
            return true;
        }

        private static onMouseOverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                                               $reflection : Reflection) : void {
            const element : any = $eventArgs.Owner();
            element.getEvents().FireAsynchronousMethod(() : void => {
                if (!$manager.IsActive($reflection.getClass(element.getClassName()))
                    && !LinkSelector.focusedByTab
                    && !$manager.IsActive(<ClassName>ScrollBar)
                    && !$manager.IsActive(<ClassName>ResizeBar)
                    && !$manager.IsActive(<ClassName>DragBar)) {
                    const elementClass : any = $reflection.getClass(element.getClassName());
                    if (ObjectValidator.IsSet(elementClass.TurnOn)) {
                        elementClass.TurnOn(element, $manager, $reflection);
                    } else {
                        ElementManager.TurnOn(element.Id() + "_Enabled");
                    }

                    const link : HTMLAnchorElement = <HTMLAnchorElement>ElementManager.getElement(element.Id() + "_Link");
                    if (!element.Enabled()) {
                        ElementManager.Show(link);
                    }
                    element.TabIndex(element.TabIndex());
                    if (!$manager.IsActive(<ClassName>TextField)
                        && !$manager.IsActive(<ClassName>TextArea)
                        && !$manager.IsActive(<ClassName>DropDownList)) {
                        $manager.setActive(element, true);
                        link.focus();
                    }
                    if (!element.Enabled()) {
                        element.getEvents().FireAsynchronousMethod(() : void => {
                            link.blur();
                            ElementManager.Hide(link);
                        }, 1000);
                    }
                }
            }, 10);
        }

        private static onMouseOutEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                                              $reflection : Reflection) : void {
            const element : any = $eventArgs.Owner();
            if (element.Enabled() && !LinkSelector.focusedByTab) {
                element.getEvents().FireAsynchronousMethod(() : void => {
                    ElementManager.getElement(element.Id() + "_Link").blur();
                    const elementClass : any = $reflection.getClass(element.getClassName());
                    if (ObjectValidator.IsSet(elementClass.TurnOff)) {
                        elementClass.TurnOff(element, $manager, $reflection);
                    } else {
                        ElementManager.TurnOff(element.Id() + "_Enabled");
                    }
                });
            }
        }

        private static onMouseDownEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                                               $reflection : Reflection) : void {
            const element : any = $eventArgs.Owner();
            if (!LinkSelector.isLinkNull(element) && element.Enabled()) {
                $manager.setActive(element, false);
                const elementClass : any = $reflection.getClass(element.getClassName());
                if (ObjectValidator.IsSet(elementClass.TurnActive)) {
                    elementClass.TurnActive(element, $manager, $reflection);
                } else {
                    ElementManager.TurnActive(element.Id() + "_Enabled");
                }
                $eventArgs.PreventDefault();
            }
        }

        private static onMouseUpEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                                             $reflection : Reflection) : void {
            $manager.setActive($eventArgs.Owner(), true);
            LinkSelector.onLinkClickEventHandler($eventArgs, $manager, $reflection);
            $manager.setActive($eventArgs.Owner(), false);
        }

        private static onClickEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void {
            const element : any = $eventArgs.Owner();
            if (LinkSelector.isLinkNull(element) || !element.Enabled() || !$manager.IsActive(element)) {
                $eventArgs.PreventDefault();
            }
        }

        private static onLinkClickEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager,
                                               $reflection : Reflection) : void {
            const element : any = $eventArgs.Owner();
            if (!LinkSelector.isLinkNull(element) && element.Enabled()) {
                const elementClass : any = $reflection.getClass(element.getClassName());
                if (ObjectValidator.IsSet(elementClass.TurnOff)) {
                    elementClass.TurnOff(element, $manager, $reflection);
                } else {
                    ElementManager.TurnOff(element.Id() + "_Enabled");
                }
                let newWindowRequired : boolean = false;
                if (element.Implements(ILink)) {
                    newWindowRequired = element.OpenInNewWindow();
                } else if (ObjectValidator.IsSet(element.getSelector)) {
                    newWindowRequired = element.getSelector().OpenInNewWindow();
                }
                if (newWindowRequired && Loader.getInstance().getHttpManager().getRequest().IsWuiJre()) {
                    element.getHttpManager()
                        .ReloadTo((<HTMLAnchorElement>ElementManager.getElement(element.Id() + "_Link", true)).href, true);
                } else {
                    ElementManager.getElement(element.Id() + "_Link", true).click();
                }
            } else {
                $eventArgs.PreventDefault();
            }
        }

        private static onBodyClickEventHandler($elementType : IGuiCommons, $manager : GuiObjectManager,
                                               $reflection : Reflection) : void {
            const elements : ArrayList<IGuiCommons> = $manager.getType($elementType);
            elements.foreach(($element : IGuiCommons) : void => {
                if (!$manager.IsActive($element)) {
                    const elementClass : any = $reflection.getClass($element.getClassName());
                    if (ObjectValidator.IsSet(elementClass.TurnOff)) {
                        elementClass.TurnOff($element, $manager, $reflection);
                    } else {
                        ElementManager.TurnOff($element.Id() + "_Enabled");
                    }
                }
            });
        }

        private static onKeyPressEventHandler($eventArgs : KeyEventArgs, $manager : GuiObjectManager,
                                              $reflection : Reflection) : void {
            const eventArgs : KeyEventArgs = new KeyEventArgs();
            eventArgs.Owner($eventArgs.Owner());
            eventArgs.NativeEventArgs($eventArgs.NativeEventArgs());
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            if (eventArgs.getKeyCode() === KeyMap.SPACE) {
                if (manager.IsActive($eventArgs.Owner())) {
                    LinkSelector.onLinkClickEventHandler($eventArgs, $manager, $reflection);
                }
            }
        }

        private static onKeyEventHandler($eventArgs? : KeyEventArgs) : void {
            if ($eventArgs.getKeyCode() === KeyMap.TAB) {
                LinkSelector.tabPressed = true;
            }
        }

        /**
         * @param {IGuiCommons} $owner Set selector's parent.
         */
        constructor($owner : IGuiCommons) {
            super();
            this.owner = $owner;
            this.enabled = true;
            this.blankTargetAllowed = false;
            this.linkEvents = new ElementEventsManager(<any>$owner, $owner.Id() + "_Link");

            this.setLinkFormatter(($value : string, $self? : LinkSelector) : string => {
                if (StringUtils.Contains($value, Loader.getInstance().getHttpManager().getRequest().getRelativeRoot())) {
                    if ($self.blankTargetAllowed) {
                        $value = Loader.getInstance().getHttpManager().getRequest().getHostUrl() + "#" + $value;
                    } else {
                        $value = "#" + $value;
                    }
                }
                return $value;
            });
        }

        /**
         * @return {string} Returns original link value before formatting.
         */
        public getValue() : string {
            return this.value;
        }

        /**
         * @param {Function} $formatter Specify link value format handler.
         * @return {void}
         */
        public setLinkFormatter($formatter : ($value : string, $self? : LinkSelector) => string) : void {
            if (!ObjectValidator.IsEmptyOrNull($formatter)) {
                this.linkFormatter = $formatter;
            }
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            this.enabled = Property.Boolean(this.enabled, $value);
            if (ObjectValidator.IsBoolean($value) && ElementManager.IsVisible(this.owner.Id())) {
                if ($value) {
                    ElementManager.Show(this.owner.Id() + "_Link");
                } else {
                    ElementManager.Hide(this.owner.Id() + "_Link");
                }
            }
            return this.enabled;
        }

        /**
         * @param {string} [$value] Set link value, which will be used for redirect in case of element click.
         * @return {string} Returns element's link value.
         */
        public ReloadTo($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                this.value = $value;
                this.reloadTo = Loader.getInstance().getHttpManager().CreateLink($value);
                if (ElementManager.IsVisible(this.owner.Id())) {
                    const element : HTMLAnchorElement = <HTMLAnchorElement>ElementManager.getElement(this.owner.Id() + "_Link");
                    if (ElementManager.Exists(element)) {
                        element.href = this.linkFormatter(this.reloadTo, this);
                        this.TabIndex(this.TabIndex());
                    }
                }
            }
            return this.reloadTo;
        }

        /**
         * @param {boolean} [$value] Specify, if reloaded content should be opened in new window.
         * @return {boolean} Returns true, if reloaded content will be opened in new window, otherwise false.
         */
        public OpenInNewWindow($value? : boolean) : boolean {
            this.blankTargetAllowed = Property.Boolean(this.blankTargetAllowed, $value);
            if (ObjectValidator.IsSet($value)) {
                if (ElementManager.IsVisible(this.owner.Id())) {
                    const element : HTMLAnchorElement = <HTMLAnchorElement>ElementManager.getElement(this.owner.Id() + "_Link");
                    if (ElementManager.Exists(element)) {
                        if (StringUtils.StartsWith(this.reloadTo, "mailto:")) {
                            element.target = "_top";
                        } else if (this.blankTargetAllowed) {
                            element.target = "_blank";
                        } else {
                            element.removeAttribute("target");
                        }
                    }
                }
            }
            return this.blankTargetAllowed;
        }

        /**
         * @param {number} [$value] Set index for TAB key navigation in the page.
         * @return {number} Returns index of element in the page based on TAB key elements register.
         */
        public TabIndex($value? : number) : number {
            if (ObjectValidator.IsSet($value)) {
                if (ObjectValidator.IsEmptyOrNull($value)) {
                    this.tabIndex = null;
                } else if (ObjectValidator.IsInteger($value)) {
                    if ($value === -1) {
                        this.tabIndex = -1;
                    } else {
                        this.tabIndex = Property.PositiveInteger(this.tabIndex, $value);
                    }
                }

                if (ElementManager.IsVisible(this.owner.Id())) {
                    const link : HTMLAnchorElement = <HTMLAnchorElement>ElementManager.getElement(this.owner.Id() + "_Link");
                    if (!this.Enabled()) {
                        link.tabIndex = -1;
                    } else if (!ObjectValidator.IsEmptyOrNull(this.TabIndex())) {
                        link.tabIndex = this.TabIndex();
                    } else {
                        link.removeAttribute("tabindex");
                    }
                }
            }
            return this.tabIndex;
        }

        /**
         * Register selector events and generate html output.
         * @return {IGuiElement} Returns selector html code.
         */
        public getInnerCode() : IGuiElement {
            this.owner.getEvents().setOnMouseOver(LinkSelector.onMouseOverEventHandler);
            this.owner.getEvents().setOnMouseOut(LinkSelector.onMouseOutEventHandler);
            this.owner.getEvents().setOnMouseDown(LinkSelector.onMouseDownEventHandler);
            this.owner.getEvents().setOnMouseUp(LinkSelector.onMouseUpEventHandler);

            this.linkEvents.setOnFocus(($eventArgs? : MouseEventArgs) : void => {
                LinkSelector.Focus($eventArgs.Owner());
            });
            this.linkEvents.setOnClick(LinkSelector.onClickEventHandler);
            this.linkEvents.setEvent(EventType.ON_KEY_PRESS, LinkSelector.onKeyPressEventHandler);

            WindowManager.getEvents().setOnKeyDown(LinkSelector.onKeyEventHandler);

            this.linkEvents.setOnBlur(
                ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    LinkSelector.Blur($reflection.getClass($eventArgs.Owner().getClassName()));
                });

            WindowManager.getEvents().setOnClick(
                ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    const elementClass : IGuiCommons = $reflection.getClass($eventArgs.Owner().getClassName());
                    LinkSelector.Blur(elementClass);
                    LinkSelector.tabPressed = false;
                    LinkSelector.focusedByTab = false;
                    LinkSelector.onBodyClickEventHandler(elementClass, $manager, $reflection);
                });

            this.owner.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                $eventArgs.Owner().getSelector().linkEvents.Subscribe();
            });

            const link : HTMLAnchorElement = document.createElement("a");
            link.id = this.owner.Id() + "_Link";
            link.href = this.linkFormatter(this.reloadTo, this);
            link.className = GeneralCssNames.GUI_SELECTOR;
            if (StringUtils.StartsWith(this.reloadTo, "mailto:")) {
                link.target = "_top";
            } else if (this.blankTargetAllowed) {
                link.target = "_blank";
            }
            if (!this.Enabled()) {
                link.tabIndex = -1;
            } else if (!ObjectValidator.IsEmptyOrNull(this.TabIndex())) {
                link.tabIndex = this.TabIndex();
            }

            const guiElementClass : any = this.owner.getGuiElementClass();
            return new guiElementClass().Add(link);
        }
    }
}
