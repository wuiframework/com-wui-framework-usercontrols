# com-wui-framework-usercontrols v2019.1.0

> Basic GUI User Controls mainly focused on the implementation of the business logic and extendability to the graphical user controls.

## Requirements

This library does not have any special requirements, but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.1.0
Responsive design enhancements. Fixed Chromium performance issues. Enable to set base64 encoded image source.
### v2019.0.2
Bug fix for text area hint positioning. Usage of new app loader.
### v2019.0.1
Fixed active state for dialogs. Fixed enablement of blur for not focused elements. Added ability to disable resize bars.
### v2019.0.0
Fixed responsive design for DropDownList, ScrollBar init and TextArea. Added better control over DropDownList and TextArea disable mode 
and ResizeBar cursor. Added more Dialog events.
### v2018.3.0
Updates based on usage in WUI Builder. Conversion of String to StringUtils. Updated tests. User controls performance enhancements.
### v2018.2.0
Updates required by better code sharing at back-end projects. Refactoring of Accordion for better flexibility, 
performance and extensibility. Performance updates mostly focused on responsive design needs. 
Added new runtime tests focused on automated functional testing of user controls. Convert of docs to TypeDoc.
### v2018.1.3
Added initial implementation of functional tests.
### v2018.1.2
Fixed resize of Image loader. Test clean up.
### v2018.1.1
Fixed tests connected with updated core. API Clean up. Fixed handling of scroll propagation. Bug fixes for DropDownList and TextArea.
### v2018.1.0
Update of WUI Core for ability to support multithreading model. API clean up.
### v2018.0.4
Stability bug fixes for DropDownList and NumberPicker. Added ability to modify readonly mode for TextField on the fly.
### v2018.0.3
Added ability to control scrollbar size animation. Fixed stability bug for number picker notification.
### v2018.0.2
Stability bug fixes for content scrolling. Code coverage increase.
### v2018.0.1
RuntimeTests for Components and UserControls converted to UnitTests. Fixed linting issues.
### v2018.0.0
Unit tests clean up. Minor UI bug fixes. Change version format.
### v2.2.1
Updated dialog autorezise and autocenter behavior.
### v2.2.0
Bug fixes for NumberPicker and DirectoryBrowser. Update of SCR and change of history ordering.
### v2.1.1
Added ability to disable usage of HTML5 canvas.
### v2.1.0
Update of Commons and GUI libraries.
### v2.0.3
Added fixed version for dependencies.
### v2.0.2
Update of unit tests required for compatibility with jsdom.
### v2.0.1
Update of TypeScript syntax. Modification of DirectoryBrowser for better support of updated back-end.
### v2.0.0
Namespaces refactoring.
### v1.1.0
Changed the licence from proprietary to BSD-3-Clause.
### v1.0.4
Usage of new version compiler features—mainly the protected, lambda expression, and multi-type arguments.
### v1.0.3
Usage of a stand-alone builder.
### v1.0.2
Package renamed from com-freescale-mvc-gui-baseusercontrols to com-freescale-wui-usercontrols and minor bug fixes.
### v1.0.1
Enhancements of all general Primitives, Components, and UserControls for an easy extension to the graphical UserControls libraries.
### v1.0.0
Initial release.

## License

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
