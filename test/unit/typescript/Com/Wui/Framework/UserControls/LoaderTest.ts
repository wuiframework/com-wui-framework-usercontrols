/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls {
    "use strict";

    export class LoaderTest extends UnitTestRunner {

        public testLoad() : void {
            UnitTestLoader.Load(<any>{
                build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-usercontrols", version: "1.0.0"
            });
            assert.equal(this.getEnvironmentArgs().getProjectName(), "com-wui-framework-usercontrols");
            this.initSendBox();
        }
    }
}
