/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Events {
    "use strict";

    export class EventsManagerTest extends UnitTestRunner {

        public testisOnLoadKey() : void {
            const event : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true};
            assert.equal((<any>EventsManager.getInstanceSingleton()).isOnLoadKey(event), false);
        }

        public testisElementMovedAllowed() : void {
            assert.equal((<any>EventsManager.getInstanceSingleton()).isElementMoveAllowed(), true);
        }
    }
}
