/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;

    class MockHolder extends BasePanelHolderViewer {
        public getInstance() : MockBasePanelHolder {
            return <MockBasePanelHolder>super.getInstance();
        }
    }

    class MockHolderViewerArgs extends BasePanelHolderViewerArgs {
    }

    class MockBasePanelViewer extends BasePanelViewer {
    }

    class MockBasePanelViewerArgs extends BasePanelViewerArgs {
    }

    class MockBasePanelHolder extends BasePanelHolder {
        private basepanelviewer : MockBasePanelViewer;
        private basepanelviewerArgs : MockBasePanelViewerArgs;
        private test : string = "";
        private id : string = "";

        constructor($basepanelviewer : MockBasePanelViewer, $basepanelviewerArgs : MockBasePanelViewerArgs, $test = "TestHolderType",
                    $id = "id666") {
            super(new MockBasePanelViewer());
            this.basepanelviewerArgs = $basepanelviewerArgs;
            this.basepanelviewer = $basepanelviewer;
            this.test = $test;
            this.id = $id;
        }
    }

    export class BasePanelHolderViewerTest extends UnitTestRunner {
        public testgetInstance() : void {
            const args : MockHolderViewerArgs = new MockHolderViewerArgs();
            const holderViewer : MockHolder = new MockHolder(args);
            assert.equal(holderViewer.getInstance().getClassName(), "Com.Wui.Framework.UserControls.Primitives.BasePanelHolder");
            Echo.PrintCode(JSON.stringify(holderViewer.getInstance().Draw()));
        }

        public testViewerArgs() : void {
            const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            args.DescriptionText("testViewerArgs");
            args.HeaderText("includeHeader");
            const holderViewer : BasePanelHolderViewer = new BasePanelHolderViewer();
            assert.equal(holderViewer.ViewerArgs(args), args);
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
