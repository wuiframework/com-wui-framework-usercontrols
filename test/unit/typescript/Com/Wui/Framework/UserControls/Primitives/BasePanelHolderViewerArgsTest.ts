/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;

    class MockBasePanelHolder extends BasePanelHolder {
        constructor($bodyClass : BasePanelViewer, $args? : BasePanelViewerArgs, $holderType? : any, $id? : string) {
            super($bodyClass, $args, $holderType, $id);
        }
    }

    export class BasePanelHolderViewerArgsTest extends UnitTestRunner {
        public testHeaderText() : void {
            const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            assert.equal(args.HeaderText("Lighthouse"), "Lighthouse");
        }

        public testDescriptionText() : void {
            const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            assert.equal(args.DescriptionText("Describe yourself here..."), "Describe yourself here...");
        }

        public testIsOpened() : void {
            const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            assert.equal(args.IsOpened(), true);
        }

        public testBodyArgs() : void {
            const bodyargs : BasePanelViewerArgs = new BasePanelViewerArgs();
            bodyargs.Visible(true);
            const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            assert.equal(args.BodyArgs(bodyargs), bodyargs);
        }

        public testHolderViewerClass() : void {
            const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            const viewer : BasePanelViewer = new BasePanelViewer();
            const holder : BasePanelHolder = new MockBasePanelHolder(viewer);
            assert.equal(args.HolderViewerClass(holder), holder);
            this.initSendBox();
        }

        public testBodyViewerClass() : void {
            const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            const viewer : BasePanelViewer = new BasePanelViewer();
            assert.equal(args.BodyViewerClass(viewer), viewer);
        }
    }
}
