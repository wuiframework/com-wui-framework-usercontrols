/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    class MockBasePanelHolder extends BasePanelHolder {
        constructor($bodyClass : BasePanelViewer, $args? : BasePanelViewerArgs, $holderType? : any, $id? : string) {
            super($bodyClass, $args, $holderType, $id);
        }
    }

    export class BasePanelHolderTest extends UnitTestRunner {
        public testgetEvents() : void {
            const basepanelviewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            basepanelviewerArgs.AsyncEnabled(true);
            basepanelviewerArgs.Visible(true);
            const basepanelViewer : BasePanelViewer = new BasePanelViewer();
            const baseholder : BasePanelHolder = new MockBasePanelHolder(basepanelViewer, basepanelviewerArgs);

            const handler : any = () : void => {
                // test event handler
            };
            baseholder.getEvents().setEvent("test", handler);
            assert.equal(baseholder.getEvents().Exists("test"), true);
        }

        public testGuiType() : void {
            Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" >Multiple</div>");
            const basepanelviewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            const basepanelViewer : BasePanelViewer = new BasePanelViewer();
            const baseholder : BasePanelHolder = new MockBasePanelHolder(basepanelViewer, basepanelviewerArgs);
            baseholder.GuiType("GuiWraper");
        }

        public testIsOpened() : void {
            const basepanelviewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            const basepanelViewer : BasePanelViewer = new BasePanelViewer();
            const baseholder : BasePanelHolder = new MockBasePanelHolder(basepanelViewer, basepanelviewerArgs);
            assert.equal(baseholder.IsOpened(false), false);

            const basepanelviewerArgs2 : BasePanelViewerArgs = new BasePanelViewerArgs();
            const basepanelViewer2 : BasePanelViewer = new BasePanelViewer();
            const baseholder2 : BasePanelHolder = new MockBasePanelHolder(basepanelViewer2, basepanelviewerArgs2);
            assert.equal(baseholder2.IsOpened(true), true);
        }

        public testgetBody() : void {
            const basepanelviewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            const basepanelViewer : BasePanelViewer = new BasePanelViewer();
            const baseholder : BasePanelHolder = new MockBasePanelHolder(basepanelViewer, basepanelviewerArgs);
            assert.equal(baseholder.getBody(), null);
        }

        public testgetHeaderEvents() : void {
            const basepanelviewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            const basepanelViewer : BasePanelViewer = new BasePanelViewer();
            const baseholder : BasePanelHolder = new MockBasePanelHolder(basepanelViewer, basepanelviewerArgs);

            const handler : any = () : void => {
                // test event handler
            };
            baseholder.getHeaderEvents().setEvent("test", handler);
            assert.equal(baseholder.getHeaderEvents().Exists("test"), true);
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
