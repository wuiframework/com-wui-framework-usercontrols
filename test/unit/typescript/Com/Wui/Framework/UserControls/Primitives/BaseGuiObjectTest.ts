/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    class MockBaseGuiObject extends BaseGuiObject {
    }

    export class BaseGuiObjectTest extends UnitTestRunner {

        public testBaseGuiObject() : void {
            const basegui : BaseGuiObject = new MockBaseGuiObject("id103");
            basegui.StyleClassName("testStyleClass");
            basegui.DisableAsynchronousDraw();
            assert.equal(basegui.Draw(),
                "\r\n<div class=\"ComWuiFrameworkUserControlsPrimitives\">" +
                "\r\n   <div id=\"id103_GuiWrapper\" guiType=\"GuiWrapper\" class=\"testStyleClass\">" +
                "\r\n      <div id=\"id103\" class=\"BaseGuiObject\" style=\"display: block;\"></div>" +
                "\r\n   </div>" +
                "\r\n</div>");
            this.initSendBox();
        }
    }
}
