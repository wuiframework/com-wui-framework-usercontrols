/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;

    class MockFormsObject extends FormsObject {
    }

    export class FormsObjectTest extends UnitTestRunner {
        public testTurnOn() : void {
            const form : FormsObject = new MockFormsObject();
            const guimanager : GuiObjectManager = new GuiObjectManager();
            const reflection : Reflection = new Reflection();
            FormsObject.TurnOn(form, guimanager, reflection);
            assert.equal(guimanager.IsActive(FormsObject), false);
        }

        public testBlur() : void {
            const form : FormsObject = new MockFormsObject();
            FormsObject.Blur();
            assert.equal(form.Error(), false);
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
