/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Primitives {
    "use strict";
    import ResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeEventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;

    export class BasePanelTest extends UnitTestRunner {
        public testResizeEventHandler() : void {
            const panel : BasePanel = new BasePanel();
            const args : ResizeEventArgs = new ResizeEventArgs();
            args.Owner(panel);
            args.Height(40);
            args.Width(20);
            args.AvailableHeight(80);
            args.AvailableWidth(40);
            const manager : GuiObjectManager = new GuiObjectManager();
            const reflection : Reflection = new Reflection();
            BasePanel.ResizeEventHandler(args, manager, reflection);
            assert.equal(panel.Width(), 40);
            assert.equal(panel.Height(), 80);
        }

        public testWidth() : void {
            const panel : BasePanel = new BasePanel();
            assert.equal(panel.Width(250), 250);
        }

        public testHeight() : void {
            const panel : BasePanel = new BasePanel();
            assert.equal(panel.Height(500), 500);
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
