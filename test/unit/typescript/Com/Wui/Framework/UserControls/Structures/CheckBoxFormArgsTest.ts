/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Structures {
    "use strict";

    export class CheckBoxFormArgsTest extends UnitTestRunner {

        public testValue() : void {
            const checkbox : CheckBoxFormArgs = new CheckBoxFormArgs();
            assert.equal(checkbox.Value(true), true);
            const checkbox2 : CheckBoxFormArgs = new CheckBoxFormArgs();
            assert.equal(checkbox2.Value(), false);
        }
    }
}
