/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Structures {
    "use strict";

    export class AutocompleteOptionTest extends UnitTestRunner {
        public testConstructor() : void {
            const autocomplete : AutocompleteOption = new AutocompleteOption("textOption", 20, "test", "AutocompleteOption");
            assert.equal(autocomplete.Text(), "textOption");
        }

        public testValue() : void {
            const autocomplete : AutocompleteOption = new AutocompleteOption("textOption", 20, "test", "AutocompleteOption");
            assert.equal(autocomplete.Value(), 20);
        }

        public testLookupData() : void {
            const autocomplete : AutocompleteOption = new AutocompleteOption("textOption", 20, null, "AutocompleteOption");
            assert.equal(autocomplete.LookupData(), "textOption");
            const autocomplete2 : AutocompleteOption = new AutocompleteOption("textOption");
            assert.equal(autocomplete2.LookupData(), "textOption");
        }

        public testStyleClassName() : void {
            const autocomplete : AutocompleteOption = new AutocompleteOption("textOption", 20, "test", "AutocompleteOption");
            assert.equal(autocomplete.StyleClassName(), "AutocompleteOption");
        }
    }
}
