/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Structures {
    "use strict";

    export class TextFieldFormArgsTest extends UnitTestRunner {

        public testValue() : void {
            const textfield : TextFieldFormArgs = new TextFieldFormArgs();
            assert.equal(textfield.Value("this is content of TextField"), "this is content of TextField");
            const textfield2 : TextFieldFormArgs = new TextFieldFormArgs();
            assert.equal(textfield2.Value(230), 230);
            const textfield3 : TextFieldFormArgs = new TextFieldFormArgs();
            assert.equal(textfield3.Value(null), null);
        }

        public testReadOnly() : void {
            const textfield : TextFieldFormArgs = new TextFieldFormArgs();
            assert.equal(textfield.ReadOnly(), false);
            const textfield2 : TextFieldFormArgs = new TextFieldFormArgs();
            assert.equal(textfield2.ReadOnly(true), true);
        }

        public testIntegerOnly() : void {
            const textfield : TextFieldFormArgs = new TextFieldFormArgs();
            assert.equal(textfield.IntegerOnly(), false);
            const textfield2 : TextFieldFormArgs = new TextFieldFormArgs();
            assert.equal(textfield2.IntegerOnly(true), true);
        }

        public testPasswordEnabled() : void {
            const textfield : TextFieldFormArgs = new TextFieldFormArgs();
            assert.equal(textfield.PasswordEnabled(), false);
            const textfield2 : TextFieldFormArgs = new TextFieldFormArgs();
            assert.equal(textfield2.PasswordEnabled(true), true);
        }

        public testSize() : void {
            const textfield : TextFieldFormArgs = new TextFieldFormArgs();
            textfield.Value("test1");
            assert.equal(textfield.Size(), -1);
        }
    }
}
