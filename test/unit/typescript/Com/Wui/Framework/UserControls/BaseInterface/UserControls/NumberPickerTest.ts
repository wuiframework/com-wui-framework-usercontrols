/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import NumberPickerType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.NumberPickerType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import NotificationType = Com.Wui.Framework.UserControls.BaseInterface.Enums.Components.NotificationType;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;

    class MockBasePanelViewer extends BasePanelViewer {
    }

    class MockViewer extends BaseViewer {
    }

    export class NumberPickerTest extends UnitTestRunner {

        public testConstructor() : void {
            const numberpicker : NumberPicker = new NumberPicker(NumberPickerType.BLUE, "id1");
            assert.equal(numberpicker.Id(), "id1");
            const numPicker : NumberPicker = new NumberPicker(NumberPickerType.BLUE, "id50");
            assert.equal(numPicker.Draw(),
                "\r\n<div class=\"ComWuiFrameworkUserControlsBaseInterfaceUserControls\">" +
                "\r\n   <div id=\"id50_GuiWrapper\" guiType=\"GuiWrapper\">" +
                "\r\n      <div id=\"id50\" class=\"NumberPicker\" style=\"display: none;\"></div>" +
                "\r\n   </div>" +
                "\r\n</div>");
        }

        public testGuiType() : void {
            const numberpicker : NumberPicker = new NumberPicker();
            assert.equal(numberpicker.GuiType(NumberPickerType.GREEN), NumberPickerType.GREEN);
            const numberpicker2 : NumberPicker = new NumberPicker();
            assert.equal(numberpicker2.GuiType(), "");
            const numberpicker3 : NumberPicker = new NumberPicker(NumberPickerType.BLUE);
            numberpicker3.Visible(true);
            assert.equal(numberpicker3.GuiType(), "Blue");
        }

        public __IgnoretestValueAsync() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const numberpicker : NumberPicker = new NumberPicker(NumberPickerType.GENERAL);
                const manager : GuiObjectManager = new GuiObjectManager();
                const viewer : BaseViewer = new MockViewer();
                manager.Add(numberpicker);
                numberpicker.InstanceOwner(viewer);
                assert.onGuiComplete(numberpicker,
                    () : void => {
                        manager.setActive(numberpicker, true);
                        numberpicker.Visible(true);
                        numberpicker.Enabled(true);
                        numberpicker.Value(80);
                        numberpicker.ValueStep(20);
                    },
                    () : void => {
                        assert.equal(numberpicker.Value(), 80);
                        assert.equal(numberpicker.Enabled(), true);
                        assert.equal(numberpicker.Value(), 80);
                        assert.equal(numberpicker.ValueStep(), 20);
                        assert.equal(numberpicker.IsPreventingScroll(), true);
                        this.initSendBox();
                        $done();
                    });
            };
        }

        public testEnabled() : void {
            const numberpicker : NumberPicker = new NumberPicker(NumberPickerType.GENERAL);
            assert.equal(numberpicker.Enabled(), true);
            assert.equal(numberpicker.Enabled(false), false);
            const numberpicker2 : NumberPicker = new NumberPicker(NumberPickerType.BLUE);
            numberpicker2.Visible(true);
        }

        public testValue() : void {
            const numberpicker : NumberPicker = new NumberPicker(NumberPickerType.RED);
            assert.equal(numberpicker.Value(), 0);
            numberpicker.Visible(true);
            numberpicker.RangeStart(60);
            assert.equal(numberpicker.Value(), numberpicker.Value(60));
        }

        public testValueStep() : void {
            const numberpicker : NumberPicker = new NumberPicker();
            assert.equal(numberpicker.ValueStep(), -1);
            assert.equal(numberpicker.ValueStep(2), 2);
        }

        public testRangeStart() : void {
            const numberpicker : NumberPicker = new NumberPicker();
            assert.equal(numberpicker.RangeStart(), 0);
            assert.equal(numberpicker.RangeStart(5), 5);
            numberpicker.RangeEnd(3);
            assert.equal(numberpicker.RangeStart(5), numberpicker.RangeEnd(5));
            assert.equal(numberpicker.RangeStart(150), numberpicker.RangeEnd(150));
        }

        public testRangeEnd() : void {
            const numberpicker : NumberPicker = new NumberPicker();
            assert.equal(numberpicker.RangeEnd(), 100);
            assert.equal(numberpicker.RangeEnd(2), 2);
            assert.equal(numberpicker.RangeEnd(200), 200);
        }

        public testWidth() : void {
            const numberpicker : NumberPicker = new NumberPicker(NumberPickerType.BLUE, "id11");
            assert.equal(numberpicker.Width(), 250);
            assert.equal(numberpicker.Width(50), 50);
            numberpicker.Visible(true);
            ElementManager.setInnerHtml("id11", "Text");
            assert.equal(numberpicker.Width(300), 300);
        }

        public testText() : void {
            const numberpicker : NumberPicker = new NumberPicker(NumberPickerType.RED, "id4");
            assert.equal(numberpicker.Text(), "<span id=\"id4_TextValue\"></span>");
            assert.equal(numberpicker.Text("TestOfNumberPicker"), "TestOfNumberPicker");
        }

        public testDecimalPlaces() : void {
            const numberpicker : NumberPicker = new NumberPicker(NumberPickerType.GENERAL);
            assert.equal(numberpicker.DecimalPlaces(), 0);
            assert.equal(numberpicker.DecimalPlaces(1000), 1000);
        }

        public testNotificationShow() : void {
            const numPicker : NumberPicker = new NumberPicker(NumberPickerType.BLUE);
            numPicker.Notification().StyleClassName("style");
            numPicker.Notification().GuiType(NotificationType.INFO);
            numPicker.Notification().Width(800);

            numPicker.DisableAsynchronousDraw();
            Echo.Print(numPicker.Draw());
            assert.equal(ElementManager.IsVisible(numPicker.Notification().Id() + "_NotificationEnvelop"), false);

            numPicker.Notification().DisableAsynchronousDraw();
            Echo.Print(numPicker.Notification().Draw());
            NumberPicker.NotificationShow(numPicker);

            assert.equal(ElementManager.IsVisible(numPicker.Notification().Id() + "_NotificationEnvelop"), false);
        }

        public __IgnoretestNotificationHide() : void {
            const numPicker : NumberPicker = new NumberPicker(NumberPickerType.GREEN, "id70");
            numPicker.Notification().StyleClassName("style");
            numPicker.Notification().GuiType(NotificationType.INFO);
            numPicker.Notification().Width(600);

            numPicker.DisableAsynchronousDraw();
            Echo.Print(numPicker.Draw());
            assert.equal(numPicker.Draw(),
                "\r\n<div class=\"ComWuiFrameworkUserControlsBaseInterfaceUserControls\">\r\n" +
                "   <div id=\"id70_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                "      <div id=\"id70\" class=\"NumberPicker\" style=\"display: block;\">\r\n" +
                "         <div id=\"id70_Type\" class=\"Green\">\r\n" +
                "            <div id=\"id70_Status\" guiType=\"NumberPicker\">\r\n" +
                "               <input type=\"text\" id=\"id70_Input\" name=\"id70\" class=\"GuiSelector\">\r\n" +
                "               <div id=\"id70_Notification\" class=\"Notification\">\r\n" +
                "                  <div class=\"Pointer\">\r\n" +
                "                     <div id=\"id70_Pointer\" class=\"Off\"></div>\r\n" +
                "                  </div>\r\n" +
                "                  <div id=\"id70_NotificationFixedHolder\" class=\"GuiFixedHolder\">\r\n" +
                "                     <div id=\"id70_NotificationEnvelop\" class=\"Background\" style=\"display: none;\">\r\n" +
                "                        <div id=\"id70_NotificationLeft\" class=\"Left\"></div>\r\n" +
                "                        <div id=\"id70_NotificationCenter\" class=\"Center\"></div>\r\n" +
                "                           <div id=\"id70_NotificationRight\" class=\"Right\"></div>\r\n" +
                "                        <div id=\"id70_NotificationArrow\" class=\"Arrow\"></div>\r\n" +
                "                        <div id=\"id70_Text\" class=\"Text\"><span id=\"id70_TextValue\"></span></div>\r\n" +
                "                     </div>\r\n" +
                "                  </div>\r\n" +
                "               </div>\r\n" +
                "               <div id=\"id70_Tracker\" class=\"Background\">\r\n" +
                "                  <div id=\"id70_Left\" class=\"Left\"></div>\r\n" +
                "                  <div id=\"id70_Center\" class=\"Center\" style=\"width: 225px;\">\r\n" +
                "                     <div id=\"id70_Bar\" class=\"Bar\">\r\n" +
                "                        <div id=\"id70_BarEnvelop\" class=\"Envelop\">\r\n" +
                "                           <div id=\"id70_BarLeft\" class=\"Left\"></div>\r\n" +
                "                           <div id=\"id70_BarCenter\" class=\"Center\"></div>\r\n" +
                "                           <div id=\"id70_BarRight\" class=\"Right\"></div>\r\n" +
                "                        </div>\r\n" +
                "                     </div>\r\n" +
                "                  </div>\r\n" +
                "                  <div id=\"id70_Right\" class=\"Right\"></div>\r\n" +
                "               </div>\r\n" +
                "            </div>\r\n" +
                "         </div>\r\n" +
                "      </div>\r\n" +
                "   </div>\r\n" +
                "</div>"
            );

            numPicker.Notification().DisableAsynchronousDraw();
            Echo.Print(numPicker.Notification().Draw());

            NumberPicker.NotificationHide();
            assert.equal(numPicker.Notification().Draw(), "");

        }

        public testPointerOn() : void {
            const numPicker : NumberPicker = new NumberPicker(NumberPickerType.GENERAL);
            numPicker.DisableAsynchronousDraw();
            Echo.Print(numPicker.Draw());
            NumberPicker.PointerOn(numPicker);
            assert.equal(ElementManager.getClassName(numPicker.Id() + "_Pointer"), GeneralCssNames.ON);
        }

        public testPointerOff() : void {
            const numPicker : NumberPicker = new NumberPicker(NumberPickerType.GENERAL);
            numPicker.DisableAsynchronousDraw();
            Echo.Print(numPicker.Draw());
            NumberPicker.PointerOff(numPicker);
            assert.equal(ElementManager.getClassName(numPicker.Id() + "_Pointer"), GeneralCssNames.OFF);
        }

        public testFocus() : void {
            const numPicker : NumberPicker = new NumberPicker(NumberPickerType.GENERAL);
            numPicker.DisableAsynchronousDraw();
            Echo.Print(numPicker.Draw());
            NumberPicker.Focus(numPicker);
            assert.equal(ElementManager.getClassName(numPicker.Id() + "_Pointer"), GeneralCssNames.ACTIVE);
        }

        public __IgnoretestBlur() : void {
            const numPicker : NumberPicker = new NumberPicker(NumberPickerType.GENERAL);
            numPicker.DisableAsynchronousDraw();
            Echo.Print(numPicker.Draw());
            NumberPicker.Blur();
            assert.equal(ElementManager.getClassName(numPicker.Id() + "_Pointer"), GeneralCssNames.OFF);
        }

        public testEvent() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const numberpicker : NumberPicker = new NumberPicker(NumberPickerType.GENERAL, "id8");
                const viewer : BasePanelViewer = new MockBasePanelViewer();
                numberpicker.InstanceOwner(viewer);
                const args : MoveEventArgs = new MoveEventArgs();
                args.getDistanceX();
                args.getDistanceY();
                args.getDistanceX();
                args.getDistanceY();
                assert.equal(numberpicker.getEvents().Exists(EventType.ON_MOUSE_MOVE), false);
                numberpicker.InstanceOwner(viewer);
                numberpicker.Text("Picker");
                numberpicker.RangeStart(0);
                numberpicker.RangeEnd(10);
                numberpicker.ValueStep(1);
                numberpicker.Visible(true);
                numberpicker.Enabled(true);
                numberpicker.DisableAsynchronousDraw();
                StaticPageContentManager.BodyAppend(numberpicker.Draw());
                Echo.Print(numberpicker.Draw());

                EventsManager.getInstanceSingleton().setEvent(numberpicker, EventType.ON_MOUSE_MOVE,
                    ($eventArgs : MoveEventArgs) : void => {
                        LogIt.Debug("this is event OnMouseMove");
                        assert.equal($eventArgs.Owner(), numberpicker);
                        assert.equal(numberpicker.getEvents().Exists(EventType.ON_MOUSE_MOVE), true);
                    });
                EventsManager.getInstanceSingleton().FireEvent(numberpicker, EventType.ON_MOUSE_MOVE, args, true);
                this.initSendBox();
                $done();
            };
        }

        public __IgnoretestEventHide() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const numberpicker : NumberPicker = new NumberPicker(NumberPickerType.GENERAL, "id8");
                const viewer : BasePanelViewer = new MockBasePanelViewer();
                const manager : GuiObjectManager = new GuiObjectManager();
                numberpicker.InstanceOwner(viewer);
                manager.Add(numberpicker);
                //    const args : MoveEventArgs = new MoveEventArgs();
                //    args.getDistanceX();
                //    args.getDistanceY();
                //    args.getDistanceX();
                //    args.getDistanceY();
                assert.onGuiComplete(numberpicker.Notification(),
                    () : void => {
                        numberpicker.Text("Picker");
                        //    numberpicker.RangeStart(0);
                        //    numberpicker.RangeEnd(10);
                        //    numberpicker.ValueStep(1);
                        //    numberpicker.Visible(true);
                        numberpicker.Enabled(true);

                        EventsManager.getInstanceSingleton().setEvent(numberpicker, EventType.ON_HIDE,
                            ($eventArgs : EventArgs) : void => {
                                NumberPicker.NotificationHide();
                                //  assert.equal(numberpicker.Notification().getEvents().Exists(EventType.ON_HIDE), true);
                            });
                        //    EventsManager.getInstanceSingleton().FireEvent(numberpicker, EventType.ON_HIDE, args, true);
                    },
                    () : void => {
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
