/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    class MockBasePanelViewer extends BasePanelViewer {
    }

    class MockLabel extends Label {
        public testExcludeCacheData() : string[] {
            return this.excludeCacheData();
        }
    }

    export class LabelTest extends UnitTestRunner {
        public testConstructor() : void {
            const label : Label = new Label("testOfLabel");
            assert.equal(label.Text(), "testOfLabel");
        }

        public testgetEvents() : void {
            const label : Label = new Label("Label");

            const handler : any = () : void => {
                // test event handler
            };
            label.getEvents().setEvent("test", handler);
            assert.equal(label.getEvents().Exists("test"), true);
        }

        public testText() : void {
            const label : Label = new Label("Label");
            (<any>GuiCommons).loaded = true;
            assert.equal(label.Text("ContentLabelElement"), "ContentLabelElement");
        }

        public testValue() : void {
            const label : Label = new Label();
            const value : any = () : void => {
                const text : string = "Hello!!!";
            };
            assert.equal(label.Value("value"), null);
        }

        public testEnabled() : void {
            const label : Label = new Label("test");
            label.getGuiOptions().Add(GuiOptionType.DISABLE);
            label.Visible(true);
            assert.equal(label.Enabled(true), true);
            const label2 : Label = new Label();
            assert.equal(label2.Enabled(false), false);
        }

        public testgetArgs() : void {
            const label : Label = new Label();
            label.setArg(<IGuiCommonsArg>{
                name : "Labeltest",
                type : GuiCommonsArgType.TEXT,
                value: "Content"
            }, true);
            assert.equal(label.getArgs().length, 10);
        }

        public __IgnoretestLabelEvent() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const label : Label = new Label("354");
                const viewer : BasePanelViewer = new MockBasePanelViewer();
                label.InstanceOwner(viewer);
                label.Visible(true);
                label.Enabled(true);
                const args : MouseEventArgs = new MouseEventArgs();
                assert.equal(label.getEvents().Exists(EventType.ON_CHANGE), false);
                label.DisableAsynchronousDraw();
                Echo.Print(label.Draw());
                assert.onGuiComplete(label,
                    () : void => {
                        label.getEvents().setOnChange(() : void => {
                            label.Changed();
                            assert.equal(label.getEvents().Exists(EventType.ON_CHANGE), true);
                            assert.equal(label.Changed(), true);
                        });
                    },
                    () : void => {
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }

        public __IgnoretestLabelEventSecond() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const label : Label = new Label("354");
                const viewer : BasePanelViewer = new MockBasePanelViewer();
                label.InstanceOwner(viewer);
                label.Visible(true);
                label.Enabled(true);
                const args : MouseEventArgs = new MouseEventArgs();
                assert.equal(label.getEvents().Exists(EventType.ON_LOAD), false);

                assert.onGuiComplete(label,
                    () : void => {
                        label.getEvents().setOnLoad(() : void => {
                            label.Changed();
                            label.Text("ChangeText");
                            assert.equal(label.getEvents().Exists(EventType.ON_LOAD), true);
                            assert.equal(label.Changed(), true);
                        });
                    },
                    () : void => {
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }

        public __IgnoretestLabelEventThird() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const label : Label = new Label("Name:");
                const label2 : Label = new Label("Last name:");
                const viewer : BasePanelViewer = new MockBasePanelViewer();
                label.InstanceOwner(viewer);
                label.Visible(true);
                label.Enabled(true);
                label2.Value("value");
                label.DisableAsynchronousDraw();
                Echo.Print(label.Draw());
                assert.equal(label.Value(), "value");
                assert.equal(label.getEvents().Exists(EventType.ON_CHANGE), false);
                this.initSendBox();
                $done();
            };
        }

        public testexcludeCacheData() : void {
            const label : MockLabel = new MockLabel();
            assert.deepEqual(label.testExcludeCacheData(), [
                "options", "availableOptionsList", "events",
                "childElements", "waitFor", "cached", "prepared", "completed",
                "parent", "owner", "guiPath", "interfaceClassName",
                "styleClassName", "containerClassName", "innerHtmlMap", "loaded",
                "title", "changed", "text"
            ]);
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
