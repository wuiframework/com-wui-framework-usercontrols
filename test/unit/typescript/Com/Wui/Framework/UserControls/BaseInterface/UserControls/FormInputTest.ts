/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import BaseFormInputArgs = Com.Wui.Framework.UserControls.Structures.BaseFormInputArgs;
    import BaseGuiGroupObjectArgs = Com.Wui.Framework.Gui.Structures.BaseGuiGroupObjectArgs;
    import TextFieldFormArgs = Com.Wui.Framework.UserControls.Structures.TextFieldFormArgs;

    class MockBaseGuiGroupObjectArgs extends BaseGuiGroupObjectArgs {
    }

    class MockBaseFormInputArgs extends BaseFormInputArgs {
    }

    export class FormInputTest extends UnitTestRunner {
        public testConfiguration() : void {
            const baseguigrupArgs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            const baseinpuArgs : BaseFormInputArgs = new MockBaseFormInputArgs();
            const forminput : FormInput = new FormInput(baseguigrupArgs);
            assert.equal(forminput.Configuration(baseinpuArgs), baseinpuArgs);
        }

        public testgetWidth() : void {
            const baseguigrupArgs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            const forminput : FormInput = new FormInput(baseguigrupArgs);
            assert.equal(forminput.getWidth(), -1);
        }

        public testValue() : void {
            const baseguigrupArgs : TextFieldFormArgs = new TextFieldFormArgs();
            const forminput : FormInput = new FormInput(baseguigrupArgs);
            assert.equal(forminput.Value(10), 10);
            assert.equal(forminput.Value(), 10);
        }

        public testClear() : void {
            const baseguigrupArgs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            const forminput : FormInput = new FormInput(baseguigrupArgs, "id135");
            forminput.Value("test");
            forminput.Clear();
            assert.equal(forminput.Value(), null);
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
