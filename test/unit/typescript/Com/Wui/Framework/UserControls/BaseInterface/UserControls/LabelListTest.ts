/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;
    import IconType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.IconType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;

    class MockViewer extends BaseViewer {
    }

    class MockLabelList extends LabelList {
        public testserializationData() : string[] {
            return this.excludeSerializationData();
        }

        public testcacheData() : string[] {
            return this.excludeCacheData();
        }
    }

    export class LabelListTest extends UnitTestRunner {

        public testConstructor() : void {
            const labellist : LabelList = new LabelList(IconType.BLUE_SQUARE, "id14");
            assert.equal(labellist.Id(), "id14");
        }

        public testIconName() : void {
            const labellist : LabelList = new LabelList(IconType.SPINNER_BIG);
            assert.equal(labellist.IconName(), IconType.SPINNER_BIG);
            const labellist2 : LabelList = new LabelList();
            assert.equal(labellist2.IconName(IconType.GENERAL), IconType.GENERAL);
        }

        public testAdd() : void {
            const label : Label = new Label("test", "id16");
            const label2 : Label = new Label("label", "id17");
            const labellist : LabelList = new LabelList(IconType.RED_SQUARE, "id18");
            assert.equal(labellist.Add(label), true);
            assert.equal(labellist.Add(label2), true);
            assert.equal(labellist.Add("id30"), true);
            assert.equal(labellist.Add(null), false);
        }

        public testgetItem() : void {
            const label : Label = new Label("test");
            const label2 : Label = new Label("label");
            const labellist : LabelList = new LabelList(IconType.RED_SQUARE);
            assert.equal(labellist.Add(label), true);
            assert.equal(labellist.Add(label2), true);
            assert.equal(labellist.getItem(1), label2);
        }

        public __IgnoretestAsync() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const labellist : LabelList = new LabelList(IconType.GENERAL);
                const viewer : BaseViewer = new MockViewer();
                labellist.InstanceOwner(viewer);
                const label1 : Label = new Label();
                const label2 : Label = new Label();
                assert.onGuiComplete(labellist,
                    () : void => {
                        labellist.Add(label1);
                        labellist.Add(label2);
                        labellist.Visible(true);
                        labellist.Enabled(true);
                    },
                    () : void => {
                        assert.equal(labellist.getItem(1), label2);
                        assert.equal(labellist.Enabled(), true);
                        assert.equal(labellist.IconName(), IconType.GENERAL);
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }

        public __IgnoretestAsyncNext() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const labellist : LabelList = new LabelList(IconType.GENERAL);
                const viewer : BaseViewer = new MockViewer();
                labellist.InstanceOwner(viewer);
                const label1 : Label = new Label();
                const label2 : Label = new Label();
                assert.onGuiComplete(labellist,
                    () : void => {
                        labellist.Add(label1);
                        labellist.Add(label2);
                        labellist.Visible(true);
                        labellist.Enabled(false);

                        labellist.getEvents().setOnLoad(($eventArgs : EventArgs) : void => {
                            $eventArgs.StopAllPropagation();
                        });
                    },
                    () : void => {
                        assert.equal(labellist.getItem(1), label2);
                        assert.equal(labellist.Enabled(), false);
                        assert.equal(labellist.IconName(), IconType.GENERAL);
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }

        public testClear() : void {
            const label : Label = new Label("test");
            const label2 : Label = new Label("label");
            const labellist : LabelList = new LabelList(IconType.RED_SQUARE);
            assert.equal(labellist.Add(label), true);
            assert.equal(labellist.Add(label2), true);
            assert.equal(labellist.getItem(1), label2);
            labellist.Clear();
            assert.equal(labellist.getItem(1), null);
        }

        public testEnabled() : void {
            const listlabel : LabelList = new LabelList(IconType.SPINNER_SMALL);
            assert.equal(listlabel.Enabled(), true);
            const listlabel2 : LabelList = new LabelList(IconType.RED_SQUARE);
            assert.equal(listlabel2.Enabled(false), false);
        }

        public setArgs() : void {
            const listlabel : LabelList = new LabelList(IconType.GENERAL);
            listlabel.setArg(<IGuiCommonsArg>{
                name : "test",
                type : GuiCommonsArgType.TEXT,
                value: "Content"
            }, true);
            assert.deepEqual(listlabel.getArgs(), []);
        }

        public setArgsInputName() : void {
            const listlabel : LabelList = new LabelList(IconType.GENERAL);
            listlabel.setArg(<IGuiCommonsArg>{
                name : "name",
                type : GuiCommonsArgType.TEXT,
                value: "IconName"
            }, true);
            assert.deepEqual(listlabel.getArgs(), []);
        }

        public testserializationData() : void {
            const labellist : MockLabelList = new MockLabelList();
            assert.deepEqual(labellist.testserializationData(), [
                "objectNamespace", "objectClassName", "options",
                "availableOptionsList", "parent", "owner", "guiPath", "visible", "enabled", "prepared", "completed", "interfaceClassName",
                "styleClassName", "containerClassName", "loaded", "asyncDrawEnabled", "contentLoaded", "waitFor", "outputEndOfLine",
                "innerHtmlMap", "events", "changed", "items"
            ]);
        }

        public testcacheData() : void {
            const labellist : MockLabelList = new MockLabelList();
            assert.deepEqual(labellist.testcacheData(), [
                "options", "availableOptionsList", "events", "childElements",
                "waitFor", "cached", "prepared", "completed", "parent", "owner", "guiPath", "interfaceClassName", "styleClassName",
                "containerClassName", "innerHtmlMap", "loaded", "title", "changed", "iconType"
            ]);
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
