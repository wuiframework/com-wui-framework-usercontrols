/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.Components {
    "use strict";
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;

    class MockBaseViewer extends BaseViewer {
    }

    class MockGuiCommons extends GuiCommons {
    }

    export class DragBarTest extends UnitTestRunner {
        public testConstructor() : void {
            const dragbar : DragBar = new DragBar("id558");
            assert.equal(dragbar.Id(), "id558");
            this.initSendBox();
        }

        public testgetEvents() : void {
            const dragbar : DragBar = new DragBar();
            const handler : any = () : void => {
                // test event handler
            };
            dragbar.getEvents().setEvent("Complete", handler);
            assert.equal(dragbar.getEvents().Exists("Complete"), true);
            this.initSendBox();
        }

        public testEventOnStart() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const dragbar : DragBar = new DragBar("id5");
                const viewer : BaseViewer = new MockBaseViewer();
                dragbar.InstanceOwner(viewer);
                const gui1 : GuiCommons = new MockGuiCommons("id87");
                dragbar.Parent(gui1);
                assert.onGuiComplete(dragbar,
                    () : void => {
                        dragbar.Width(300);
                        dragbar.Height(500);
                        dragbar.Visible(true);
                        dragbar.Enabled(true);

                        const manager : GuiObjectManager = new GuiObjectManager();
                        // manager.setActive(gui1, true);

                        const event : any = {altKey: true, button: 2};
                        const moveEventArgs : MoveEventArgs = new MoveEventArgs(event);
                        moveEventArgs.Owner(dragbar);
                        this.getEventsManager().FireAsynchronousMethod(() : void => {
                            manager.setActive(dragbar, true);
                        }, 500);
                        manager.setActive(dragbar, true);
                        assert.equal(manager.IsActive(<ClassName>DragBar), true);
                        (<any>DragBar).moveInit(moveEventArgs, manager, Reflection.getInstance());
                        EventsManager.getInstanceSingleton().FireEvent(DragBar.ClassName(), EventType.ON_START, moveEventArgs);
                    },
                    () : void => {
                        $done();
                    }, viewer);
            };
        }

        public testEventOnChangeAsync() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const dragbar : DragBar = new DragBar("id2");
                const gui1 : GuiCommons = new MockGuiCommons("id23");
                dragbar.Parent(gui1);
                const viewer : BaseViewer = new MockBaseViewer();
                dragbar.InstanceOwner(viewer);

                assert.onGuiComplete(dragbar,
                    () : void => {
                        dragbar.getEvents().setOnDragChange(() : void => {
                            const args : DragEvent = new DragEvent("drag", null);
                            dragbar.IsCached();
                        });
                    },
                    () : void => {
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }

        public testEventOnChange() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const dragbar : DragBar = new DragBar("id2");
                const gui1 : GuiCommons = new MockGuiCommons("id23");
                dragbar.Parent(gui1);
                const viewer : BaseViewer = new MockBaseViewer();
                dragbar.InstanceOwner(viewer);
                assert.onGuiComplete(dragbar,
                    () : void => {
                        dragbar.Visible(true);
                        dragbar.Enabled(true);
                        const manager : GuiObjectManager = new GuiObjectManager();

                        // manager.setActive(gui1, true);
                        // manager.setActive(dragbar, true);

                        const event : any = {altKey: true, button: 2};
                        const moveEventArgs : MoveEventArgs = new MoveEventArgs(event);
                        moveEventArgs.Owner(dragbar);

                        this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                            ($args : MoveEventArgs, manager) : void => {
                                assert.equal($args.getClassName(), "Com.Wui.Framework.Gui.Events.Args.MoveEventArgs");
                                this.initSendBox();
                                $done();
                            });

                        manager.setActive(gui1, true);
                        manager.setActive(dragbar, true);
                        (<any>DragBar).moveInit(moveEventArgs, manager, Reflection.getInstance());
                        EventsManager.getInstanceSingleton().FireEvent(DragBar.ClassName(), EventType.ON_CHANGE, moveEventArgs);
                        // this.getEventsManager().FireEvent(DragBar.ClassName(), EventType.ON_DRAG_CHANGE, moveEventArgs);
                    },
                    () : void => {
                        $done();
                    }, viewer);
            };
        }

        public testEventOnComplete() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const dragbar : DragBar = new DragBar("id21");
                const gui1 : GuiCommons = new MockGuiCommons("id231");
                dragbar.Parent(gui1);
                const viewer : BaseViewer = new MockBaseViewer();
                dragbar.InstanceOwner(viewer);
                assert.onGuiComplete(dragbar,
                    () : void => {
                        dragbar.Visible(true);
                        dragbar.Enabled(true);
                        const manager : GuiObjectManager = new GuiObjectManager();
                        // manager.setActive(gui1, true);
                        const event : any = {altKey: true, button: 2};
                        const moveEventArgs : MoveEventArgs = new MoveEventArgs(event);
                        moveEventArgs.Owner(dragbar);
                        this.getEventsManager().FireAsynchronousMethod(() : void => {
                            manager.setActive(dragbar, true);
                        }, 400);
                        (<any>DragBar).moveInit(moveEventArgs, manager, Reflection.getInstance());
                        this.getEventsManager().FireEvent(DragBar.ClassName(), EventType.ON_COMPLETE, moveEventArgs, true);
                    },
                    () : void => {
                        $done();
                    }, viewer);
            };
        }

        public testEventOnCompleteSecond() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const dragbar : DragBar = new DragBar("id21");
                const gui1 : GuiCommons = new MockGuiCommons("id231");
                dragbar.Parent(gui1);
                const viewer : BaseViewer = new MockBaseViewer();
                dragbar.InstanceOwner(viewer);

                dragbar.Visible(true);
                dragbar.Enabled(true);
                // dragbar.DisableAsynchronousDraw();
                // Echo.Print(dragbar.Draw());
                assert.onGuiComplete(dragbar,
                    () : void => {
                        const manager : GuiObjectManager = new GuiObjectManager();
                        manager.setActive(dragbar, true);
                        // ElementManager.TurnActive(dragbar.Id() + "_Button");
                        assert.equal(manager.IsActive(dragbar), true);
                        const event : any = {altKey: true, button: 2};
                        const moveEventArgs : MoveEventArgs = new MoveEventArgs(event);

                        this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
                            ($args : MoveEventArgs, manager) : void => {
                                this.getEventsManager().FireAsynchronousMethod(() : void => {
                                    assert.equal($args.getClassName(), "Com.Wui.Framework.Gui.Events.Args.MouseEventArgs");
                                    // assert.equal($args.Owner(), scrollbar);
                                    manager.setActive(dragbar, true);
                                    this.initSendBox();
                                    $done();
                                }, 400);
                            });

                        (<any>DragBar).moveInit(moveEventArgs, manager, Reflection.getInstance());
                        EventsManager.getInstanceSingleton().FireEvent(DragBar.ClassName(), EventType.ON_COMPLETE, moveEventArgs, true);
                        EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                            // empty handler
                        }, 800);
                    },
                    () : void => {
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }
    }
}
