/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import InputLabelType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.InputLabelType;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    class MockGuiCommons extends GuiCommons {
    }

    class MockViewer extends BaseViewer {
    }

    class MockLabel extends InputLabel {
        public testStatusCss() : string {
            return this.statusCss();
        }

        public testExcludeCacheData() : string[] {
            return this.excludeCacheData();
        }

        public testErrorCssName() : string {
            return this.errorCssName();
        }
    }

    export class InputLabelTest extends UnitTestRunner {
        public testConstructor() : void {
            const inputlabel : InputLabel = new InputLabel(InputLabelType.GENERAL, "test");
            assert.equal(inputlabel.Text(), "test");
        }

        public testGuiType() : void {
            const inputlabel : InputLabel = new InputLabel(InputLabelType.RED, "test");
            const gui : GuiCommons = new MockGuiCommons("id37");
            gui.Visible(true);
            inputlabel.Visible(true);
            assert.equal(inputlabel.GuiType(InputLabelType.GREEN), InputLabelType.GREEN);
        }

        public testText() : void {
            const inputlabel : InputLabel = new InputLabel();
            (<any>InputLabel).complete = true;
            assert.equal(inputlabel.Text("TextOfLabel"), "TextOfLabel");
        }

        public __IgnoretestEvent() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const inputlabel : InputLabel = new InputLabel();
                const manager : GuiObjectManager = new GuiObjectManager();
                const viewer : BaseViewer = new MockViewer();
                inputlabel.InstanceOwner(viewer);
                manager.Add(inputlabel);
                assert.onGuiComplete(inputlabel,
                    () : void => {
                        manager.setActive(inputlabel, true);
                        inputlabel.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                            assert.patternEqual(inputlabel.Text(), "InputLabel*");
                        });
                    },
                    () : void => {
                        manager.Clear();
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }

        public __IgnoretestEventOnLoad() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const inputlabel : InputLabel = new InputLabel(InputLabelType.GENERAL);
                const manager : GuiObjectManager = new GuiObjectManager();
                const viewer : BaseViewer = new MockViewer();
                inputlabel.InstanceOwner(viewer);
                inputlabel.Enabled(false);
                inputlabel.Width(600);
                manager.Add(inputlabel);
                manager.setHovered(inputlabel, true);
                inputlabel.Visible(true);
                assert.onGuiComplete(inputlabel,
                    () : void => {
                        manager.setActive(inputlabel, true);
                        inputlabel.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                            ElementManager.IsVisible(inputlabel.Id());
                            ElementManager.setSize(inputlabel.Id(), 830, 750);
                            inputlabel.Error(true);
                            assert.patternEqual(inputlabel.Text(), "InputLabel*");
                            assert.equal(inputlabel.getEvents().Exists("oncomplete"), true);
                        });
                    },
                    () : void => {
                        manager.Clear();
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }

        public testWidth() : void {
            const inputlabel : InputLabel = new InputLabel(InputLabelType.BLUE, "test");
            (<any>GuiCommons).completed = true;
            (<any>InputLabel).complete = true;
            assert.equal(inputlabel.Width(), undefined);
            assert.equal(inputlabel.Width(80), 80);
        }

        public testEnabled() : void {
            const inputlabel : InputLabel = new InputLabel();
            inputlabel.Visible(true);
            assert.equal(inputlabel.Enabled(), true);
            (<any>InputLabel).complete = true;
            inputlabel.Error(false);
            assert.equal(inputlabel.Enabled(false), false);
        }

        public testError() : void {
            const inputlabel : InputLabel = new InputLabel();
            assert.equal(inputlabel.Error(), false);
            assert.equal(inputlabel.Error(true), true);
        }

        public testStatusCss() : void {
            const inputlabel : MockLabel = new MockLabel();
            const viewer : BaseViewer = new MockViewer();
            inputlabel.InstanceOwner(viewer);
            assert.equal(inputlabel.testStatusCss(), "");
        }

        public __IgnoreteststyleClassNameAsync() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const inputlabel : InputLabel = new InputLabel();
                const manager : GuiObjectManager = new GuiObjectManager();
                const viewer : BaseViewer = new MockViewer();
                inputlabel.InstanceOwner(viewer);
                manager.Add(inputlabel);
                assert.onGuiComplete(inputlabel,
                    () : void => {
                        manager.setActive(inputlabel, true);
                        inputlabel.StyleClassName("labelActive");
                    },
                    () : void => {
                        assert.equal(inputlabel.StyleClassName(), "labelActive");
                        manager.Clear();
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }

        public testexCacheData() : void {
            const inputlabel : MockLabel = new MockLabel();
            assert.deepEqual(inputlabel.testExcludeCacheData(), [
                "options", "availableOptionsList", "events", "childElements",
                "waitFor", "cached", "prepared", "completed", "parent", "owner", "guiPath", "interfaceClassName", "styleClassName",
                "containerClassName", "innerHtmlMap", "loaded", "title", "changed", "text", "borders", "width"
            ]);
        }

        public testerrorCssName() : void {
            const inputlabel : MockLabel = new MockLabel();
            assert.equal(inputlabel.testErrorCssName(), "");
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
