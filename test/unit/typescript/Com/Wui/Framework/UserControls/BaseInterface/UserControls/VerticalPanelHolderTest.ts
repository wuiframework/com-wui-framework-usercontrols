/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import BasePanelHolderEventType = Com.Wui.Framework.Gui.Enums.Events.BasePanelHolderEventType;

    class MockBasePanelViewer extends BasePanelViewer {
    }

    export class VerticalPanelHolderTest extends UnitTestRunner {

        public testConstructor() : void {
            const viewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            const basepanelviewer : BasePanelViewer = new BasePanelViewer(viewerArgs);
            const verticalpanel : VerticalPanelHolder = new VerticalPanelHolder(basepanelviewer, viewerArgs,
                BasePanelHolderEventType.ON_CHANGE, "id80");
            assert.equal(verticalpanel.Id(), "id80");
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
