/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.Components {
    "use strict";
    import NotificationType = Com.Wui.Framework.UserControls.BaseInterface.Enums.Components.NotificationType;
    import AbstractGuiObject = Com.Wui.Framework.UserControls.Primitives.AbstractGuiObject;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import Button = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Button;
    import ButtonType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;

    class MockBaseViewer extends BaseViewer {
    }

    class MockNotification extends Notification {
        public testexcludeSerializationData() : string[] {
            return this.excludeSerializationData();
        }
    }

    export class NotificationTest extends UnitTestRunner {
        public testConstructorFirst() : void {
            const object : AbstractGuiObject = new AbstractGuiObject("id357");
            assert.equal(object.Notification().Id(), "id357_Notification");
            this.initSendBox();
        }

        public testWidth() : void {
            const object : AbstractGuiObject = new AbstractGuiObject("id2588");
            object.Visible(true);
            ElementManager.IsVisible("id2588");
            assert.equal(object.Notification().Width(60), 60);
            this.initSendBox();
        }

        public testShow() : void {
            const object : AbstractGuiObject = new AbstractGuiObject("id1617");
            ElementManager.setCssProperty("id1617", "Hide", 100);
            ElementManager.setCssProperty("id1617", "Enable", "true");
            const viewer : BaseViewer = new MockBaseViewer();
            object.InstanceOwner(viewer);
            object.Notification().Visible(true);
            object.Notification().GuiType(NotificationType.SUCCESS);
            object.Notification().Width(400);
            object.Notification().Text("NotificationTest");
            object.Notification().Enabled(true);
            object.DisableAsynchronousDraw();
            Echo.Print(object.Draw());

            assert.equal(ElementManager.IsVisible("id1617"), true);
            assert.equal(object.Draw(),
                "\r\n<div class=\"ComWuiFrameworkUserControlsPrimitives\">\r\n" +
                "   <div id=\"id1617_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                "      <div id=\"id1617\" class=\"AbstractGuiObject\" style=\"display: block;\">\r\n" +
                "         <div style=\"border: 1px solid red; color: red; height: 20px;" +
                " overflow-x: hidden; overflow-y: hidden; width: 100px;\">id1617</div>\r\n" +
                "      </div>\r\n" +
                "   </div>\r\n" +
                "</div>");
            this.initSendBox();
        }

        public testGuiType2() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const object : AbstractGuiObject = new AbstractGuiObject("id1");
                const viewer : BaseViewer = new MockBaseViewer();
                object.Notification().InstanceOwner(viewer);
                const manager : GuiObjectManager = new GuiObjectManager();

                assert.onGuiComplete(object,
                    () : void => {
                        object.Notification().Visible(true);
                        object.Notification().Enabled(true);
                        object.Notification().DisableAsynchronousDraw();
                        Echo.Print(object.Notification().Draw());
                        Reflection.getInstance();
                        assert.equal(object.Notification().GuiType(NotificationType.SUCCESS), NotificationType.SUCCESS);
                        object.Notification().Visible(true);
                    },
                    $done, viewer);
            };

        }

        public testText2() : void {
            const args : BaseViewerArgs = new BaseViewerArgs();
            const viewer : BaseViewer = new BaseViewer(args);
            const object : AbstractGuiObject = new AbstractGuiObject("id24");
            object.InstanceOwner(viewer);
            assert.equal(object.Notification().Text("TestOfNotification"), "TestOfNotification");
            const object2 : AbstractGuiObject = new AbstractGuiObject("id30");
            assert.equal(object2.Notification().Text("Notification"), "Notification");
            this.initSendBox();
        }

        public testText() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const args : BaseViewerArgs = new BaseViewerArgs();
                const viewer : BaseViewer = new BaseViewer(args);
                const object : AbstractGuiObject = new AbstractGuiObject("id2");
                const manager : GuiObjectManager = new GuiObjectManager();
                object.Notification().InstanceOwner(viewer);
                assert.onGuiComplete(object,
                    () : void => {
                        object.Notification().Visible(true);
                        object.Notification().Enabled(true);
                        manager.setActive(object.Notification(), true);
                        object.getEvents().FireAsynchronousMethod(() : void => {
                            object.getEvents().setOnMouseMove(($eventArgs : MouseEventArgs) : void => {
                                object.Notification().Text("testNotification");
                                // (<any>GuiCommons).loaded = true;
                                Reflection.getInstance();
                            });
                            assert.equal(object.getEvents().Exists("onmousemove"), true);
                            assert.equal(object.Notification().Text(), "testNotification");
                        }, 200);
                    },
                    () : void => {
                        this.initSendBox();
                        $done();
                    },
                    viewer);
            };
        }

        public testWidthSecond() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const args : BaseViewerArgs = new BaseViewerArgs();
                const viewer : BaseViewer = new BaseViewer(args);
                const object : AbstractGuiObject = new AbstractGuiObject("id3");
                const manager : GuiObjectManager = new GuiObjectManager();
                object.Notification().InstanceOwner(viewer);
                assert.onGuiComplete(object,
                    () : void => {
                        object.Notification().Visible(true);
                        object.Notification().Enabled(true);
                        object.Notification().Width(30);
                        Reflection.getInstance();

                        object.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                            object.Notification().Text("Click on object");
                            assert.equal(object.getEvents().Exists(""), true);
                            assert.equal(object.Notification().Text(), "Click on object");
                        });
                    },
                    () : void => {
                        assert.equal(object.Notification().Width(), 30);
                        assert.equal(ElementManager.IsVisible(object.Notification().Id()), false);
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }

        public testsetArgs() : void {
            const object : AbstractGuiObject = new AbstractGuiObject();
            object.Notification().Text("Hello");
            object.Notification().GuiType(NotificationType.GENERAL);
            object.Notification().Width(300);
            object.Notification().setArg(<IGuiCommonsArg>{
                name : "Value",
                type : "Text",
                value: "testValue"
            }, true);
            assert.deepEqual(object.Notification().getArgs().length, 11);
            this.initSendBox();
        }

        public testsetArgsSecond() : void {
            const object : AbstractGuiObject = new AbstractGuiObject();
            object.Notification().Text("Hello");
            object.Notification().GuiType(NotificationType.GENERAL);
            object.Notification().Width(300);
            object.Notification().setArg(<IGuiCommonsArg>{
                name : "Width",
                type : "Number",
                value: 100
            }, true);
            assert.deepEqual(object.Notification().getArgs().length, 11);
            this.initSendBox();
        }

        public testsetArgsSecondThird() : void {
            const object : AbstractGuiObject = new AbstractGuiObject();
            object.Notification().Text("Hello");
            object.Notification().GuiType(NotificationType.GENERAL);
            object.Notification().Width(300);
            object.Notification().setArg(<IGuiCommonsArg>{
                name : "GuiType",
                type : "Text",
                value: NotificationType.GENERAL
            });
            assert.deepEqual(object.Notification().getArgs().length, 11);
            this.initSendBox();
        }

        public testsetArgsDefault() : void {
            const object : AbstractGuiObject = new AbstractGuiObject();
            object.Notification().Text("Hello");
            object.Notification().GuiType(NotificationType.GENERAL);
            object.Notification().Width(300);
            object.Notification().setArg(<IGuiCommonsArg>{
                name : "testName",
                type : "testType",
                value: "testValue"
            }, true);
            assert.deepEqual(object.Notification().getArgs().length, 11);
            this.initSendBox();
        }

        public testConstructor() : void {
            const object : Button = new Button(ButtonType.GENERAL, "testId1");
            object.Notification().Text("testNotification");
            object.Notification().Width(100);
            object.Notification().DisableAsynchronousDraw();
            Echo.Print(object.Notification().Draw());
            Notification.Show(<Notification>object.Notification());
            Echo.PrintCode(JSON.stringify(object.Notification().Draw()));
            assert.equal(object.Notification().Draw(),
                "\r\n<div class=\"ComWuiFrameworkUserControlsBaseInterfaceComponents\">" +
                "\r\n   <div id=\"testId1_Notification_GuiWrapper\" guiType=\"GuiWrapper\">" +
                "\r\n      <div id=\"testId1_Notification\" class=\"Notification\" style=\"display: block;\">" +
                "\r\n         <div id=\"testId1_Notification_Type\" guiType=\"Notification\">" +
                "\r\n            <div class=\"Envelop\">" +
                "\r\n               <div id=\"testId1_Notification_Border\" class=\"Background\">" +
                "\r\n                  <div id=\"testId1_Notification_Left\" class=\"Left\"></div>" +
                "\r\n                  <div id=\"testId1_Notification_Center\" class=\"Center\">" +
                "\r\n                     <div id=\"testId1_Notification_Text\" class=\"Text\">testNotification</div>" +
                "\r\n                  </div>" +
                "\r\n                  <div id=\"testId1_Notification_Right\" class=\"Right\"></div>" +
                "\r\n               </div>" +
                "\r\n            </div>" +
                "\r\n         </div>" +
                "\r\n      </div>" +
                "\r\n   </div>" +
                "\r\n</div>");
            this.initSendBox();
        }

        public testserializationData() : void {
            const notification : MockNotification = new MockNotification();
            assert.deepEqual(notification.testexcludeSerializationData().toString(), "objectNamespace,objectClassName,options," +
                "availableOptionsList,parent,owner,guiPath,visible,enabled,prepared,completed,interfaceClassName,styleClassName," +
                "containerClassName,loaded,asyncDrawEnabled,contentLoaded,waitFor,outputEndOfLine,innerHtmlMap,events,size");
            this.initSendBox();
        }
    }
}
