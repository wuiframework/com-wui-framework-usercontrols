/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import BasePanelHolderEventType = Com.Wui.Framework.Gui.Enums.Events.BasePanelHolderEventType;
    import HorizontalPanelHolderViewer = Com.Wui.Framework.UserControls.BaseInterface.Viewers.UserControls.HorizontalPanelHolderViewer;

    export class HorizontalPanelHolderTest extends UnitTestRunner {
        public testConstructor() : void {
            const basepanelviewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            basepanelviewerArgs.AsyncEnabled(true);
            basepanelviewerArgs.Visible(true);
            const basepanelviewer : HorizontalPanelHolderViewer = new HorizontalPanelHolderViewer();
            const horizontalpanel : HorizontalPanelHolder = new HorizontalPanelHolder(basepanelviewer,
                basepanelviewerArgs, BasePanelHolderEventType.ON_MESSAGE, "id9999");
            assert.equal(horizontalpanel.Id(), "id9999");
            this.initSendBox();
        }
    }
}
