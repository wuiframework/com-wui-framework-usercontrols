/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import DropDownListType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.DropDownListType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;

    export class DropDownListTest extends UnitTestRunner {
        public testItemTurnActive() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.BLUE);
            droplist.Add("OP", 0);
            droplist.Add("QR", 1);
            droplist.Add("ST", 2);
            droplist.Add("UV", 3);
            DropDownList.ItemTurnActive(droplist, 2);
            assert.equal((<any>DropDownList).selectedIndex = 2, 2);
        }

        public testTurnActive() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.BLUE);
            DropDownList.TurnActive(droplist);
        }

        public testSelectItem() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.BLUE);
            droplist.Add("OP", 0);
            droplist.Add("QR", 1);
            droplist.Add("ST", 2);
            droplist.Add("UV", 3);
            DropDownList.SelectItem(droplist, 2);
            assert.equal((<any>DropDownList).selectedItem = "ST", "ST");
        }

        public testgetEvents() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.BLUE);

            const handler : any = () : void => {
                // test event handler
            };
            droplist.getEvents().setEvent("test", handler);
            assert.equal(droplist.getEvents().Exists("test"), true);
        }

        public testGuiType() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.GREEN);
            assert.equal(droplist.GuiType(), DropDownListType.GREEN);
        }

        public testValue() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.RED);
            assert.equal(droplist.Value("ValueTest"), "ValueTest");
        }

        public testWidth() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.GENERAL);
            assert.equal(droplist.Width(20), 20);
        }

        public testHeight() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.BLUE);
            assert.equal(droplist.Height(), undefined);
        }

        public testMaxVisibleItemCount() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.GENERAL);
            assert.equal(droplist.MaxVisibleItemsCount(5), 5);
        }

        public testAdd() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.BLUE);
            droplist.Add("item1", 0);
            droplist.Add("item2", 1);
            droplist.Add("item3", 2);
            droplist.Add("item4", 3);
            assert.deepEqual(droplist.getItemsCount(), 4);
        }

        public testClear() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.GREEN);
            droplist.Add("item1", 0);
            droplist.Add("item2", 1);
            droplist.Clear();
            assert.deepEqual(droplist.getItemsCount(), 0);
        }

        public testSelect() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.GREEN);
            droplist.Add("item1", 0);
            droplist.Add("item2", 1);
            droplist.Select(1);
            assert.equal(droplist.Value(), 1);
        }

        public testShowField() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.GREEN);
            assert.equal(droplist.ShowField(), true);
            assert.equal(droplist.ShowField(false), false);
        }

        public testShowButton() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.GREEN);
            assert.equal(droplist.ShowButton(), true);
            assert.equal(droplist.ShowButton(false), false);
        }

        public testHint() : void {
            const droplist : DropDownList = new DropDownList(DropDownListType.GENERAL);
            assert.equal(droplist.Hint("node or id"), "node or id");
        }

        public testTurnActiveSecond() : void {
            const list : DropDownList = new DropDownList();
            const guimanager : GuiObjectManager = new GuiObjectManager();
            const reflection : Reflection = new Reflection();
            list.DisableAsynchronousDraw();
            Echo.Print(list.Draw());
            DropDownList.TurnActive(list, guimanager, reflection);
            assert.equal(ElementManager.getClassName(list.Id() + "_Enabled"), GeneralCssNames.ACTIVE);
        }

        public testItemTurnActiveSecond() : void {
            const list : DropDownList = new DropDownList();
            list.Add("test", 1);
            list.Add("drop", 2);
            list.Add("down", 3);
            list.DisableAsynchronousDraw();
            Echo.Print(list.Draw());
            DropDownList.ItemTurnActive(list, 2);
            assert.equal(ElementManager.getClassName(list.Id() + "_Item_" + 2 + "_Status"), GeneralCssNames.ACTIVE);
        }

        public testSelectItemSecond() : void {
            const list : DropDownList = new DropDownList();
            list.Add("drop", 0, "styleName");
            list.Add("down", 1);
            list.Add("list", 2);
            list.DisableAsynchronousDraw();
            Echo.Print(list.Draw());
            DropDownList.SelectItem(list, 2);
        }

        public testFocus() : void {
            const list : DropDownList = new DropDownList();
            list.Add("test", 0);
            list.Add("drop", 1);
            list.DisableAsynchronousDraw();
            Echo.Print(list.Draw());
            DropDownList.Focus(list);
            assert.equal(ElementManager.getClassName(list.Id() + "_Item_" + 1 + "_Status"), GeneralCssNames.OFF);
        }

        public testBlur() : void {
            const list : DropDownList = new DropDownList();
            list.Add("drop", 0);
            list.Add("down", 1);
            list.DisableAsynchronousDraw();
            Echo.Print(list.Draw());
            DropDownList.Blur();
            assert.equal(ElementManager.getClassName(list.Id() + "_Enabled"), GeneralCssNames.OFF);
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
