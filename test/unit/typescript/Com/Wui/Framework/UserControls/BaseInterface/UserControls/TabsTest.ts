/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.BaseInterface.UserControls {
    "use strict";
    import TabsType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.TabsType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;

    class Mockviewer extends BaseViewer {
    }

    export class TabsTest extends UnitTestRunner {

        public testConstructor() : void {
            const tabs : Tabs = new Tabs(TabsType.GREEN, "id40");
            assert.equal(tabs.Id(), "id40");
        }

        public testAdd() : void {
            const tabs : Tabs = new Tabs(TabsType.GENERAL);
            assert.equal(tabs.Add("first"), true);
            assert.equal(tabs.Add(null), false);
        }

        public testWidth() : void {
            const tabs : Tabs = new Tabs(TabsType.BLUE);
            assert.equal(tabs.Width(-1), -1);
            assert.equal(tabs.Width(50), -1);
            assert.equal(tabs.Width(), -1);
        }

        public __IgnoretestWidthAsync() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const tab : Tabs = new Tabs(TabsType.RED);
                const viewer : BaseViewer = new Mockviewer();
                const manager : GuiObjectManager = new GuiObjectManager();
                manager.Add(tab);
                tab.InstanceOwner(viewer);
                assert.onGuiComplete(tab,
                    () : void => {
                        manager.setActive(tab, true);
                        tab.Visible(true);
                        tab.Enabled(true);
                        tab.Select(55);
                        tab.Width(80);
                    },
                    () : void => {
                        assert.equal(tab.Width(), 300);
                        assert.equal(tab.IsPrepared(), true);
                        assert.equal(tab.GuiType(), TabsType.RED);
                        manager.Clear();
                        this.initSendBox();
                        $done();
                    }, viewer);
            };
        }

        public testgetItem() : void {
            const tabs : Tabs = new Tabs(TabsType.GENERAL);
            assert.equal(tabs.getItem(10), null);
        }

        public testClear() : void {
            const tabs : Tabs = new Tabs(TabsType.RED);
            tabs.Add("second");
            tabs.Add("third");
            tabs.Clear();
            assert.deepEqual(tabs.getItem(1), null);
        }

        public testSelect() : void {
            const tabs : Tabs = new Tabs(TabsType.GREEN);
            tabs.Select("id46");
            const tabs2 : Tabs = new Tabs(TabsType.GENERAL);
            tabs2.Select(-1);
            const tabs3 : Tabs = new Tabs(TabsType.BLUE);
            tabs3.Select(0);
        }

        public testEnabled() : void {
            const tabs : Tabs = new Tabs(TabsType.RED);
            tabs.Add("test");
            tabs.Add("testSecond");
            assert.equal(tabs.Enabled(), true);
            assert.equal(tabs.Enabled(false), false);
        }

        public testGuiType() : void {
            const tabs : Tabs = new Tabs(TabsType.RED);
            tabs.Visible(true);
            assert.equal(tabs.GuiType(TabsType.RED), TabsType.RED);
            assert.equal(tabs.GuiType(), "Red");
        }

        public testTurnOn() : void {
            const tabs : Tabs = new Tabs(TabsType.GENERAL);
            const manager : GuiObjectManager = new GuiObjectManager();
            const reflection : Reflection = new Reflection();
            tabs.DisableAsynchronousDraw();
            Echo.Print(tabs.Draw());
            Tabs.TurnOn(tabs, manager, reflection);
            assert.equal(ElementManager.getClassName(tabs.Id() + "_Status"), GeneralCssNames.ON);
        }

        public testTurnOff() : void {
            const tabs : Tabs = new Tabs(TabsType.GENERAL);
            const manager : GuiObjectManager = new GuiObjectManager();
            const reflection : Reflection = new Reflection();
            tabs.DisableAsynchronousDraw();
            Echo.Print(tabs.Draw());
            //   manager.setActive(tabs, false);
            Tabs.TurnOff(tabs, manager, reflection);
            assert.equal(ElementManager.getClassName(tabs.Id() + "_Status"), GeneralCssNames.OFF);
        }

        public testTurnActive() : void {
            const tabs : Tabs = new Tabs(TabsType.GENERAL);
            tabs.DisableAsynchronousDraw();
            Echo.Print(tabs.Draw());
            (<any>Tabs).tabPressed = true;
            Tabs.TurnActive(tabs);
            assert.equal(ElementManager.getClassName(tabs.Id() + "_Status"), GeneralCssNames.ACTIVE);
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
