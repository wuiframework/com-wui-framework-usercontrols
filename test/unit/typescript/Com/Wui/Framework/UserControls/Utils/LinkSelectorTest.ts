/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.UserControls.Utils {
    "use strict";
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;

    class MockGuiCommons extends GuiCommons {
    }

    export class LinkSelectorTest extends UnitTestRunner {
        public __IgnoretestsetLinkFormater() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const gui : GuiCommons = new MockGuiCommons("888");
                const linkselector : LinkSelector = new LinkSelector(gui);
                const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                const reflection : Reflection = Reflection.getInstance();
                linkselector.setLinkFormatter(($value : string) : string => {
                    return "testLink";
                });
                assert.equal(linkselector.getValue(), undefined);
                this.initSendBox();
                $done();
            };
        }

        public testEnabled() : void {
            const gui : GuiCommons = new MockGuiCommons("811");
            const linkselector : LinkSelector = new LinkSelector(gui);
            gui.Visible(true);
            ElementManager.IsVisible("811");
            assert.equal(linkselector.Enabled(true), true);
            const gui2 : GuiCommons = new MockGuiCommons("911");
            gui2.Visible(true);
            const linkselector2 : LinkSelector = new LinkSelector(gui2);
            assert.equal(linkselector2.Enabled(), true);
            assert.equal(linkselector2.Enabled(false), false);
            this.initSendBox();
        }

        public testReloadTo() : void {
            const gui : GuiCommons = new MockGuiCommons("999");
            const linkselector : LinkSelector = new LinkSelector(gui);
            assert.equal(linkselector.ReloadTo("unit/some/relative/link"), "/unit/some/relative/link");
            const element : HTMLAnchorElement = document.createElement("a");
            const gui2 : GuiCommons = new MockGuiCommons("565");
            gui2.Visible(true);
            const linkselector2 : LinkSelector = new LinkSelector(gui2);
            assert.equal(linkselector2.ReloadTo("unit/some/relative/link"), "/unit/some/relative/link");
            const gui3 : GuiCommons = new MockGuiCommons("753");
            const linkselector3 : LinkSelector = new LinkSelector(gui3);
            assert.equal(linkselector3.ReloadTo(), undefined);
            this.initSendBox();
        }

        public testOpenInNewWindow() : void {
            const gui : GuiCommons = new MockGuiCommons("101");
            gui.Visible(true);
            const linkselector : LinkSelector = new LinkSelector(gui);
            assert.equal(linkselector.OpenInNewWindow(), false);
            const gui2 : GuiCommons = new MockGuiCommons("100");
            const linkselector2 : LinkSelector = new LinkSelector(gui2);
            assert.equal(linkselector2.OpenInNewWindow(true), true);
            this.initSendBox();
        }

        public testTabIndex() : void {
            const gui : GuiCommons = new MockGuiCommons("99");
            gui.Visible(true);
            const linkselector : LinkSelector = new LinkSelector(gui);
            assert.equal(linkselector.TabIndex(3), 3);
            const gui2 : GuiCommons = new MockGuiCommons("98");
            const linkselector2 : LinkSelector = new LinkSelector(gui2);
            assert.equal(linkselector2.TabIndex(), null);
            assert.equal(linkselector2.TabIndex(null), null);
            assert.equal(linkselector2.TabIndex(-1), -1);
            this.initSendBox();
        }

        public testgetInnerCode() : void {
            const gui : GuiCommons = new MockGuiCommons("98");
            const linkselector : LinkSelector = new LinkSelector(gui);
            // assert.equal(linkselector.getInnerCode(), new guiElementClass().Add(link));
            this.initSendBox();
        }
    }
}
