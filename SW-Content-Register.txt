Release Name: com-wui-framework-usercontrols v2019.1.0

com-wui-framework-usercontrols
Description: WUI Framework library focused on basic User Controls
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/graphics/Com/Wui/Framework/UserControls,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          test/resource/graphics/Com/Wui/Framework/UserControls,
          index.html

com-wui-framework-gui v2019.1.0
Description: WUI Framework base GUI library
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/graphics/Com/Wui/Framework/Gui,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          test/resource/data/Com/Wui/Framework/Gui,
          test/resource/graphics/Com/Wui/Framework/Gui,
          index.html

Oxygen Font N/A
Description: Web font in ttf format
Author: Vernon Adams
License: OFL-1.1, See OFL.txt
Format: font
Location: resource/libs/FontOxygen

com-wui-framework-commons v2019.1.0
Description: Commons library for WUI Framework front-end projects.
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/graphics/Com/Wui/Framework/Commons,
          resource/graphics/icon.ico,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          resource/javascript/loader.min.js,
          test/resource/data/Com/Wui/Framework/Commons,
          index.html
